<?php

use \Illuminate\Support\Str;


class LocationDistrict extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'location_districts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'name', 'slug', 'city_id');

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::saving(function($model) {
            $slug = $model->name .' '. $model->city->name .' '. $model->city->estate->short;
            $model->slug = STR::slug($slug);
        });
        static::deleting(function($model) {
            Job::where('district_id', $model->id)->update(
                array('district_id' => null)
            );
        });
    }

    /**
     * Retorna a cidade do bairro
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('LocationCity', 'city_id');
    }
}