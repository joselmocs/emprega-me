<?php


class Language extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'languages';

}