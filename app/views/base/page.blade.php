@extends("base/template")

@section("site-title")
{{ $context->page->title .' - '.Config::get('domain.site.title') }}
@endsection

@section("navigation")
<li><a href="{{ URL::current() }}" title="{{ $context->page->title }}">{{ $context->page->title }}</a></li>
@endsection

@section("content")

<div id="pagina" class="page_template"><!--pagina-->

<div class="titulo_grande"><!--titulo_grande-->
{{ $context->page->title }}
</div><!--titulo_grande-->

<div style="font-size: 14px; margin: 0 0 25px 0"><!--page_template_links-->
{{ nl2br(html_entity_decode($context->page->page, ENT_QUOTES, 'utf-8')) }}
</div><!--page_template_links-->

@if ($context->page->form)
<div class="titulo_pequeno"><!--titulo_medio-->
Formulário de contato:
</div><!--titulo_medio-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
<td>
Preencha os campos abaixo e envie sua mensagem para nós.
<br />
Nossos atendentes entrarão em contato o mais rápido possível.
</td>
</tr>
<tr>
<td height="15"></td>
</tr>
<tr>
<td>Seu nome:</td>
</tr>
<tr>
<td>
<input type="text" style="width: 400px;"/>
</td>
</tr>
<tr>
<td height="5"></td>
</tr>
<tr>
<td>Seu e-mail:</td>
</tr>
<tr>
<td>
<input type="text" style="width: 400px;"/>
</td>
</tr>
<tr>
<td height="10"></td>
</tr>
<tr>
<td>Mensagem:</td>
</tr>
<tr>
<td>
<textarea style="height: 200px; width: 800px;"></textarea>
</td>
</tr>
<tr>
<td height="10"></td>
</tr>
<tr>
<td>
<input type="submit" value="Enviar"/>
</td>
</tr>
</table>
@endif

</div><!--pagina-->

</div><!--container-->
@include("advertising/footer")
@endsection