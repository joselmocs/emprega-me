/*!
 * apps.backend.links.js
 * Scripts responsáveis pela criação e edição de tipos de trabalho.
 *
 * Joselmo Cardozo
 */

require([
    "helpers/core",
    "libs/jquery.sortable"
], function(core) {
    var
        initialize = function() {
            events();
        },
        events = function() {
            $('a[name=remover]').unbind('click').click(function() {
                if (confirm('Deseja remover o link *'+ $(this).attr('data-name') +'*?')) {
                    window.location = core.context.urls.remove +'/'+ $(this).attr('data-id');
                }
            });

            $("input[name=save_order]").unbind('click').click(function() {
                core.loading.show();
                $("#alert_success").hide();
                var position = $(this).attr('data-position');

                var ordered = [];
                $.each($(".highlighted li[data-position="+ position +"]"), function(i, li) {
                    ordered.push($(li).attr('data-id'));
                });

                $.ajax({
                    type: 'post',
                    url: core.context.urls.reorder,
                    data: {
                        _token: $("meta[name=csrf-token]").attr('content'),
                        'position': position,
                        'ordered': JSON.stringify(ordered)
                    },
                    error: function() {
                        alert("Ocorreu um erro ao salvar a ordem dos links, por favor contacte um administrador.");
                    },
                    success: function(response) {
                        if (response.success) {
                            $("#alert_success").show(300);
                        }
                    },
                    complete: function() {
                        core.loading.hide();
                    }
                });

            });

            $(".highlighted").sortable({
                onDrop: function (item, targetContainer, _super) {
                    var clonedItem = $('<li/>').css({height: 0});
                    item.before(clonedItem);
                    clonedItem.animate({'height': item.height()});

                    item.animate(clonedItem.position(), function  () {
                        clonedItem.detach();
                        _super(item)
                    })
                }
            });
        };

    initialize();
});