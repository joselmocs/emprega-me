<?php


class AdvertisingController extends BaseController {

    /**
     * Exibe o banner adsense no template
     *
     * @param string $short Slug do banner selecionado
     * @throws Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return Illuminate\Support\Facades\View
     */
    public static function adsense($short)
    {
        $adsense = AdvertisingAdsense::where('short', '=', $short)
            ->where('active', true)
            ->first();

        if (!$adsense) {
            return null;
        }
        return $adsense->{'code_' . Config::get('domain.code')};
    }

    /**
     * Rotaciona um banner
     *
     * @param string $type tipo do banner selecionado
     * @throws Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return Illuminate\Support\Facades\View
     */
    public static function banner($type)
    {
        $banner_type = AdvertisingBannerType::where('short', '=', $type)
            ->first();

        if (!$banner_type) {
            return null;
        }

        $domain = Config::get('domain.code');
        $banner = AdvertisingBanner::where('type_id', '=', $banner_type->id)
            ->where('active', true)
            ->whereIn('domain', array("", $domain))
            ->orderBy(DB::raw('RAND()'))->first();

        if (!$banner) {
            return null;
        }

        $banner->views += 1;

        if ($banner->max_views > 0 && $banner->views >= $banner->max_views) {
            $banner->active = false;
        }

        $banner->save();

        return '<a href="'. $banner->url .'" title="'. $banner->name .'" target="_blank"><img alt="'. $banner->name .'" src="'. $banner->image .'"></a>';
    }
}
