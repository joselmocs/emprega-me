/*!
 * apps.user.search.js
 * Scripts responsáveis pela busca de curriculo
 *
 * Joselmo Cardozo
 */

require([
    "helpers/core"
], function(core) {
    var
        initialize = function() {
            events();
        },
        events = function() {
            $("select[name=gender_select]").change(function() {
                var path_frag = core.uri.path().split('/');
                path_frag[3] = $(this).val();
                window.location = path_frag.join('/');
            });

            $("select[name=city_select]").change(function() {
                var path_frag = core.uri.path().split('/');
                path_frag[4] = $(this).val();
                window.location = path_frag.join('/');
            });
        };

    initialize();
});
