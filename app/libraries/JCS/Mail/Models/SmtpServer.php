<?php

namespace JCS\Mail\Models;

use Swift_Mailer,
    Swift_Message,
    Swift_Plugins_LoggerPlugin,
    Swift_Plugins_Loggers_ArrayLogger,
    Swift_SmtpTransport;


class SmtpServer extends \Eloquent
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mail_servers';

    /**
     * Instancia do Switft Mailer.
     *
     * @var null|Swift_Mailer
     */
    private $mailer = null;

    /**
     * Instancia do plugin Swift Mailer Logger
     *
     * @var null|Swift_Plugins_Loggers_ArrayLogger
     */
    private $logger = null;

    /**
     * Faz a validacao dos dados do model e inicializa os objetos Swift para que o model
     * esteja pronto para fazer o envio de emails.
     *
     * @throws \InvalidArgumentException
     * @return void
     */
    public function prepare()
    {
        // Precisamos verificar se todos os dados obrigatÃ³rios estÃ£o preenchidos.
        if (empty($this->host_name) || empty($this->host_port)) {
            throw new \InvalidArgumentException('invalid smtp configuration');
        }

        $transport = Swift_SmtpTransport::newInstance(
            $this->host_name, $this->host_port, $this->host_tls ? 'tls' : 'ssl'
        );

        // HÃ¡ servidores smtp que nÃ£o Ã© necessÃ¡rio autenticaÃ§Ã£o para comunicaÃ§Ã£o.
        if (!empty($this->host_username)) {
            $transport->setUsername($this->host_username);
        }
        if (!empty($this->host_password)) {
            $transport->setPassword($this->host_password);
        }

        $this->mailer = Swift_Mailer::newInstance($transport);

        // Registramos o plugin de log
        $this->logger = new Swift_Plugins_Loggers_ArrayLogger();
        $this->mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($this->logger));
    }

    /**
     * Verifica se os objetos Swift jÃ¡ foram inicializados.
     *
     * @return boolean
     */
    public function isPrepared()
    {
        return null !== $this->mailer && null !== $this->logger;
    }

    /**
     * Faz a validaÃ§Ã£o de alguns dados e dispara a mensagem.
     *
     * @param string $subject assunto da mensagem.
     * @param array $to array contendo os destinatÃ¡rios.
     * @param string $body corpo da mensagem.
     * @param string $content_type tipo do corpo da mensagem
     * @param array $cc array contendo emails que receberÃ£o cÃ³pia da mensagem
     * @param array $bcc array contendo emails que receberÃ£o cÃ³pia oculta da mensagem
     *
     * @throws \InvalidArgumentException
     *
     * @return void
     */
    public function send($subject, $to = array(), $body = null, $content_type = 'plain', $cc = array(), $bcc = array())
    {
        if (false === $this->isPrepared()) {
            $this->prepare();
        }

        if (empty($subject) || is_null($body)) {
            throw new \InvalidArgumentException("invalid subject and/or body values");
        }

        if (!in_array($content_type, array('plain', 'html'))) {
            throw new \InvalidArgumentException("invalid mail content type");
        }

        $message = Swift_Message::newInstance()
            ->setFrom($this->from, \Config::get('global.site.name'))
            ->setSubject($this->prefix .' '. $subject)
            ->setTo($to)
            ->setCc($cc)
            ->setBcc($bcc)
            ->setBody($body);

        $message->setContentType("text/". $content_type);

        // Dispara o email
        $this->mailer->send($message);
    }

    /**
     * Retorna o objeto de logger do Swift.
     *
     * @return Swift_Plugins_Loggers_ArrayLogger
     */
    public function getLogger()
    {
        return $this->logger;
    }
}
