<?php

use JCS\Mail\Models\SmtpServer;


class BackendConfigController extends BaseBackendController {

    /**
     * Exibe a página de configurações do sistema.
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        try {
            $config = Configuration::firstOrFail();
        }
        catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $config = new Configuration;
            $config->save();
        }

        $this->set_context(array(
            'input' => (object) array(
                'time_announcement' => Input::get('time_announcement', $config->time_announcement),
                'former_job' => Input::get('former_job', $config->former_job),
                'highlighted_vacancies' => Input::get('highlighted_vacancies', $config->highlighted_vacancies),
                'featured_jobs' => Input::get('featured_jobs', $config->featured_jobs),
                'recent_jobs' => Input::get('recent_jobs', $config->recent_jobs),
                'busiest_month' => Input::get('busiest_month', $config->busiest_month),
                'busiest_week' => Input::get('busiest_week', $config->busiest_week),
                'busiest_day' => Input::get('busiest_day', $config->busiest_day),
                'highlights_category' => Input::get('highlights_category', $config->highlights_category),
                'jobs_category' => Input::get('jobs_category', $config->jobs_category),
                'competitive_category' => Input::get('competitive_category', $config->competitive_category),
                'recent_jobs_company' => Input::get('recent_jobs_company', $config->recent_jobs_company),
                'similar_jobs' => Input::get('similar_jobs', $config->similar_jobs),
                'smtp_hostname' => Input::get('smtp_hostname', $config->smtp_hostname),
                'smtp_port' => Input::get('smtp_port', $config->smtp_port),
                'smtp_auth' => Input::get('smtp_auth', $config->smtp_auth),
                'smtp_secure' => Input::get('smtp_secure', $config->smtp_secure),
                'smtp_username' => Input::get('smtp_username', $config->smtp_username),
                'smtp_password' => Input::get('smtp_password', $config->smtp_password)
            )
        ));

        if (Request::isMethod('get')) {
            $this->set_json_context(array(
                'urls' => array(
                    'reorder' => URL::action('BackendConfigController@highlighted_reorder')
                )
            ));
            $this->set_context(array(
                'categories' => Category::orderBy('name', 'asc')->get(),
                'highlighted' => CategoryHighlighted::orderBy('order', 'asc')->get()
            ));

            return $this->view_make('backend/config/index');
        }

        $validate = array(
            "time_announcement" => "required|integer",
            "former_job" => "required|integer",
            "highlighted_vacancies" => "required|integer",
            "featured_jobs" => "required|integer",
            "recent_jobs" => "required|integer",
            "busiest_month" => "required|integer",
            "busiest_week" => "required|integer",
            "busiest_day" => "required|integer",
            "highlights_category" => "required|integer",
            "jobs_category" => "required|integer",
            "competitive_category" => "required|integer",
            "recent_jobs_company" => "required|integer",
            "similar_jobs" => "required|integer",
            "smtp_hostname" => "required",
            "smtp_port" => "required|integer",
            "smtp_auth" => "required|integer",
            "smtp_secure" => "required|integer",
            "smtp_username" => "required",
            "smtp_password" => "required"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('backend/config/index');
        }

        $config->time_announcement = Input::get('time_announcement');
        $config->former_job = Input::get('former_job');
        $config->highlighted_vacancies = Input::get('highlighted_vacancies');
        $config->featured_jobs = Input::get('featured_jobs');
        $config->recent_jobs = Input::get('recent_jobs');
        $config->busiest_month = Input::get('busiest_month');
        $config->busiest_week = Input::get('busiest_week');
        $config->busiest_day = Input::get('busiest_day');
        $config->highlights_category = Input::get('highlights_category');
        $config->jobs_category = Input::get('jobs_category');
        $config->competitive_category = Input::get('competitive_category');
        $config->recent_jobs_company = Input::get('recent_jobs_company');
        $config->similar_jobs = Input::get('similar_jobs');
        $config->smtp_hostname = Input::get('smtp_hostname');
        $config->smtp_port = Input::get('smtp_port');
        $config->smtp_auth = Input::get('smtp_auth');
        $config->smtp_secure = Input::get('smtp_secure');
        $config->smtp_username = Input::get('smtp_username');
        $config->smtp_password = Input::get('smtp_password');
        $config->save();

        try {
            $smtp = SmtpServer::firstOrFail();
        }
        catch(Exception $ex) {
            $smtp = new SmtpServer();
        }

        $smtp->name = "Empregame SMTP";
        $smtp->description = "Empregame SMTP";
        $smtp->prefix = "Empregame";
        $smtp->from = Input::get('smtp_username');
        $smtp->host_name = Input::get('smtp_hostname');
        $smtp->host_port = intval(Input::get('smtp_port'));
        $smtp->host_tls = intval(Input::get('smtp_secure')) == 2;
        $smtp->host_username = Input::get('smtp_username');
        $smtp->host_password = Input::get('smtp_password');
        $smtp->enabled = true;
        $smtp->save();

        return Redirect::action('BackendConfigController@index');
    }

    /**
     * Adiciona uma categoria às categoria destaques
     *
     * @return Illuminate\Support\Facades\Redirect
     */
    public function highlighted_add()
    {
        $category = Category::find(Input::get('category_id'));

        if (!CategoryHighlighted::where('category_id', '=', $category->id)->count()) {
            $highlighted = new CategoryHighlighted;
            $highlighted->category_id = $category->id;
            $highlighted->order = CategoryHighlighted::getLastPosition() + 1;
            $highlighted->save();
        }

        return Redirect::back();
    }

    /**
     * Remove uma categoria das categorias destaques
     *
     * @param integer $id
     * @return Illuminate\Support\Facades\Redirect
     */
    public function highlighted_remove($id)
    {
        CategoryHighlighted::find($id)->delete();
        return Redirect::back();
    }

    /**
     * Ordena as categorias destaques
     *
     * @return Illuminate\Support\Facades\Response
     */
    public function highlighted_reorder()
    {
        $highlighted = json_decode(Input::get('highlighted', array()));
        foreach($highlighted as $i => $category) {
            $category = CategoryHighlighted::where('category_id', '=', $category)->firstOrFail();
            $category->order = $i + 1;
            $category->save();
        }
        return Response::json(array('success' => true));
    }


}
