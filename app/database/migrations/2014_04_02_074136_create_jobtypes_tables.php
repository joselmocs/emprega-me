<?php

use Illuminate\Database\Migrations\Migration;


class CreateJobtypesTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the job_types table
        Schema::create('job_types', function($table)
        {
            $table->increments('id');
            $table->string('name', 128);
            $table->string('image', 128);
            $table->string('slug', 128);
            $table->integer('count')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_types');
    }

}
