@extends("backend/template")

@section('requirejs')
requirejs(["apps/backend/configuration"]);
@endsection

@section("content")
<div id="configuracoes" class="page_template"><!--configuracoes-->

<div class="titulo_medio"><!--titulo_medio-->
    <img src="/images/backend/icone_configuracoes.jpg"/> CONFIGURAÇÕES
</div><!--titulo_medio-->

<div style="margin: 0 0 25px 0"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_medio-->
Categorias principais:
</div><!--titulo_medio-->

<div class="alerta_verde" id="alert_success" style="display: none;">Categorias destaques salvas com sucesso.</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza" style="margin: 0 0 2px 0;">
<form action="{{ URL::action('BackendConfigController@highlighted_add') }}" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<tr>
<td align="left">
<select name="category_id" required>
<option value="" selected="selected">Selecione a categoria</option>
@foreach($context->categories as $category)
<option value="{{ $category->id }}">{{ $category->name }}</option>
@endforeach
</select>
<input type="submit" style="font-size: 11px;" value="Incluir categoria"/>
</td>
</tr>
</form>
<tr>
<td height="15"><hr width="100%" style=" background: #EEEEEE; color: #EEEEEE; height:2px;"></td>
</tr>
<tr>
<td height="20" align="left" bgcolor="#FFFFFF">

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: 0 #FFFFFF solid; border-left: 2px #FFFFFF solid; border-right: 2px #FFFFFF solid; border-top: 2px #FFFFFF solid;">
<tr>
<td>

<ol style="list-style-type: none;" class="highlighted">
@foreach($context->highlighted as $highlight)
<li class="row-alt" data-category="{{ $highlight->category_id }}"><!--row/row-alt-->
<span class="row-info">
<div style="float:left; width: 100%;"><img src="/images/set_right.png" title="{{ $highlight->category->name }}" /><u>{{ $highlight->category->name }}</u></div>
</span>

<div class="row_menu">
<a href="{{ URL::action('BackendConfigController@highlighted_remove', array($highlight->id)) }}" title="Remover categoria" name="rem_category"><img src="/images/backend/row_menu_remover.png" title="Remover"/></a>
</div>
</li><!--row/row-alt-->
@endforeach
</ol>

</td>
</tr>
</table>

</td>
</tr>
</table>

</div><!--div margin-->


<form action="" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <div style="margin: 0 0 25px 0"><!--div margin-->

        <div class="titulo_pequeno"><!--titulo_medio-->
            Configuração de tempo:
        </div><!--titulo_medio-->

        <table width="100%" border="0" cellspacing="2" cellpadding="0" class="moldura_cinza formulario">
            <tr>
                <td width="200" align="right">Tempo de duração do anuncio:</td>
                <td>
                    <input type="text" style="width: 125px;" name="time_announcement" value="{{ $context->input->time_announcement }}" required="required"
                    @if ($context->field_errors && $context->field_errors->has('time_announcement')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('time_announcement'))
                    <div class="error">{{ $context->field_errors->first('time_announcement') }}</div>
                    @endif

                </td>
            </tr>
            <tr>
                <td width="200" align="right">Notificação de vaga antiga:</td>
                <td>
                    <input type="text" style="width: 125px;" name="former_job" value="{{ $context->input->former_job }}" required="required"
                    @if ($context->field_errors && $context->field_errors->has('former_job')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('former_job'))
                    <div class="error">{{ $context->field_errors->first('former_job') }}</div>
                    @endif
                </td>
            </tr>
        </table>

    </div><!--div margin-->


    <div style="margin: 0 0 25px 0"><!--div margin-->

        <div class="titulo_pequeno"><!--titulo_medio-->
            Configurações para empresas:
        </div><!--titulo_medio-->

        <table width="100%" border="0" cellspacing="2" cellpadding="0" class="moldura_cinza formulario">
            <tr>
                <td width="200" align="right">Limite de vagas destacadas:</td>
                <td>
                    <input type="text" style="width: 125px;" name="highlighted_vacancies" value="{{ $context->input->highlighted_vacancies }}" required="required"
                    @if ($context->field_errors && $context->field_errors->has('highlighted_vacancies')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('highlighted_vacancies'))
                    <div class="error">{{ $context->field_errors->first('highlighted_vacancies') }}</div>
                    @endif
                </td>
            </tr>
        </table>

    </div><!--div margin-->



    <div style="margin: 0 0 25px 0"><!--div margin-->

        <div class="titulo_pequeno"><!--titulo_medio-->
            Configuração da home:
        </div><!--titulo_medio-->

        <table width="100%" border="0" cellspacing="2" cellpadding="0" class="moldura_cinza formulario">
            <tr>
                <td width="200" align="right">Vagas em destaque:</td>
                <td>
                    <input type="text" style="width: 125px;" name="featured_jobs" value="{{ $context->input->featured_jobs }}" required="required"
                    @if ($context->field_errors && $context->field_errors->has('featured_jobs')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('featured_jobs'))
                    <div class="error">{{ $context->field_errors->first('featured_jobs') }}</div>
                    @endif
                </td>
            </tr>
            <tr>
                <td width="200" align="right">Vagas recentes:</td>
                <td>
                    <input type="text" style="width: 125px;" name="recent_jobs" value="{{ $context->input->recent_jobs }}" required="required"
                    @if ($context->field_errors && $context->field_errors->has('recent_jobs')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('recent_jobs'))
                    <div class="error">{{ $context->field_errors->first('recent_jobs') }}</div>
                    @endif
                </td>
            </tr>
            <tr>
                <td width="200" align="right">Mais concorridas do mês:</td>
                <td>
                    <input type="text" style="width: 125px;" name="busiest_month" value="{{ $context->input->busiest_month }}" required="required"
                    @if ($context->field_errors && $context->field_errors->has('busiest_month')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('busiest_month'))
                    <div class="error">{{ $context->field_errors->first('busiest_month') }}</div>
                    @endif
                </td>
            </tr>
            <tr>
                <td width="200" align="right">Mais concoridas da semana:</td>
                <td>
                    <input type="text" style="width: 125px;" name="busiest_week" value="{{ $context->input->busiest_week }}" required="required"
                    @if ($context->field_errors && $context->field_errors->has('busiest_week')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('busiest_week'))
                    <div class="error">{{ $context->field_errors->first('busiest_week') }}</div>
                    @endif
                </td>
            </tr>
            <tr>
                <td width="200" align="right">Mais concorridas hoje:</td>
                <td>
                    <input type="text" style="width: 125px;" name="busiest_day" value="{{ $context->input->busiest_day }}" required="required"
                    @if ($context->field_errors && $context->field_errors->has('busiest_day')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('busiest_day'))
                    <div class="error">{{ $context->field_errors->first('busiest_day') }}</div>
                    @endif
                </td>
            </tr>
        </table>

    </div><!--div margin-->



    <div style="margin: 0 0 25px 0"><!--div margin-->

        <div class="titulo_pequeno"><!--titulo_medio-->
            Configuração da categoria:
        </div><!--titulo_medio-->

        <table width="100%" border="0" cellspacing="2" cellpadding="0" class="moldura_cinza formulario">
            <tr>
                <td width="200" align="right">Destaques da categoria:</td>
                <td>
                    <input type="text" style="width: 125px;" name="highlights_category" value="{{ $context->input->highlights_category }}" required="required"
                    @if ($context->field_errors && $context->field_errors->has('highlights_category')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('highlights_category'))
                    <div class="error">{{ $context->field_errors->first('highlights_category') }}</div>
                    @endif
                </td>
            </tr>
            <tr>
                <td width="200" align="right">Vagas da categoria:</td>
                <td>
                    <input type="text" style="width: 125px;" name="jobs_category" value="{{ $context->input->jobs_category }}" required="required"
                    @if ($context->field_errors && $context->field_errors->has('jobs_category')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('jobs_category'))
                    <div class="error">{{ $context->field_errors->first('jobs_category') }}</div>
                    @endif
                </td>
            </tr>
            <tr>
                <td width="200" align="right">Mais concorridas da categoria:</td>
                <td>
                    <input type="text" style="width: 125px;" name="competitive_category" value="{{ $context->input->competitive_category }}" required="required"
                    @if ($context->field_errors && $context->field_errors->has('competitive_category')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('competitive_category'))
                    <div class="error">{{ $context->field_errors->first('competitive_category') }}</div>
                    @endif
                </td>
            </tr>
        </table>

    </div><!--div margin-->



    <div style="margin: 0 0 25px 0"><!--div margin-->

        <div class="titulo_pequeno"><!--titulo_medio-->
            Configuração da vaga:
        </div><!--titulo_medio-->

        <table width="100%" border="0" cellspacing="2" cellpadding="0" class="moldura_cinza formulario">
            <tr>
                <td width="200" align="right">Vagas recentes de empresa:</td>
                <td>
                    <input type="text" style="width: 125px;" name="recent_jobs_company" value="{{ $context->input->recent_jobs_company }}" required="required"
                    @if ($context->field_errors && $context->field_errors->has('recent_jobs_company')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('recent_jobs_company'))
                    <div class="error">{{ $context->field_errors->first('recent_jobs_company') }}</div>
                    @endif
                </td>
            </tr>
            <tr>
                <td width="200" align="right">Vagas semelhantes:</td>
                <td>
                    <input type="text" style="width: 125px;" name="similar_jobs" value="{{ $context->input->similar_jobs }}" required="required"
                    @if ($context->field_errors && $context->field_errors->has('similar_jobs')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('similar_jobs'))
                    <div class="error">{{ $context->field_errors->first('similar_jobs') }}</div>
                    @endif
                </td>
            </tr>
        </table>

    </div><!--div margin-->



    <div style="margin: 0 0 25px 0"><!--div margin-->

        <div class="titulo_pequeno"><!--titulo_medio-->
            Configurações de correio:
        </div><!--titulo_medio-->

        <table width="100%" border="0" cellspacing="2" cellpadding="0" class="moldura_cinza formulario">
            <tr>
                <td width="200" align="right">SMTP Hostname:</td>
                <td>
                    <input type="text" style="width: 250px;" name="smtp_hostname" value="{{ $context->input->smtp_hostname }}" required="required"
                    @if ($context->field_errors && $context->field_errors->has('smtp_hostname')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('smtp_hostname'))
                    <div class="error">{{ $context->field_errors->first('smtp_hostname') }}</div>
                    @endif
                </td>
            </tr>
            <tr>
                <td width="200" align="right">SMTP port:</td>
                <td>
                    <input type="text" style="width: 50px;" name="smtp_port" value="{{ $context->input->smtp_port }}" required="required"
                    @if ($context->field_errors && $context->field_errors->has('smtp_port')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('smtp_port'))
                    <div class="error">{{ $context->field_errors->first('smtp_port') }}</div>
                    @endif
                </td>
            </tr>
            <tr>
                <td width="200" align="right">SMTP authentication:</td>
                <td>
                    <select name="smtp_auth" required @if ($context->field_errors && $context->field_errors->has('smtp_auth')) class="error" @endif />
                        <option value="1" @if ($context->input->smtp_auth) selected="selected" @endif>Sim</option>
                        <option value="0" @if (!$context->input->smtp_auth) selected="selected" @endif>Não</option>
                    </select>
                    @if ($context->field_errors && $context->field_errors->has('smtp_auth'))
                    <div class="error">{{ $context->field_errors->first('smtp_auth') }}</div>
                    @endif
                </td>
            </tr>
            <tr>
                <td width="200" align="right">SMTP secure connection:</td>
                <td>
                    <select name="smtp_secure" required @if ($context->field_errors && $context->field_errors->has('smtp_secure')) class="error" @endif />
                        <option value="1" @if ($context->input->smtp_secure == 1) selected="selected" @endif>SSL</option>
                        <option value="2" @if ($context->input->smtp_secure == 2) selected="selected" @endif>TLS</option>
                    </select>
                    @if ($context->field_errors && $context->field_errors->has('smtp_secure'))
                    <div class="error">{{ $context->field_errors->first('smtp_secure') }}</div>
                    @endif
                </td>
            </tr>
            <tr>
                <td height="10" colspan="2"></td>
            </tr>
            <tr>
                <td width="200" align="right">SMTP username:</td>
                <td>
                    <input type="text" style="width: 250px;" name="smtp_username" value="{{ $context->input->smtp_username }}" required="required"
                    @if ($context->field_errors && $context->field_errors->has('smtp_username')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('smtp_username'))
                    <div class="error">{{ $context->field_errors->first('smtp_username') }}</div>
                    @endif
                </td>
            </tr>
            <tr>
                <td width="200" align="right">SMTP password:</td>
                <td><input type="password" style="width: 200px;" name="smtp_password" value="{{ $context->input->smtp_password }}" required="required"
                    @if ($context->field_errors && $context->field_errors->has('smtp_password')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('smtp_password'))
                    <div class="error">{{ $context->field_errors->first('smtp_password') }}</div>
                    @endif
                </td>
            </tr>
        </table>

    </div><!--div margin-->

    <div class="moldura_cinza formulario"><!--div margin-->
        <table width="100%" border="0" cellspacing="2" cellpadding="0">
            <tr>
                <td align="left">
                    <input type="submit" value="Salvar" />
                </td>
            </tr>
        </table>
    </div><!--div margin-->
</form>
</div><!--configuracoes-->

@endsection