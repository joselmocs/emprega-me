<?php

use Illuminate\Database\Migrations\Migration;


class CreatePagesTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the pages table
        Schema::create('pages', function($table)
        {
            $table->increments('id');
            $table->string('title', 256);
            $table->text('page')->nullable();
            $table->boolean('form')->default(false);

            $table->string('url', 256);
            $table->string('seo', 256);
            $table->string('keywords', 256);
            $table->string('description', 256);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop pages table
        Schema::dropIfExists('pages');
    }

}
