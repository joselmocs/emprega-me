<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>{{ Config::get('domain.site.title') }}</title>
    <link href="/styles/screen.css" rel="stylesheet" type="text/css"/>
    <link href="/domains/{{ Config::get('domain.folder') }}/css/screen_colors.css" rel="stylesheet" type="text/css"/>
    <style type="text/css">
        body {
            background-color: #46008C;
        }
    </style>
</head>

<body>

<div id="index"><!--index-->

    <div id="index_1"><!--index_1-->

        <div id="index_1_estatisticas"><!--index_1_estatisticas-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td><b>9.000</b> vagas de emprego.</td>
                </tr>
                <tr>
                    <td><b>500</b> novas vagas esta semana.</td>
                </tr>
                <tr>
                    <td><b>10.000</b> curriculos enviados essa semana.</td>
                </tr>
                <tr>
                    <td><b>6.000</b> currículos cadastradas.</td>
                </tr>
                <tr>
                    <td><b>6.000</b> empresas cadastradas.</td>
                </tr>
            </table>
        </div><!--index_1_estatisticas-->

    </div><!--index_1-->

    <div id="index_2"><!--index_2-->

        <div id="index_2_lista_estados_texto"><!--index_2_lista_estados_texto-->
            Escolha seu estado aqui
        </div><!--index_2_lista_estados_texto-->

        <div id="index_2_lista_estados"><!--index_2_lista_estados-->
            <ul>
                @foreach(LocationEstate::all() as $estate)
                <li><a href="?estate={{ $estate->id }}" title="{{ $estate->name }}/{{ $estate->short }}">{{ $estate->short }}</a></li>
                @endforeach
            </ul>
        </div><!--index_2_lista_estados-->

    </div><!--index_2-->

    <div id="index_3"><!--index_3-->

        <div id="index_3_copyright"><!--index_copyright-->
            © 2012-2014 {{ Config::get('domain.site.name') }} - Todos os direitos reservados
        </div><!--index_copyright-->

    </div><!--index_3-->

</div><!--index-->

</body>
</html>
