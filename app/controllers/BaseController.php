<?php


class BaseController extends Controller {

    /**
     * Context base
     *
     * @var array
     */
    public $context = array(
        'template' => array(),
        'json' => array(),
        'cookie' => array()
    );

    /**
     * Definimos algumas variáveis retornadas em todas requisicoes
     *
     */
    public function __construct()
    {
        $this->context['template'] = array(
            'field_errors' => array(),
            'auth_errors' => array(),
            'form_errors' => array()
        );

        $this->context['template']['term'] = null;
        $this->context['template']['present'] = $this->parsePresentation();
        $this->context['template']['today'] = Carbon::now();
    }

    /**
     * Definimos a apresentação para o usuário de acordo com o horário
     *
     * @return str
     */
    public function parsePresentation()
    {
        $hour = Carbon::now()->hour;
        $minute = Carbon::now()->minute;
        $time = (float) "{$hour}.{$minute}";

        $present = "boa noite";
        if ($time >= 0.01 && $hour <= 11.59) {
            $present = "bom dia";
        }
        else if ($hour >= 12.0 && $hour <= 17.59) {
            $present = "boa tarde";
        }

        return $present;
    }

    /**
     * Prepara o context enviado para o javascript
     *
     * @param array $contexts
     */
    public function set_json_context($contexts = array())
    {
        foreach ($contexts as $key => $value) {
            $this->context['json'][$key] = $value;
        }
    }

    /**
     * Prepara o context enviado para o template
     *
     * @param array $contexts
     */
    public function set_context($contexts = array())
    {
        foreach ($contexts as $key => $value) {
            $this->context['template'][$key] = $value;
        }
    }

    /**
     * Constroi o template
     *
     * @param string $template
     * @return \Illuminate\View\View
     */
    public function view_make($template)
    {
        return View::make($template)
            ->with('json', json_encode($this->context['json']))
            ->with('context', (object) $this->context['template']);
    }
}
