<?php

use JCS\Auth\Models\Role,
    JCS\Auth\Models\Permission,
    JCS\Auth\Models\PermissionScope;


class UserPermissionsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $scopes = array(
            // Globais
            'Permissões Globais' => array(
                // Administrador do Sistema
                array('Administrador do Sistema', 'sys_admin', ''),
                array('Acessar Backend', 'backend', ''),
                array('Gerenciar Usuários', 'manage_users', ''),
                array('Visualizar Candidatos', 'view_candidates', ''),
                array('Gerenciar Candidatos', 'manage_candidates', ''),
                array('Visualizar Vagas', 'view_jobs', ''),
                array('Gerenciar Vagas', 'manage_jobs', ''),
                array('Visualizar Estatísticas', 'view_stats', ''),
                array('Visualizar Localizações', 'view_locations', ''),
                array('Gerenciar Localizações', 'manage_locations', ''),
                array('Visualizar Empresas', 'view_companies', ''),
                array('Gerenciar Empresas', 'manage_companies', ''),
                array('Visualizar Links', 'view_links', ''),
                array('Gerenciar Links', 'manage_links', ''),
                array('Gerenciar Configurações', 'manage_config', ''),
                array('Visualizar Páginas', 'view_pages', ''),
                array('Gerenciar Páginas', 'manage_pages', ''),
                array('Gerenciar Categorias', 'manage_category', ''),
                array('Gerenciar Propaganda', 'manage_advertising', ''),
                array('Visualizar Log', 'view_log', ''),
            )
        );

        foreach ($scopes as $scope => $permissions) {
            $newScope = new PermissionScope;
            $newScope->name = $scope;
            $newScope->save();

            foreach ($permissions as $permission) {
                $newPerm = new Permission;
                $newPerm->scope_id = $newScope->id;
                $newPerm->name = $permission[0];
                $newPerm->tag = $permission[1];
                $newPerm->description = $permission[2];
                $newPerm->save();
            }
        }

        // Atribuímos permissão para Administrador do Sistema
        $group = Role::where('tag', '=', 'sys_admin')->firstOrFail();
        $permissions = Permission::whereIn('tag', array('sys_admin'))->get();
        $group->permissions()->sync($permissions);

        $group = Role::where('tag', '=', 'moderator')->firstOrFail();
        $permissions = Permission::whereIn('tag', array(
            'backend', 'manage_jobs', 'view_stats', 'view_locations', 'view_typejobs', 'view_companies', 'view_links',
            'view_pages', 'view_jobs', 'view_candidates'
        ))->get();
        $group->permissions()->sync($permissions);

        $group = Role::where('tag', '=', 'manager')->firstOrFail();
        $permissions = Permission::whereIn('tag', array(
            'backend', 'manage_jobs', 'view_stats', 'view_locations', 'view_typejobs', 'view_companies', 'view_links',
            'view_pages', 'view_jobs', 'view_candidates', 'manage_users'
        ))->get();
        $group->permissions()->sync($permissions);

    }
}
