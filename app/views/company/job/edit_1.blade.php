@extends('base/template')

@section("site-title")
Anuncie um novo emprego - {{ Config::get('domain.site.title') }}
@endsection

@section('requirejs')
requirejs(["apps/job/register"]);
@endsection

@section('content')

<div class="page_template page_template_links"><!--page_template-->

<div class="titulo_medio"><!--titulo_medio-->
@if ($context->editing)
Editar vaga
@else
Cadastrar nova vaga
@endif
</div><!--titulo_medio-->

<div style="margin: 0 0 25px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_medio-->
Destalhes da vaga:
</div><!--titulo_medio-->

<form action="" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
<td align="left">

@foreach($context->job_types as $type)
<label><input type="radio" name="type_id" value="{{ $type->id }}" @if($type->id == $context->input->type_id) checked="checked" @endif /><img style="margin: 0 10px 0 0; position: relative; top: 2px; left: 2px;" src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $type->image }}" title="{{ $type->name }}"/></label>
@endforeach
@if ($context->field_errors && $context->field_errors->has('type_id'))
<div class="error">{{ $context->field_errors->first('type_id') }}</div>
@endif
</td>
</tr>
<tr>
<td height="15"><hr width="100%" style=" background: #EEEEEE; color: #EEEEEE; height:2px;"></td>
</tr>
<tr>
<td align="left">Categoria:</td>
</tr>
<tr>
<td align="left">
<select name="category_id" required @if ($context->field_errors && $context->field_errors->has('category_id')) class="error" @endif >
<option value="">Selecione a categoria</option>
@foreach($context->categories as $category)
<option value="{{ $category->id }}" @if($category->id == $context->input->category_id) selected="selected" @endif>{{ $category->name }}</option>
@endforeach
</select>
@if ($context->field_errors && $context->field_errors->has('category_id'))
<div class="error">{{ $context->field_errors->first('category_id') }}</div>
@endif
</td>
</tr>
<tr>
<td height="15"></td>
</tr>
<tr>
<td align="left">Cargo/titulo:</td>
</tr>
<tr>
<td align="left">
<input type="text" style="width: 400px;" name="position" required="required" value="{{ $context->input->position }}"
@if ($context->field_errors && $context->field_errors->has('position')) class="error" @endif >
@if ($context->field_errors && $context->field_errors->has('position'))
<div class="error">{{ $context->field_errors->first('position') }}</div>
@endif
</td>
</tr>
<tr>
<td height="5"></td>
</tr>
<tr>
<td align="left">Localização:</td>
</tr>
<tr>
<td align="left">

<table width="0%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<select name="estate_id" required @if ($context->field_errors && $context->field_errors->has('estate_id')) class="error" @endif >
<option value="">Selecione o Estado</option>
@foreach($context->estates as $estate)
<option value="{{ $estate->id }}" @if($estate->id == $context->input->estate_id) selected="selected" @endif>{{ $estate->name }}</option>
@endforeach
</select>
@if ($context->field_errors && $context->field_errors->has('estate_id'))
<div class="error">{{ $context->field_errors->first('estate_id') }}</div>
@endif

<select name="city_id" required @if ($context->field_errors && $context->field_errors->has('city_id')) class="error" @endif >
<option value="">Selecione a Cidade</option>
</select>
@if ($context->field_errors && $context->field_errors->has('city_id'))
<div class="error">{{ $context->field_errors->first('city_id') }}</div>
@endif
</td>
<td>

<table width="0%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td style="padding: 0 0 0 5px;">
<select name="district_id">
<option value="">Selecione o bairro</option>
</select>
</td>
<td style="padding: 0 0 0 5px;"><small>(opicional)</small></td>
</tr>
</table>

</td>
</tr>
</table>

</td>
</tr>
<tr>
<td height="15"></td>
</tr>
<tr>
<td align="left">Descrição da vaga:</td>
</tr>
<tr>
<td align="left">
<textarea style="height: 300px; width: 1048px;" required placeholder="Escreva a descrição da vaga aqui." name="description"
@if ($context->field_errors && $context->field_errors->has('city_id')) class="error" @endif >{{ $context->input->description }}</textarea>
@if ($context->field_errors && $context->field_errors->has('description'))
<div class="error">{{ $context->field_errors->first('description') }}</div>
@endif
</td>
</tr>
<tr>
<td align="left">
<a class="fancybox fancybox.iframe" title="Leia as regras dos site" href="cadastrar-editar_vaga_1_regras.php" data-fancybox-width="420" data-fancybox-height="auto"><b>Leia as regras dos site.</b></a>
</td>
</tr>
</table>

</div><!--div margin-->



<div style="margin: 0 0 20px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Dados da empresa:
</div><!--titulo_pequeno-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
<td align="left">Nome da empresa/responsável:</td>
</tr>
<tr>
<td align="left">
<select name="profile_name" style="width: 400px;">
@foreach($context->profile_names as $name)
<option value="{{ $name }}" @if($name == $context->input->company) selected="selected" @endif>{{ $name }}</option>
@endforeach
</select>

<input type="text" style="width: 400px;" required="required" value="{{ $context->input->company }}" name="company"
@if ($context->field_errors && $context->field_errors->has('company')) class="error" @endif >
@if ($context->field_errors && $context->field_errors->has('company'))
<div class="error">{{ $context->field_errors->first('company') }}</div>
@endif

<input type="email" style="width: 200px;" required="required" value="{{ $context->input->email }}" name="email"
@if ($context->field_errors && $context->field_errors->has('email')) class="error" @endif >
@if ($context->field_errors && $context->field_errors->has('email'))
<div class="error">{{ $context->field_errors->first('email') }}</div>
@endif

</td>
</tr>
<tr>
<td height="15"></td>
</tr>
<tr>
<td align="left" style="color: #999999;">Telefone: <small>(opicional)</small></td>
</tr>
<tr>
<td align="left">

<table width="0%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<input type="text" style="width: 300px;" value="{{ $context->input->phone }}" name="phone" />
</td>
<td>

<table width="0%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 2px 0 5px;">
<input type="checkbox" name="show_phone" @if (@$context->input->show_phone) checked="checked" @endif>
</td>
<td width="100%" height="25" align="left" style="font-size: 11px;">Exibir telefone.</td>
</tr>
</table>

</td>
</tr>
</table>

</td>
</tr>
<tr>
<td height="5"></td>
</tr>
<tr>
<td align="left" style="color: #999999;">Website: <small>(opicional)</small></td>
</tr>
<tr>
<td align="left">

<table width="0%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<input type="text" style="width: 175px;" value="{{ $context->input->website }}" name="website" />
</td>
<td>

<table width="0%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 2px 0 5px;">
<input type="checkbox" name="show_website" @if (@$context->input->show_website) checked="checked" @endif>
</td>
<td width="100%" height="25" align="left" style="font-size: 11px;">Exibir website.</td>
</tr>
</table>

</td>
</tr>
</table>

</td>
</tr>
</table>

</div><!--div margin-->



<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
<td align="left">
<button type="submit" name="submit" value="check">Próxima etapa</button>
</td>
</tr>
</table>
</form>

</div><!--page_template-->

</div><!--container-->
@endsection
