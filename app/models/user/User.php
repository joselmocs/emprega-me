<?php

use Illuminate\Support\Str;


class User extends JCS\Auth\Models\User
{
    /**
     * Soft delete
     *
     * @var boolean
     */
    protected $softDelete = false;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::saving(function($model) {
            $model->username = STR::slug($model->email);
        });

        static::created(function($model)
        {
            $profile = new UserProfile;
            $profile->user_id = $model->id;
            $profile->save();
        });

        static::deleting(function ($model) {
            $model->roles()->sync(array());
            $model->profile()->delete();
        });
    }

    /**
     * Retorna o perfil do usuario.
     *
     * @return UserProfile
     */
    public function profile()
    {
        return $this->hasOne('UserProfile', 'user_id');
    }
}
