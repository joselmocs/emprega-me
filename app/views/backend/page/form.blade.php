@extends('backend/template')

@section('scripts')
<script type="text/javascript" src="/packages/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        tinymce.init({
            selector: "textarea",
            theme: "modern",
            skin: 'light',
            language : "pt_BR",
            width: 745,
            height: 300,
            plugins: [
                "advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
                "save contextmenu directionality template paste textcolor"
            ],
            toolbar: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | print preview fullpage | forecolor backcolor",
            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ]
        });
    });
</script>
@endsection

@section('content')
<div id="paginas_adicionar-editar" class="page_template"><!--newsletter-->

    @if ($context->page_id)
    <form action="{{ URL::action('BackendPageController@register', array($context->page_id)) }}" method="post">
    @else
    <form action="{{ URL::action('BackendPageController@register') }}" method="post">
    @endif

    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <div class="titulo_medio"><!--titulo_medio-->
        <img src="/images/backend/icone_vaga.jpg"/>@if ($context->page_id) EDITAR @else ADICIONAR @endif PÁGINA
    </div><!--titulo_medio-->

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>

            <td width="100%" valign="top"><!--coluna_1-->

                <div style="margin: 0 0 25px 0">

                    <div class="titulo_pequeno"><!--titulo_pequeno-->
                        Detalhes da vaga:
                    </div><!--titulo_pequeno-->

                    <table width="100%" border="0" cellspacing="2" cellpadding="0" class="moldura_cinza formulario page_template_links">
                        <tr>
                            <td height="22" align="left" valign="bottom">Título:</td>
                        </tr>
                        <tr>
                            <td align="left">
                                <input type="text" style="width: 350px;" name="title" value="{{ $context->input->title }}"
                                @if ($context->field_errors && $context->field_errors->has('title')) class="error" @endif />
                                @if ($context->field_errors && $context->field_errors->has('title'))
                                <div class="error">{{ $context->field_errors->first('title') }}</div>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td height="22" align="left" valign="bottom">Descrição:</td>
                        </tr>
                        <tr>
                            <td align="left">
                                <textarea name="page">{{ $context->input->page }}</textarea>
                                @if ($context->field_errors && $context->field_errors->has('page'))
                                <div class="error">{{ $context->field_errors->first('page') }}</div>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td align="left">

                                <table width="0%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="0%" align="center" style="padding: 2px 4px 0px 0px"><input type="checkbox" name="form" @if ($context->input->form == true) checked="checked" @endif></td>
                                        <td align="left" style="font-size: 11px;">Adicionar formulário de contato?</td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                </div>

                <div class="moldura_cinza"><!--moldura_cinza-->
                    <table width="100%" border="0" cellspacing="2" cellpadding="0" class="formulario">
                        <tr>
                            <td align="left">
                                @if ($context->page_id)
                                <input type="submit" value="Editar" />
                                @else
                                <input type="submit" value="Adicionar" />
                                @endif
                            </td>
                        </tr>
                    </table>
                </div><!--moldura_cinza-->

            </td><!--coluna_1-->

            <td width="0%" valign="top"><!--coluna_2-->
                <div id="paginas_adicionar-editar_sidebar"><!--paginas_adicionar-editar_sidebar-->

                    <div class="titulo_pequeno"><!--titulo_pequeno-->
                        Dados do empregador/responsável:
                    </div><!--titulo_pequeno-->

                    <table width="100%" border="0" cellspacing="2" cellpadding="0" class="moldura_cinza formulario">
                        <tr>
                            <td align="left" valign="bottom">URL:</td>
                        </tr>
                        <tr>
                            <td align="left">
                                <input type="text" style="width: 250px;" name="url" value="{{ $context->input->url }}"
                                @if ($context->field_errors && $context->field_errors->has('url')) class="error" @endif />
                                @if ($context->field_errors && $context->field_errors->has('url'))
                                <div class="error">{{ $context->field_errors->first('url') }}</div>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td height="22" align="left" valign="bottom">Título SEO:</td>
                        </tr>
                        <tr>
                            <td align="left">
                                <input type="text" style="width: 250px;" name="seo" value="{{ $context->input->seo }}"/>
                            </td>
                        </tr>
                        <tr>
                            <td height="22" align="left" valign="bottom">Keywords:</td>
                        </tr>
                        <tr>
                            <td align="left">
                                <input type="text" style="width: 200px;" name="keywords" value="{{ $context->input->keywords }}"/>
                            </td>
                        </tr>
                        <tr>
                            <td height="22" align="left" valign="bottom">Descrição:</td>
                        </tr>
                        <tr>
                            <td align="left">
                                <input type="text" style="width: 250px;" name="description" value="{{ $context->input->description }}"/>
                            </td>
                        </tr>
                    </table>

                </div><!--paginas_adicionar-editar_sidebar-->
            </td><!--coluna_2-->

        </tr>
    </table>
    </form>

</div>

@endsection