/*!
 * apps.company.jobs.js
 * Scripts responsáveis listagem de vagas da empresa
 *
 */

require([
    "helpers/core"
], function(core) {
    var
        initialize = function() {
            events();
        },
        events = function() {
            $("select[name=view]").change(function() {
                var path_frag = core.uri.path().split('/');
                path_frag[4] = $(this).val();
                window.location = path_frag.join('/');
            });
        };

    initialize();
});
