@extends('backend/template')

@section('requirejs')
    requirejs(["apps/backend/backend"]);
@endsection

@section('content')
<div class="page_template"><!--page_template-->

<div class="titulo_grande" style="text-transform: uppercase;"><!--titulo_grande-->
<img src="/images/backend/icone_empresas.jpg"> {{ $context->profile->company }}
</div><!--titulo_grande-->

<div style="margin: 0 0 5px 0"><!--div margin-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario">
<tr>
<td width="100%">
<a href="../linkaqui"><img src="../domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="../linkaqui"><img src="../domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="../linkaqui"><img src="../domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="../linkaqui"><img src="../domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="../linkaqui"><img src="../domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="../linkaqui"><img src="../domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
</td>
<td align="right">
<select>
<option value="Todo os estados">Todo os estados</option>
<option value="Minas Gerais/MG">Todo os estados</option>
</select>
</td>
</tr>
</table>
</div><!--div margin-->

@if (!$context->profile->jobs()->count())
<span style="color: #666666; font-size: 12px;">Nenhuma vaga cadastrada.</span>
@else

@if ($context->profile->jobs()->where('status', '=', Job::REPROVED)->count())
<div class="alerta_vermelho"><!--alerta_cinza-->
Você tem <b>{{ $context->profile->jobs()->where('status', '=', Job::REPROVED)->count() }}</b> vaga(s) reprovada(s), verifique essas vagas para que elas não sejam removidas.
</div><!--alerta_cinza-->
@endif

<div style="margin: 0 0 25px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Vagas cadastradas <span style="color: #999999;">({{ $context->profile->jobs()->count() }})</span>
</div><!--titulo_pequeno-->

@foreach($context->profile->jobs as $job)
<div class="row"><!--row/row-alt-->
<div id="row_1"><!--row_1-->
<span class="row-info">
<img src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $job->type->image }}" title="{{ $job->type->name }}">
<a class="fancybox fancybox.iframe" title="{{ $job->position }}" href="{{ URL::action('CompanyJobController@view', array($job->id)) }}" data-fancybox-width="800" data-fancybox-height="600">
{{ $job->position }}
</a>
<small><span>por</span> {{ $job->company }} <span>em</span> {{ $job->city->name }}/{{ $job->estate->short }}</small>
</span>

<span class="row_menu">
@if ($job->status == Job::IN_APPROVAL)
<img src="/images/row_menu_analise.png" title="Vaga em análise">
@endif
@if ($job->status == Job::APPROVED)
<img src="/images/row_menu_yes.png" title="Vaga aprovada">
@endif
@if ($job->status == Job::REPROVED)
<img src="/images/row_menu_erro.png" title="Vaga reprovada">
@endif
</span>

<div class="row_menu">
<a href="{{ URL::action('JobController@view', array($job->type->slug, $job->id, $job->city->slug, $job->slug)) }}" target="_blank"><img src="/images/backend/row_menu_acessar_vaga.png" title="Acessar vaga"></a>
@if ($job->featured)
<a href="@if (Auth::user()->can('manage_jobs')){{ URL::action('BackendCompanyController@feature', array($job->id)) }}@else javascript:void(0); @endif"><img src="/images/backend/row_menu_destaque_on.png" title="Destaque ON"></a>
@else
<a href="@if (Auth::user()->can('manage_jobs')){{ URL::action('BackendCompanyController@feature', array($job->id)) }}@else javascript:void(0); @endif"><img src="/images/backend/row_menu_destaque_off.png" title="Destaque OFF"></a>
@endif
@if (Auth::user()->can('manage_jobs'))
<a href="{{ URL::action('BackendCompanyController@edit', array($job->id)) }}?next={{ URL::current() }}"><img src="/images/backend/row_menu_editar.png" title="Editar"></a>
<a href="javascript: void(0);" name="remover" data-id="{{ $job->id }}" data-name="{{ $job->position }}"><img src="/images/backend/row_menu_remover.png" title="Remover"></a>
@endif
</div>

<span class="time_post">
@if (Carbon::parse($job->created_at)->isToday())
<img src="/images/hoje.png" title="Vaga publicada hoje">
@endif
@if (Carbon::parse($job->created_at)->isYesterday())
<img src="/images/ontem.png" title="Vaga publicada ontem">
@endif
<img src="/images/clock.png" title="Data da publicação">
<small title="{{ Carbon::parse($job->created_at)->format('d \d\e F \d\e Y') }}">05/03</small>
</span>

</div><!--row_1-->

<div id="row_2"><!--row_2-->
<div class="row_2-info">
▪ Visualizado: <span>{{ $job->views }} vez(es)</span> ▪ Candidato(s): <span>{{ $job->candidacies->count() }}</span> ▪ <span>Desativação em 00 dias</span>
</div>
</div><!--row_2-->
</div>

@endforeach
</div><!--div margin-->

<div class="page_count-letter"><!--page_count-letter-->
<li><a href="#" title="Página anterior">Anterior</a></li>
<li><a href="#" title="Página 1">1</a></li>
<li><a href="#" title="Página 2">2</a></li>
<li><a href="#" title="Próxima página">Próxima</a></li>
</div><!--page_count-letter-->

@endif
</div>

</div><!--container-->
@endsection