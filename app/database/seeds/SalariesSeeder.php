<?php


class SalariesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $salaries = array(
            'A combinar',
            'Até R$ 500,00',
            'Até R$ 1.000,00',
            'A partir de R$ 1.000,00',
            'A partir de R$ 2.000,00',
            'A partir de R$ 3.000,00',
            'A partir de R$ 4.000,00',
            'A partir de R$ 5.000,00',
            'A partir de R$ 6.000,00',
            'A partir de R$ 7.000,00',
            'A partir de R$ 8.000,00',
            'A partir de R$ 9.000,00',
            'A partir de R$ 10.000,00',
            'A partir de R$ 15.000,00',
            'Acima de R$ 20.000,00'
        );

        foreach ($salaries as $salary) {
            $newSalary = new Salary;
            $newSalary->description = $salary;
            $newSalary->save();
        }
    }
}