@extends("base/template")

@section("site-title")
{{ Config::get('domain.site.title') }}
@endsection

@section("content")
@include("advertising/header")

<div class="page_template"><!--home-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="auto 100%" valign="top"><!--coluna_1-->

<div style="margin: 0 0 25px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Vagas em destaque
</div><!--titulo_pequeno-->

@if (!$context->featured->count())
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><u>Nenhuma vaga em destaque.</u>
</span>
</div>
@else
@foreach($context->featured as $i => $job)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $job->type->image }}" title="{{ $job->type->name }}"/><a href="{{ URL::action('JobController@view', array($job->type->slug, $job->id, $job->city->slug, $job->slug)) }}" title="{{ $job->position }}">{{ $job->position }}</a>
<small><span>por </span>{{ $job->company }}<span> em </span>{{ $job->city->name }}/{{ $job->estate->short }}</small>
</span>

<span class="featured">
<img src="/domains/{{ Config::get('domain.folder') }}/images/destaque.png" title="Vaga em destaque."/>
</span>
</div><!--row/row-alt-->
@endforeach
@endif
</div><!--div margin-->



<div style="margin: 0 0 25px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Vagas recentes
</div><!--titulo_pequeno-->

@if (!$context->recents->count())
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><u>Nenhuma vaga encontrada.</u>
</span>
</div>
@else
@foreach($context->recents as $i => $job)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $job->type->image }}" title="{{ $job->type->name }}"/><a href="{{ URL::action('JobController@view', array($job->type->slug, $job->id, $job->city->slug, $job->slug)) }}" title="{{ $job->position }}">{{ $job->position }}</a>
<small><span>por </span>{{ $job->company }}<span> em </span>{{ $job->city->name }}/{{ $job->estate->short }}</small>
</span>

<span class="time_post">
@if (Carbon::parse($job->created_at)->isToday())
<img src="images/hoje.png" title="Vaga publicada hoje"/>
@endif
@if (Carbon::parse($job->created_at)->isYesterday())
<img src="images/ontem.png" title="Vaga publicada ontem"/>
@endif
<img src="images/clock.png" title="Data da publicação"/>
<small title="{{ Carbon::parse($job->created_at)->format('d \d\e F \d\e Y') }}">{{ Carbon::parse($job->created_at)->format('d/m') }}</small>
</span>
</div><!--row/row-alt-->
@endforeach
<div class="page_template_links"  style="font-size: 12px; margin: 5px 0 0 0"><!--div margin-->
<a href="{{ URL::action('JobController@index', array('-', '-', '-')) }}" title="Ver todas as vagas recentes">Ver todas as vagas.</a>
</div>
@endif
</div><!--div margin-->

<div style="margin: 0 0 25px 0;" ><!--div margin-->
<table width="0%" border="0" cellspacing="0" cellpadding="0" style="margin: 0 auto;">
<tr>
<td width="728" align="center">{{ AdvertisingController::adsense('footer_728x90') }}</td>
</tr>
<tr>
<td width="728" align="center">{{ AdvertisingController::adsense('links_728x15') }}</td>
</tr>
</table>
</div><!--div margin-->

<div style="margin: 0 0 25px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Mais concorridas do mês
</div><!--titulo_pequeno-->

@if (!$context->busiest_month->count())
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><u>Nenhuma vaga cadastrada nos últimos 30 dias.</u>
</span>
</div>
@else
@foreach($context->busiest_month as $i => $job)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $job->type->image }}" title="{{ $job->type->name }}"/><a href="{{ URL::action('JobController@view', array($job->type->slug, $job->id, $job->city->slug, $job->slug)) }}" title="{{ $job->position }}">{{ $job->position }}</a>
<small><span>por </span>{{ $job->company }}<span> em </span>{{ $job->city->name }}/{{ $job->estate->short }}</small>
</span>

<span class="counter">
<small title="{{ $job->t }} candidatos"><b>{{ $job->t }}</b> candidatos</small>
</span>
</div><!--row/row-alt-->
@endforeach
@endif
</div><!--div margin-->
</td><!--coluna_1-->

<td style="width: 16px;"></td>

<td width="336" valign="top"><!--coluna_2-->
@include("advertising/sidebar_home")


<div style="margin: 0 0 25px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Mais concoridas hoje
</div><!--titulo_pequeno-->

@if (!$context->busiest_today->count())
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><u>Nenhuma vaga cadastrada hoje.</u>
</span>
</div>
@else
@foreach($context->busiest_today as $i => $job)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a href="{{ URL::action('JobController@view', array($job->type->slug, $job->id, $job->city->slug, $job->slug)) }}" title="{{ $job->type->name }} - {{ $job->position }} por {{ $job->company }}">{{ $job->position }}</a>
</span>

<span class="counter">
<small title="{{ $job->t }} candidatos"><b>{{ $job->t }}</b></small>
</span>
</div><!--row/row-alt-->
@endforeach
@endif
</div><!--div margin-->



<div class="titulo_pequeno"><!--titulo_pequeno-->
Mais concoridas da semana
</div><!--titulo_pequeno-->

@if (!$context->busiest_week->count())
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><u>Nenhuma vaga cadastrada na última semana.</u>
</span>
</div>
@else
@foreach($context->busiest_week as $i => $job)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a href="{{ URL::action('JobController@view', array($job->type->slug, $job->id, $job->city->slug, $job->slug)) }}" title="{{ $job->type->name }} - {{ $job->position }} por {{ $job->company }}">{{ $job->position }}</a>
</span>

<span class="counter">
<small title="{{ $job->t }} candidatos"><b>{{ $job->t }}</b></small>
</span>
</div><!--row/row-alt-->
@endforeach
@endif
</td><!--coluna_2-->
</tr>
</table>
</div><!--home-->

</div><!--container-->
@include("advertising/facebook")
@endsection
