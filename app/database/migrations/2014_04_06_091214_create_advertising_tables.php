<?php

use Illuminate\Database\Migrations\Migration;


class CreateAdvertisingTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the advertising_adsense table
        Schema::create('advertising_adsense', function($table)
        {
            $table->increments('id');
            $table->string('name', 128);
            $table->string('short', 32)->index();
            $table->text('code_empregame')->nullable();
            $table->text('code_bhjobs')->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        // Create the advertising_banner_types table
        Schema::create('advertising_banner_types', function($table)
        {
            $table->increments('id');
            $table->string('name', 128);
            $table->string('short', 32)->index();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        // Create the advertising_banner table
        Schema::create('advertising_banners', function($table)
        {
            $table->increments('id');
            $table->string('name', 128);
            $table->string('url', 256)->nullable();
            $table->string('image', 256)->nullable();
            $table->integer('views')->default(0);
            $table->integer('max_views')->default(0);
            $table->boolean('active')->default(true);
            $table->integer('type_id')->unsigned()->nullable();
            $table->string('domain')->default('');
            $table->text('notes')->nullable();
            $table->timestamps();

            $table->foreign('type_id')->references('id')->on('advertising_banner_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertising_adsense');
        Schema::dropIfExists('advertising_banners');
        Schema::dropIfExists('advertising_banner_types');
    }

}
