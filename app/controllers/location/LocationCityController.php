<?php

class LocationCityController extends BaseController
{
    /**
     * Carrega a lista de cidades do estado informado
     *
     * @return Illuminate\Support\Facades\Response
     */
    public function load_all_json()
    {
        $estate_id = Input::get('estate_id');
        $estate = LocationEstate::where('id', '=', $estate_id)->firstOrFail();
        $cities = $estate->cities()->where('active', '=', 1)->orderBy('name')->get()->toArray();
        return Response::json(array(
            'success' => true,
            'cities' => $cities
        ));
    }
}