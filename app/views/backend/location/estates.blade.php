@extends("backend/template")

@section("content")

<div class="page_template"><!--page_template-->

<div class="titulo_grande" style="text-transform: uppercase;"><!--titulo_grande-->
<img src="/images/backend/icone_localizacoes.jpg"/> LOCALIZAÇÕES - ESTADOS
</div><!--titulo_medio-->

@if (!$context->estates->count())
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a>Nenhum estado cadastrado.</a>
</span>
</div>
@else
@foreach($context->estates as $i => $estate)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a href="{{ URL::action('BackendLocationController@cities', array(strtolower($estate->short), 'a')) }}" title="{{ $estate->name }}">{{ $estate->name }}</a>
</span>

@if (Auth::user()->can('manage_locations'))
<div class="row_menu">
@if ($estate->active)
<a href="{{ URL::action('BackendLocationController@estate_deactive', array($estate->short)) }}"><img src="/images/backend/row_menu_power_on.png" title="Ligado" /></a>
@else
<a href="{{ URL::action('BackendLocationController@estate_active', array($estate->short)) }}"><img src="/images/backend/row_menu_power_off.png" title="Desligado" /></a>
@endif
</div>
@endif
<span class="counter">
<small><b>000</b> vagas cadastradas</small>
</span>
</div><!--row/row-alt-->
@endforeach
@endif

</div><!--page_template-->

</div><!--container-->
@endsection
