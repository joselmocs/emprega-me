<?php

use Illuminate\Database\Migrations\Migration;


class CreateUserExperiencesTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the user_experiences table
        Schema::create('user_experiences', function($table)
        {
            $table->increments('id');
            $table->integer('profile_id')->unsigned()->nullable();
            $table->string('company');
            $table->string('position');
            $table->float('salary');
            $table->timestamp('started_at')->default('0000-00-00 00:00:00');
            $table->timestamp('ended_at')->default('0000-00-00 00:00:00');
            $table->timestamps();

            $table->foreign('profile_id')->references('id')->on('user_profile');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop user_experiences table
        Schema::dropIfExists('user_experiences');
    }

}
