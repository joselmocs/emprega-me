
<div id="div_formation">
<div style="margin: 0 0 25px 0"><!--div margin-->

    <div class="titulo_pequeno"><!--titulo_medio-->
        Formação acadêmica:
    </div><!--titulo_medio-->

    <div id="container_formations"></div>

    <script type="text/template" id="formation_tpl">
        <table width="100%" border="0" cellspacing="2" cellpadding="0" class="formulario moldura_cinza page_template_links formation_tables">
            <tr>
                <td width="160" align="right">Grau de escolaridade:</td>
                <td align="left">

                    <table width="0%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <select name="level">
                                    <option value="" selected="selected">Selecione</option>
                                    @foreach(UserEducation::$VERBOSE_LEVEL as $i => $education)
                                    <option value="{{ $i }}">{{ $education }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td width="20"></td>
                            <td style="padding: 0 2px 0 0;">Situação:</td>
                            <td>
                                <select name="situation">
                                    <option value="" selected="selected">Selecione</option>
                                    @foreach(UserEducation::$VERBOSE_SITUATION as $i => $situation)
                                    <option value="{{ $i }}">{{ $situation }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td width="160" align="right">Curso:</td>
                <td align="left">

                    <table width="0%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <input type="text" style="width: 175px;" name="study_area" />
                            </td>
                            <td width="20"></td>
                            <td style="color: #999999; padding: 0 2px 0 0;">Especialização:</td>
                            <td>
                                <input type="text" style="width: 150px;" name="specialization" />
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td width="160" align="right">Instituição:</td>
                <td align="left">
                    <input type="text" style="width: 200px;" name="institution" />
                </td>
            </tr>
            <tr>
                <td width="160" align="right">Mês/ano de início:</td>
                <td align="left">

                    <table width="0%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <input type="text" style="width: 75px;" class="date_mask" name="started_at" date-time="m/Y" />
                            </td>
                            <td width="20"></td>
                            <td style="padding: 0 2px 0 0;">Conclusão:</td>
                            <td>
                                <input type="text" style="width: 75px;" class="date_mask" name="ended_at" date-time="m/Y" />
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td height="5" colspan="2"></td>
            </tr>
            <tr>
                <td align="right"></td>
                <td align="left">
                    <a href="javascript:void(0);" title="Remover formação" name="rem_formation"><img src="/images/remover_9x9.png"> Remover formação</a>
                </td>
            </tr>
        </table>
    </script>

</div><!--div margin-->

<div class="formulario moldura_cinza page_template_links " style="margin-bottom: 20px;"><!--moldura_cinza-->
    <table width="100%" border="0" cellspacing="2" cellpadding="0">
        <tbody><tr>
            <td width="160" align="right"><img src="/images/plus_16x16.png"/></td>
            <td align="left">
                <a href="javascript:void(0);" title="Adicionar formação" id="add_formation"><b>Adicionar formação</b></a>
            </td>
        </tr>
        </tbody>
    </table>
</div>


<div style="margin: 0 0 25px 0;"><!--div margin-->

    <div class="titulo_pequeno"><!--titulo_medio-->
        Cursos complementares:
    </div><!--titulo_medio-->

    <div id="container_courses"></div>

    <script type="text/template" id="course_tpl">
        <table width="100%" border="0" cellspacing="2" cellpadding="0" class="formulario moldura_cinza page_template_links course_tables">
            <tr>
                <td width="160" align="right">Título do curso:</td>
                <td align="left">
                    <input type="text" style="width: 250px;" name="course" />
                </td>
            </tr>
            <tr>
                <td width="160" align="right">Instituição:</td>
                <td align="left">
                    <input type="text" style="width: 200px;" name="institution" />
                </td>
            </tr>
            <tr>
                <td width="160" align="right">Mês/ano de início:</td>
                <td align="left">

                    <table width="0%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <input type="text" style="width: 75px;" class="date_mask" name="started_at" date-time="m/Y" />
                            </td>
                            <td width="20"></td>
                            <td style="padding: 0 2px 0 0;">Duração:</td>
                            <td>
                                <input type="text" style="width: 75px;" name="duration" />
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td height="5" colspan="2"></td>
            </tr>
            <tr>
                <td align="right"></td>
                <td align="left">
                    <a href="javascript:void(0);" title="Remover curso" name="rem_course"><img src="/images/remover_9x9.png"> Remover curso</a>
                </td>
            </tr>
        </table>
    </script>
</div><!--div margin-->

<div class="formulario moldura_cinza page_template_links " style="margin-bottom: 20px;"><!--moldura_cinza-->
    <table width="100%" border="0" cellspacing="2" cellpadding="0">
        <tbody><tr>
            <td width="160" align="right"><img src="/images/plus_16x16.png"/></td>
            <td align="left">
                <a href="javascript:void(0);" title="Adicionar curso" id="add_course"><b>Adicionar curso</b></a>
            </td>
        </tr>
        </tbody>
    </table>
</div>



<div style="margin: 0 0 25px 0"><!--div margin-->

    <div class="titulo_pequeno"><!--titulo_medio-->
        Idiomas:
    </div><!--titulo_medio-->

    <table width="100%" border="0" cellspacing="2" cellpadding="0" class="formulario moldura_cinza">
        <tr>
            <td width="160" align="right">
                <select style="width:160px;" id="select_languages"></select>
            </td>
            <td align="left">

                <table width="0%" border="0" cellspacing="0" cellpadding="0" style="font-size: 11px;">
                    <tr>
                        @foreach(UserLanguage::$VERBOSE_LEVEL as $i => $level)
                        <td width="20"></td>
                        <td align="left" style="padding: 2px 4px 0 4px;">
                            <input type="radio" name="language" value="{{ $i }}" data-name="{{ $level }}">
                        </td>
                        <td align="left">{{ $level }}</td>
                        @endforeach

                        <td width="20"></td>
                        <td align="left">
                            <input type="button" style="font-size: 12px;" value="Incluir idioma" id="add_language" />
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
        <tr>
            <td height="10" colspan="2" align="center"><hr width="100%" noshade style="border: 1px solid #E5E5E5;"></td>
        </tr>
        <tr>
            <td colspan="2" align="left">

                <div id="container_languages"></div>

                <script type="text/template" id="language_tpl">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 11px;" class="language_tables">
                        <tr>
                            <td width="0%" height="20" style="padding: 0 5px 0 5px;">
                                <a href="javascript:void(0);" name="rem_language"><img src="/images/remover_9x9.png" /></a>
                            </td>
                            <td width="100%" height="20">
                                <b data-name="language" data-language=""></b> - Nível: <span data-name="level" data-level=""></span>
                            </td>
                        </tr>
                    </table>
                </script>

            </td>
        </tr>
    </table>

</div><!--div margin-->
</div>
