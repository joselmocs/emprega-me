/*!
 * apps.backend.backend.js
 * Scripts responsáveis pelas funcoes gerais usadas por toda adminsitração.
 *
 * Joselmo Cardozo
 */

require([
    "helpers/core"
], function(core) {
    var
        initialize = function() {
            events();
        },
        events = function() {
            $('a[name=remover]').unbind('click').click(function() {
                if (confirm('Deseja remover a vaga *'+ $(this).attr('data-name') +'*?')) {
                    window.location = core.context.urls.remove +'/'+ $(this).attr('data-id');
                }
            });
        };

    initialize();
});
