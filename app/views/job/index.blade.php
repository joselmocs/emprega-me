@extends("base/template")

@section("site-title")
Todas as vagas - '. Config::get('domain.site.title') }}
@endsection

@section("navigation")
<li><a href="{{ URL::action('JobController@index', array('-', '-', '-')) }}" title="Todas as vagas">Todas as Vagas</a></li>
@endsection

@section('requirejs')
requirejs(["apps/job/list"]);
@endsection

@section("content")
@include("advertising/header")

<div class="page_template"><!--page_template-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="auto 100%" valign="top"><!--coluna_1-->

<div class="titulo_medio"><!--titulo_medio-->
Todas as vagas
</div><!--titulo_medio-->

<div style="margin: 0 0 20px 0;"><!--div margin-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario">
<tr>
<td width="100%">
@foreach($context->job_types as $job_type)
<a href="{{ URL::action('JobController@index', array($job_type->slug, $context->estate_selected, $context->city_selected)) }}"><img src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $job_type->image }}" title="{{ $job_type->name }}"/></a>
@endforeach
</td>
<td align="right">
<select name="city_id" style="margin-left: 5px;">
<option value="-">Todas as cidades</option>
</select>
</td>
</tr>
</table>
</div><!--div margin-->

@if (!$context->jobs->count())
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a>Nenhuma vaga encontrada.</a>
</span>
</div>
@else
@foreach($context->jobs as $i => $job)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $job->type->image }}" title="{{ $job->type->name }}"/><a href="{{ URL::action('JobController@view', array($job->type->slug, $job->id, $job->city->slug, $job->slug)) }}" title="{{ $job->position }}">{{ $job->position }}</a>
<small><span>por </span>{{ $job->company }}<span> em </span>{{ $job->city->name }}/{{ $job->estate->short }}</small>
</span>

<span class="time_post">
@if (Carbon::parse($job->created_at)->isToday())
<img src="/images/hoje.png" title="Vaga publicada hoje"/>
@endif
@if (Carbon::parse($job->created_at)->isYesterday())
<img src="/images/ontem.png" title="Vaga publicada ontem"/>
@endif
<img src="/images/clock.png" title="Data da publicação"/>
<small title="{{ Carbon::parse($job->created_at)->format('d \d\e F \d\e Y') }}">{{ Carbon::parse($job->created_at)->format('d/m') }}</small>
</span>
</div><!--row/row-alt-->
@endforeach

<div class="page_count-letter"><!--page_count-letter-->
{{ $context->jobs->links() }}
</div><!--page_count-letter-->
@endif

</td><!--coluna_1-->

<td style="width: 15px;"></td>

<td width="300" valign="top"><!--coluna_2-->
<div style="margin: 0 0 25px 0"><!--div margin-->
{{ AdvertisingController::adsense('large_600x300') }}
</div><!--div margin-->


<div class="categorias_sidebar" style="font-size: 12px; margin: 0 0 25px 0"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Escolha a categoria
</div><!--titulo_pequeno-->

<ul>
@foreach($context->categories as $category)
<li><a href="{{ URL::action('CategoryController@view', array($category->url, '-', '-', '-')) }}">{{ Str::limit($category->name, 40, '...') }} <small>({{ $category->jobs->count() }})</small></a></li>
@endforeach
</ul>

</div><!--div margin-->
</td><!--coluna_2-->
</tr>
</table>

</div><!--categoria-->

</div><!--container-->
@include("advertising/footer")
@include("advertising/facebook")
@endsection
