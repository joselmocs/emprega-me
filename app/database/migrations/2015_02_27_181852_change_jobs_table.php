<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeJobsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobs', function($table)
        {
            $table->dropColumn('notify');
            $table->boolean('show_website')->default(false);
            $table->boolean('show_phone')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobs', function($table)
        {
            $table->boolean('notify')->default(true);
            $table->dropColumn('show_website')->default(false);
            $table->dropColumn('show_phone')->default(false);
        });
    }

}
