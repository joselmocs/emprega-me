@extends('base/template')

@section('requirejs')
requirejs(["apps/company/edit"]);
@endsection

@section('content')
<div id="cadastrar-editar_candidato_1" class="page_template"><!--cadastrar-editar_candidato_1-->

    <div class="titulo_medio"><!--titulo_medio-->
        Editar dados
    </div><!--titulo_medio-->

    <div class="cadastrar_sleps" style="margin-bottom: 20px;"><!--cadastrar_sleps-->
        <div id="cadastrar_slep_1" class="cadastrar_slep_active">
            <b>1</b> Preenchimento de dados
        </div>

        <div id="cadastrar_slep_4">
            <b>2</b> Finalização
        </div>
    </div>

    <form action="{{ URL::action('CompanyController@edit') }}" method="post">
        <input type="hidden" name="_token" value="{{  csrf_token() }}" />

        <div style="margin: 0 0 25px 0"><!--div margin-->

            <div class="titulo_pequeno"><!--titulo_medio-->
                Dados do empregador/responsável:
            </div><!--titulo_medio-->

            <table width="100%" border="0" cellspacing="2" cellpadding="0" class="formulario moldura_cinza">
                <tr>
                    <td width="160" align="right">Nome completo:</td>
                    <td align="left">
                        <input type="text" style="width: 350px;" name="fullname" required="true" value="{{ $context->input->fullname }}"
                        @if ($context->field_errors && $context->field_errors->has('fullname')) class="error" @endif />
                        @if ($context->field_errors && $context->field_errors->has('fullname'))
                        <div class="error">{{ $context->field_errors->first('fullname') }}</div>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td width="160" align="right">Empresa:</td>
                    <td align="left">
                        <input type="text" style="width: 350px;" name="company" required="true" value="{{ $context->input->company }}"
                        @if ($context->field_errors && $context->field_errors->has('company')) class="error" @endif />
                        @if ($context->field_errors && $context->field_errors->has('company'))
                        <div class="error">{{ $context->field_errors->first('company') }}</div>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td width="160" align="right">Estado:</td>
                    <td align="left">
                        <select name="estate_id" required="true">
                            <option value="">Selecione</option>
                            @foreach(LocationEstate::where('active', '=', 1)->get() as $estate)
                            <option value="{{ $estate->id }}"  @if($context->input->estate_id == $estate->id) selected="selected" @endif>{{ $estate->name }}</option>
                            @endforeach
                        </select>
                        @if ($context->field_errors && $context->field_errors->has('estate_id'))
                        <div class="error">{{ $context->field_errors->first('estate_id') }}</div>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td width="160" align="right">Cidade:</td>
                    <td align="left">
                        <select name="city_id" required="true">
                            <option value="">Selecione</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="160" align="right">Telefone:</td>
                    <td align="left">
                        <table width="0%" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                <td>
                                    <input type="text" style="width: 200px;" name="phone" value="{{ $context->input->phone }}" required="true">
                                    @if ($context->field_errors && $context->field_errors->has('phone'))
                                    <div class="error">{{ $context->field_errors->first('phone') }}</div>
                                    @endif
                                </td>
                                <td style="color: #999999;"><small>(não será publicado)</small></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="160" align="right" style="color: #999999;">Website:</td>
                    <td align="left">

                        <table width="0%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <input type="text" style="width: 175px;" name="website" value="{{ $context->input->website }}" />
                                </td>
                                <td style="color: #999999;"><small>(opcional)</small></td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>

        </div><!--div margin-->

        <div class="formulario moldura_cinza"><!--moldura_cinza-->
            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                <tr>
                    <td width="160"></td>
                    <td align="left">
                        <input type="submit" value="Editar Dados" name="submit" />
                    </td>
                </tr>
            </table>
        </div><!--moldura_cinza-->

    </form>

</div>
@endsection