/*!
 * apps.backend.locations.js
 * Scripts responsáveis pela criação e edição de bairros.
 *
 * Joselmo Cardozo
 */

require([
    "helpers/core"
], function(core) {
    var
        initialize = function() {
            events();
        },
        events = function() {
            $('a[name=remover]').unbind('click').click(function() {
                if (confirm('Deseja remover o bairro *'+ $(this).attr('data-name') +'*?')) {
                    window.location = core.context.urls.remove +'/'+ $(this).attr('data-id');
                }
            });
        };

    initialize();
});