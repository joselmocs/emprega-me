@extends("base/template")

@section("site-title")
Minhas candidaturas - {{ Config::get('domain.site.title') }}
@endsection

@section('requirejs')
requirejs(["apps/candidate/candidacies"]);
@endsection

@section("content")
@include("advertising/header")

<div class="page_template"><!--page_template-->

<div class="titulo_medio"><!--titulo_medio-->
Minhas candidaturas <span style="color: #999999;">({{ $context->candidacies->count() }})</span>
</div><!--titulo_medio-->

<div style="margin: 0 0 20px 0"><!--div margin-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario">
<tr>
<td align="right">
<select name="view">
<option value="-" @if ($context->view == "-") selected="selected" @endif>Todo o período</option>
<option value="0" @if ($context->view == "0") selected="selected" @endif>Hoje</option>
<option value="1" @if ($context->view == "1") selected="selected" @endif>Ontem</option>
<option value="7" @if ($context->view == "7") selected="selected" @endif>Últimos 7 dias</option>
<option value="30" @if ($context->view == "30") selected="selected" @endif>Últimos 30 dias</option>
</select>
</td>
</tr>
</table>
</div><!--div margin-->

@if (!$context->candidacies->count())
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a>Nenhuma candidatura.</a>
</span>
</div>
@else
@foreach($context->candidacies as $i => $candidacy)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $candidacy->job->type->image }}" title="{{ $candidacy->job->type->name }}">
<a href="{{ URL::action('JobController@view', array($candidacy->job->type->slug, $candidacy->job->id, $candidacy->job->city->slug, $candidacy->job->slug)) }}" title="{{ $candidacy->job->position }}">{{ $candidacy->job->position }}</a>
<small><span>por</span> {{ $candidacy->job->company }} <span>em</span> {{ $candidacy->job->city->name }}/{{ $candidacy->job->estate->short }}</small>
</span>

<div class="row_menu">
@if ($candidacy->job->removed_at)
<img src="/images/row_menu_erro.png" title="Vaga removida em {{ Carbon::parse($candidacy->job->deleted_at)->format('d/m/Y') }}"/>
@else
<a href="{{ URL::action('JobController@cancel', array($candidacy->job->id .'-'. Illuminate\Support\Str::slug($candidacy->job->position))) }}"><img src="/images/row_menu_cancelar.png" title="Cancelar candidatura"/></a>
@endif
</div>

<div class="row_menu">
<img src="/images/row_menu_enviado.png" title="Enviado em {{ Carbon::parse($candidacy->created_at)->format('d/m/Y') }}"/>
@if ($candidacy->viewed_at)
<img src="/images/row_menu_yes.png" title="Visualizado em {{ Carbon::parse($candidacy->viewed_at)->format('d/m/Y') }}"/>
@endif
@if ($candidacy->archived_at)
<img src="/images/row_menu_arquivar.png" title="Arquivado em {{ Carbon::parse($candidacy->archived_at)->format('d/m/Y') }}"/>
@endif
</div>

<span class="time_post">
@if (Carbon::parse($candidacy->job->created_at)->isToday())
<img src="/images/hoje.png" title="Vaga publicada hoje">
@endif
@if (Carbon::parse($candidacy->job->created_at)->isYesterday())
<img src="/images/ontem.png" title="Vaga publicada ontem">
@endif
<img src="/images/clock.png" title="Data da publicação">
<small title="{{ Carbon::parse($candidacy->job->created_at)->format('d \d\e F \d\e Y') }}">{{ Carbon::parse($candidacy->job->created_at)->format('d/m') }}</small>
</span>
</div><!--row/row-alt-->
@endforeach
@endif

</div><!--minhas_candidaturas-->

</div><!--container-->
@include("advertising/facebook")
@endsection
