<?php

use Illuminate\Database\Migrations\Migration;


class ConfigurationTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the configuration
        Schema::create('configuration', function($table)
        {
            $table->increments('id');
            $table->tinyInteger('time_announcement')->default(0);
            $table->tinyInteger('former_job')->default(0);
            $table->tinyInteger('highlighted_vacancies')->default(0);
            $table->tinyInteger('featured_jobs')->default(0);
            $table->tinyInteger('recent_jobs')->default(0);
            $table->tinyInteger('busiest_month')->default(0);
            $table->tinyInteger('busiest_week')->default(0);
            $table->tinyInteger('busiest_day')->default(0);
            $table->tinyInteger('highlights_category')->default(0);
            $table->tinyInteger('jobs_category')->default(0);
            $table->tinyInteger('competitive_category')->default(0);
            $table->tinyInteger('recent_jobs_company')->default(0);
            $table->tinyInteger('similar_jobs')->default(0);
            $table->string('smtp_hostname')->nullable();
            $table->integer('smtp_port')->nullable();
            $table->boolean('smtp_auth')->default(true);
            $table->tinyInteger('smtp_secure')->default(0);
            $table->string('smtp_username')->nullable();
            $table->string('smtp_password')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configuration');
    }

}
