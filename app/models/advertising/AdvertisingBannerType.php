<?php


class AdvertisingBannerType extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'advertising_banner_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'name', 'short', 'active');

    /**
     * Retorna os banners cadastrado neste tipo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function banners()
    {
        return $this->hasMany('AdvertisingBanner', 'type_id');
    }
}