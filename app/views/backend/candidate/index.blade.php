@extends("backend/template")

@section("requirejs")
requirejs(["apps/backend/candidates"]);
@endsection

@section("content")
<div class="page_template"><!--page_template-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="auto 100%" valign="top"><!--coluna_1-->

<div class="botao"><!--botao-->
@if (Auth::user()->can('manage_candidates'))
<li><a title="Adicionar candidato" href="{{ URL::action('BackendCandidateController@register') }}">Adicionar candidato</a></li>
@endif
</div><!--botao-->

<div class="titulo_grande" style="text-transform: uppercase;"><!--titulo_grande-->
<img src="/images/backend/icone_candidatos.jpg"/> CANDIDATOS
</div><!--titulo_grande-->

<span style="color: #666666; display: none; font-size: 12px;">Nenhum candidato até o momento.</span>

<div class="page_count-letter"><!--page_count-letter-->
@foreach(range('A', 'Z') as $letter)
<li><a href="{{ $context->filter->url }}/{{ strtolower($letter) }}" title="Candidatos com a letra {{ $letter }}" @if(strtoupper($context->letter) == $letter) class="active" @endif>{{ $letter }}</a></li>
@endforeach
</div><!--page_count-letter-->


<div class="titulo_pequeno"><!--titulo_pequeno-->
@if($context->new_search)
50 últimos candidatos cadastrados
@else
Exibindo {{ $context->candidates->getFrom() }}
 a {{ $context->candidates->getTo() }}
 de {{ $context->candidates->getTotal() }} candidato(s).
@endif
</div><!--titulo_pequeno-->

@if (!$context->candidates->count())
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a>Nenhum candidado encontrado.</a>
</span>
</div>
@else
@foreach($context->candidates as $i => $candidate)
<div class="@if ($i % 2) row-alt @else row @endif">
<span class="row-info">
<img src="/images/set_right.png"><a class="fancybox fancybox.iframe" data-fancybox-width="800" data-fancybox-height="850" title="Currículo {{ $candidate->profile->fullname }}" href="{{ URL::action('CurriculumController@view', array($candidate->id)) }}">{{ $candidate->profile->fullname }}</a>
<small><span>de </span>{{ $candidate->profile->city->name }}/{{ $candidate->profile->city->estate->short }}</small>
</span>

@if (Auth::user()->can('manage_candidates'))
<div class="row_menu">
<a title="Editar usuário" href="{{ URL::action('BackendCandidateController@register', array($candidate->id)) }}"><img src="/images/backend/row_menu_editar.png" title="Editar"></a>
<a href="javascript:void(0)" data-id="{{ $candidate->id }}" data-name="{{ $candidate->profile->fullname }}" name="remover"><img src="/images/backend/row_menu_remover.png" title="Remover conta"></a>
</div>
@endif
</div>
@endforeach
@endif

@if (!$context->new_search)
<div class="page_count-letter"><!--page_count-letter-->
{{ $context->candidates->links() }}
</div><!--page_count-letter-->
@endif

</td><!--coluna_1-->

<td style="width: 15px;"></td>

<td width="300" valign="top"><!--coluna_2-->

<div style="margin: 0 0 20px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Estatisticas
</div><!--titulo_pequeno-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
  <td align="left">
    
  <table width="0%" border="0" cellspacing="0" cellpadding="4">
  <tr>
  <td width="100%" height="25" align="left">Total de candidatos: {{ $context->total_candidates }}</td>
  </tr>
  </table>
    
  </td>
</tr>
<tr>
  <td height="15"><hr width="100%" style=" background: #EEEEEE; color: #EEEEEE; height:2px;"></td>
</tr>
<tr>
  <td align="left">
    
  <table width="0%" border="0" cellspacing="0" cellpadding="4">
  <tr>
  <td height="25" align="left">Candidatos hoje: 00</td>
  </tr>
  <tr>
  <td height="25" align="left">Candidatos ontem: 00</td>
  </tr>
  <tr>
  <td height="25" align="left">Candidatos na semana: 00</td>
  </tr>
  <tr>
    <td height="25" align="left">Candidatos no mês: 00</td>
  </tr>
  </table>
    
    
  </td>
</tr>
</table>

</div><!--div margin-->



<div style="margin: 0 0 20px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Filtro de candidatos
</div><!--titulo_pequeno-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
<td align="left">Genero:</td>
</tr>
<tr>
<td align="left">

<table width="0%" border="0" cellspacing="0" cellpadding="4">
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;"><input type="checkbox" name="cnh_a" ></td>
<td width="100%" height="25" align="left">Todos os generos</td>
</tr>
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;"><input type="checkbox" name="cnh_b"></td>
<td width="100%" height="25" align="left" style="color: #888888;">Masculino</td>
</tr>
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;"><input type="checkbox" name="cnh_c"></td>
<td width="100%" height="25" align="left" style="color: #888888;">Feminino</td>
</tr>
</table>

</td>
</tr>
<tr>
<td height="15"><hr width="100%" style=" background: #EEEEEE; color: #EEEEEE; height:2px;"></td>
</tr>
<tr>
  <td height="15">Localização/estado:</td>
</tr>
<tr>
<td align="left">

<table width="0%" border="0" cellspacing="0" cellpadding="4">
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;"><input name="checkbox" type="checkbox" id="checkbox_mark_all" @if ($context->filter->estates == '-') checked="checked" @endif></td>
<td width="100%" height="25" align="left">Todos os estados</td>
</tr>
@foreach($context->estates as $estate)
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;"><input name="checkbox_estates" value="{{ strtolower($estate->short) }}" type="checkbox" @if ($context->filter->estates == '-' || strpos($context->filter->estates, strtolower($estate->short)) !== false) checked="checked" @endif></td>
<td width="100%" height="25" align="left" style="color: #888888;">{{ $estate->name }}/{{ $estate->short }}</td>
</tr>
@endforeach
</table>


</td>
</tr>
</table>

</div><!--div margin-->


</td><!--coluna_2-->
</tr>
</table>
</div><!--page_template-->

</div><!--container-->
@endsection