<?php

namespace JCS\Mail\Models;


class Mail extends \Eloquent
{
    const STATUS_WAITING = 0;
    const STATUS_ERROR   = 2;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mail_items';

    /**
     * Retorna a lista do item
     *
     * @return \JCS\Mail\Models\Queue
     */
    public function queue()
    {
        return $this->belongsTo('JCS\Mail\Models\Queue', 'queue_id');
    }
}
