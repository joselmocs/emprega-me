@extends("base/template")

@section("site-title")
Lista de categorias - {{ Config::get('domain.site.title') }}
@endsection

@section("navigation")
<li><a href="{{ URL::action('CategoryController@index') }}" title="Lista de categorias">Categorias</a></li>
@endsection

@section("content")
@include("advertising/header")

@endsection
