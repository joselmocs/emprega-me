<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>{{ $context->profile->fullname }}</title>
    <style type="text/css">
        * {
            margin: 0px;
        }
        body {
            color: #333333;
            font-size: 14px;
            font-family: tahoma;
            margin: 0;
        }
    </style>
    <style type="text/css" media="print">
        #tableprint {
            display: none;
        }
    </style>
</head>

<body>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="100%" align="left" valign="top">

            <table width="100%" border="0" cellpadding="2" cellspacing="0">
                <tr>
                    <td align="left" valign="top">

                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="100%" style="font-size: 22px; text-transform: uppercase">{{ $context->profile->fullname }}</td>
                                <td align="right">

                                    <table width="0%" border="0">
                                        <tr>
                                            <td>
                                                @if ($context->profile->facebook)
                                                <a href="{{ $context->profile->facebook }}" target="_blank"><img src="../images/curriculo_facebook.png"/></a>
                                                @endif
                                            </td>

                                            <td>
                                                @if ($context->profile->linkedin)
                                                <a href="{{ $context->profile->linkedin }}" target="_blank"><img src="../images/curriculo_linkedin.png"/></a>
                                                @endif
                                            </td>

                                            <td>
                                                @if ($context->profile->gplus)
                                                <a href="{{ $context->profile->gplus }}" target="_blank"><img src="../images/curriculo_google_plus.png"/></a>
                                                @endif
                                            </td>

                                            <td>
                                                @if ($context->profile->twitter)
                                                <a href="{{ $context->profile->twitter }}" target="_blank"><img src="../images/curriculo_twitter.png"/></a>
                                                @endif
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="5" align="left"><hr width="100%" size="2px"noshade color="#DDDDDD"></td>
                </tr>
                <tr>
                    <td align="left"><b>Data de nascimento:</b> {{ Carbon::parse($context->profile->birthday)->format('d/m/Y') }} ({{ Carbon::parse($context->profile->birthday)->age }} anos)</td>
                </tr>
                <tr>
                    <td align="left"><b>Local:</b> {{ $context->profile->city->name }}/{{ $context->profile->city->estate->short }}</td>
                </tr>
                <tr>
                    <td align="left"><b>Sexo:</b> {{ UserProfile::$VERBOSE_GENDER[$context->profile->gender] }}</td>
                </tr>
                <tr>
                    <td align="left"><b>Estado civil:</b> {{ UserProfile::$VERBOSE_MARITAL[$context->profile->marital_status] }}</td>
                </tr>
                <tr>
                    <td align="left">
                        <b>Carteira  de habilitação:</b> {{ $context->profile->cnh() }} - <b>Possui veículo:</b> {{ $context->profile->auto()    }}</td>
                </tr>
                <tr>
                    <td align="left"><b>Disponivel para viagens</b>: {{ ($context->profile->travel_availability) ? 'Sim' : 'Não' }} - <b>Morar em outra cidade:</b> {{ ($context->profile->live_another_city_availability) ? 'Sim' : 'Não' }}</td>
                </tr>
                <tr>
                    <td align="left"><b>Pessoa com deficiencia:</b> {{ ($context->profile->special_needs) ? 'Sim - '. UserProfile::$VERBOSE_SPECIAL_NEEDS[$context->profile->special_needs] : 'Não' }}</td>
                </tr>
            </table>

        </td>
        <td align="center" valign="top">
            <img style="margin: 4px; border-radius: 2px; -moz-border-radius: 2px; -webkit-border-radius: 2px;" src="{{ $context->profile->get_avatar() }}" width="125" height="125"/>
        </td>
    </tr>
</table>

<br />

<table width="100%" border="0" cellpadding="4" cellspacing="0">
    <tr>
        <td height="20" align="left" bgcolor="#DDDDDD" style="font-size: 16px">DADOS PARA CONTATO:</td>
    </tr>
    @if ($context->profile->mobile)
    <tr>
        <td align="left">▪ Telefone celular: {{ $context->profile->mobile }}</td>
    </tr>
    @endif
    @if ($context->profile->phone)
    <tr>
        <td align="left">▪ Telefone residencial: {{ $context->profile->phone }}</td>
    </tr>
    @endif
    <tr>
        <td align="left">▪ E-mail: {{ $context->profile->user->email }}</td>
    </tr>
</table>
<br />

@if ($context->profile->desired_positions->count())
<table width="100%" border="0" cellpadding="4" cellspacing="0">
    <tr>
        <td height="20" align="left" bgcolor="#DDDDDD" style="font-size: 16px">OBJETIVO/CARGO PRETENDIDO:</td>
    </tr>
    @foreach($context->profile->desired_positions as $position)
    <tr>
        <td align="left">▪ {{ $position->position }} - Pretenção salárial: {{ $position->salary->description }}</td>
    </tr>
    @endforeach
</table>
<br />
@endif

@foreach($context->profile->educations as $education)
<table width="100%" border="0" cellpadding="4" cellspacing="0">
    <tr>
        <td height="20" align="left" bgcolor="#DDDDDD" style="font-size: 16px">FORMAÇÃO ACADEMICA:</td>
    </tr>
    <tr>
        <td align="left">▪ Grau de escolaridade: {{ UserEducation::$VERBOSE_LEVEL[$education->level] }}</td>
    </tr>
    @if ($education->study_area)
    <tr>
        <td align="left">▪ Área de estudo: {{ $education->study_area }}</td>
    </tr>
    @endif
    @if ($education->specialization)
    <tr>
        <td align="left">▪ Especialização: {{ $education->specialization }}</td>
    </tr>
    @endif
    <tr>
        <td align="left">▪ Instituição: {{ $education->institution }}</td>
    </tr>
    <tr>
        <td align="left">▪ Mês/ano de inicio: {{ Carbon::parse($education->started_at)->format('m/Y') }} - Conclusão: {{ Carbon::parse($education->ended_at)->format('m/Y') }}</td>
    </tr>
</table>
@endforeach
<br />

@if ($context->profile->courses->count())
<table width="100%" border="0" cellpadding="4" cellspacing="0">
    <tr>
        <td height="20" align="left" bgcolor="#DDDDDD" style="font-size: 16px">CURSOS COMPLEMENTARES:</td>
    </tr>
    @foreach($context->profile->courses as $course)
    <tr>
        <td align="left">▪ {{ $course->course }} - {{ $course->institution }} - {{ Carbon::parse($course->started_at)->format('m/Y') }} ({{ $course->duration }})</td>
    </tr>
    @endforeach
</table>
<br />
@endif

@if ($context->profile->languages->count())
<table width="100%" border="0" cellpadding="4" cellspacing="0">
    <tr>
        <td height="20" align="left" bgcolor="#DDDDDD" style="font-size: 16px">IDIOMAS:</td>
    </tr>
    <tr>
        @foreach($context->profile->languages as $language)
        <td align="left">▪ {{ $language->language->language }} - Nível: {{ UserLanguage::$VERBOSE_LEVEL[$language->level] }}</td>
    </tr>
    @endforeach
</table>
<br />
@endif

@if ($context->profile->experiences->count())
<table width="100%" border="0" cellpadding="4" cellspacing="0">
    <tr>
        <td height="20" align="left" bgcolor="#DDDDDD" style="font-size: 16px">EXPERIENCIA PROFISSIONAL:</td>
    </tr>
    @foreach($context->profile->experiences as $experience)
    <tr>
        <td align="left">▪ {{ $experience->position }} - {{ $experience->company }} - {{ Carbon::parse($experience->started_at)->format('m/Y') }} á {{ Carbon::parse($experience->ended_at)->format('m/Y') }} - Salário final R$ {{ number_format($experience->salary, 2, ',', '.') }}</td>
    </tr>
    @endforeach
</table>
<br />
@endif

@if (strlen($context->profile->notes))
<table width="100%" border="0" cellpadding="4" cellspacing="0">
    <tr>
        <td height="20" align="left" bgcolor="#DDDDDD" style="font-size: 16px">OBSERVAÇÕES/INFORMAÇÕES ADICIONAIS:</td>
    </tr>
    <tr>
        <td align="left">{{ nl2br($context->profile->notes) }}</td>
    </tr>
</table>
<br />
@endif

<table width="100%" border="0" cellpadding="4" cellspacing="0" id="tableprint">
    <tr>
        <td width="0" align="right">

            <table width="0%" border="0" cellpadding="4" cellspacing="0" style="font-size: 12px">
                <tr>
                    <td><img src="/images/row_menu_impressora.png"/></td>
                    <td><a href="" target="_blank">Imprimir curriculo</a></td>
                </tr>
            </table>

        </td>
    </tr>
</table>

</br>

<table width="100%" border="0" cellpadding="4" cellspacing="0" style="font-size: 12px">
    <tr>
        <td align="right">© 2012-2014 <a href="{{ Config::get('domain.site.url') }}" title="{{ Config::get('domain.site.name') }}">{{ Config::get('domain.site.name') }}</a> - Todos os direitos reservados</td>
    </tr>
</table>

</body>
</html>
