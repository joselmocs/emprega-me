@extends('backend/template')

@section('requirejs')
    requirejs(["apps/backend/backend"]);
@endsection

@section('content')
<div class="page_template"><!--page_template-->

<div class="titulo_grande" style="text-transform: uppercase;"><!--titulo_grande-->
<img src="/images/backend/icone_painel_inicial.jpg"/> PAINEL INICIAL
</div><!--titulo_grande-->

<div class="alerta_cinza"><!--alerta_cinza-->
Há (##) vagas denunciadas no painel, <a href="denuncias.php">clique aqui</a> para ver.
</div><!--alerta_cinza-->

<div style="margin: 0 0 5px 0"><!--div margin-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario">
<tr>
<td width="100%">
<a href="../linkaqui"><img src="../domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="../linkaqui"><img src="../domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="../linkaqui"><img src="../domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="../linkaqui"><img src="../domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="../linkaqui"><img src="../domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="../linkaqui"><img src="../domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
</td>
<td align="right">
<select>
<option value="Todo os estados">Todo os estados</option>
<option value="Minas Gerais/MG">Todo os estados</option>
</select>
</td>
</tr>
</table>
</div><!--div margin-->

@if (!$context->jobs->count())
<span style="color: #666666; font-size: 12px;">Nenhuma vaga para análise.</span>
@else

<div class="titulo_pequeno"><!--titulo_pequeno-->
Vagas desativadas <span style="color: #999999;">(##)</span>
</div><!--titulo_pequeno-->
@foreach($context->jobs as $job)
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $job->type->image }}" title="{{ $job->type->name }}">
<a class="fancybox fancybox.iframe" title="{{ $job->position }}" href="{{ URL::action('CompanyJobController@view', array($job->id)) }}" data-fancybox-width="800" data-fancybox-height="600">
{{ $job->position }}
</a>
<small><span>por</span> <a href="{{ URL::action('BackendCompanyController@jobs', array($job->profile->id)) }}">{{ $job->company }}</a><span> em </span>{{ $job->city->name }}/{{ $job->estate->short }}</small>
</span>

<div class="row_menu">
<a href="{{ URL::action('BackendCompanyController@approve', array($job->id)) }}"><img src="/images/backend/row_menu_aprovar.png" title="Aprovar vaga"></a>
</div>

<div class="row_menu">
<a href="{{ URL::action('BackendCompanyController@reprove', array($job->id)) }}"><img src="/images/backend/row_menu_reprovar.png" title="Reprovar vaga"></a>
</div>

<div class="row_menu">
@if ($job->featured)
<a href="{{ URL::action('BackendCompanyController@feature', array($job->id)) }}"><img src="/images/backend/row_menu_destaque_on.png" title="Destaque ON"></a>
@else
<a href="{{ URL::action('BackendCompanyController@feature', array($job->id)) }}"><img src="/images/backend/row_menu_destaque_off.png" title="Destaque OFF"></a>
@endif
<a href="{{ URL::action('BackendCompanyController@edit', array($job->id)) }}?next={{ URL::current() }}"><img src="/images/backend/row_menu_editar.png" title="Editar"></a>
<a href="javascript: void(0);" name="remover" data-id="{{ $job->id }}" data-name="{{ $job->position }}"><img src="/images/backend/row_menu_remover.png" title="Remover"></a>
</div>

<span class="time_post">
@if (Carbon::parse($job->created_at)->isToday())
<img src="/images/hoje.png" title="Vaga publicada hoje">
@endif
@if (Carbon::parse($job->created_at)->isYesterday())
<img src="/images/ontem.png" title="Vaga publicada ontem">
@endif
<img src="/images/clock.png" title="Data da publicação">
<small title="{{ Carbon::parse($job->created_at)->format('d \d\e F \d\e Y') }}">05/03</small>
</span>
</div><!--row/row-alt-->

@endforeach
@endif
</div>

</div><!--container-->
@endsection