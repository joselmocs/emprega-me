<?php

use Illuminate\Database\Migrations\Migration;


class CreateLinksTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the links table
        Schema::create('links', function($table)
        {
            $table->increments('id');
            $table->string('name', 64);
            $table->string('title', 256);
            $table->string('url', 256);
            $table->tinyInteger('position')->default(1);
            $table->tinyInteger('order')->default(1);
            $table->boolean('newpage')->default(false);
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop links table
        Schema::dropIfExists('links');
    }

}
