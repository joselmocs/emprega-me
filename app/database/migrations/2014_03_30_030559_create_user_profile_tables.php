<?php

use Illuminate\Database\Migrations\Migration;


class CreateUserProfileTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the user profile table
        Schema::create('user_profile', function($table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('photo', 256)->nullable();
            $table->string('fullname', 256)->default('');
            $table->string('company', 256)->default('');
            $table->tinyInteger('gender')->default(1);
            $table->timestamp('birthday')->default('0000-00-00 00:00:00');
            $table->tinyInteger('marital_status')->default(1);
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('facebook', 256)->nullable();
            $table->string('linkedin', 256)->nullable();
            $table->string('gplus', 256)->nullable();
            $table->string('twitter', 256)->nullable();
            $table->string('website', 256)->nullable();
            $table->text('notes')->nullable();
            $table->text('search_string')->nullable();
            $table->boolean('travel_availability')->default(false);
            $table->boolean('live_another_city_availability')->default(false);
            $table->tinyInteger('special_needs')->default(0);
            $table->boolean('allow_search')->default(true);
            $table->integer('count_login')->default(0);
            $table->integer('count_login_failed')->default(0);
            $table->timestamp('last_login')->default('0000-00-00 00:00:00');
            $table->timestamp('last_login_failed')->default('0000-00-00 00:00:00');
            $table->timestamp('previous_login')->default('0000-00-00 00:00:00');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop user profile table
        Schema::dropIfExists('user_profile');
    }

}
