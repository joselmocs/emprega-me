<?php


class JobController extends BaseController {

    /**
     * Short-url para a vaga de emprego.
     *
     * @param integer $job
     * @return Illuminate\Support\Facades\Redirect
     */
    public function short($job)
    {
        try {
            $job = Job::where('id', $job)->firstOrFail();
        }
        catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            App::abort(404);
        }

        return Redirect::action('JobController@view', array(
            $job->type->slug, $job->id, $job->city->slug, $job->slug
        ));
    }

    /**
     * Página de visualização da vaga
     *
     * @param string $job_type
     * @param integer $job
     * @param string $city
     * @param string $title
     * @return Illuminate\Support\Facades\View
     */
    public function view($job_type, $job, $city, $title)
    {
        $job_obj = null;
        try {
            $job_obj = Job::where('id', $job)->firstOrFail();
        }
        catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            App::abort(404);
        }

        // Se for a primeira visita do usuário no site, selecionamos o estado para filtro
        // de acordo com o estado desta vaga.
        if (Session::get('estate', null) == null) {
            Session::set('estate', $job_obj->estate->short);
        }

        $job_obj->increment('views');

        $recents = Job::where('profile_id', $job_obj->profile->id)
            ->where('company', $job_obj->company)
            ->where('estate_id', Session::get('estate'))
            ->orderBy('created_at', 'asc')
            ->limit(Configuration::firstOrFail()->recent_jobs_company)
            ->get();

        $this->set_context(array(
            'request' => (object) array(
                'job_type' => $job_type,
                'city' => $city,
                'title' => $title,
                'job' => $job
            ),
            'job' => $job_obj,
            'recents' => $recents,
            'busiest_category' => JobController::busiest_category($job_obj->category_id)->get(),
            'categories' => Category::where('active', true)->orderByRaw('RAND()')->limit(10)->get()
        ));

        return $this->view_make('job/view');
    }

    /**
     * Envia o currículo para uma vaga de emprego
     *
     * @param string $job_type
     * @param string $city
     * @param string $job
     * @return Illuminate\Support\Facades\Redirect
     */
    public function join($job_type, $job, $city)
    {
        $profile = Auth::user()->profile;
        $job = Job::where('id', intval($job))->firstOrFail();

        $candidate = JobCandidate::where('profile_id', $profile->id)
            ->where('job_id', $job->id);

        if (!$candidate->count()) {
            $candidate = new JobCandidate;
            $candidate->profile()->associate($profile);
            $candidate->job()->associate($job);
            $candidate->save();
        }

        if (Config::get('app.mail') && $job->notify) {
            $message = View::make('job/email/new_candidate')
                ->with('site_name', Config::get('domain.site.name'))
                ->with('fullname', $job->profile->fullname)
                ->with('job', $job)
                ->with('candidate', $profile)
                ->render();

            $campaign = new \JCS\Mail\Campaign();
            $campaign->addAddress(new \JCS\Mail\ArraySource(array($job->email)));

            $campaign
                ->setMessage("Nova candidatura: ". $job->position, $message, "html")
                ->setMode(\JCS\Mail\Models\Queue::MODE_SINGLE)
                ->save();
            $campaign->execute();
        }

        return Redirect::back();
    }

    /**
     * Cancela a candidatura em uma vaga
     *
     * @param string $job
     * @return Illuminate\Support\Facades\Redirect
     */
    public function cancel($job)
    {
        try {
            $canditacy = JobCandidate::where('profile_id', Auth::user()->profile->id)
                ->where('job_id', intval(explode('-', $job)[0]))
                ->firstOrFail();
            $canditacy->delete();
        }
        catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            App::abort(404);
        }

        return Redirect::back();
    }

    /**
     * Faz o reporte da vaga como sendo falsa
     *
     * @param string $job_type
     * @param string $city
     * @param string $job
     * @return Illuminate\Support\Facades\Redirect
     */
    public function report($job_type, $city, $job)
    {
        $profile = Auth::user()->profile;
        $job = Job::where('id', intval(explode('-', $job)[0]))
            ->firstOrFail();

        $report = JobReport::where('profile_id', $profile->id)
            ->where('job_id', $job->id);
        if (!$report->count()) {
            $report = new JobReport;
            $report->profile()->associate($profile);
            $report->job()->associate($job);
            $report->save();
        }

        return Redirect::back();
    }

    /**
     * Retorna as vagas destaque categoria
     *
     * @param integer $category
     * @return Job
     */
    public static function featured_category($category)
    {
        return Job::where('featured', true)
            ->where('status', Job::APPROVED)
            ->where('estate_id', Session::get('estate'))
            ->where('category_id', $category)
            ->orderByRaw('RAND()')
            ->limit(Configuration::firstOrFail()->highlights_category);
    }

    /**
     * Retorna as vagas mais concorridas da categoria
     *
     * @param integer $category
     * @return Job
     */
    public static function busiest_category($category)
    {
        return Job::selectRaw("jobs.*, (select count(*) from job_candidates where job_candidates.job_id = jobs.id) as t")
            ->where('status', Job::APPROVED)
            ->where('estate_id', Session::get('estate'))
            ->where('category_id', $category)
            ->orderBy('t', 'desc')
            ->limit(Configuration::firstOrFail()->competitive_category);
    }

    /**
     * Retorna as vagas mais concorridas do mês
     *
     * @return Job
     */
    public static function busiest_month()
    {
        return Job::selectRaw("jobs.*, (select count(*) from job_candidates where job_candidates.job_id = jobs.id) as t")
            ->where('status', Job::APPROVED)
            ->where('estate_id', Session::get('estate'))
            ->where('created_at', '>=', Carbon::now()->subMonth()->startOfDay())
            ->orderBy('t', 'desc')
            ->limit(Configuration::firstOrFail()->busiest_month);
    }

    /**
     * Retorna as vagas mais concorridas da semana
     *
     * @return Job
     */
    public static function busiest_week()
    {
        return Job::selectRaw("jobs.*, (select count(*) from job_candidates where job_candidates.job_id = jobs.id) as t")
            ->where('status', Job::APPROVED)
            ->where('estate_id', Session::get('estate'))
            ->where('created_at', '>=', Carbon::now()->subWeek()->startOfDay())
            ->orderBy('t', 'desc')
            ->limit(Configuration::firstOrFail()->busiest_week);
    }

    /**
     * Retorna as vagas mais concorridas de hoje
     *
     * @return Job
     */
    public static function busiest_today()
    {
        return Job::selectRaw("jobs.*, (select count(*) from job_candidates where job_candidates.job_id = jobs.id) as t")
            ->where('status', Job::APPROVED)
            ->where('estate_id', Session::get('estate'))
            ->where('created_at', '>=', Carbon::now()->startOfDay())
            ->orderBy('t', 'desc')
            ->limit(Configuration::firstOrFail()->busiest_day);
    }

    /**
     * Retorna as vagas mais recentes
     *
     * @return Job
     */
    public static function recents()
    {
        return Job::where('status', Job::APPROVED)
            ->where('estate_id', Session::get('estate'))
            ->orderBy('created_at', 'desc')
            ->limit(Configuration::firstOrFail()->recent_jobs);
    }

    /**
     * Retorna as vagas destaques
     *
     * @return Job
     */
    public static function featured()
    {
        return Job::where('featured', true)
            ->where('status', Job::APPROVED)
            ->where('estate_id', Session::get('estate'))
            ->orderByRaw('RAND()')
            ->limit(Configuration::firstOrFail()->featured_jobs);
    }

    /**
     * Faz uma pesquisa pelas vagas
     *
     * @return Job
     */
    public function search()
    {
        $term = Input::get('term');
        $jobs = Job::where('position', 'like', '%'. $term .'%')
            ->where('status', Job::APPROVED)
            ->orderBy('created_at', 'desc');

        $this->set_context(array(
            'jobs' => $jobs,
            'term' => $term
        ));

        $stat = new SearchStatistic;
        $stat->keyword = $term;
        $stat->estate_id = Session::get('estate');
        $stat->save();

        return $this->view_make('job/search');
    }

    /**
     * Página de listagem de todas as vagas
     *
     * @param string $job_type
     * @param string $estate
     * @param string $city
     * @return Illuminate\Support\Facades\View
     */
    public function index($job_type, $estate, $city)
    {

        $estate = LocationEstate::where('id', Session::get('estate'))->firstOrFail();

        if ($city != '-') {
            $city = LocationCity::where('slug', $city)->firstOrFail();
        }

        if ($job_type != '-') {
            $job_type = JobType::where('slug', $job_type)->firstOrFail();
        }

        $jobs = Job::where('active', true)
            ->where('status', Job::APPROVED)
            ->where('estate_id', Session::get('estate'));

        if ($job_type != '-') {
            $jobs->where('type_id', $job_type->id);
        }

        if ($city != '-') {
            $jobs->where('city_id', $city->id);
        }

        $location = array(
            'estate_id' => Session::get('estate'),
            'city_id' => ($city != '-') ? $city->id : '-'
        );

        $this->set_context(array(
            'input' => (object) $location
        ));

        $this->set_json_context(array(
            'data' => (object) $location
        ));

        $this->set_context(array(
            'jobs' => $jobs->paginate(15),
            'estate_selected' => ($estate != '-') ? strtolower($estate->short) : '-',
            'city_selected' => ($city != '-') ? $city->slug : '-',
            'job_types' => JobType::all(),
            'categories' => Category::where('active', true)->orderBy('name')->get(),
        ));

        return $this->view_make('job/index');
    }

}
