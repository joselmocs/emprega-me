<?php


class BackendSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos_trabalho = array(
            array("Emprego", "emprego"),
            array("Estágio", "estagio"),
            array("Temporário", "temporario"),
            array("Trainne", "trainne"),
            array("Freelancer", "freelancer"),
            array("Vaga PcD", "pcd")
        );

        // Emprego – Estágio – Temporário  - Trainee – Freelancer - Vaga PcD

        foreach ($tipos_trabalho as $tipo) {
            $newJob = new JobType;
            $newJob->name = $tipo[0];
            $newJob->image = $tipo[1] .".png";
            $newJob->save();
        }
    }
}
