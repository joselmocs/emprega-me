/*!
 * core.js
 * Container de objetos singleton
 *
 * Joselmo Cardozo
 */

(function() {
    define([
        "libs/jsuri",
        "libs/jquery.maskedinput",
        "libs/jquery.maskmoney",
        "libs/jquery.isloading"
    ], function(jsuri) {
        var
            phonemask = function(input) {
                var $input = $(input);
                $input.focusout(function(){
                    var phone, element;
                    element = $(this);
                    element.unmask();
                    phone = element.val().replace(/\D/g, '');
                    if(phone.length > 10) {
                        element.mask("(99) 99999-999?9");
                    } else {
                        element.mask("(99) 9999-9999?9");
                    }
                }).trigger('focusout');
            },
            moneymask = function(input) {
                $(input).maskMoney({
                    prefix:'R$ ',
                    allowNegative: false,
                    thousands:'.',
                    decimal:',',
                    affixesStay: false
                });
            },
            loading = {
                show: function() {
                    $.isLoading({ text: '<img src="/images/loading.gif">' });
                },

                hide: function() {
                    $.isLoading("hide");
                }
            },
            singleton = function() {
                return {
                    uri: jsuri.parse(document.URL),
                    context: _context,
                    loading: loading,
                    phonemask: phonemask,
                    moneymask: moneymask
                }
            };
        return singleton();
    });
})();