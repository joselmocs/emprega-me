<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCompanyNames extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profile_names', function($table)
        {
            $table->increments('id');
            $table->string('name', 256);
            $table->integer('profile_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('profile_id')->references('id')->on('user_profile');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profile_names');
    }

}
