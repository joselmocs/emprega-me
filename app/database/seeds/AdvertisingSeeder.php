<?php


class AdvertisingSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $advertising_banners_types = array(
            array('Banner médio 272x90 (cabeçalho)', 'header_272x90'),
            array('Banner médio 272x90 (rodapé)', 'footer_272x90'),
            array('Banner mini 160x50', 'mini_160x50'),
        );

        foreach ($advertising_banners_types as $type) {
            $newPerm = new AdvertisingBannerType;
            $newPerm->name = $type[0];
            $newPerm->short = $type[1];
            $newPerm->save();
        }

        $advertising_adsense = array(
            array('Arranha-céu largo 600x160', 'large_600x160'),
            array('Arranha-céu largo 600x300', 'large_600x300'),
            array('Bloco de link 160x90', 'links_160x90'),
            array('Bloco de link 728x15', 'links_728x15'),
            array('Cabeçalho 728x90 (cabeçalho)', 'header_728x90'),
            array('Cabeçalho 728x90 (rodapé)', 'footer_728x90'),
            array('Cabeçalho grande 970x90 (cabeçalho)', 'header_970x90'),
            array('Cabeçalho grande 970x90 (rodapé)', 'footer_970x90'),
            array('Retângulo grande 336x280 (inicial)', 'home_rectangle_336x280'),
            array('Retângulo grande 336x280 (vaga)', 'job_rectangle_336x280'),
            array('Retângulo médio 300x250', 'rectangle_300x250')
        );

        foreach ($advertising_adsense as $adsense) {
            $newPerm = new AdvertisingAdsense;
            $newPerm->name = $adsense[0];
            $newPerm->short = $adsense[1];
            $newPerm->save();
        }
    }
}
