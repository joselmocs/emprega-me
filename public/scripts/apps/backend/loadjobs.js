/*!
 * apps.backend.loadjobs.js
 * Scripts responsáveis pela atualização e notificação de novas vagas para
 * aprovação.
 *
 * Joselmo Cardozo
 */

require([
    "helpers/core"
], function(core) {
    var
        initialize = function () {
            events();
        },
        events = function () {
            update_title();
            setInterval(check_not_viewed, 5000);
        },
        update_title = function () {
            var title = _title;
            if (_notviewed) {
                title = "#"+ _notviewed +" - "+ _title;
            }
            $("title").html(title);
        },
        play_sound = function () {
            $("#notification_ring")[0].play();
        },
        check_not_viewed = function () {
            $.ajax({
                type: 'get',
                url: '/backend/update',
                success: function(response) {
                    if (response.count > _notviewed) {
                        play_sound();
                    }
                    _notviewed = response.count;
                    update_title();
                }
            });
        };

    initialize();
});
