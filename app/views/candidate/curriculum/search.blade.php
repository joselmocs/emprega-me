@extends("base/template")

@section("site-title")
Resultado da busca - {{ Config::get('domain.site.title') }}
@endsection

@section('requirejs')
requirejs(["apps/user/search"]);
@endsection

@section("content")
@include("advertising/header")
<div class="page_template"><!--busca-->

<div style="margin: 0 0 20px 0"><!--div margin-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario">
<tr>
<td width="100%">
<a href="linkaqui"><img src="domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="linkaqui"><img src="domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="linkaqui"><img src="domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="linkaqui"><img src="domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="linkaqui"><img src="domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="linkaqui"><img src="domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
</td>
<td align="right">
<select name="gender_select">
<option value="-">Todos os generos</option>
@foreach(UserProfile::$VERBOSE_GENDER as $val => $gender)
<option value="{{ $val }}" @if ($context->sexo == $val) selected="selected" @endif>{{ $gender }}</option>
@endforeach
</select>
</td>
<td align="right" style="padding: 0 0 0 5px;">
<select name="city_select">
<option value="-">Todos as cidades</option>
@foreach($context->cidades as $city)
<option value="{{ $city->id }}" @if ($city->id == $context->cidade) selected="selected" @endif>{{ $city->name }}</option>
@endforeach
</select>
</td>
</tr>
</table>
</div><!--div margin-->

<div class="titulo_medio"><!--titulo_medio-->
Encontrados <span style="color: #999999;">({{ $context->users->count() }})</span> resultados para <span style="color: #999999; text-transform: lowercase;">({{ $context->term }})</span>
</div><!--titulo_medio-->

@if (!$context->users->count())
<span style="color: #666666; font-size: 12px;">Nenhum resultado encontrado.</span>
@endif

@foreach($context->users->get() as $i => $user)
<div class="@if ($i % 2) row-alt @else row @endif">
<span class="row-info">
<img src="/images/set_right.png" />
<a class="fancybox fancybox.iframe" title="{{ $user->fullname }}" href="{{ URL::action('CurriculumController@view', array($user->user->id)) }}" data-fancybox-height="800" data-fancybox-width="850">{{ $user->fullname }}</a>
<small><span>de </span>{{ $user->city->name }}/{{ $user->city->estate->short }}</small>
</span>

@if ($user->ultima_candidatura())
<span class="time_post">
<img src="/images/clock.png" title="Ultima candidatura"/>
<small title="{{ Carbon::parse($user->ultima_candidatura())->format('d \d\e F \d\e Y') }}">{{ Carbon::parse($user->ultima_candidatura())->format('d/m') }}</small>
</span>
@endif
</div>
@endforeach

</div><!--page_template-->

</div><!--container-->
@endsection