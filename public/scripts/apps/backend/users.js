/*!
 * apps.backend.users.js
 * Scripts responsáveis pela criação e edição de usuários.
 *
 * Joselmo Cardozo
 */

require([
    "helpers/core"
], function(core) {
    var
        initialize = function() {
            events();
        },
        events = function() {
            core.phonemask('input[name=phone]');

            $('a[name=remover]').unbind('click').click(function() {
                if (confirm('Deseja remover o usuário *'+ $(this).attr('data-name') +'*?')) {
                    window.location = core.context.urls.remove +'/'+ $(this).attr('data-id');
                }
            });

            $("input[name=password_change]").change(function() {
                if ($(this).is(':checked')) {
                    $(".tr-password").show();
                    $(".tr-password").find('input').attr('required', true);
                }
                else {
                    $(".tr-password").hide();
                    $(".tr-password").find('input').removeAttr('required');
                }
            });
        };

    initialize();
});
