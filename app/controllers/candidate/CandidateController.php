<?php

class CandidateController extends BaseController {

    /**
     * Exibe a lista de candidaturas do usuário
     *
     * @return \Illuminate\View\View
     */
    public function candidacies($view)
    {
        $candidacies = Auth::user()->profile->candidacies();

        switch ($view) {
            case '0':
                $start = Carbon::now()->startOfDay();
                $end = Carbon::now()->endOfDay();
                break;
            case '1':
                $start = Carbon::now()->subDays(1)->startOfDay();
                $end = Carbon::now()->subDays(1)->endOfDay();
                break;
            case '7':
                $start = Carbon::now()->subDays(7)->startOfDay();
                $end = Carbon::now()->endOfDay();
                break;
            case '30':
                $start = Carbon::now()->subDays(30)->startOfDay();
                $end = Carbon::now()->endOfDay();
                break;
        }

        if ($view != '-') {
            $candidacies
                ->whereBetween('created_at', array($start, $end));
        }

        $this->set_context(array(
            'view' => $view,
            'candidacies' => $candidacies->get()
        ));

        return $this->view_make('candidate/candidacies');
    }

    /**
     * Página de registro de novo usuário
     *
     * @return Illuminate\Support\Facades\View
     */
    public function register()
    {
        $this->set_json_context(array(
            'input' => (object) array(
                'city_id' => Input::get('city_id', ''),
                'estate_id' => Input::get('estate_id', Session::get('estate'))
            )
        ));

        $this->set_json_context(array(
            'data' => (object) array(
                'estate_id' => Input::get('estate_id', Session::get('estate')),
                'city_id' => Input::get('city_id'),
            )
        ));

        $this->set_context(array(
            'input' => (object) array(
                'email' => Input::get('email', Session::get('new_user_email', '')),
                'password' => Input::get('password', ''),
                'password_confirm' => Input::get('password', ''),
                'fullname' => Input::get('fullname', ''),
                'birthday' => Input::get('birthday'),
                'estate_id' => Input::get('estate_id', Session::get('estate')),
                'city_id' => Input::get('city_id'),
            )
        ));

        Session::remove('new_user_email');

        if (!Input::get('submit')) {
            return $this->view_make('candidate/register');
        }

        $validate = array(
            "email" => "required|email|unique:users",
            "password" => "required|min:3|max:30",
            "password_confirm" => "required|same:password",
            "fullname" => "required|min:2|max:256",
            "birthday" => "required",
            "estate_id" => "required|numeric",
            "city_id" => "required|numeric"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('candidate/register');
        }

        $user = new User;
        DB::transaction(function($user) use ($user) {
            $user->email = Input::get('email');
            $user->password = Input::get('password');
            $user->verified = 1;
            $user->save();
            $user->roles()->sync(array(Role::where('tag', '=', 'user')->firstOrFail()->id));

            $user->profile->city_id = Input::get('city_id');
            $user->profile->fullname = Input::get('fullname');
            $user->profile->birthday = Carbon::createFromFormat('d/m/Y', Input::get('birthday'))->startOfDay();
            $user->profile->save();
        });

        $message = View::make('user/email/new_account')
            ->with('site_name', Config::get('domain.site.name'))
            ->with('fullname', Input::get('fullname'))
            ->with('username', $user->username)
            ->with('salt', $user->salt)
            ->render();

        $campaign = new \JCS\Mail\Campaign();
        $campaign->addAddress(new \JCS\Mail\ArraySource(array(Input::get('email'))));

        $campaign
            ->setMessage("Ativação de Conta", $message, "html")
            ->setMode(\JCS\Mail\Models\Queue::MODE_SINGLE)
            ->save();
        $campaign->execute();

        return Redirect::action('CandidateController@register_complete');
    }

    /**
     * Exibe a página de conclusão de cadastro novo usuário.
     *
     * @return \Illuminate\View\View
     */
    public function register_complete()
    {
        return $this->view_make('candidate/register_complete');
    }

}
