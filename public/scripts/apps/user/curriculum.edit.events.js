/*!
 * apps.user.curriculum.edit.events.js
 * Scripts responsáveis pelos eventos do formulário da edição do currículo.
 *
 * Joselmo Cardozo
 */

(function() {
    define([
        "helpers/core",
        "libs/jquery.appear"
    ], function(core) {
        var
            initialize = function() {
                core.loading.show();
                var data = core.context.data;

                start_floating_bar();

                load_languages();
                load_salaries();
                load_cities(data.personal.estate_id, data.personal.city_id);

                fill.personal(data.personal);
                fill.formations(data.formations);
                fill.courses(data.courses);
                fill.languages(data.languages);
                fill.experiences(data.experiences);
                fill.desired_positions(data.positions);

                events();

                core.loading.hide();
            },

            start_floating_bar = function() {
                $(window).scroll(function() {
                    if ($(document).scrollTop() >= 330) {
                        $(".cadastrar_sleps").addClass('floating');
                    }
                    else {
                        $(".cadastrar_sleps").removeClass('floating');
                    }

                    $(".cadastrar_sleps div").removeClass('cadastrar_slep_active');

                    if ($('.personal_tables').is(':appeared')) {
                        $("#cadastrar_slep_1").addClass('cadastrar_slep_active');
                    }
                    else if ($('#div_formation').is(':appeared')) {
                        $("#cadastrar_slep_2").addClass('cadastrar_slep_active');
                    }
                    else if ($('#div_experience').is(':appeared')) {
                        $("#cadastrar_slep_3").addClass('cadastrar_slep_active');
                    }
                });
            },

            // Carrega os Idiomas
            load_languages = function() {
                $.each(core.context.languages, function(i, x) {
                    var $option = $("<option></option>").attr('value', x.id).html(x.language);
                    if (x.language == "Inglês") {
                        $option.attr("selected", "selected");
                    }
                    $("#select_languages").append($option);
                });
            },
            // Carrega os salários
            load_salaries = function() {
                $.each(core.context.salaries, function(i, x) {
                    var $option = $("<option></option>").attr('value', x.id).html(x.description);
                    $("#select_salaries").append($option);
                });
            },

            // Carrega as cidades do estado selecionado
            load_cities = function(estate_id, city_id, load) {

                $("select[name=city_id] option").remove();
                var $option = $("<option></option>").attr('value', '').html("Selecione");
                $("select[name=city_id]").append($option);

                if (!estate_id) {
                    return false;
                }

                if (load) core.loading.show();

                $.ajax({
                    type: 'post',
                    url: '/location/city/load_all_json',
                    data: {
                        _token: $("meta[name=csrf-token]").attr('content'),
                        estate_id: estate_id
                    },
                    error: function() {
                        alert("Ocorreu um erro ao carregar a lista de cidades, por favor contacte um administrador.");
                    },
                    success: function(response) {
                        if (response.success) {
                            $.each(response.cities, function(i, city) {
                                var $option = $("<option></option>").attr('value', city.id).html(city.name);
                                if (city.id == city_id) {
                                    $option.attr("selected", "selected");
                                }
                                $("select[name=city_id]").append($option);
                            });
                        }
                    },
                    complete: function() {
                        if (load) core.loading.hide();
                    }
                });
            },

            fill = {
                fragment: {
                    date: function(value, format) {
                        var date = value.split(' ')[0].split('-');

                        if (format == "d/m/Y") {
                            return date[2] +"/"+ date[1] +"/"+ date[0];
                        }

                        if (format == "m/Y") {
                            return date[1] +"/"+ date[0];
                        }

                        return '';
                    }
                },

                container: function($container, object) {
                    $.each(object, function(input, value) {

                        var $input = $container.find('[name='+ input +']');
                        if ($input.length) {

                            if ($input.is('input')) {
                                if ($input.attr('type') == "checkbox") {
                                    if (value == "1") {
                                        $input.attr('checked', 'checked');
                                    }
                                }
                                if (typeof($input.attr('date-time')) != "undefined") {
                                    value = fill.fragment.date(value, $input.attr('date-time'));
                                }
                            }

                            if ($input.is('select')) {

                                var $option = $input.find('option[value='+ value +']');
                                if ($option.length) {
                                    $option.attr('selected', 'selected');
                                }
                            }

                            $input.val(value);
                        }
                    });
                },

                // Preenche os dados pre-carregados para informações Pessoais e de Contato
                personal: function(object) {
                    $("textarea[name=notes]").html(object.notes);
                    if (object.special_needs) {
                        $(".special_needs_desc").show();
                        $("select[name=select_special_needs] option[value='1']").attr("selected", "selected");
                        $("select[name=special_needs] option[value='"+ object.special_needs +"']").attr("selected", "selected");
                    }

                    fill.container($(".personal_tables"), object);
                },

                // Preenche os dados pre-carregados para Formação Acadêmica
                formations: function(objects) {
                    $.each(objects, function(i, object) {
                        add_table_formation();

                        var $container = $("#container_formations").find('table:last-child');
                        fill.container($container, object);
                    });
                },

                // Preenche os dados pre-carregados para Cursos
                courses: function(objects) {
                    $.each(objects, function(i, object) {
                        add_table_course();

                        var $container = $("#container_courses").find('table:last-child');
                        fill.container($container, object);
                    });
                },

                // Preenche os dados pre-carregados para Idiomas
                languages: function(objects) {
                    $.each(objects, function(i, object) {
                        var $level = $("input[type=radio][name=language][value="+ object.level +"]");
                        var $language = $("select[id=select_languages] option[value="+ object.language_id +"]");
                        add_table_language(object.language_id, $language.html(), object.level, $level.attr('data-name'));
                    });
                },

                // Preenche os dados pre-carregados para Cargos Pretendidos
                desired_positions: function(objects) {
                    $.each(objects, function(i, object) {
                        var $position = $("select[id=select_salaries] option[value="+ object.salary_id +"]");
                        add_table_desired_position(object.position, object.salary_id, $position.html());
                    });
                },

                // Preenche os dados pre-carregados para Experiência Profissional
                experiences: function(objects) {
                    $.each(objects, function(i, object) {
                        add_table_experience();

                        var $container = $("#container_experiences").find('table:last-child');
                        fill.container($container, object);
                    });
                }
            },

            events = function() {
                core.phonemask('input[name=phone], input[name=mobile]');
                $('input[name=birthday]').mask("99/99/9999");

                $("#add_formation").unbind('click').click(function() {
                    add_table_formation();
                });

                $("#add_course").unbind('click').click(function() {
                    add_table_course();
                });

                $("#add_language").unbind('click').click(function() {
                    var $level = $("input[name=language]:checked");
                    if (!$level.val()) {
                        return false;
                    }
                    var $option = $("#select_languages option:selected");
                    add_table_language($option.attr('value'), $option.html(), $level.val(), $level.attr('data-name'));
                });

                $("#add_desired_position").unbind('click').click(function() {
                    var $position = $("input[name=desired_position]");
                    var position = $position.val().trim();
                    if (!position) {
                        return false;
                    }
                    var $salary = $("#select_salaries option:selected");
                    add_table_desired_position(position, $salary.val(), $salary.html());

                    $position.val('');
                    $("#select_salaries").find('option[value=1]').attr('selected', true);
                });

                $("#add_experience").unbind('click').click(function() {
                    add_table_experience();
                });

                $("select[name=estate_id]").change(function() {
                    load_cities($(this).val(), null, true);
                });

                $("[name=select_special_needs]").change(function() {
                    if ($(this).val() == "1") {
                        $(".special_needs_desc").show();
                    }
                    else {
                        $("select[name=special_needs] option").removeAttr('selected');
                        $("select[name=special_needs] option[value=0]").attr('selected', 'selected');
                        $(".special_needs_desc").hide();
                    }
                });
            },
            rem_element = function(e) {
                e.currentTarget.parentElement.parentElement.parentElement.parentElement.remove();
            },

            // Formation
            add_table_formation = function() {
                var $tpl = $($("#formation_tpl").html());
                $tpl.find("a[name=rem_formation]").click(function(e) {
                    rem_element(e);
                });
                $tpl.find('input.date_mask').mask("99/9999");
                $("#container_formations").append($tpl);
            },

            // Courses
            add_table_course = function() {
                var $tpl = $($("#course_tpl").html());
                $tpl.find("a[name=rem_course]").click(function(e) {
                    rem_element(e);
                });
                $tpl.find('input.date_mask').mask("99/9999");
                $("#container_courses").append($tpl);
            },

            add_table_language = function(id_lang, lang, id_level, level) {
                // Checa se o idioma já foi incluído e o substitui pela ultima seleção (esta)
                var $container = $("#container_languages");
                $.each($container.find("table"), function(idx, table) {
                    if ($(table).find("b[data-language="+ id_lang +"]").length) {
                        $(table).remove();
                    }
                });

                var $tpl = $($("#language_tpl").html());
                var $level = $tpl.find('span[data-name=level]');
                $level.html(level);
                $level.attr('data-level', id_level);

                var $lang = $tpl.find('b[data-name=language]');
                $lang.html(lang);
                $lang.attr('data-language', id_lang);

                $("#container_languages").append($tpl);
                $("a[name=rem_language]").click(function(e) {
                    rem_element(e);
                });
            },

            add_table_desired_position = function(position, id_salary, salary) {
                var $tpl = $($("#desired_position_tpl").html());

                var $salary = $tpl.find('span[data-name=salary]');
                $salary.html(salary);
                $salary.attr('data-salary', id_salary);

                $tpl.find('b[name=desired_position]').html(position);

                $("#container_desired_positions").append($tpl);
                $("a[name=rem_position]").click(function(e) {
                    rem_element(e);
                });
            },

            // Experiences
            add_table_experience = function() {
                var $tpl = $($("#experience_tpl").html());
                $tpl.find("a[name=rem_experience]").click(function(e) {
                    rem_element(e);
                });
                $tpl.find('input.date_mask').mask("99/9999");
                core.moneymask($tpl.find('input[name=salary]'));
                $("#container_experiences").append($tpl);
            };

        return {
            initialize: initialize
        }
    });
})();
