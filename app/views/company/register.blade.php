@extends("base/template")

@section("site-title")
Registro de nova empresa - {{ Config::get('domain.site.title') }}
@endsection

@section('requirejs')
requirejs(["apps/company/register"]);
@endsection

@section("content")

<div class="page_template page_template_links"><!--page_template-->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="auto 100%" valign="top"><!--coluna_1-->

<div class="titulo_grande"><!--titulo_grande-->
Cadastro de empresa
</div><!--titulo_grande-->

<form action="{{ URL::action('CompanyController@register') }}" method="post">
<input type="hidden" name="_token" value="{{  csrf_token() }}" />
<div style="margin: 0 0 20px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Login e senha:
</div><!--titulo_pequeno-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
<td align="left">E-mail:</td>
</tr>
<tr>
<td align="left">
<input type="email" style="width: 400px;" name="email" required="true" value="{{ $context->input->email }}"
@if ($context->field_errors && $context->field_errors->has('email')) class="error" @endif />
@if ($context->field_errors && $context->field_errors->has('email'))
<div class="error">{{ $context->field_errors->first('email') }}</div>
@endif
</td>
</tr>
<tr>
<td height="15"></td>
</tr>
<tr>
<td align="left">Senha:</td>
</tr>
<tr>
<td align="left">
<input type="password" style="width: 250px;" name="password" required="true"
@if ($context->field_errors && $context->field_errors->has('password')) class="error" @endif />
@if ($context->field_errors && $context->field_errors->has('password'))
<div class="error">{{ $context->field_errors->first('password') }}</div>
@endif
</td>
</tr>
<tr>
<td height="5"></td>
</tr>
<tr>
<td align="left">Confirmar senha:</td>
</tr>
<tr>
<td align="left">
<input type="password" style="width: 175px;" name="password_confirm" required="true"
@if ($context->field_errors && $context->field_errors->has('password_confirm')) class="error" @endif />
@if ($context->field_errors && $context->field_errors->has('password_confirm'))
<div class="error">{{ $context->field_errors->first('password_confirm') }}</div>
@endif
</td>
</tr>
</table>

</div><!--div margin-->



<div style="margin: 0 0 20px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Dados pessoais:
</div><!--titulo_pequeno-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
<td align="left">Seu nome:</td>
</tr>
<tr>
<td align="left">
<input type="text" style="width: 400px;" name="fullname" required="true" value="{{ $context->input->fullname }}"
@if ($context->field_errors && $context->field_errors->has('fullname')) class="error" @endif />
@if ($context->field_errors && $context->field_errors->has('fullname'))
<div class="error">{{ $context->field_errors->first('fullname') }}</div>
@endif
</td>
</tr>
<tr>
<td height="5"></td>
</tr>
<tr>
<td align="left">Profissão/função na empresa:</td>
</tr>
<tr>
<td align="left">
<input type="text" style="width: 250px;"/>
</td>
</tr>
</table>

</div><!--div margin-->



<div style="margin: 0 0 20px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Dados da empresa:
</div><!--titulo_pequeno-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
<td align="left">Nome da empresa:</td>
</tr>
<tr>
<td align="left">
<input type="text" style="width: 400px;" name="company" required="true" value="{{ $context->input->company }}"
@if ($context->field_errors && $context->field_errors->has('company')) class="error" @endif />
@if ($context->field_errors && $context->field_errors->has('company'))
<div class="error">{{ $context->field_errors->first('company') }}</div>
@endif
</td>
</tr>
<tr>
<td height="15"></td>
</tr>
<tr>
<td align="left">Estado:</td>
</tr>
<tr>
<td align="left">
<select name="estate_id" required>
<option value="">Selecione</option>
@foreach(LocationEstate::where('active', '=', 1)->get() as $estate)
<option value="{{ $estate->id }}" @if($context->input->estate_id == $estate->id) selected="selected" @endif>{{ $estate->name }}</option>
@endforeach
</select>
</td>
</tr>
<tr>
<td height="5"></td>
</tr>
<tr>
<td align="left">Cidade:</td>
</tr>
<tr>
<td align="left">
<select name="city_id" required>
<option value="">Selecione</option>
</select>
</td>
</tr>
<tr>
<td height="15"></td>
</tr>
<tr>
<td align="left">Telefone:</td>
</tr>
<tr>
<td align="left">
<input type="text" style="width: 300px;" name="phone" value="{{ $context->input->phone }}" required="true">
@if ($context->field_errors && $context->field_errors->has('phone'))
<div class="error">{{ $context->field_errors->first('phone') }}</div>
@endif
</td>
</tr>
<tr>
<td height="5"></td>
</tr>
<tr>
<td align="left">Website:</td>
</tr>
<tr>
<td align="left">

<table width="0%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<input type="text" style="width: 300px;" name="website" value="{{ $context->input->website }}" />
</td>
<td style="padding: 0 0 0 5px;"><small>(opicional)</small></td>
</tr>
</table>

</td>
</tr>
</table>

</div><!--div margin-->



<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
<td align="left">

<table width="0%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;">
<input type="checkbox" checked>
</td>
<td width="100%" height="25" align="left" style="font-size: 11px;">
Concordo com os <a class="fancybox fancybox.iframe" title="Recuperar senha" href="login_recuperar_senha.php" data-fancybox-height="auto" data-fancybox-width="420">Termos de serviço</a> e a <a class="fancybox fancybox.iframe" title="Recuperar senha" href="login_recuperar_senha.php" data-fancybox-height="auto" data-fancybox-width="420">Politica de privacidade</a>.
</td>
</tr>
</table>

</td>
</tr>
<tr>
<td height="15"><hr width="100%" style=" background: #EEEEEE; color: #EEEEEE; height:2px;"></td>
</tr>
<tr>
<td align="left">
<input type="submit" value="Cadastrar" name="submit" />
</td>
</tr>
</table>
</form>

</td><!--coluna_1-->

<td style="width: 15px;"></td>

<td width="300" valign="top"><!--coluna_2-->

Imagem promocional.

</td><!--coluna_2-->
</tr>
</table>
</div><!--page_template-->

</div><!--container-->
@endsection