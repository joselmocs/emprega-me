<?php


class BackendLinkController extends BaseBackendController {

    /**
     * Exibe a lista de links cadastrados.
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        $this->set_json_context(array(
            'urls' => array(
                'remove' => URL::action('BackendLinkController@remove'),
                'reorder' => URL::action('BackendLinkController@reorder')
            )
        ));

        $this->set_context(array(
            'links_header' => Link::where('position', '=', 1)->orderBy('order', 'asc')->get(),
            'links_footer' => Link::where('position', '=', 2)->orderBy('order', 'asc')->get()
        ));

        return $this->view_make('backend/link/index');
    }

    /**
     * Página de criação de links
     *
     * @return Illuminate\Support\Facades\View
     */
    public function register()
    {
        if (Request::isMethod('get')) {
            return $this->view_make('backend/link/new');
        }

        $link = new Link;
        $link->name = Input::get('name');
        $link->title = Input::get('title');
        $link->url = Input::get('url');
        $link->position = Input::get('position');
        $link->newpage = Input::get('newpage') == "1";
        $link->order = Link::getLastPosition(Input::get('position')) + 1;
        $link->save();

        return Redirect::action('BackendLinkController@index');
    }

    /**
     * Página de edição de links
     *
     * @param int $id
     * @return Illuminate\Support\Facades\View
     */
    public function edit($id)
    {
        if (Request::isMethod('get')) {
            $this->set_context(array(
                'link' => Link::find($id)
            ));

            return $this->view_make('backend/link/edit');
        }

        $link = Link::find($id);
        $link->name = Input::get('name');
        $link->title = Input::get('title');
        $link->url = Input::get('url');
        $link->position = Input::get('position');
        $link->newpage = Input::get('newpage') == "1";
        $link->save();

        return Redirect::action('BackendLinkController@index');
    }

    /**
     * Ativa um link
     *
     * @param int $id código para identificação do link
     * @return Illuminate\Support\Facades\View
     */
    public function active($id)
    {
        $city = Link::where('id', '=', $id)->firstOrFail();
        $city->active = true;
        $city->save();

        return Redirect::back();
    }

    /**
     * Desativa um link
     *
     * @param int $id código para identificação do link
     * @return Illuminate\Support\Facades\View
     */
    public function deactive($id)
    {
        $city = Link::where('id', '=', $id)->firstOrFail();
        $city->active = false;
        $city->save();

        return Redirect::back();
    }

    /**
     * Remove um link.
     *
     * @param $id int
     * @return Redirect
     */
    public function remove($id)
    {
        if ($id) {
            Link::find($id)->delete();
        }
        return Redirect::back();
    }

    /**
     * Ordena os links
     *
     * @return Illuminate\Support\Facades\Response
     */
    public function reorder()
    {
        $ordered = json_decode(Input::get('ordered', array()));
        foreach($ordered as $i => $ordered_link) {
            $link = Link::where('id', '=', $ordered_link)->firstOrFail();
            $link->order = $i + 1;
            $link->save();
        }
        return Response::json(array('success' => true));
    }
}
