@extends("backend/template")

@section("content")

<div class="page_template"><!--page_template-->

<div class="titulo_grande" style="text-transform: uppercase;"><!--titulo_grande-->
<img src="/images/backend/icone_publicidade.jpg"/> PUBLICIDADE
</div><!--titulo_grande-->

<div style="margin: 0 0 25px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Anúncios independentes
</div><!--titulo_pequeno-->

@foreach($context->banner_types as $i => $type)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="../images/set_right.png" title="Emprego"/><a href="{{ URL::action('BackendAdvertisingController@banners_list', array($type->short)) }}" title="{{ $type->name }}">{{ $type->name }}</a>
</span>

<div class="row_menu">
<a href="linkaqui"><img src="/images/backend/row_menu_power_on.png" title="Desligado"/></a>
<a href="linkaqui"><img src="/images/backend/row_menu_power_on.png" title="Ligado"/></a>
</div>
</div><!--row/row-alt-->
@endforeach

</div><!--div margin-->



<div style="margin: 0 0 25px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Google AdSense
</div><!--titulo_pequeno-->

@foreach($context->adsenses as $i => $adsense)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="../images/set_right.png" title="Emprego"/><a href="{{ URL::action('BackendAdvertisingController@adsense_view', array($adsense->short)) }}" title="{{ $adsense->name }}">{{ $adsense->name }}</a>
</span>

<div class="row_menu">
@if ($adsense->active)
<a href="{{ URL::action('BackendAdvertisingController@adsense_deactive', array($adsense->short)) }}" name="adsense_off"><img src="/images/backend/row_menu_power_on.png" title="Ligado"/></a>
@else
<a href="{{ URL::action('BackendAdvertisingController@adsense_active', array($adsense->short)) }}" name="adsense_on"><img src="/images/backend/row_menu_power_off.png" title="Desligado"/></a>
@endif
</div>

</div><!--row/row-alt-->
@endforeach

</div><!--div margin-->

</div><!--page_template-->

</div><!--container-->

@endsection