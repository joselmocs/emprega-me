<p>Olá {{ $fullname }},</p>

<p>Obrigado por se registar em {{ $site_name }}. Sua conta foi criada e deve ser ativada antes que você possa usá-lo.</p>

<p>Para ativar a conta clique no link abaixo ou copie e cole no seu navegador:<br />
{{ URL::action('UserController@verify', array($username, $salt)) }}</p>

<p>Cordialmente,<br />
Equipe {{ $site_name }}</p>
