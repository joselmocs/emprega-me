<?php

use Illuminate\Database\Migrations\Migration;


class CreateUserCoursesTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the user_courses table
        Schema::create('user_courses', function($table)
        {
            $table->increments('id');
            $table->integer('profile_id')->unsigned()->nullable();
            $table->string('course');
            $table->string('institution');
            $table->timestamp('started_at')->default('0000-00-00 00:00:00');
            $table->string('duration');
            $table->timestamps();

            $table->foreign('profile_id')->references('id')->on('user_profile');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop user_courses table
        Schema::dropIfExists('user_courses');
    }

}
