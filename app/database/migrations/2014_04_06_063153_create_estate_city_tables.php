<?php

use Illuminate\Database\Migrations\Migration;


class CreateEstateCityTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the location_estates table
        Schema::create('location_estates', function($table)
        {
            $table->increments('id');
            $table->string('name', 64);
            $table->string('slug', 64)->index();
            $table->string('short', 2);
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        // Create the location_cities table
        Schema::create('location_cities', function($table)
        {
            $table->increments('id');
            $table->string('name', 128)->nullable();
            $table->string('slug', 128)->index();
            $table->integer('estate_id')->unsigned()->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->foreign('estate_id')->references('id')->on('location_estates');
        });

        // Create the location_district table
        Schema::create('location_districts', function($table)
        {
            $table->increments('id');
            $table->string('name', 128)->nullable();
            $table->string('slug', 256)->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->foreign('city_id')->references('id')->on('location_cities');
        });

        Schema::table('user_profile', function($table)
        {
            $table->integer('city_id')->unsigned()->nullable()->after('marital_status');
            $table->foreign('city_id')->references('id')->on('location_cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_profile', function($table)
        {
            $table->dropForeign('user_profile_city_id_foreign');
            $table->dropColumn('city_id');
        });

        Schema::dropIfExists('location_districts');
        Schema::dropIfExists('location_cities');
        Schema::dropIfExists('location_estates');
    }
}
