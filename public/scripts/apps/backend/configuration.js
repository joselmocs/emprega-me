/*!
 * apps.backend.configuration.js
 * Scripts responsáveis pela página de configuração no backend.
 *
 * Joselmo Cardozo
 */

require([
    "helpers/core",
    "libs/jquery.sortable"
], function(core) {
    var
        initialize = function() {
            events();
        },
        events = function() {

            $("input[name=save_order]").unbind('click').click(function() {
                core.loading.show();
                $("#alert_success").hide();

                var highlighted = [];
                $.each($("#highlighted li"), function(i, li) {
                    highlighted.push($(li).attr('data-category'));
                });

                $.ajax({
                    type: 'post',
                    url: core.context.urls.reorder,
                    data: {
                        _token: $("meta[name=csrf-token]").attr('content'),
                        'highlighted': JSON.stringify(highlighted)
                    },
                    error: function() {
                        alert("Ocorreu um erro ao salvar as categorias destaques, por favor contacte um administrador.");
                    },
                    success: function(response) {
                        if (response.success) {
                            $("#alert_success").show(300);
                        }
                    },
                    complete: function() {
                        core.loading.hide();
                    }
                });

            });

            $(".highlighted").sortable({
                onDrop: function (item, targetContainer, _super) {
                    var clonedItem = $('<li/>').css({height: 0});
                    item.before(clonedItem);
                    clonedItem.animate({'height': item.height()});

                    item.animate(clonedItem.position(), function  () {
                        clonedItem.detach();
                        _super(item)
                    })
                }
            });
        };

    initialize();
});