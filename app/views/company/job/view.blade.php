<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>{{ Config::get('domain.site.name') }}</title>
    <link href="/styles/screen.css" rel="stylesheet" type="text/css"/>
    <link href="/domains/{{ Config::get('domain.folder') }}/styles/screen_colors.css" rel="stylesheet" type="text/css"/>
</head>

<body>

<div id="ver_vaga"><!--vaga-->

    <div class="info_estatisticas" style="position: relative; right: 5px;"><!--info_estatisticas-->
        Total de candidatos: <b>{{ $context->job->candidacies->count() }}</b>
    </div><!--info_estatisticas-->


    <div class="titulo_medio" style="text-transform: uppercase;"><!--titulo_medio-->
        {{ $context->job->position }} <img style="position: relative; top: 2px;" src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $context->job->type->image }}" title="{{ $context->job->type->name }}"/>
    </div><!--titulo_medio-->


    <div id="ver_vaga_dados" class="page_template_links"><!--vaga_dados-->
        Por {{ $context->job->company }} em {{ $context->job->city->name }}/{{ $context->job->estate->short }}@if ($context->job->district), bairro <a href="" title="{{ $context->job->district->name }}">{{ $context->job->district->name }}</a>@endif.
    </div><!--vaga_dados-->


    <div id="ver_vaga_descricao" class="page_template_links" ><!--vaga_descricao-->
        {{ $context->job->description }}
    </div><!--vaga_descricao-->


    <div style="margin: 25px 0 0 0" class="moldura_cinza formulario"><!--div margin-->
        <table width="100%" border="0" cellspacing="2" cellpadding="0">
            <tr>
                <td style="font-size:12px;">Link:</td>
                <td width="100%">
                    <input type="text" style="width: 400px;" value="http://bhjobs.com.br/vaga-de/grupo-selpe/"/>
                </td>
            </tr>
        </table>
    </div><!--div margin-->

</div><!--vaga-->

</body>
</html>