<?php


class JobCandidateController extends BaseController {

    /**
     * Retorna a quantidade de candidaturas hoje
     *
     * @param integer $estate
     * @return JobCandidate
     */
    public static function today($estate)
    {
        $candidates = JobCandidate::select('job_candidates.*');
        if ($estate > 0) {
            $candidates
                ->join('jobs', 'jobs.id', '=', 'job_candidates.job_id')
                ->where('jobs.estate_id', '=', $estate);
        }

        $candidates
            ->whereBetween('job_candidates.created_at',
                array(
                    Carbon::now()->startOfDay(),
                    Carbon::now()->endOfDay()
                )
            );

        return $candidates->count();
    }

    /**
     * Retorna a quantidade de candidaturas ontem
     *
     * @param integer $estate
     * @return JobCandidate
     */
    public static function yesterday($estate)
    {
        $candidates = JobCandidate::select('job_candidates.*');
        if ($estate > 0) {
            $candidates
                ->join('jobs', 'jobs.id', '=', 'job_candidates.job_id')
                ->where('jobs.estate_id', '=', $estate);
        }

        $candidates
            ->whereBetween('job_candidates.created_at',
                array(
                    Carbon::now()->subDay()->startOfDay(),
                    Carbon::now()->subDay()->endOfDay()
                )
            );

        return $candidates->count();
    }

    /**
     * Retorna a quantidade de candidaturas da semana
     *
     * @param integer $estate
     * @return JobCandidate
     */
    public static function week($estate)
    {
        $candidates = JobCandidate::select('job_candidates.*');
        if ($estate > 0) {
            $candidates
                ->join('jobs', 'jobs.id', '=', 'job_candidates.job_id')
                ->where('jobs.estate_id', '=', $estate);
        }

        $candidates
            ->whereBetween('job_candidates.created_at',
                array(
                    Carbon::now()->subDays(6)->startOfDay(),
                    Carbon::now()->endOfDay()
                )
            );

        return $candidates->count();
    }

    /**
     * Retorna a quantidade de candidaturas da semana
     *
     * @param integer $estate
     * @return JobCandidate
     */
    public static function month($estate)
    {
        $candidates = JobCandidate::select('job_candidates.*');
        if ($estate > 0) {
            $candidates
                ->join('jobs', 'jobs.id', '=', 'job_candidates.job_id')
                ->where('jobs.estate_id', '=', $estate);
        }

        $candidates
            ->whereBetween('job_candidates.created_at',
                array(
                    Carbon::now()->subMonth()->startOfDay(),
                    Carbon::now()->endOfDay()
                )
            );

        return $candidates->count();
    }
}
