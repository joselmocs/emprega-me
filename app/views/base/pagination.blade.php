@if ($paginator->getLastPage() > 1)
<?php $previousPage = ($paginator->getCurrentPage() > 1) ? $paginator->getCurrentPage() - 1 : 1; ?>
<ul class="ui pagination menu">
    @if ($paginator->getCurrentPage() > 1)
    <li><a href="{{ $paginator->getUrl($previousPage) }}" title="Página anterior">Anterior</a></li>
    @endif
    @for ($i = 1; $i <= $paginator->getLastPage(); $i++)
    <li><a href="{{ $paginator->getUrl($i) }}" title="Página {{ $i }}" @if ($paginator->getCurrentPage() == $i) class="active" @endif >{{ $i }}</a></li>
    @endfor
    @if ($paginator->getCurrentPage() != $paginator->getLastPage())
    <li><a href="{{ $paginator->getUrl($paginator->getCurrentPage()+1) }}" title="Próxima página">Próxima</a></li>
    @endif
</ul>
@endif