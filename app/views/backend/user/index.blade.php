@extends("backend/template")

@section("requirejs")
requirejs(["apps/backend/users"]);
@endsection

@section("content")
<div class="page_template"><!--page_template-->

<div class="botao">
<ul><li><a href="{{ URL::action('BackendUserController@register') }}">Adicionar usuário</a></li></ul>
</div>

<div class="titulo_grande" style="text-transform: uppercase;"><!--titulo_grande-->
<img src="/images/backend/icone_usuarios.jpg"/> USUÁRIOS
</div><!--titulo_medio-->

@foreach($context->users as $i => $user)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<div id="row_1"><!--row_1-->
<span class="row-info"><img src="/images/set_right.png" /><a href="{{ URL::action('BackendUserController@register', array($user->id)) }}">{{ $user->profile->fullname }}</a> <small><span>- Cargo: </span>{{ $user->roles()->firstOrFail()->name }}<span></small></span>
<div class="row_menu">
@if (!$user->disabled)
<a href="{{ URL::action('BackendUserController@deactive', array($user->id)) }}"><img src="/images/backend/row_menu_power_on.png" title="Ligado"/></a>
@else
<a href="{{ URL::action('BackendUserController@active', array($user->id)) }}"><img src="/images/backend/row_menu_power_off.png" title="Desligado"/></a>
@endif
</div>
<div class="row_menu">
<a href="{{ URL::action('BackendUserController@register', array($user->id)) }}"><img src="/images/backend/row_menu_editar.png" title="Editar"/></a>
<a href="" name="remover" data-id="{{ $user->id }}" data-name="{{ $user->profile->fullname }}"><img src="/images/backend/row_menu_remover.png" title="Remover"/></a>
</div>
</div><!--row_1-->
<div id="row_2"><!--row_2-->
<div class="row_2-info">
Login/E-mail: <span>{{ $user->email }}</span>
</div>
</div><!--row_2-->
</div><!--row/row-alt-->
@endforeach

</div><!--page_template-->

</div><!--container-->
@endsection
