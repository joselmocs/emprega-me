@extends("backend/template")

@section("content")
<div id="publicidade_editar_banner_adicionar-editar"><!--publicidade_editar_banner_adicionar-editar-->

    <div class="titulo_pequeno"><!--titulo_medio-->
        Editar Banner:
    </div><!--titulo_medio-->

    <form action="{{ URL::action('BackendAdvertisingController@banners_save') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="id" value="{{ $context->banner->id }}" />
        <input type="hidden" name="type" value="{{ $context->banner->type->short }}" />
        <table width="100%" border="0" cellspacing="2" cellpadding="0" class="moldura_cinza formulario">
            <tr>
                <td align="left">Nome:</td>
            </tr>
            <tr>
                <td align="left">
                    <input type="text" style="width: 362px;" name="name" required="required" value="{{ $context->banner->name }}" />
                </td>
            </tr>
            <tr>
                <td height="5" align="left"></td>
            </tr>
            <tr>
                <td align="left">URL:</td>
            </tr>
            <tr>
                <td align="left">
                    <input type="text" style="width: 250px;" name="url" required="required" value="{{ $context->banner->url }}" />
                </td>
            </tr>
            <tr>
                <td align="left">Imagem:</td>
            </tr>
            <tr>
                <td align="left">
                    <input type="text" style="width: 250px;" name="image" required="required" value="{{ $context->banner->image }}" />
                </td>
            </tr>
            <tr>
                <td align="left">Limite de visualizações:</td>
            </tr>
            <tr>
                <td align="left">
                    <input type="text" style="width: 250px;" name="max_views" required="required" value="{{ $context->banner->max_views }}" />
                    <small>0 = Sem limite de visualizações</small>
                </td>
            </tr>
            <tr>
                <td align="left">Ativo:</td>
            </tr>
            <tr>
                <td align="left">
                    <select name="active" required="required">
                        <option value="0" @if (!$context->banner->active) selected="selected" @endif>Não</option>
                        <option value="1" @if ($context->banner->active) selected="selected" @endif>Sim</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td align="left">Domínio:</td>
            </tr>
            <tr>
                <td align="left">
                    <select name="domain">
                        <option value="" @if ($context->banner->domain == "") selected="selected" @endif>Todos</option>
                        <option value="empregame" @if ($context->banner->domain == "empregame") selected="selected" @endif>Emprega-me</option>
                        <option value="bhjobs" @if ($context->banner->domain == "bhjobs") selected="selected" @endif>BHJobs</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td>
                    <input type="submit" value="Editar" />
                </td>
            </tr>
        </table>
    </form>

</div><!--publicidade_editar_banner_adicionar-editar-->
@endsection
