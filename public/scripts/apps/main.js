/*!
 * apps.main.js
 * Inicialização de plugins
 *
 * Joselmo Cardozo
 */

define([], function() {
    // Facebook
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&appId=345728168772643&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // Fancybox
    $(".fancybox")
        .attr('rel', 'gallery')
        .fancybox({
            type: 'iframe',
            arrows : false,
            autoSize : true,
            beforeLoad : function() {
                this.width  = parseInt(this.element.data('fancybox-width'));
                this.height = parseInt(this.element.data('fancybox-height'));
            }
        });

    // Adicionar aos favoritos
    $('a[name=bookmarks]').click(function() {
        var url = $("meta[property='og:url']").attr('content');
        var title = $("meta[property='og:title']").attr('content');

        if (window.sidebar) {
            window.sidebar.addPanel(title, url, "");
        }
        else if(document.all) {
            window.external.AddFavorite(url, title);
        }
        else if(window.opera && window.print) {
            alert('Pressione Ctrl+D para adicionar aos favoritos (Command+D para macs)');
        }
        else if(window.chrome){
            alert('Pressione Ctrl+D para adicionar aos favoritos (Command+D para macs)');
        }
    });
});