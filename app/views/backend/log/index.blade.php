@extends("backend/template")

@section("content")

<div id="error_logs" class="page_template"><!--error_logs-->

    <div class="titulo_medio"><!--titulo_medio-->
        <img src="/images/backend/icone_error_logs.jpg"/> ERROR LOGS
    </div><!--titulo_medio-->

    <div style="margin: 0 0 25px 0" class="moldura_cinza formulario"><!--div margin-->
        <table width="100%" border="0" cellspacing="2" cellpadding="0">
            <tr>
                <td>
                    <textarea style="height: 350px; width: 1052px;">{{ $context->log }}</textarea>
                </td>
            </tr>
        </table>
    </div><!--div margin-->

    <div class="moldura_cinza formulario">
        <table width="100%" border="0" cellspacing="2" cellpadding="0">
            <tr>
                <td align="left">
                    <form action="{{ URL::action('BackendLogController@clear') }}" method="get">
                        <input type="submit" value="Limpar" />
                    </form>
                </td>
            </tr>
        </table>
    </div>

</div><!--error_logs-->

@endsection