<div id="div_personal">
<div class="page_template page_template_links"><!--page_template-->

<div class="curriculo_sleps"><!--cadastro_editar-->
<li><a href="#" title="Experiência profissional">Dados pessoais</a></li>
<li><a href="#" title="Formação acadêmica">Formação acadêmica</a></li>
<li><a href="#" title="Formação acadêmica">Experiência profissional</a></li>
</div><!--cadastro_editar-->  

<div class="titulo_grande"><!--titulo_medio-->
Dados pessoais
</div><!--titulo_medio-->

<div style="margin: 0 0 25px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Envie sua foto:
</div><!--titulo_pequeno-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
<td align="left">
<input type="file"/>
</td>
</tr>
<tr>
<td height="15"><hr width="100%" style=" background: #EEEEEE; color: #EEEEEE; height:2px;"></td>
</tr>
<tr>
<td align="left">

▪ Escolha uma foto com perfil sério e profissional.
<br/ >
▪ Prefira uma imagem de boa qualidade.
<br/ >
▪ Selecione apenas fotos com as extensões .jpg, .gif, outros formatos não serão aceitos.
<br/ >
▪ Não nos responsabilizamos pelo conteúdo do arquivo.

</td>
</tr>
</table>

</div><!--div margin-->



<div style="margin: 0 0 20px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Dados pessoais:
</div><!--titulo_pequeno-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
<td align="left">Nome completo:</td>
</tr>
<tr>
<td align="left">
<input type="text" style="width: 400px;" name="fullname" required="true" value="" />
</td>
</tr>
<tr>
<td height="5"></td>
</tr>
<tr>
<td align="left">Data de nascimento:</td>
</tr>
<tr>
<td align="left">
<input type="text" style="width: 20px;" name="birthday" required="true" value="" disabled="disabled" date-time="d/m/Y" />
</td>
</tr>
<tr>
<td height="15"><hr width="100%" style=" background: #EEEEEE; color: #EEEEEE; height:2px;"></td>
</tr>
<tr>
<td align="left">Genero:</td>
</tr>
<tr>
<td align="left">
<select name="gender" disabled="disabled" required>
<option value="">Selecione</option>
@foreach(UserProfile::$VERBOSE_GENDER as $i => $gender)
<option value="{{ $i }}">{{ $gender }}</option>
@endforeach
</select>
</td>
</tr>
<tr>
<td height="5"></td>
</tr>
<tr>
<td align="left">Estado civil:</td>
</tr>
<tr>
<td align="left">
<select name="marital_status" required>
@foreach(UserProfile::$VERBOSE_MARITAL as $i => $status)
<option value="{{ $i }}">{{ $status }}</option>
@endforeach
</select>
</td>
</tr>
<tr>
<td height="15"><hr width="100%" style=" background: #EEEEEE; color: #EEEEEE; height:2px;"></td>
</tr>
<tr>
<td align="left">Estado:</td>
</tr>
<tr>
<td align="left">
<select name="estate_id" required>
<option value="">Selecione</option>
@foreach(LocationEstate::where('active', '=', 1)->get() as $estate)
<option value="{{ $estate->id }}">{{ $estate->name }}</option>
@endforeach
</select>
</td>
</tr>
<tr>
<td height="5"></td>
</tr>
<tr>
<td align="left">Cidade:</td>
</tr>
<tr>
<td align="left">
<select name="city_id" required></select>
</td>
</tr>
<tr>
  <td height="15"><hr width="100%" style=" background: #EEEEEE; color: #EEEEEE; height:2px;"></td>
</tr>
<tr>
<td align="left">Pessoa com deficiencia:</td>
</tr>
<tr>
<td align="left">
<select name="select_special_needs" required>
<option value="0">Não</option>
<option value="1">Sim</option>
</select>

<select name="special_needs" required>
@foreach(UserProfile::$VERBOSE_SPECIAL_NEEDS as $i => $need)
<option value="{{ $i }}">{{ $need }}</option>
@endforeach
</select>
</td>
</tr>
</table>

</div><!--div margin-->



<div style="margin: 0 0 20px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Dados complementares:
</div><!--titulo_pequeno-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
  <td align="left">Carteira de habilitação:</td>
</tr>
<tr>
<td align="left">

<table width="0%" border="0" cellspacing="0" cellpadding="4">
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;"><input type="checkbox" name="cnh_a" ></td>
<td width="100%" height="25" align="left">Categoria A</td>
</tr>
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;"><input type="checkbox" name="cnh_b"></td>
<td width="100%" height="25" align="left">Categoria B</td>
</tr>
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;"><input type="checkbox" name="cnh_c"></td>
<td width="100%" height="25" align="left">Categoria C</td>
</tr>
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;"><input type="checkbox" name="cnh_d"></td>
<td width="100%" height="25" align="left">Categoria D</td>
</tr>
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;"><input type="checkbox" name="cnh_e"></td>
<td width="100%" height="25" align="left">Categoria E</td>
</tr>
<tr>
<td colspan="2"><a href="#1" title="Todas as categorias">Mostrar mais</a></td>
</tr>
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;"><input type="checkbox" name="cnh_acc"></td>
<td width="100%" height="25" align="left">Categoria ACC</td>
</tr>
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;"><input type="checkbox" name="cnh_moto-casa"></td>
<td width="100%" height="25" align="left">Categoria Motor-casa</td>
</tr>
</table>

</td>
</tr>
<tr>
<td height="15"><hr width="100%" style=" background: #EEEEEE; color: #EEEEEE; height:2px;"></td>
</tr>
<tr>
<td align="left">Possui veículo:</td>
</tr>
<tr>
<td align="left">

<table width="0%" border="0" cellspacing="0" cellpadding="4">
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;"><input type="checkbox" name="auto_motorcycle"></td>
<td width="100%" height="25" align="left">Moto</td>
</tr>
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;"><input type="checkbox" name="auto_car"></td>
<td width="100%" height="25" align="left">Carro</td>
</tr>
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;"><input type="checkbox" name="auto_bus"></td>
<td width="100%" height="25" align="left">Ônibus</td>
</tr>
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;"><input type="checkbox" name="auto_truck"></td>
<td width="100%" height="25" align="left">Caminhão</td>
</tr>
</table>

</td>
</tr>
<tr>
<td height="15"><hr width="100%" style=" background: #EEEEEE; color: #EEEEEE; height:2px;"></td>
</tr>
<tr>
<td align="left">Disponivel para viagens:</td>
</tr>
<tr>
<td align="left">
<select name="travel_availability" required>
<option value="0">Não</option>
<option value="1" selected="selected">Sim</option>
</select>
</td>
</tr>
<tr>
<td height="5"></td>
</tr>
<tr>
<td align="left">Morar em outra cidade:</td>
</tr>
<tr>
<td align="left">
<select name="live_another_city_availability" required>
<option value="0" selected="selected">Não</option>
<option value="1">Sim</option>
</select>
</td>
</tr>
</table>

</div><!--div margin-->



<div style="margin: 0 0 20px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Formas de contato:
</div><!--titulo_pequeno-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
<td align="left">Atenção no preenchimento dos campos abaixo, pois é através deles que as das empresas entrarão em contato com você.</td>
</tr>
<tr>
<td height="15" align="left"><hr width="100%" style=" background: #EEEEEE; color: #EEEEEE; height:2px;"></td>
</tr>
<tr>
<td align="left">E-mail:</td>
</tr>
<tr>
<td align="left">
<input type="text" disabled="disabled" style="width: 400px;" value="emaildocadastro@aqui.com"/>
</td>
</tr>
<tr>
<td height="15"></td>
</tr>
<tr>
<td align="left">Telefone residencial:</td>
</tr>
<tr>
<td align="left"><input type="text" style="width: 250px;" name="phone" required="true" value="" /></td>
</tr>
<tr>
<td height="5"></td>
</tr>
<tr>
<td align="left">Telefone celular:</td>
</tr>
<tr>
<td align="left">
<input type="text" style="width: 200px;" name="mobile" required="true" value="" />
</td>
</tr>
</table>

</div><!--div margin-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
<td align="left">

<table width="0%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;">
<input type="checkbox" checked>
</td>
<td width="100%" height="25" align="left" style="font-size: 11px;">
Permitir que empresas/recrutadores busquem meu currículo.
</td>
</tr>
</table>

</td>
</tr>
<tr>
<td height="15"><hr width="100%" style=" background: #EEEEEE; color: #EEEEEE; height:2px;"></td>
</tr>
<tr>
<td align="left">
<input type="submit" value="Cadastrar"/>
</td>
</tr>
</table>
</div><!--div margin-->

</div>
</div><!--container-->