<?php


class Salary extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'salary';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'description');

}