<div id="div_experience">
<div style="margin: 0 0 25px 0"><!--div margin-->

    <div class="titulo_pequeno"><!--titulo_medio-->
        Objetivo:
    </div><!--titulo_medio-->

    <table width="100%" border="0" cellspacing="2" cellpadding="0" class="formulario moldura_cinza">
        <tr>
            <td width="160" align="right">Função/cargo pretendido:</td>
            <td align="left">

                <table width="0%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="left">
                            <input type="text" style="width: 200px;" name="desired_position" />
                        </td>
                        <td width="20"></td>
                        <td align="left" style="padding: 0 2px 0 0;">Pretensão salarial:</td>
                        <td align="left">
                            <select id="select_salaries"></select>
                        </td>
                        <td width="20"></td>
                        <td align="left">
                            <input type="button" style="font-size: 12px;" value="Incluir pretensão" id="add_desired_position" />
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
        <tr>
            <td height="10" colspan="2" align="center"><hr width="100%" noshade style="border: 1px solid #E5E5E5;"></td>
        </tr>
        <tr>
            <td colspan="2" align="left">

                <div id="container_desired_positions"></div>

                <script type="text/template" id="desired_position_tpl">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-size: 11px;" class="position_tables">
                        <tr>
                            <td width="0%" height="20" style="padding: 0 5px 0 5px;">
                                <a href="javascript:void(0);" name="rem_position"><img src="/images/remover_9x9.png" /></a>
                            </td>
                            <td width="100%" height="20">
                                <b name="desired_position"></b> - Pretenção salárial: <span data-name="salary" data-salary=""></span>
                            </td>
                        </tr>
                    </table>
                </script>
            </td>
        </tr>
    </table>

</div><!--div margin-->



<div style="margin: 0 0 25px 0"><!--div margin-->

    <div class="titulo_pequeno"><!--titulo_medio-->
        Experiência profissional:
    </div><!--titulo_medio-->

    <div id="container_experiences"></div>

    <script type="text/template" id="experience_tpl">
        <table width="100%" border="0" cellspacing="2" cellpadding="0" class="formulario moldura_cinza page_template_links experience_tables">
            <tr>
                <td width="160" align="right">Empresa:</td>
                <td align="left">
                    <input type="text" style="width: 200px;" name="company" />
                </td>
            </tr>
            <tr>
                <td width="160" align="right">Cargo/função:</td>
                <td align="left">
                    <input type="text" style="width: 200px;" name="position" />
                </td>
            </tr>
            <tr>
                <td width="160" align="right">Salário final:</td>
                <td align="left">
                    <input type="text" style="width: 100px;" name="salary" />
                </td>
            </tr>
            <tr>
                <td width="160" align="right">Mês/ano de admissão:</td>
                <td align="left">

                    <table width="0%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <input type="text" style="width: 75px;" class="date_mask" name="started_at" date-time="m/Y" />
                            </td>
                            <td width="20"></td>
                            <td style="padding: 0 2px 0 0;">Mês/ano de saída:</td>
                            <td>
                                <input type="text" style="width: 75px;" class="date_mask" name="ended_at" date-time="m/Y" />
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td height="5" colspan="2"></td>
            </tr>
            <tr>
                <td align="right"></td>
                <td align="left">
                    <a href="javascript:void(0);" title="Remover experiência" name="rem_experience"><img src="/images/remover_9x9.png"> Remover experiência</a>
                </td>
            </tr>
        </table>
    </script>
</div><!--div margin-->

<div class="formulario moldura_cinza page_template_links " style="margin-bottom: 20px;"><!--moldura_cinza-->
    <table width="100%" border="0" cellspacing="2" cellpadding="0">
        <tbody><tr>
            <td width="160" align="right"><img src="/images/plus_16x16.png"/></td>
            <td align="left">
                <a href="javascript:void(0);" title="Adicionar experiência" id="add_experience"><b>Adicionar experiência</b></a>
            </td>
        </tr>
        </tbody>
    </table>
</div>


<div style="margin: 0 0 25px 0"><!--div margin-->

    <div class="titulo_pequeno"><!--titulo_medio-->
        Observações:
    </div><!--titulo_medio-->

    <table width="100%" border="0" cellspacing="2" cellpadding="0" class="formulario moldura_cinza">
        <tr>
            <td align="left">Faça um resumo dos objetivos, habilidades e qualidades.</td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <textarea style="height: 100px; width: 600px;" name="notes"></textarea>
            </td>
        </tr>
    </table>

</div><!--div margin-->
</div>
