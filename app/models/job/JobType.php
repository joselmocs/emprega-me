<?php

use Illuminate\Support\Str;


class JobType extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'job_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'name', 'image');

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::saving(function($model) {
            $model->slug = STR::slug($model->name);
        });
    }

}