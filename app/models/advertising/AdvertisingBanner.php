<?php


class AdvertisingBanner extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'advertising_banners';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'name', 'url', 'image', 'views', 'max_views', 'active', 'notes');

    /**
     * Retorna o tipo do banner
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('AdvertisingBannerType', 'type_id');
    }
}