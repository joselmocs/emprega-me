<?php


class UserGroupsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $grupos = array(
            array('Administrador', 'sys_admin', 10),
            array('Gerente', 'manager', 9),
            array('Moderador', 'moderator', 3),
            array('Empresa', 'company', 2),
            array('Usuário', 'user', 1)
        );

        foreach ($grupos as $grupo) {
            DB::table('user_roles')->insert(array(
                'name' => $grupo[0],
                'tag' => $grupo[1],
                'level' => $grupo[2],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ));
        }
    }
}
