<?php
use JCS\Mail\Models\SmtpServer;


class IndexController extends BaseController {

    /**
     * Action inicial que verifica se é ou não um subsite que está sendo requisitado.
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        // Estado para filtro de vagas ainda não selecionado
        if (Input::get('estate', null) !== null) {
            // Checamos se o estado que o usuário selecionou existe.
            try {
                LocationEstate::where('active', true)->where('id', Input::get('estate'))->firstOrFail();
                Session::set('estate', Input::get('estate'));
            }
            catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                Session::forget('estate');
            }

            // Recarregamos a página para re-checagem.
            return Redirect::action('IndexController@index');
        }

        // Se não existir a sessão do estado selecionado, é exibido a index para seleção.
        if (Session::get('estate', null) == null) {
            return $this->view_make('base/index');
        }

        // Checamos se o estado que está na sessão existe e está ativo
        try {
            LocationEstate::where('active', true)->where('id', Session::get('estate'))->firstOrFail();
        }
        catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            Session::forget('estate');

            // Recarregamos a página para re-checagem.
            return Redirect::action('IndexController@index');
        }

        $recents = JobController::recents()->get();
        $featured = JobController::featured()->get();
        $busiest_month = JobController::busiest_month()->get();
        $busiest_week = JobController::busiest_week()->get();
        $busiest_today = JobController::busiest_today()->get();

        $this->set_context(array(
            'recents' => $recents,
            'featured' => $featured,
            'busiest_month' => $busiest_month,
            'busiest_week' => $busiest_week,
            'busiest_today' => $busiest_today
        ));

        return $this->view_make('base/home');
    }

}
