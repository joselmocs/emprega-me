<?php


class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        DB::transaction(function()
        {
            $this->call('SalariesSeeder');
            $this->call('LanguagesSeeder');
            $this->call('AdvertisingSeeder');

            $this->call('UserGroupsSeeder');
            $this->call('UserPermissionsSeeder');
            $this->call('UserSeeder');

            $this->call('CityEstateSeeder');

            $this->call('BackendSeeder');

            // Seeders com conteúdo para testes em desenvolvimento
            if (Config::get('app.debug')) {
                $this->call('TestSeeder');
            }
        });
    }
}
