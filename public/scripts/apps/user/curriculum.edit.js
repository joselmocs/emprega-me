/*!
 * apps.user.curriculum.edit.js
 * Scripts responsáveis pela edição do currículo.
 *
 * Joselmo Cardozo
 */

require([
    "jquery",
    "helpers/core",
    "apps/user/curriculum.edit.events",
    "apps/user/curriculum.edit.validations"
], function($, core, events, validations) {
    var
        initialize = function() {
            events.initialize();

            $("#save_curriculum").unbind('click').click(function() {
                if (validated()) {
                    form.submit();
                }
            });
        },
        validated = function() {
            validations.validate();
            if (!validations.validated) {
                validations.first_element_error().focus();
            }
            return validations.validated;
        },
        form = {
            submit: function() {
                core.loading.show();
                $.ajax({
                    type: 'post',
                    url: '',
                    data: {
                        _token: $("meta[name=csrf-token]").attr('content'),
                        data: JSON.stringify(validations.objects())
                    },
                    error: function() {
                        alert("Ocorreu um erro ao salvar seu currículo, por favor contacte um administrador.");
                    },
                    success: function(response) {
                        if (response.success) {
                            window.location = response.redirect;
                        }
                    },
                    complete: function() {
                        core.loading.hide();
                    }
                });
            }
        };

    initialize();
});
