@extends("base/template")

@section("site-title")
Entrar - {{ Config::get('domain.site.title') }}
@endsection

@section("content")
@include("advertising/header")
<div class="page_template page_template_links"><!--page_template-->
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin: 0 auto;">
<tr>
<td width="50%" valign="top"><!--coluna_1-->
<div class="titulo_grande"><!--titulo_pequeno-->
Já sou cadastrado
</div><!--titulo_pequeno-->

@if (count($context->auth_errors) > 0)
<div class="alerta_vermelho">
@foreach ($context->auth_errors as $e)
{{ $e }}<br />
@endforeach
</div>
@endif

<form action="{{ URL::action('UserController@login') }}" method="post">
<input type="hidden" name="_token" value="{{  csrf_token() }}" />
<input type="hidden" name="next" value="{{ $context->input->next }}" />
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
<td>Digite seu e-mail:</td>
</tr>
<tr>
<td>
<input type="email" name="email" value="{{ $context->input->email }}" required="true" style="width: 400px;"
@if ($context->field_errors && $context->field_errors->has('email')) class="error" @endif />
@if ($context->field_errors && $context->field_errors->has('email'))
<div class="error">{{ $context->field_errors->first('email') }}</div>
@endif
</td>
</tr>
<tr>
<td height="10"></td>
</tr>
<tr>
<td>Digite sua senha:</td>
</tr>
<tr>
<td>
<input type="password" name="password" required="true" style="width: 250px;"
@if ($context->field_errors && $context->field_errors->has('password')) class="error" @endif />
@if ($context->field_errors && $context->field_errors->has('password'))
<div class="error">{{ $context->field_errors->first('password') }}</div>
@endif
</td>
</tr>
<tr>
<td height="5"></td>
</tr>
<tr>
<td>

<table width="0%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="0%" align="center" style="padding: 2px 4px 0 0;">
<input type="checkbox" name="remember_me" @if (@$context->input->remember_me) checked="checked" @endif>
</td>
<td width="100%" align="left" style="font-size: 11px;">Mantenha-me conectado.</td>
</tr>
</table>

</td>
</tr>
<tr>
<td height="5"></td>
</tr>
<tr>
<td>
<a class="fancybox fancybox.iframe" title="Recuperar senha" href="{{ URL::action('UserController@lostPassword') }}" data-fancybox-width="430" data-fancybox-height="auto">Esqueci minha senha.</a>
</td>
</tr>
<tr>
<td height="10"></td>
</tr>
<tr>
<td>
<input type="submit" name="submit" value="Entrar"/>
</td>
</tr>
</table>
</form>

</td><!--coluna_1-->

<td width="15"><img src="/images/coluna_space_15.jpg" alt="" width="15" height="15"/></td>

<td width="50%" valign="top"><!--coluna_2-->
<div class="titulo_grande"><!--titulo_pequeno-->
Ainda não tenho cadastro
</div><!--titulo_pequeno-->

@if (count($context->form_errors) > 0)
<div class="alerta_vermelho">
@foreach ($context->form_errors as $e)
{{ $e }}<br />
@endforeach
</div>
@endif

<form action="{{ URL::action('UserController@check_new_user') }}" method="post">
<input type="hidden" name="_token" value="{{  csrf_token() }}" />
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
<td>Crie sua conta gratuitamente em poucos segundos e cadastre seu currículo ou suas vagas totalmente grátis.</td>
</tr>
<tr>
<td height="15"></td>
</tr>
<tr>
<td> Digite seu e-mail:</td>
</tr>
<tr>
<td>
<input type="email" name="nemail" value="{{ $context->input->nemail }}" style="width: 350px;" required="true"
@if ($context->field_errors && $context->field_errors->has('nemail')) class="error" @endif />
@if ($context->field_errors && $context->field_errors->has('nemail'))
<div class="error">{{ $context->field_errors->first('nemail') }}</div>
@endif
</td>
</tr>
<tr>
<td height="10"></td>
</tr>
<tr>
<td>

<table width="0%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="0%" align="center" style="padding: 2px 4px 0 0;">
<input type="radio" name="type" value="1" @if($context->input->candidate) checked="checked" @endif>
</td>
<td align="left" style="font-size: 11px;">Candidato</td>
<td width="25"></td>
<td width="0%" align="center" style="padding: 2px 4px 0 0;">
<input type="radio" name="type" value="2" @if($context->input->company) checked="checked" @endif>
</td>
<td align="left" style="font-size: 11px;">Empresa</td>
</tr>
</table>

</td>
</tr>
<tr>
<td height="10"></td>
</tr>
<tr>
<td>
<input type="submit" value="Continuar"/>
</td>
</tr>
</table>
</form>

</td><!--coluna_2-->
</tr>
</table>
</div><!--page_template-->

</div><!--container-->
@include("advertising/facebook")
@endsection
