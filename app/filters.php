<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
    if (App::environment()) {
        Session::set('estate', Config::get('domain.estate'));
    }
});


App::after(function($request, $response)
{
    //
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function($route, $request)
{
    if (Auth::guest()) {
        return Redirect::action('UserController@login', array("next" => "/". Request::path()));
    }
});

Route::filter('auth_backend', function()
{
    if (Auth::guest() || !Auth::user()->can('backend')) {
        return Redirect::action('BackendController@login');
    }
});


Route::filter('auth.basic', function()
{
    return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Can Filter
|--------------------------------------------------------------------------
|
| The "can" filter check if the authenticated user has the permission
| to access the route.
|
*/
Route::filter('can', function($route, $request, $value)
{
    if (!Auth::check() || !Auth::user()->can($value)) {
        if (Request::ajax()) {
            throw new RuntimeException('permission denied');
        }

        return Redirect::action('UserController@login');
    }
});

/*
|--------------------------------------------------------------------------
| Is Filter
|--------------------------------------------------------------------------
|
| The "is" filter check if the authenticated user has the group
| to access the route.
|
*/
Route::filter('is', function($route, $request, $value)
{
    if (!Auth::check() || !Auth::user()->is($value)) {
        if (Request::ajax()) {
            throw new RuntimeException('permission denied');
        }

        return Redirect::action('UserController@login');
    }
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
    if (Auth::check()) {
        return Redirect::to('/');
    }
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
    if (Session::token() != Input::get('_token'))
    {
        //throw new Illuminate\Session\TokenMismatchException;
    }
});

/*
|--------------------------------------------------------------------------
| Estate Filter
|--------------------------------------------------------------------------
|
| Checa se o usuário já tem o estado selecionado na sessão.
|
*/
Route::filter('check_estate', function()
{
    if (Session::get('estate', null) == null) {
        return Redirect::action('IndexController@index');
    }
});

/*
|--------------------------------------------------------------------------
| OnlyMain Filter
|--------------------------------------------------------------------------
|
| Rotas que só poderão ser acessadas pelo domain principal
|
*/
Route::filter('only_main', function()
{
    if (Config::get('domain.main') == false) {
        return \Illuminate\Support\Facades\Redirect::action('IndexController@index');
    }
});
