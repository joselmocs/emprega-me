<?php


class UserProfileName extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_profile_names';

    /**
     * Retorna o perfil do usuário desde idioma
     *
     * @return UserProfile
     */
    public function profile()
    {
        return $this->belongsTo('UserProfile', 'profile_id');
    }

}
