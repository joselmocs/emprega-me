@extends("base/template")

@section("site-title")
Candidatos para {{ $context->job->position }} - {{ Config::get('domain.site.title') }}
@endsection

@section("navigation")
<li><a href="{{ URL::action('CompanyJobController@lists', array('-')) }}" title="Minhas Vagas">Minhas Vagas</a></li>
<li><a href="{{ URL::action('CompanyJobController@job_candidacies', array($context->job->id .'-'. Illuminate\Support\Str::slug($context->job->position))) }}">{{ $context->job->position }}</a></li>
@endsection

@section('requirejs')
requirejs(["apps/company/candidacy"]);
@endsection

@section("content")
@include("advertising/header")

<div id="candidatos" class="page_template"><!--candidatos-->

    <div class="titulo_medio"><!--titulo_medio-->
        Candidatos para {{ strtoupper($context->job->position) }}
    </div><!--titulo_medio-->

    <div class="alerta_verde" style="display:none;"><!--alerta_verde-->
        Currículo arquivado com sucesso!!!
    </div><!--alerta_verde-->

    @if (!$context->candidacies->count())
    <div class="row"><!--row/row-alt-->
        <span class="row-info">
            <img src="/images/set_right.png" /><a>Nenhum candidato encontrado.</a>
        </span>
    </div>
    @else

    <div class="titulo_pequeno"><!--titulo_pequeno-->
        Exibindo {{ $context->candidacies->getFrom() }}
        a {{ $context->candidacies->getTo() }}
        de {{ $context->candidacies->getTotal() }} candidato(s).
    </div><!--titulo_pequeno-->

    @foreach($context->candidacies as $i => $candidacy)
    <div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
        <span class="row-info">
            <img src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $context->job->type->image }}" title="{{ $context->job->type->name }}"/>
            <a class="fancybox fancybox.iframe" title="Currículo {{ $candidacy->profile->fullname }}" href="{{ URL::action('CurriculumController@view', array($candidacy->profile->user->id)) }}" data-fancybox-width="800" data-fancybox-height="600">{{ $candidacy->profile->fullname }}</a>
            <small><span>de </span>{{ $candidacy->profile->city->name }}/{{ $candidacy->profile->city->estate->short }}</small>
        </span>

        <span class="row_menu">
            <a href="javascript: void(0);" name="remover" data-name="{{ $candidacy->profile->fullname }}" data-id="{{ $candidacy->id }}"><img src="/images/row_menu_remover.png" title="Remover candidatura"></a>
        </span>

        <span class="row_menu">
            <a href="linkaqui"><img src="/images/row_menu_impressora.png" title="Imprimir currículo"/></a>
        </span>

        <span class="row_menu">
            <a class="fancybox fancybox.iframe" title="{{ $context->job->id }} - {{ $context->job->position }}" href="{{ URL::action('CompanyJobController@view', array($context->job->id)) }}" data-fancybox-width="800" data-fancybox-height="600">
                <img src="/images/row_menu_ver_vaga.png" title="Ver vaga"/>
            </a>
        </span>

        <span class="time_post">
        @if (Carbon::parse($candidacy->created_at)->isToday())
            <img src="/images/hoje.png" title="Se candidatou hoje">
        @endif
        @if (Carbon::parse($candidacy->created_at)->isYesterday())
            <img src="/images/ontem.png" title="Se candidatou ontem">
        @endif
            <img src="/images/clock.png" title="Data da candidatura">
            <small title="{{ Carbon::parse($candidacy->created_at)->format('d \d\e F \d\e Y') }}">{{ Carbon::parse($candidacy->created_at)->format('d/m') }}</small>
        </span>

    </div><!--row/row-alt-->
    @endforeach
    @endif

    <div class="page_count-letter"><!--page_count-letter-->
        {{ $context->candidacies->links() }}
    </div><!--page_count-letter-->

</div><!--candidatos-->


@endsection
