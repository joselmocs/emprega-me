<?php
use JCS\Auth\Models\Role;


class CurriculumController extends BaseController {

    /**
     * Faz uma pesquisa pelos candidatos
     * @param  integer $sexo
     * @param  integer $cidade
     * @return View
     */
    public function search($sexo, $cidade)
    {
        $term = Input::get('term');
        $estate = Session::get('estate');

        $group = Role::where('tag', '=', 'user')->firstOrFail();
        $users = UserProfile::select('user_profile.*')
            ->where('user_profile.search_string', 'like', '%'. $term .'%')
            ->join('location_cities', 'location_cities.id', '=', 'user_profile.city_id')
            ->where('location_cities.estate_id', $estate)
            ->join('user_role_user', 'user_role_user.user_id', '=', 'user_profile.user_id')
            ->where('user_role_user.role_id', '=', $group->id)
            ->orderBy('user_profile.updated_at', 'desc');

        if (intval($sexo)) {
            $users->where('user_profile.gender', $sexo);
        }

        if (intval($cidade)) {
            $users->where('location_cities.id', $cidade);
        }

        $cidades = LocationCity::where('estate_id', $estate)
            ->where('active', true)
            ->orderBy('name', 'asc')
            ->get();

        $stat = new SearchStatistic;
        $stat->keyword = $term;
        $stat->estate_id = Session::get('estate');
        $stat->save();

        $this->set_context(array(
            'sexo' => $sexo,
            'cidade' => $cidade,
            'cidades' => $cidades,
            'users' => $users,
            'term' => $term
        ));

        return $this->view_make('candidate/curriculum/search');
    }

    /**
     * Página de visualização do currículo
     *
     * @param integer $id
     * @return Illuminate\Support\Facades\View
     */
    public function view($id = null)
    {
        $user = ($id) ? User::find($id) : Auth::user();

        if (Auth::user()->is('company')) {
            $myJobs = array_map(function ($job) {
                return intval($job['id']);
            }, Auth::user()->profile->jobs()->get()->toArray());

            JobCandidate::where('profile_id', $user->profile->id)
                ->whereIn('job_id', $myJobs)
                ->update(array(
                    'viewed_at' => Carbon::now()
                ));
        }

        $this->set_context(array(
            'profile' => $user->profile
        ));

        return $this->view_make('candidate/curriculum/view');
    }

    /**
     * Carrega a lista de cidades do estado informado
     *
     * @return Illuminate\Support\Facades\Response
     */
    public function load_cities()
    {
        $estate_id = Input::get('estate_id');
        $estate = LocationEstate::where('id', '=', $estate_id)->firstOrFail();
        $cities = $estate->cities()->where('active', '=', 1)->get()->toArray();
        return Response::json(array(
            'success' => true,
            'cities' => $cities
        ));
    }

    /**
     * Página de edição do currículo
     *
     * @return Illuminate\Support\Facades\View
     */
    public function edit()
    {
        $profile = Auth::user()->profile;

        if (Request::isMethod('get')) {

            $data_personal = $profile->toArray();
            unset($data_personal['user_id']);
            $data_personal['estate_id'] = $profile->city->estate->id;
            $data_personal['email'] = $profile->user->email;

            // Converte o salário para float
            $experiences = array();
            $db_experiences = $profile->experiences->toArray();
            foreach($db_experiences as $experience) {
                $experience['salary'] = number_format($experience['salary'], 2, ',', '.');
                $experiences[] = $experience;
            }

            $this->set_json_context(array(
                'languages' => Language::all()->toArray(),
                'salaries' => Salary::all()->toArray(),
                'data' => array(
                    'personal' => $data_personal,
                    'formations' => $profile->educations->toArray(),
                    'courses' => $profile->courses->toArray(),
                    'languages' => $profile->languages->toArray(),
                    'positions' => $profile->desiredpositions->toArray(),
                    'experiences' => $experiences
                )
            ));

            return $this->view_make('candidate/curriculum/edit/template');
        }

        $data = json_decode(Input::get('data'));

        DB::transaction(function() use ($data, $profile) {
            $profile->fullname = $data->personal->fullname;
            $profile->gender = $data->personal->gender;
            $profile->marital_status = $data->personal->marital_status;
            $profile->city_id = $data->personal->city_id;
            $profile->notes = $data->notes;
            $profile->travel_availability = $data->personal->travel_availability;
            $profile->live_another_city_availability = $data->personal->live_another_city_availability;
            $profile->special_needs = $data->personal->special_needs;

            $profile->cnh_a = $data->personal->cnh_a;
            $profile->cnh_b = $data->personal->cnh_b;
            $profile->cnh_c = $data->personal->cnh_c;
            $profile->cnh_d = $data->personal->cnh_d;
            $profile->cnh_e = $data->personal->cnh_e;

            $profile->auto_motorcycle = $data->personal->auto_motorcycle;
            $profile->auto_car = $data->personal->auto_car;
            $profile->auto_bus = $data->personal->auto_bus;
            $profile->auto_truck = $data->personal->auto_truck;

            $profile->phone = $data->contact->phone;
            $profile->mobile = $data->contact->mobile;
            $profile->facebook = $data->contact->facebook;
            $profile->linkedin = $data->contact->linkedin;
            $profile->gplus = $data->contact->gplus;
            $profile->twitter = $data->contact->twitter;
            $profile->save();

            $profile->educations()->delete();
            foreach($data->formations as $formation) {
                $new_education = new UserEducation;
                $new_education->profile_id = $profile->id;
                $new_education->level = $formation->level;
                $new_education->situation = $formation->situation;
                $new_education->study_area = $formation->study_area;
                $new_education->specialization = $formation->specialization;
                $new_education->institution = $formation->institution;
                $new_education->started_at = Carbon::createFromFormat('m/Y', $formation->started_at)->startOfMonth();
                $new_education->ended_at = Carbon::createFromFormat('m/Y', $formation->ended_at)->startOfMonth();
                $new_education->save();
            }

            $profile->courses()->delete();
            foreach($data->courses as $course) {
                $new_course = new UserCourse;
                $new_course->profile_id = $profile->id;
                $new_course->course = $course->course;
                $new_course->institution = $course->institution;
                $new_course->started_at = Carbon::createFromFormat('m/Y', $course->started_at)->startOfMonth();
                $new_course->duration = strtolower($course->duration);
                $new_course->save();
            }

            $profile->languages()->delete();
            foreach($data->languages as $language) {
                $new_language = new UserLanguage;
                $new_language->profile_id = $profile->id;
                $new_language->language_id = $language->language_id;
                $new_language->level = $language->level;
                $new_language->save();
            }

            $profile->desiredpositions()->delete();
            foreach($data->positions as $position) {
                $new_position = new UserDesiredPosition;
                $new_position->profile_id = $profile->id;
                $new_position->position = $position->position;
                $new_position->salary_id = $position->salary_id;
                $new_position->save();
            }

            $profile->experiences()->delete();
            foreach($data->experiences as $experience) {
                $new_experience = new UserExperience;
                $new_experience->profile_id = $profile->id;
                $new_experience->company = $experience->company;
                $new_experience->position = $experience->position;
                $new_experience->salary = floatval(str_replace(',', '.', str_replace('.', '', $experience->salary)));
                $new_experience->started_at = Carbon::createFromFormat('m/Y', $experience->started_at)->startOfMonth();
                $new_experience->ended_at = Carbon::createFromFormat('m/Y', $experience->ended_at)->startOfMonth();
                $new_experience->save();
            }

            $profile->save();
        });

        return Response::json(array(
            'success' => true,
            'redirect' => URL::action('CurriculumController@complete')
        ));
    }

    /**
     * Página de confirmação de alterações no currículo
     *
     * @return Illuminate\Support\Facades\View
     */
    public function complete()
    {
        return $this->view_make('candidate/curriculum/edit/complete');
    }
}
