<?php


class BackendLogController extends BaseBackendController {

    /**
     * Retorna o caminho do arquivo de log de erros.
     *
     * @return string
     */
    public function getLogFilePath()
    {
        return storage_path('logs') .'/laravel.log';
    }

    /**
     * Exibe a página com o log de erros.
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        $log = file_get_contents($this->getLogFilePath());

        $this->set_context(array(
            'log' => $log
        ));

        return $this->view_make('backend/log/index');
    }

    /**
     * Limpa o arquivo.
     *
     * @return Illuminate\Support\Facades\Redirect
     */
    public function clear()
    {
        $f = fopen($this->getLogFilePath(), 'w+');
        fclose($f);

        return Redirect::back();
    }
}
