<?php


class BackendPageController extends BaseBackendController {

    /**
     * Exibe a lista de páginas cadastradas.
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        $this->set_json_context(array(
            'urls' => array(
                'remove' => URL::action('BackendPageController@remove')
            )
        ));

        $this->set_context(array(
            'pages' => Page::all()
        ));

        return $this->view_make('backend/page/index');
    }

    /**
     * Exibe o formulário de criação e edição de páginas
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function register($id = null)
    {
        if ($id) {
            $page = Page::find($id);
            $this->set_context(array(
                'page_id' => $page->id,
                'input' => (object) array(
                    'title' => Input::get('title', $page->title),
                    'page' => Input::get('page', $page->page),
                    'form' => Input::get('form', ($page->page) ? "on" : "off") == "on",
                    'url' => Input::get('url', $page->url),
                    'seo' => Input::get('seo', $page->seo),
                    'keywords' => Input::get('keywords', $page->keywords),
                    'description' => Input::get('description', $page->description)
                )
            ));
        }
        else {
            $page = new Page;
            $this->set_context(array(
                'page_id' => null,
                'input' => (object) array(
                    'title' => Input::get('title'),
                    'page' => Input::get('page'),
                    'form' => Input::get('form') == "on",
                    'url' => Input::get('url'),
                    'seo' => Input::get('seo'),
                    'keywords' => Input::get('keywords'),
                    'description' => Input::get('description')
                )
            ));
        }

        if (Request::isMethod('get')) {
            return $this->view_make('backend/page/form');
        }

        $validate = array(
            "title" => "required",
            "page" => "required",
            "url" => ($id) ? "required|unique:pages,url,". $page->id : "required|unique:pages"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('backend/page/form');
        }

        $page->title = Input::get('title');
        $page->page = htmlentities(Input::get('page'), ENT_QUOTES, 'utf-8');
        $page->form = Input::get('form') == "on";
        $page->url = Input::get('url');
        $page->seo = Input::get('seo');
        $page->keywords = Input::get('keywords');
        $page->description = Input::get('description');
        $page->save();

        return Redirect::action('BackendPageController@index');
    }

    /**
     * Remove uma página.
     *
     * @param $id int
     * @return Redirect
     */
    public function remove($id)
    {
        if ($id) {
            Page::find($id)->delete();
        }
        return Redirect::back();
    }

}
