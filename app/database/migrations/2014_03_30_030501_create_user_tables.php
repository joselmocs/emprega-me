<?php

use Illuminate\Database\Migrations\Migration;


class CreateUserTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the permissions table
        Schema::create('user_permission_scope', function($table)
        {
            $table->increments('id');
            $table->string('name', 128);
            $table->timestamps();
        });

        // Create the permissions table
        Schema::create('user_permissions', function($table)
        {
            $table->increments('id');
            $table->integer('scope_id')->unsigned()->index();
            $table->string('name', 128);
            $table->string('tag', 128)->index();
            $table->string('description', 255)->nullable();
            $table->timestamps();

            $table->foreign('scope_id')->references('id')->on('user_permission_scope');
        });

        // Create the roles table
        Schema::create('user_roles', function($table)
        {
            $table->increments('id');
            $table->string('name', 128);
            $table->string('tag', 128)->index();
            $table->string('description', 255)->nullable();
            $table->integer('level');
            $table->timestamps();
        });

        // Create the users table
        Schema::create('users', function($table)
        {
            $table->increments('id');
            $table->string('username', 30)->index();
            $table->string('password', 60)->index();
            $table->string('salt', 32);
            $table->string('email', 255)->index();
            $table->string('remember_token', 100)->nullable();
            $table->boolean('verified')->default(0);
            $table->boolean('disabled')->default(0);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });

        // Create the role/user relationship table
        Schema::create('user_role_user', function($table)
        {
            $table->integer('user_id')->unsigned()->index();
            $table->integer('role_id')->unsigned()->index();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('role_id')->references('id')->on('user_roles');
        });

        // Create the permission/role relationship table
        Schema::create('user_permission_role', function($table)
        {
            $table->integer('permission_id')->unsigned()->index();
            $table->integer('role_id')->unsigned()->index();
            $table->timestamps();

            $table->foreign('permission_id')->references('id')->on('user_permissions');
            $table->foreign('role_id')->references('id')->on('user_roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_role_user');
        Schema::dropIfExists('user_permission_role');
        Schema::dropIfExists('user_roles');
        Schema::dropIfExists('user_permissions');
        Schema::dropIfExists('user_permission_scope');
        Schema::dropIfExists('users');
    }

}
