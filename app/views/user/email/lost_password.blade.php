<p>Olá {{ $fullname }},</p>

<p>Este email foi enviado devido à solicitação de recuperação de senha em nosso site.</p>

<p>Sua nova senha é:</p>
<p>{{ $password }}</p>

<p>Cordialmente,<br />
Equipe {{ $site_name }}</p>
