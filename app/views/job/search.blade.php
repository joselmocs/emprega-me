@extends("base/template")

@section("site-title")
Resultado da busca - {{ Config::get('domain.site.title') }}
@endsection

@section("content")
@include("advertising/header")
<div class="page_template"><!--busca-->

<div style="margin: 0 0 5px 0;"><!--div margin-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario">
<tr>
<td width="100%">
<a href="linkaqui"><img src="domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="linkaqui"><img src="domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="linkaqui"><img src="domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="linkaqui"><img src="domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="linkaqui"><img src="domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="linkaqui"><img src="domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
</td>
<td align="right">
<select>
<option value="Todas os bairros">Todas os bairros</option>
<option value="Buritis">Buritis</option>
</select>
</td>
<td align="right">
<select style="margin-left: 5px;">
<option value="Todas as cidades">Todas as cidades</option>
<option value="Belo Horizonte">Belo Horizonte</option>
</select>
</td>
</tr>
</table>
</div><!--div margin-->

<div class="titulo_medio"><!--titulo_medio-->
Encontramos <span style="color: #999999;">({{ $context->jobs->count() }})</span> resultados para <span style="color: #999999; text-transform: lowercase;">{{ $context->term }}</span>
</div><!--titulo_medio-->

@if (!$context->jobs->count())
<span style="color: #666666; font-size: 12px;">Nenhuma vaga foi encontrada.</span>
@endif

@foreach($context->jobs->get() as $job)
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $job->type->image }}" title="{{ $job->type->name }}">
<a href="{{ URL::action('JobController@view', array($job->type->slug, $job->id, $job->city->slug, $job->slug)) }}" title="{{ $job->position }}">{{ $job->position }}</a>
<small><span>por</span> {{ $job->company }} <span>em</span> {{ $job->city->name }}/{{ $job->estate->short }}</small>
</span>

<span class="time_post">
@if (Carbon::parse($job->created_at)->isToday())
<img src="/images/hoje.png" title="Vaga publicada hoje">
@endif
@if (Carbon::parse($job->created_at)->isYesterday())
<img src="/images/ontem.png" title="Vaga publicada ontem">
@endif
<img src="/images/clock.png" title="Data da publicação">
<small title="{{ Carbon::parse($job->created_at)->format('d \d\e F \d\e Y') }}">{{ Carbon::parse($job->created_at)->format('d/m') }}</small>
</span>
</div><!--row/row-alt-->
@endforeach

</div><!--busca-->

</div><!--container-->
@include("advertising/footer")
@include("advertising/facebook")
@endsection