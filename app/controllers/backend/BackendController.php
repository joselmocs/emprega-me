<?php


class BackendController extends BaseBackendController {

    /**
     * Exibe a página inicial da administração.
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        $jobs = Job::where('status', JOB::IN_APPROVAL)
            ->orderBy('edited', 'desc')
            ->orderBy('created_at', 'asc');

        $this->set_context(array(
            'jobs' => $jobs->get(),
            'job_types' => JobType::all()
        ));

        $this->set_json_context(array(
            'urls' => array(
                'remove' => URL::action('BackendCompanyController@remove_job')
            )
        ));

        return $this->view_make('backend/index');
    }

    public function update() {
        return Response::json(
            array('count' => Job::where('viewed', false)->count())
        );
    }

    /**
     * Página e controle de autenticação do usuário
     *
     * @return Illuminate\Support\Facades\View
     */
    public function login()
    {
        $this->set_context(array(
            'input' => (object) array(
                'email' => Input::get('email', ''),
                'remember_me' => (bool) Input::get('remember_me', false)
            )
        ));

        if (!Input::get('submit')) {
            return $this->view_make('backend/login');
        }

        $validate = array(
            "email" => "required|email",
            "password" => "required"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('backend/login');
        }

        $errors = array();
        $login_failed_message = "Desculpe, o email e senha informados estão incorretos.";
        try {
            Auth::attempt(array(
                'identifier' => Input::get('email'),
                'password' => Input::get('password')
            ), (bool) Input::get('remember_me', false));
        }
        catch (JCS\Auth\UserPasswordIncorrectException $e) {
            $user = User::where('email', '=', Input::get('email'))->first();
            if ($user) {
                // Atualizamos as informações de falha na antenticação
                $user->profile->increment('count_login_failed');
                $user->profile->last_login_failed = date("Y-m-d H:i:s");
                $user->profile->save();
            }
            array_push($errors, $login_failed_message);
        }
        catch (JCS\Auth\UserUnverifiedException $e) {
            array_push($errors, "Por favor, verifique seu email para ativação da sua conta.");
        }
        catch (Exception $e) {
            array_push($errors, $login_failed_message);
        }

        if (!Auth::user()->is(array('sys_admin', 'manager', 'moderator'))) {
            array_push($errors, "Acesso não permitido.");
            Auth::logout();
        }

        if (count($errors) > 0) {
            $this->set_context(array(
                'auth_errors' => $errors
            ));
            return $this->view_make('backend/login');
        }

        // Atualizamos as informações de último login do usuário
        $user = Auth::user();
        $user->profile->increment('count_login');
        $user->profile->previous_login = $user->profile->last_login;
        $user->profile->last_login = date("Y-m-d H:i:s");
        $user->profile->save();

        return Redirect::action('BackendController@index');
    }
}
