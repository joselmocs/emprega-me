<?php


class UserExperience extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_experiences';

    /**
     * Retorna o perfil do usuário desta experiência profissional
     *
     * @return UserProfile
     */
    public function profile()
    {
        return $this->belongsTo('UserProfile', 'profile_id');
    }

}