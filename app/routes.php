<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'IndexController@index');
Route::get('/sair', 'UserController@logout');
Route::get('/pagina/{page}', 'PageController@view');

Route::get('conta/verificar/{username}/{salt}/', 'UserController@verify');
Route::get('empresa/verificar/{username}/{salt}/', 'CompanyController@verify');
Route::any('conta/recuperar/', 'UserController@lostPassword');

/*
|--------------------------------------------------------------------------
| Category Routes
|--------------------------------------------------------------------------
*/
Route::group(array('before' => 'check_estate', 'prefix' => 'vagas'), function() {
    /*
    |--------------------------------------------------------------------------
    | Lista de vagas
    |--------------------------------------------------------------------------
    */
    Route::get('/', 'CategoryController@index');
    Route::get('{category}/{type}/{estate}/{city}', 'CategoryController@view');
});

/*
|--------------------------------------------------------------------------
| Job Routes
|--------------------------------------------------------------------------
*/
Route::group(array('prefix' => 'vaga'), function() {
    Route::get('procurar', 'JobController@search');
    Route::get('listar/{type}/{job}/{city}', 'JobController@index');

    /*
    |--------------------------------------------------------------------------
    | Funções de candidatura
    |--------------------------------------------------------------------------
    */
    Route::group(array('before' => 'auth|is:user'), function() {
        Route::get('reportar/{type}/{job}/{city}', 'JobController@report');
        Route::get('candidatar/{type}/{job}/{city}', 'JobController@join');
        Route::get('cancelar/{job}', 'JobController@cancel');
    });

    /*
    |--------------------------------------------------------------------------
    | Exibe a vaga
    |--------------------------------------------------------------------------
    */
    Route::get('{job}', 'JobController@short');
    Route::get('{type}/{job}/{city}/{title}', 'JobController@view');


});

/*
|--------------------------------------------------------------------------
| Location Routes
|--------------------------------------------------------------------------
*/
Route::group(array('prefix' => 'location'), function() {
    /*
    |--------------------------------------------------------------------------
    | Cidades
    |--------------------------------------------------------------------------
    */
    Route::group(array('before' => 'csrf', 'prefix' => 'city'), function() {
        Route::post('load_all_json', 'LocationCityController@load_all_json');
    });

    /*
    |--------------------------------------------------------------------------
    | Bairros
    |--------------------------------------------------------------------------
    */
    Route::group(array('before' => 'csrf', 'prefix' => 'district'), function() {
        Route::post('load_all_json', 'LocationDistrictController@load_all_json');
    });
});

/*
|--------------------------------------------------------------------------
| Company Routes
|--------------------------------------------------------------------------
*/
Route::group(array('before' => 'check_estate', 'prefix' => 'empresa'), function() {
    /*
    |--------------------------------------------------------------------------
    | Cadastro
    |--------------------------------------------------------------------------
    */
    Route::group(array('before' => 'guest'), function() {
        Route::get('cadastro', 'CompanyController@register');
        Route::group(array('before' => 'csrf'), function() {
            Route::post('cadastro', 'CompanyController@register');
        });
    });

    Route::get('parabens', 'CompanyController@register_complete');

    Route::group(array('before' => 'auth'), function() {
        Route::get('editar', 'CompanyController@edit');
        Route::get('sucesso', 'CompanyController@complete');

        Route::group(array('before' => 'csrf'), function() {
            Route::post('editar', 'CompanyController@edit');
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Vagas
    |--------------------------------------------------------------------------
    */
    Route::get('vagas/exibir/{id}', 'CompanyJobController@view');
    Route::group(array('before' => 'auth|is:company', 'prefix' => 'vagas'), function() {
        Route::get('listar/{view}', 'CompanyJobController@lists');
        Route::get('edit/{id}', 'CompanyJobController@edit');
        Route::get('nova', 'CompanyJobController@register');
        Route::get('sucesso', 'CompanyJobController@register_complete');
        Route::get('regras', 'CompanyJobController@terms');
        Route::get('candidatos', 'CompanyJobController@candidacies');
        Route::get('{job}/candidatos', 'CompanyJobController@job_candidacies');
        Route::get('candidatos/arquivar/{id?}', 'CompanyJobController@job_candidate_archive');
        Route::group(array('before' => 'csrf'), function() {
            Route::post('edit/{id}', 'CompanyJobController@edit');
            Route::post('nova', 'CompanyJobController@register');
        });

    });

});

/*
|--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------
*/

Route::group(array('before' => 'guest', 'prefix' => 'usuario'), function() {
    /*
    |--------------------------------------------------------------------------
    | Autenticação
    |--------------------------------------------------------------------------
    */

    Route::get('entrar', 'UserController@login');
    Route::group(array('before' => 'csrf'), function() {
        Route::post('entrar', 'UserController@login');
    });
});

Route::group(array('before' => 'check_estate', 'prefix' => 'candidato'), function() {
    Route::get('procurar/{sexo}/{cidade}/', 'CurriculumController@search');
    /*
    |--------------------------------------------------------------------------
    | Cadastro
    |--------------------------------------------------------------------------
    */
    Route::group(array('before' => 'guest'), function() {
        Route::get('novo', 'UserController@check_new_user');
        Route::get('cadastro', 'CandidateController@register');

        Route::group(array('before' => 'csrf'), function() {
            Route::post('novo', 'UserController@check_new_user');
            Route::post('cadastro', 'CandidateController@register');
        });
    });

    Route::get('parabens', 'CandidateController@register_complete');

    /*
    |--------------------------------------------------------------------------
    | Curriculo
    |--------------------------------------------------------------------------
    */
    Route::group(array('prefix' => 'curriculo'), function() {
        Route::get('ver/{id?}', 'CurriculumController@view');
        Route::group(array('before' => 'auth|is:user'), function() {
            Route::get('editar', 'CurriculumController@edit');
            Route::get('sucesso', 'CurriculumController@complete');
            Route::group(array('before' => 'csrf'), function() {
                Route::post('editar', 'CurriculumController@edit');
            });
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Candidaturas
    |--------------------------------------------------------------------------
    */
    Route::group(array('prefix' => 'candidaturas', 'before' => 'auth|is:user'), function() {
        Route::get('{view}', 'CandidateController@candidacies');
    });
});

/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
*/

Route::group(array('prefix' => 'backend'), function() {
    Route::get('login', 'BackendController@login');
    Route::group(array('before' => 'csrf'), function() {
        Route::post('login', 'BackendController@login');
    });
});

Route::group(array('before' => 'only_main|auth_backend', 'prefix' => 'backend'), function() {
    Route::get('/', 'BackendController@index');
    Route::get('update', 'BackendController@update');

    /*
    |--------------------------------------------------------------------------
    | Usuários
    |--------------------------------------------------------------------------
    */
    Route::group(array('prefix' => 'users', 'before' => 'can:manage_users'), function() {
        Route::get('', 'BackendUserController@index');
        Route::get('register/{id?}', 'BackendUserController@register');
        Route::get('remove/{id?}', 'BackendUserController@remove');
        Route::get('active/{id?}', 'BackendUserController@active');
        Route::get('deactive/{id?}', 'BackendUserController@deactive');
        Route::group(array('before' => 'csrf'), function() {
            Route::post('register/{id?}', 'BackendUserController@register');
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Páginas
    |--------------------------------------------------------------------------
    */
    Route::group(array('prefix' => 'pages', 'before' => 'can:view_pages'), function() {
        Route::get('', 'BackendPageController@index');

        Route::group(array('before' => 'can:manage_pages'), function() {
            Route::get('register/{id?}', 'BackendPageController@register');
            Route::get('remove/{id?}', 'BackendPageController@remove');
            Route::group(array('before' => 'csrf'), function () {
                Route::post('register/{id?}', 'BackendPageController@register');
            });
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Empresas
    |--------------------------------------------------------------------------
    */
    Route::group(array('prefix' => 'companies', 'before' => 'can:view_companies'), function() {
        Route::get('list/{estates}/{letter}', 'BackendCompanyController@index');

        Route::group(array('before' => 'can:view_jobs'), function() {
            Route::get('jobs/{company}', 'BackendCompanyController@jobs');
        });

        Route::group(array('before' => 'can:manage_jobs'), function() {
            Route::get('job/feature/{job}', 'BackendCompanyController@feature');
            Route::get('job/approve/{job}', 'BackendCompanyController@approve');
            Route::get('job/reprove/{job}', 'BackendCompanyController@reprove');
            Route::any('job/edit/{job}', 'BackendCompanyController@edit');
            Route::any('job/remove/{job?}', 'BackendCompanyController@remove_job');
        });

        Route::group(array('before' => 'can:manage_companies'), function() {
            Route::get('register/{id?}', 'BackendCompanyController@register');
            Route::get('remove/{id?}', 'BackendCompanyController@remove');
            Route::group(array('before' => 'csrf'), function () {
                Route::post('register/{id?}', 'BackendCompanyController@register');
            });
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Candidatos
    |--------------------------------------------------------------------------
    */
    Route::group(array('prefix' => 'candidates', 'before' => 'can:view_candidates'), function() {
        Route::get('list/{estates}/{letter}', 'BackendCandidateController@index');

        Route::group(array('prefix' => 'candidates', 'before' => 'can:manage_candidates'), function() {
            Route::get('register/{id?}', 'BackendCandidateController@register');
            Route::get('remove/{id?}', 'BackendCandidateController@remove');
            Route::group(array('before' => 'csrf'), function () {
                Route::post('register/{id?}', 'BackendCandidateController@register');
            });
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Links
    |--------------------------------------------------------------------------
    */
    Route::group(array('prefix' => 'links', 'before' => 'can:view_links'), function() {
        Route::get('', 'BackendLinkController@index');

        Route::group(array('prefix' => 'links', 'before' => 'can:manage_links'), function() {
            Route::get('remove/{id?}', 'BackendLinkController@remove');
            Route::get('edit/{id}', 'BackendLinkController@edit');
            Route::get('new', 'BackendLinkController@register');
            Route::get('active/{id}', 'BackendLinkController@active');
            Route::get('deactive/{id}', 'BackendLinkController@deactive');
            Route::post('reorder', 'BackendLinkController@reorder');
            Route::group(array('before' => 'csrf'), function () {
                Route::post('edit/{id}', 'BackendLinkController@edit');
                Route::post('new', 'BackendLinkController@register');
            });
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Empregos
    |--------------------------------------------------------------------------
    */
    Route::group(array('prefix' => 'jobs'), function() {
        /*
        |--------------------------------------------------------------------------
        | Tipos de trabalho
        |--------------------------------------------------------------------------
        */
        Route::group(array('prefix' => 'types', 'before' => 'can:view_typejobs'), function() {
            Route::get('', 'BackendJobTypeController@index');

            Route::group(array('prefix' => 'types', 'before' => 'can:manage_typejobs'), function() {
                Route::get('remove/{id?}', 'BackendJobTypeController@remove');
                Route::get('edit/{id}', 'BackendJobTypeController@edit');
                Route::get('new', 'BackendJobTypeController@register');
                Route::group(array('before' => 'csrf'), function () {
                    Route::post('edit/{id}', 'BackendJobTypeController@edit');
                    Route::post('new', 'BackendJobTypeController@register');
                });
            });
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Categorias
    |--------------------------------------------------------------------------
    */
    Route::group(array('prefix' => 'categories'), function() {
        Route::get('', 'BackendCategoryController@index');
        Route::get('view/{id}/{estate}', 'BackendCategoryController@view');

        Route::group(array('before' => 'can:manage_category'), function() {
            Route::get('register/{id?}', 'BackendCategoryController@register');
            Route::get('remove/{id?}', 'BackendCategoryController@remove');
            Route::get('active/{id}', 'BackendCategoryController@active');
            Route::get('deactive/{id}', 'BackendCategoryController@deactive');
            Route::group(array('before' => 'csrf'), function () {
                Route::post('register/{id?}', 'BackendCategoryController@register');
            });
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Estatísticas
    |--------------------------------------------------------------------------
    */
    Route::group(array('prefix' => 'statistics', 'before' => 'can:view_stats'), function() {
        Route::get('{estate}', 'BackendStatisticsController@index');
    });

    /*
    |--------------------------------------------------------------------------
    | Publicidade
    |--------------------------------------------------------------------------
    */
    Route::group(array('prefix' => 'advertising', 'before' => 'can:manage_advertising'), function() {
        Route::get('', 'BackendAdvertisingController@index');
        /*
        |--------------------------------------------------------------------------
        | Adsense
        |--------------------------------------------------------------------------
        */
        Route::group(array('prefix' => 'adsense'), function() {
            Route::get('view/{short}', 'BackendAdvertisingController@adsense_view');
            Route::get('active/{short}', 'BackendAdvertisingController@adsense_active');
            Route::get('deactive/{short}', 'BackendAdvertisingController@adsense_deactive');
            Route::group(array('before' => 'csrf'), function() {
                Route::post('save/{short}', 'BackendAdvertisingController@adsense_save');
            });
        });

        /*
        |--------------------------------------------------------------------------
        | Banners
        |--------------------------------------------------------------------------
        */
        Route::group(array('prefix' => 'banners'), function() {
            Route::get('list/{short}', 'BackendAdvertisingController@banners_list');
            Route::get('new/{short}', 'BackendAdvertisingController@banners_new');
            Route::get('edit/{id}', 'BackendAdvertisingController@banners_edit');
            Route::get('remove/{id?}', 'BackendAdvertisingController@banners_remove');

            Route::get('active/{id}', 'BackendAdvertisingController@banners_active');
            Route::get('deactive/{id}', 'BackendAdvertisingController@banners_deactive');

            Route::group(array('before' => 'csrf'), function() {
                Route::post('save', 'BackendAdvertisingController@banners_save');
            });
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Configurações
    |--------------------------------------------------------------------------
    */
    Route::group(array('prefix' => 'config', 'before' => 'can:manage_config'), function() {
        Route::get('', 'BackendConfigController@index');

        Route::group(array('before' => 'csrf'), function() {
            Route::post('', 'BackendConfigController@index');
        });

        Route::group(array('prefix' => 'highlighted'), function() {
            Route::get('remove/{highlighted}', 'BackendConfigController@highlighted_remove');
            Route::group(array('before' => 'csrf'), function() {
                Route::post('add', 'BackendConfigController@highlighted_add');
                Route::post('reorder', 'BackendConfigController@highlighted_reorder');
            });
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Locais (estados, cidades e bairros)
    |--------------------------------------------------------------------------
    */
    Route::group(array('prefix' => 'locations', 'before' => 'can:view_locations'), function() {
        /*
        |--------------------------------------------------------------------------
        | Estados
        |--------------------------------------------------------------------------
        */
        Route::group(array('prefix' => 'estates'), function() {
            Route::get('', 'BackendLocationController@estates');
        });
        Route::group(array('prefix' => 'estate', 'before' => 'can:manage_locations'), function() {
            Route::get('active/{short}', 'BackendLocationController@estate_active');
            Route::get('deactive/{short}', 'BackendLocationController@estate_deactive');
        });

        /*
        |--------------------------------------------------------------------------
        | Cidades
        |--------------------------------------------------------------------------
        */
        Route::group(array('prefix' => 'cities'), function() {
            Route::get('{estate}/{letter}', 'BackendLocationController@cities');
        });
        Route::group(array('prefix' => 'city', 'before' => 'can:manage_locations'), function() {
            Route::get('active/{id}', 'BackendLocationController@city_active');
            Route::get('deactive/{id}', 'BackendLocationController@city_deactive');
        });

        /*
        |--------------------------------------------------------------------------
        | Bairros
        |--------------------------------------------------------------------------
        */
        Route::group(array('prefix' => 'districts'), function() {
            Route::get('{city}/{letter}', 'BackendLocationController@districts');
        });
        Route::group(array('prefix' => 'district', 'before' => 'can:manage_locations'), function() {
            Route::get('active/{id}', 'BackendLocationController@district_active');
            Route::get('deactive/{id}', 'BackendLocationController@district_deactive');

            Route::get('new/{city}/{letter}', 'BackendLocationController@district_new');
            Route::get('edit/{id}/{letter}', 'BackendLocationController@district_edit');
            Route::get('remove/{id?}', 'BackendLocationController@district_remove');

            Route::group(array('before' => 'csrf'), function() {
                Route::post('save', 'BackendLocationController@district_save');
            });
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Logs de erros
    |--------------------------------------------------------------------------
    */
    Route::group(array('prefix' => 'log', 'before' => 'can:view_log'), function() {
        Route::get('', 'BackendLogController@index');
        Route::get('clear', 'BackendLogController@clear');
    });

});

Route::get('feed', function(){
    $feed = Feed::make();
    $feed->setCache(10);

    if (!$feed->isCached()) {
       $jobs = Job::where('status', 2)
        ->where('active', 1)
        ->orderBy('created_at', 'desc')
        ->take(20)
        ->get();

       $feed->title = Config::get('domain.site.name');
       $feed->description = Config::get('domain.site.description');
       $feed->logo = Config::get('domain.site.url') .'/domains/'. Config::get('domain.folder') .'/images/logotipo.png';
       $feed->link = URL::to('feed');
       $feed->setDateFormat('datetime');
       $feed->pubdate = ($jobs->count()) ? $jobs[0]->created_at : Carbon::now();
       $feed->lang = 'pt';
       $feed->setShortening(true);
       $feed->setTextLimit(100);

       foreach ($jobs as $job) {
            $feed->add(
                $job->position,
                $job->company,
                URL::action("JobController@short", array($job->id)),
                $job->created_at,
                $job->city->name ."/". $job->estate->short,
                $job->description
            );
       }
    }

    return $feed->render('atom');
});
