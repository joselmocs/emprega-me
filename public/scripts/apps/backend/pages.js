/*!
 * apps.backend.pages.js
 * Scripts responsáveis pelo gerenciamento de páginas.
 *
 * Joselmo Cardozo
 */

require([
    "helpers/core"
], function(core) {
    var
        initialize = function() {
            events();
        },
        events = function() {
            $('a[name=remover]').unbind('click').click(function() {
                if (confirm('Deseja remover a página *'+ $(this).attr('data-name') +'*?')) {
                    window.location = core.context.urls.remove +'/'+ $(this).attr('data-id');
                }
            });
        };

    initialize();
});