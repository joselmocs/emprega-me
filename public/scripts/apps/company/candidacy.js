/*!
 * apps.company.candidacy.js
 * Scripts responsáveis pela exibição da lista de candidatos de uma vaga
 *
 * Joselmo Cardozo
 */

require([
    "helpers/core"
], function(core) {
    var
        initialize = function() {
            events();
        },
        events = function() {
            $('a[name=remover]').unbind('click').click(function() {
                if (confirm('Deseja arquivar esta candidatura *'+ $(this).attr('data-name') +'*?')) {
                    window.location = core.context.urls.remove +'/'+ $(this).attr('data-id');
                }
            });
        };

    initialize();
});
