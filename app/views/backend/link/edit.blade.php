@extends("backend/template")

@section("content")
<div id="tipos_trabalho_adicionar-editar"><!--tipos_trabalho_adicionar-editar-->

    <form action="" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <div class="titulo_pequeno"><!--titulo_medio-->
            Adicionar Link:
        </div><!--titulo_medio-->

        <div style="margin: 0 0 10px 0" class="moldura_cinza formulario"><!--div margin-->
            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                <tr>
                    <td align="left" valign="bottom">Nome:</td>
                </tr>
                <tr>
                    <td align="left">
                        <input type="text" style="width: 362px;" name="name" required="required" value="{{ $context->link->name }}" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="bottom">Título:</td>
                </tr>
                <tr>
                    <td align="left">
                        <input type="text" style="width: 362px;" name="title" required="required" value="{{ $context->link->title }}" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="bottom">URL:</td>
                </tr>
                <tr>
                    <td align="left">
                        <input type="text" style="width: 362px;" name="url" required="required" value="{{ $context->link->url }}" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="bottom">Nova página?</td>
                </tr>
                <tr>
                    <td align="left">
                        <select name="newpage" required="required">
                            <option value="0" @if(!$context->link->newpage) selected="selected" @endif>Não</option>
                            <option value="1" @if($context->link->newpage) selected="selected" @endif>Sim</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td height="20" align="left" valign="bottom">Posição:</td>
                </tr>
                <tr>
                    <td align="left">
                        <select name="position" required="required">
                            <option value="1" @if($context->link->position == 1) selected="selected" @endif>Cabeçalho</option>
                            <option value="2" @if($context->link->position == 2) selected="selected" @endif>Rodapé</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div><!--div margin-->

        <div class="moldura_cinza formulario"><!--div margin-->
            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                <tr>
                    <td align="left">
                        <input type="submit" value="Editar" />
                    </td>
                </tr>
            </table>
        </div><!--div margin-->
    </form>
</div><!--tipos_trabalho_adicionar-editar-->

@endsection