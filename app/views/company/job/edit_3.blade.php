@extends('base/template')

@section("site-title")
Anuncie um novo emprego - {{ Config::get('domain.site.title') }}
@endsection

@section('content')
@include("advertising/header")
<div class="page_template page_template_links"><!--page_template-->

<div class="titulo_grande"><!--titulo_grande-->
Vaga cadastrada
</div><!--titulo_grande-->

<div style="font-size: 12px; margin: 0 0 25px 0;"><!--div-->
Obrigado <b>{{ Auth::user()->profile->fullname }}</b>, seu anúncio foi enviado para análize, e se aprovado será publicado em algumas horas.
<br />
Este período de análize pode demorar até <b>8 horas</b>.
</div><!--div-->


<div style="font-size: 12px;">
Quer cadastrar uma nova vaga? <a href="{{ URL::action('CompanyJobController@register') }}" title="Cadastrar nova vaga">clique aqui</a>.
<br/ >
Para voltar a página inicial <a href="/" title="Voltar para pagina inicial">clique aqui</a>.
</div>

</div><!--page_template-->

</div><!--container-->
@endsection