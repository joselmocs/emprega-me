@extends("base/template")

@section("content")
<div id="cadastrar-editar_candidato_4" class="page_template"><!--cadastrar-editar_candidato_4-->

    <div class="titulo_medio"><!--titulo_medio-->
        Editar dados
    </div><!--titulo_medio-->

    <div class="cadastrar_sleps" style="margin-bottom: 20px;"><!--cadastrar_sleps-->
        <div id="cadastrar_slep_1">
            <b>1</b> Preenchimento de dados
        </div>

        <div id="cadastrar_slep_4" class="cadastrar_slep_active">
            <b>2</b> Finalização
        </div>
    </div>

    <div id="cadastrar-editar_candidato_4_texto" style="margin: 0 0 25px 0"><!--cadastrar-editar_candidato_4_texto-->
        Parabéns <b>{{ Auth::user()->profile->fullname }}</b>, a empresa foi atualizada com sucesso!!!
        <br/>
        Você já pode começar a divulgar suas vagas, <a href="{{ URL::action('CompanyJobController@lists', array('-')) }}" title="Cadastrar nova vaga.">clique aqui</a>
    </div><!--cadastrar-editar_candidato_4_texto-->

    <span style="font-size:12px" class="page_template_links">Para voltar a página inicial <a href="/" title="Voltar para pagina inicial.">clique aqui</a>.</span>

</div><!--cadastrar-editar_candidato_4-->
@endsection
