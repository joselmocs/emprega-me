@extends("backend/template")

@section("content")

<div id="paginas" class="page_template"><!--paginas-->

<div class="titulo_grande" style="text-transform: uppercase;"><!--titulo_grande-->
<img src="/images/backend/icone_localizacoes.jpg"/> LOCALIZAÇÕES - {{ strtoupper($context->estate->name .'/'. $context->estate->short) }} - CIDADES COM A LETRA {{ strtoupper($context->letter) }}
</div><!--titulo_medio-->

<div class="page_count-letter"><!--page_count-letter-->
@foreach(range('A', 'Z') as $letter)
<li><a href="{{ $context->url_to_filter }}/{{ strtolower($letter) }}" title="Cidades com a letra {{ $letter }}" @if(strtoupper($context->letter) == $letter) class="active" @endif>{{ $letter }}</a></li>
@endforeach
</div><!--page_count-letter-->

@foreach($context->cities as $i => $city)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a href="{{ URL::action('BackendLocationController@districts', array(strtolower($city->slug), 'a')) }}" title="{{ $city->name }}">{{ $city->name }}</a>
</span>

<span class="counter">
<small><b>000</b> vagas cadastradas</small>
</span>
</div><!--row/row-alt-->
@endforeach

</div><!--paginas-->

</div><!--container-->
@endsection
