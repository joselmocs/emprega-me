@extends("backend/template")

@section('requirejs')
requirejs(["apps/backend/advertising.banner"]);
@endsection

@section("content")
<div id="publicidade_editar_banner" class="page_template"><!--publicidade-->

    <div class="botao"><!--botao-->
        <li><a title="Adicionar banner" href="{{ URL::action('BackendAdvertisingController@banners_new', array($context->banner_type->short)) }}">Adicionar</a></li>
    </div><!--botao-->

    <div class="titulo_medio" style="text-transform: uppercase;"><!--titulo_medio-->
        <img src="/images/backend/icone_publicidade.jpg"/> {{ $context->banner_type->name }}
    </div><!--titulo_medio-->

    @if (!$context->banners->count())
    <div class="row"><!--row/row-alt-->
        <span class="row-info">
        <img src="/images/set_right.png" /><a>Nenhum banner cadastrado.</a>
        </span>
    </div>
    @else
    @foreach($context->banners as $i => $banner)
    <div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
        <div id="row_1"><!--row_1-->
            <span class="row-info">
                <img src="/images/set_right.png" />
                <a href="{{ $banner->url }}" title="{{ $banner->title }}" target="_blank">{{ $banner->name }}</a>
            </span>

            <div class="row_menu">
                @if ($banner->active)
                <a href="{{ URL::action('BackendAdvertisingController@banners_deactive', array($banner->id)) }}"><img src="/images/backend/row_menu_power_on.png" title="Ligado" /></a>
                @else
                <a href="{{ URL::action('BackendAdvertisingController@banners_active', array($banner->id)) }}"><img src="/images/backend/row_menu_power_off.png" title="Desligado"/></a>
                @endif
            </div>

            <div class="row_menu">
                <a href="{{ $banner->url }}" target="_blank"><img src="/images/backend/row_menu_acessar_vaga.png" title="Acessar site"></a>
                <a title="Editar banner" href="{{ URL::action('BackendAdvertisingController@banners_edit', array($banner->id)) }}"><img src="/images/backend/row_menu_editar.png" title="Editar"/></a>
                <a href="javascript:void(0);" name="remover" data-name="{{ $banner->name }}" data-id="{{ $banner->id }}"><img src="/images/backend/row_menu_remover.png" title="Remover"/></a>
            </div>
        </div><!--row_1-->

        <div><!--row_2-->
            <img src="{{ $banner->image }}" title="" />
        </div><!--row_2-->

        <div id="row_2"><!--row_2-->
            <div class="row_2-info">▪ Limite de Visualizações: <span>{{ $banner->max_views }} vezes</span></div>
            <div class="row_2-info" style="margin-left: 10px;">▪ Visualizado: <span>{{ $banner->views }} vezes</span></div>
            <div class="row_2-info" style="margin-left: 10px;">▪ Domínio:
            @if ($banner->domain == "")<span>Todos</span>@else <span>{{ ucfirst($banner->domain) }}</span>@endif
            </div>
        </div><!--row_2-->
    </div><!--row/row-alt-->
    @endforeach
    @endif

</div><!--publicidade-->

@endsection
