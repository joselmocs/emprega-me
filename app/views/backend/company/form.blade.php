@extends("backend/template")

@section("requirejs")
requirejs(["apps/company/register"])
requirejs(["apps/backend/companies"]);
@endsection

@section("content")
<div id="tipos_trabalho_adicionar-editar"><!--tipos_trabalho_adicionar-editar-->

    <div class="titulo_medio"><!--titulo_medio-->
        @if ($context->company_id) Editar @else Adicionar @endif Empresa:
    </div><!--titulo_medio-->

    @if ($context->company_id)
    <form action="{{ URL::action('BackendCompanyController@register', array($context->company_id)) }}" method="post">
    @else
    <form action="{{ URL::action('BackendCompanyController@register') }}" method="post">
    @endif

    <input type="hidden" name="_token" value="{{  csrf_token() }}" />

    <div style="margin: 0 0 25px 0;"><!--div margin-->

        <div class="titulo_pequeno"><!--titulo_medio-->
            Dados de acesso:
        </div><!--titulo_medio-->

        <table width="100%" border="0" cellspacing="2" cellpadding="0" class="formulario moldura_cinza">
            <tr>
                <td width="160" align="right">E-mail:</td>
                <td align="left">
                    <input type="email" style="width: 250px;" name="email" required="true" value="{{ $context->input->email }}"
                    @if ($context->field_errors && $context->field_errors->has('email')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('email'))
                    <div class="error">{{ $context->field_errors->first('email') }}</div>
                    @endif
                </td>
            </tr>
            <tr class="tr-password" @if (!$context->input->password_change) style="display:none;" @endif>
                <td width="160" align="right">Senha:</td>
                <td align="left">
                    <input type="password" style="width: 175px;" name="password" @if ($context->input->password_change) required="true" @endif
                    @if ($context->field_errors && $context->field_errors->has('password')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('password'))
                    <div class="error">{{ $context->field_errors->first('password') }}</div>
                    @endif
                </td>
            </tr>
            <tr class="tr-password" @if (!$context->input->password_change) style="display:none;" @endif>
                <td width="160" align="right">Confirmar senha:</td>
                <td align="left">
                    <input type="password" style="width: 175px;" name="password_confirm" @if ($context->input->password_change) required="true" @endif
                    @if ($context->field_errors && $context->field_errors->has('password_confirm')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('password_confirm'))
                    <div class="error">{{ $context->field_errors->first('password_confirm') }}</div>
                    @endif
                </td>
            </tr>
            @if ($context->company_id)
            <tr>
                <td width="160" align="right">Alterar senha:</td>
                <td align="left">
                    <table width="0%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <input type="checkbox" style="width: 175px;" name="password_change" @if ($context->input->password_change) checked="checked" @endif />
                            </td>
                            <td style="color: #999999;"><small>(Marque para alterar a senha desta empresa.)</small></td>
                        </tr>
                    </table>
                </td>
            </tr>
            @endif
        </table>

    </div><!--div margin-->

    <div style="margin: 0 0 25px 0"><!--div margin-->

        <div class="titulo_pequeno"><!--titulo_medio-->
            Dados do empregador/responsável:
        </div><!--titulo_medio-->

        <table width="100%" border="0" cellspacing="2" cellpadding="0" class="formulario moldura_cinza">
            <tr>
                <td width="160" align="right">Nome:</td>
                <td align="left">
                    <input type="text" style="width: 350px;" name="fullname" required="true" value="{{ $context->input->fullname }}"
                    @if ($context->field_errors && $context->field_errors->has('fullname')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('fullname'))
                    <div class="error">{{ $context->field_errors->first('fullname') }}</div>
                    @endif
                </td>
            </tr>
            <tr>
                <td width="160" align="right">Empresa:</td>
                <td align="left">
                    <input type="text" style="width: 350px;" name="company" required="true" value="{{ $context->input->company }}"
                    @if ($context->field_errors && $context->field_errors->has('company')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('company'))
                    <div class="error">{{ $context->field_errors->first('company') }}</div>
                    @endif
                </td>
            </tr>
            <tr>
                <td width="160" align="right">Estado:</td>
                <td align="left">
                    <select name="estate_id" required="true">
                        <option value="">Selecione</option>
                        @foreach(LocationEstate::where('active', '=', 1)->get() as $estate)
                        <option value="{{ $estate->id }}"  @if($context->input->estate_id == $estate->id) selected="selected" @endif>{{ $estate->name }}</option>
                        @endforeach
                    </select>
                    @if ($context->field_errors && $context->field_errors->has('estate_id'))
                    <div class="error">{{ $context->field_errors->first('estate_id') }}</div>
                    @endif
                </td>
            </tr>
            <tr>
                <td width="160" align="right">Cidade:</td>
                <td align="left">
                    <select name="city_id" required="true">
                        <option value="">Selecione</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td width="160" align="right">Telefone:</td>
                <td align="left">
                    <table width="0%" border="0" cellspacing="0" cellpadding="0">
                        <tbody><tr>
                            <td>
                                <input type="text" style="width: 200px;" name="phone" value="{{ $context->input->phone }}" required="true">
                                @if ($context->field_errors && $context->field_errors->has('phone'))
                                <div class="error">{{ $context->field_errors->first('phone') }}</div>
                                @endif
                            </td>
                            <td style="color: #999999;"><small>(não será publicado)</small></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="160" align="right" style="color: #999999;">Website:</td>
                <td align="left">

                    <table width="0%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <input type="text" style="width: 175px;" name="website" value="{{ $context->input->website }}" />
                            </td>
                            <td style="color: #999999;"><small>(opcional)</small></td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>

    </div><!--div margin-->

    <div class="formulario moldura_cinza"><!--moldura_cinza-->
        <table width="100%" border="0" cellspacing="2" cellpadding="0">
            <tr>
                <td width="160"></td>
                <td align="left">
                    @if ($context->company_id)
                    <input type="submit" value="Editar" />
                    @else
                    <input type="submit" value="Adicionar" />
                    @endif
                </td>
            </tr>
        </table>
    </div><!--moldura_cinza-->


</div><!--tipos_trabalho_adicionar-editar-->


@endsection
