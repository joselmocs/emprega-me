@extends("backend/template")

@section('requirejs')
requirejs(["apps/backend/locations"]);
@endsection

@section("content")
<div id="paginas" class="page_template"><!--paginas-->

<div class="botao"><!--botao-->
@if (Auth::user()->can('manage_locations'))
<li><a href="{{ URL::action('BackendLocationController@district_new', array($context->city->id, $context->letter)) }}">Adicionar bairro</a></li>
@endif
</div><!--botao-->

<div class="titulo_grande" style="text-transform: uppercase;"><!--titulo_grande-->
<img src="/images/backend/icone_localizacoes.jpg"/> LOCALIZAÇÕES - {{ strtoupper($context->city->name .'/'. $context->city->estate->short) }} - BAIRROS COM A LETRA {{ strtoupper($context->letter) }}
</div><!--titulo_medio-->

<div class="page_count-letter"><!--page_count-letter-->
@foreach(range('A', 'Z') as $letter)
<li><a href="{{ $context->url_to_filter }}/{{ strtolower($letter) }}" title="Bairros com a letra {{ $letter }}" @if(strtoupper($context->letter) == $letter) class="active" @endif>{{ $letter }}</a></li>
@endforeach
</div><!--page_count-letter-->

@if (!$context->districts->count())
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a>Nenhum bairro com a letra <b>{{ strtoupper($context->letter) }}</b> cadastrado.</a>
</span>
</div>
@else
@foreach($context->districts as $i => $district)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a href="javascript: void(0);" title="{{ $district->name }}">{{ $district->name }}</a>
</span>

@if (Auth::user()->can('manage_locations'))
<div class="row_menu">
<a href="{{ URL::action('BackendLocationController@district_edit', array($district->id, $context->letter)) }}"><img src="/images/backend/row_menu_editar.png" title="Editar"/></a>
<a href="javascript:void(0)" name="remover" data-name="{{ $district->name }}" data-id="{{ $district->id }}"><img src="/images/backend/row_menu_remover.png" title="Remover"/></a>
</div>
@endif
</div><!--row/row-alt-->
@endforeach
@endif

</div><!--paginas-->

</div><!--container-->
@endsection
