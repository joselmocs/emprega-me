@extends('base/template')

@section("site-title")
Editar currículo - {{ Config::get('domain.site.title') }}
@endsection

@section('requirejs')
requirejs(["apps/user/curriculum.edit"]);
@endsection

@section('styles')

@endsection

@section('scripts')

@endsection

@section('content')

<div id="cadastrar-editar_candidato_1" class="page_template"><!--cadastrar-editar_candidato_1-->
    <div class="titulo_medio"><!--titulo_medio-->
        Editar Currículo
    </div><!--titulo_medio-->

    <div class="cadastrar_sleps"><!--cadastrar_sleps-->
        <div class="formulario" style="float: right;">
            <input type="button" value="Salvar Currículo" id="save_curriculum">
        </div>
    </div>

    @include('candidate/curriculum/edit/personal')
    @include('candidate/curriculum/edit/formation')
    @include('candidate/curriculum/edit/experience')
</div>
@endsection
