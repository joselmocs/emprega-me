<?php

use JCS\Auth\Models\Role;


class BackendCompanyController extends BaseBackendController {

    /**
     * Exibe a lista de empresas no backend
     *
     * @param string $estates
     * @param string $letter
     * @return Illuminate\Support\Facades\View
     */
    public function index($estates = '-', $letter = '-')
    {
        $companies = User::select('users.id', 'users.email');
        $group = Role::where('tag', '=', 'company')->firstOrFail();

        $companies->join('user_role_user', 'user_role_user.user_id', '=', 'users.id')
            ->where('user_role_user.role_id', '=', $group->id);

        $this->set_context(array(
            'total_companies' => $companies->count()
        ));

        $companies->join('user_profile', 'user_profile.user_id', '=', 'users.id');

        if ($letter == ' ') {
            $companies->whereRaw("user_profile.company REGEXP '^[0-9]'");
        }
        elseif ($letter != '-') {
            $companies->where('user_profile.company', 'like', $letter .'%');
        }

        $new_search = false;
        if ($estates == '-') {
            if ($letter == '-') {
                $new_search = true;
            }
        }
        else {
            $explode_estates = explode(',', $estates);
            $list_estates = array();

            foreach ($explode_estates as $short) {
                $estate = LocationEstate::where('short', '=', strtoupper($short))->firstOrFail();
                array_push($list_estates, $estate->id);
            }

            $companies->join('location_cities', 'location_cities.id', '=', 'user_profile.city_id')
                ->whereIn('location_cities.estate_id', $list_estates);
        }

        $this->set_json_context(array(
            'urls' => array(
                'remove' => URL::action('BackendCompanyController@remove')
            )
        ));

        $this->set_context(array(
            'filter' => (object) array(
                'url' => URL::action('BackendCompanyController@index', array($estates, '')),
                'estates' => $estates
            ),
            'new_search' => $new_search,
            'estates' => LocationEstate::all(),
            'companies' => ($new_search) ? $companies->limit(50)->get() : $companies->paginate(50),
            'letter' => $letter
        ));

        return $this->view_make('backend/company/index');
    }

    /**
     * Exibe o formulário de criação e edição de empresas no backend
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function register($id = null)
    {
        if ($id) {
            $company = User::find($id);
            $this->set_context(array(
                'company_id' => $company->id,
                'input' => (object) array(
                    'email' => Input::get('email', $company->email),
                    'password' => Input::get('password', ''),
                    'password_confirm' => Input::get('password', ''),
                    'fullname' => Input::get('fullname', $company->profile->fullname),
                    'company' => Input::get('company', $company->profile->company),
                    'estate_id' => Input::get('estate_id', $company->profile->city->estate_id),
                    'phone' => Input::get('phone', $company->profile->phone),
                    'website' => Input::get('website', $company->profile->website),
                    'password_change' => Input::get('password_change') == "on"
                )
            ));
            $this->set_json_context(array(
                'data' => (object) array(
                    'estate_id' => Input::get('estate_id', $company->profile->city->estate->id),
                    'city_id' => Input::get('city_id', $company->profile->city_id),
                )
            ));
        }
        else {
            $company = new User;
            $this->set_context(array(
                'company_id' => null,
                'input' => (object) array(
                    'email' => Input::get('email', ''),
                    'password' => Input::get('password', ''),
                    'password_confirm' => Input::get('password', ''),
                    'fullname' => Input::get('fullname', ''),
                    'company' => Input::get('company', ''),
                    'estate_id' => Input::get('estate_id'),
                    'phone' => Input::get('phone'),
                    'website' => Input::get('website'),
                    'password_change' => true
                )
            ));
            $this->set_json_context(array(
                'data' => (object) array(
                    'estate_id' => Input::get('estate_id', Input::get('estate_id')),
                    'city_id' => Input::get('city_id', Input::get('city_id')),
                )
            ));
        }

        if (Request::isMethod('get')) {
            return $this->view_make('backend/company/form');
        }

        $validate = array(
            "email" => ($id) ? "required|unique:users,email,". $company->id : "required|unique:users",
            "password" => "required_if:password_change,on|min:3|max:30",
            "password_confirm" => "required_if:password_change,on|same:password",
            "fullname" => "required|min:2|max:256",
            "company" => "required|min:2|max:256",
            "phone" => "required",
            "estate_id" => "required|numeric",
            "city_id" => "required|numeric"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('backend/company/form');
        }

        DB::transaction(function() use ($id, $company) {
            $company->email = Input::get('email');
            if (!$id || Input::get('password_change') == "on") {
                $company->password = Input::get('password');
            }
            $company->verified = 1;
            $company->save();
            $company->roles()->sync(array(Role::where('tag', '=', 'company')->firstOrFail()->id));

            $company->profile->city_id = Input::get('city_id');
            $company->profile->fullname = Input::get('fullname', '');
            $company->profile->company = Input::get('company', '');
            $company->profile->website = Input::get('website');
            $company->profile->phone = Input::get('phone');
            $company->profile->save();
        });

        return Redirect::action('BackendCompanyController@index', array('-', '-'));
    }

    /**
     * Remove uma empresa.
     *
     * @param $id int
     * @return Redirect
     */
    public function remove($id)
    {
        if ($id) {
            User::find($id)->delete();
        }
        return Redirect::back();
    }

    /**
     * Lista as vagas de emprego cadastrados na empresa
     *
     * @param integer $company
     * @return Illuminate\Support\Facades\View
     */
    public function jobs($company)
    {
        $profile = User::find($company)->profile;
        $this->set_context(array(
            'profile' => $profile,
            'job_types' => JobType::all()
        ));

        $this->set_json_context(array(
            'urls' => array(
                'remove' => URL::action('BackendCompanyController@remove_job')
            )
        ));

        return $this->view_make('backend/company/jobs');
    }

    /**
     * Altera o estado de destaque da vaga de emprego
     *
     * @param int $job
     * @return Illuminate\Support\Facades\Redirect
     */
    public function feature($job)
    {
        $job = Job::find($job);
        $job->featured = ($job->featured) ? false : true;
        $job->save();

        return Redirect::back();
    }

    /**
     * Aprova a vaga de emprego
     *
     * @param int $job
     * @return Illuminate\Support\Facades\Redirect
     */
    public function approve($job)
    {
        $job = Job::find($job);
        $job->status = Job::APPROVED;
        $job->viewed = true;
        $job->save();

        if (Config::get('app.mail')) {
            $message = View::make('company/email/job_approved')
                ->with('site_name', Config::get('domain.site.name'))
                ->with('fullname', $job->profile->fullname)
                ->with('job', $job)
                ->render();

            $campaign = new \JCS\Mail\Campaign();
            $campaign->addAddress(new \JCS\Mail\ArraySource(array($job->email)));

            $campaign
                ->setMessage("Vaga aprovada", $message, "html")
                ->setMode(\JCS\Mail\Models\Queue::MODE_SINGLE)
                ->save();
            $campaign->execute();
        }

        return Redirect::back();
    }

    /**
     * Reprova a vaga de emprego
     *
     * @param int $job
     * @return Illuminate\Support\Facades\Redirect
     */
    public function reprove($job)
    {
        $job = Job::find($job);
        $job->status = Job::REPROVED;
        $job->viewed = true;
        $job->save();

        return Redirect::back();
    }

    /**
     * Remove uma vaga.
     *
     * @param $id int
     * @return Redirect
     */
    public function remove_job($id)
    {
        if ($id) {
            Job::find($id)->delete();
        }
        return Redirect::back();
    }

    /**
     * Página de edição de vaga
     *
     * @param int $id
     * @return Illuminate\Support\Facades\View
     */
    public function edit($id)
    {
        $job = Job::find($id);
        $job->viewed = true;
        $job->save();

        $this->set_json_context(array(
            'input' => (object)array(
                'estate_id' => Input::get('estate_id', $job->city->estate->id),
                'city_id' => Input::get('city_id', $job->city->id),
                'district_id' => Input::get('district_id', ($job->district) ? $job->district->id : null)
            )
        ));

        $this->set_context(array(
            'editing' => true,
            'job_types' => JobType::all(),
            'categories' => Category::orderBy('name')->get(),
            'estates' => LocationEstate::all(),
            'input' => (object)array(
                'type_id' => Input::get('type_id', $job->type_id),
                'category_id' => Input::get('category_id', $job->category_id),
                'position' => Input::get('position', $job->position),
                'estate_id' => Input::get('estate_id', $job->city->estate->id),
                'description' => Input::get('description', $job->description),
                'company' => Input::get('company', $job->company),
                'email' => Input::get('email', $job->email),
                'website' => Input::get('website', $job->website),
                'phone' => Input::get('phone', $job->phone)
            ),
        ));

        if (Request::isMethod('get')) {
            return $this->view_make('backend/job/form');
        }

        $validate = array(
            "type_id" => "required|numeric",
            "category_id" => "required|numeric",
            "position" => "required|min:3|max:256",
            "description" => "required",
            "estate_id" => "required|numeric",
            "city_id" => "required|numeric",
            "company" => "required",
            "email" => "required|email"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('backend/job/form');
        }

        $job->type_id = Input::get('type_id');
        $job->category_id = Input::get('category_id');
        $job->city_id = Input::get('city_id');
        if (Input::get('district_id')) {
            $job->district_id = Input::get('district_id');
        }
        $job->position = trim(Input::get('position'));
        $job->description = Input::get('description');
        $job->company = trim(Input::get('company'));
        $job->email = trim(Input::get('email'));
        $job->website = trim(Input::get('website'));
        $job->phone = Input::get('phone');

        if (intval(Input::get('action')) == 2) {
            $job->status = JOB::APPROVED;
        }

        $job->edited = true;
        $job->save();

        if (Input::get('next')) {
            return Redirect::to(Input::get('next'));
        }

        return Redirect::back();
    }
}
