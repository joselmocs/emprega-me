<?php


class CategoryController extends BaseController {

    /**
     * Página de listagem de categoria
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        $this->set_context(array(
            'categories' => Category::where('active', true)->get()
        ));

        return $this->view_make('category/index');
    }

    /**
     * Página de listagem de vagas da categoria
     *
     * @param int $category
     * @param string $job_type
     * @param string $estate
     * @param string $city
     * @return Illuminate\Support\Facades\View
     */
    public function view($category, $job_type, $estate, $city)
    {
        try {
            $category = Category::where('url', $category)->where('active', true)->firstOrFail();
        }
        catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            App::abort(404);
        }

        $estate = LocationEstate::where('id', Session::get('estate'))->firstOrFail();

        if ($city != '-') {
            $city = LocationCity::where('slug', $city)->firstOrFail();
        }

        if ($job_type != '-') {
            $job_type = JobType::where('slug', $job_type)->firstOrFail();
        }

        $jobs = Job::where('category_id', $category->id)
            ->where('status', Job::APPROVED)
            ->where('active', true)
            ->where('estate_id', Session::get('estate'));

        if ($job_type != '-') {
            $jobs->where('type_id', $job_type->id);
        }

        if ($city != '-') {
            $jobs->where('city_id', $city->id);
        }

        $location = array(
            'estate_id' => Session::get('estate'),
            'city_id' => ($city != '-') ? $city->id : '-'
        );

        $this->set_context(array(
            'input' => (object) $location
        ));

        $this->set_json_context(array(
            'data' => (object) $location
        ));

        $featured = null;
        $busiest = null;
        if ($city == '-') {
            $featured = JobController::featured_category($category->id)->get();
            $busiest = JobController::busiest_category($category->id)->get();
        }

        $this->set_context(array(
            'jobs' => $jobs->paginate(Configuration::firstOrFail()->jobs_category),
            'estate_selected' => ($estate != '-') ? strtolower($estate->short) : '-',
            'city_selected' => ($city != '-') ? $city->slug : '-',
            'job_types' => JobType::all(),
            'categories' => Category::where('active', true)->orderByRaw('RAND()')->limit(10)->get(),
            'featured_category' => $featured,
            'busiest_category' => $busiest,
            'category' => $category
        ));

        return $this->view_make('category/list');
    }

}
