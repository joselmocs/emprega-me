<?php

use Illuminate\Database\Migrations\Migration;


class CreateCategoriesTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the categories table
        Schema::create('categories', function($table)
        {
            $table->increments('id');
            $table->string('name', 128);
            $table->string('title', 256);
            $table->string('url', 128);
            $table->text('description')->nullable();
            $table->text('keywords')->nullable();
            $table->integer('count')->default(0);
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }

}
