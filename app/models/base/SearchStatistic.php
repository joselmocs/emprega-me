<?php


class SearchStatistic extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'search_statistic';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('keyword');

    public static function latest_100($estate)
    {
        $estate = intval($estate);
        $stats = SearchStatistic::select();
        if ($estate) {
            $stats->where('estate_id', $estate);
        }

        return $stats
            ->orderBy('created_at', 'desc')
            ->take(100);
    }

    public static function today($estate)
    {
        $stats = SearchStatistic::select('search_statistic.*');
        if ($estate > 0) {
            $stats->where('search_statistic.estate_id', '=', $estate);
        }

        $stats
            ->whereBetween('search_statistic.created_at',
                array(
                    Carbon::now()->startOfDay(),
                    Carbon::now()->endOfDay()
                )
            );

        return $stats->count();
    }

    public static function yesterday($estate)
    {
        $stats = SearchStatistic::select('search_statistic.*');
        if ($estate > 0) {
            $stats->where('search_statistic.estate_id', '=', $estate);
        }

        $stats
            ->whereBetween('search_statistic.created_at',
                array(
                    Carbon::now()->subDay()->startOfDay(),
                    Carbon::now()->subDay()->endOfDay()
                )
            );

        return $stats->count();
    }

    public static function week($estate)
    {
        $stats = SearchStatistic::select('search_statistic.*');
        if ($estate > 0) {
            $stats->where('search_statistic.estate_id', '=', $estate);
        }

        $stats
            ->whereBetween('search_statistic.created_at',
                array(
                    Carbon::now()->subDays(6)->startOfDay(),
                    Carbon::now()->endOfDay()
                )
            );

        return $stats->count();
    }

    public static function month($estate)
    {
        $stats = SearchStatistic::select('search_statistic.*');
        if ($estate > 0) {
            $stats->where('search_statistic.estate_id', '=', $estate);
        }

        $stats
            ->whereBetween('search_statistic.created_at',
                array(
                    Carbon::now()->subMonth()->startOfDay(),
                    Carbon::now()->endOfDay()
                )
            );

        return $stats->count();
    }

}
