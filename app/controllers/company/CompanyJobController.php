<?php


class CompanyJobController extends BaseController {

    /**
     * Página de criação de vaga
     *
     * @return Illuminate\Support\Facades\View
     */
    public function register()
    {
        $this->set_json_context(array(
            'input' => (object) array(
                'estate_id' => Input::get('estate_id', Session::get('estate')),
                'city_id' => Input::get('city_id'),
                'district_id' => Input::get('district_id')
            )
        ));

        $profile = Auth::user()->profile;

        $profile_names = array($profile->company, $profile->fullname, '',);
        foreach($profile->names as $name) {
            array_push($profile_names, trim($name->name));
        }

        $this->set_context(array(
            'editing' => false,
            'job_types' => JobType::all(),
            'categories' => Category::orderBy('name')->get(),
            'estates' => LocationEstate::all(),
            'profile_names' => $profile_names,
            'input' => (object) array(
                'type_id' => Input::get('type_id'),
                'category_id' => Input::get('category_id'),
                'position' => Input::get('position'),
                'estate_id' => Input::get('estate_id', Session::get('estate')),
                'description' => Input::get('description'),
                'company' => Input::get('company', $profile->company),
                'email' => Input::get('email', $profile->user->email),
                'show_website' => Input::get('show_website', false),
                'show_phone' => Input::get('show_phone', false),
                'website' => Input::get('website'),
                'phone' => Input::get('phone')
            ),
        ));

        if (Request::isMethod('get') || Input::get('submit') == 'edit') {
            return $this->view_make('company/job/edit_1');
        }

        $validate = array(
            "type_id" => "required|numeric",
            "category_id" => "required|numeric",
            "position" => "required|min:3|max:256",
            "description" => "required",
            "estate_id" => "required|numeric",
            "city_id" => "required|numeric",
            "company" => "required",
            "email" => "required|email"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('company/job/edit_1');
        }

        if (Input::get('submit') == 'check') {
            $this->set_context(array(
                'job_type' => JobType::find(Input::get('type_id')),
                'city' => LocationCity::find(Input::get('city_id')),
                'district' => LocationDistrict::find(Input::get('district_id'))
            ));

            return $this->view_make('company/job/edit_2');
        }
        else if (Input::get('submit') == "save") {
            $job = new Job;
            $job->profile_id = $profile->id;
            $job->type_id = Input::get('type_id');
            $job->category_id = Input::get('category_id');
            $job->city_id = Input::get('city_id');
            if (Input::get('district_id')) {
                $job->district_id = Input::get('district_id');
            }
            $job->position = trim(Input::get('position'));
            $job->description = Input::get('description');
            $job->company = trim(Input::get('company'));
            $job->email = trim(Input::get('email'));
            $job->show_website = Input::get('show_website') == "on";
            $job->show_phone = Input::get('show_phone') == "on";
            $job->website = trim(Input::get('website'));
            $job->phone = trim(Input::get('phone'));
            $job->status = JOB::IN_APPROVAL;

            if (trim(Input::get('company')) != $profile->fullname && trim(Input::get('company')) != $profile->company) {
                $profile_name = new UserProfileName;
                $profile_name->name = trim(Input::get('company'));
                $profile_name->profile_id = $profile->id;
                $profile_name->save();

            }

            $job->save();

            return Redirect::action('CompanyJobController@register_complete');
        }

        App::abort(404);
    }

    /**
     * Página de edição de vaga
     *
     * @param int $id
     * @return Illuminate\Support\Facades\View
     */
    public function edit($id)
    {
        $job = Job::find($id);
        $this->set_json_context(array(
            'input' => (object) array(
                'estate_id' => Input::get('estate_id', $job->city->estate->id),
                'city_id' => Input::get('city_id', $job->city->id),
                'district_id' => Input::get('district_id', ($job->district) ? $job->district->id : null)
            )
        ));

        $this->set_context(array(
            'editing' => true,
            'job_types' => JobType::all(),
            'categories' => Category::orderBy('name')->get(),
            'estates' => LocationEstate::all(),
            'input' => (object) array(
                'type_id' => Input::get('type_id', $job->type_id),
                'category_id' => Input::get('category_id', $job->category_id),
                'position' => Input::get('position', $job->position),
                'estate_id' => Input::get('estate_id', $job->city->estate->id),
                'description' => Input::get('description', $job->description),
                'company' => Input::get('company', $job->company),
                'email' => Input::get('email', $job->email),
                'notify' => Input::get('notify', $job->notify),
                'phone' => Input::get('phone', $job->phone),
                'website' => Input::get('website', $job->website)
            ),
        ));

        if (Request::isMethod('get') || Input::get('submit') == 'edit') {
            return $this->view_make('company/job/edit_1');
        }

        $validate = array(
            "type_id" => "required|numeric",
            "category_id" => "required|numeric",
            "position" => "required|min:3|max:256",
            "description" => "required",
            "estate_id" => "required|numeric",
            "city_id" => "required|numeric",
            "company" => "required",
            "email" => "required|email"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('company/job/edit_1');
        }

        if (Input::get('submit') == 'check') {
            $this->set_context(array(
                'job_type' => JobType::find(Input::get('type_id')),
                'city' => LocationCity::find(Input::get('city_id')),
                'district' => LocationDistrict::find(Input::get('district_id'))
            ));

            return $this->view_make('company/job/edit_2');
        }
        else if (Input::get('submit') == "save") {
            $job->type_id = Input::get('type_id');
            $job->category_id = Input::get('category_id');
            $job->city_id = Input::get('city_id');
            if (Input::get('district_id')) {
                $job->district_id = Input::get('district_id');
            }
            $job->position = trim(Input::get('position'));
            $job->code = trim(Input::get('code'));
            $job->description = Input::get('description');
            $job->company = trim(Input::get('company'));
            $job->email = trim(Input::get('email'));
            $job->notify = Input::get('notify') == "on";
            $job->website = trim(Input::get('website'));
            $job->phone = trim(Input::get('phone'));
            $job->status = JOB::IN_APPROVAL;
            $job->edited = false;
            $job->viewed = false;
            $job->save();

            return Redirect::action('CompanyJobController@register_complete');
        }

        App::abort(404);
    }

    public function register_complete()
    {
        return $this->view_make('company/job/edit_3');
    }

    /**
     * Retorna uma view com as regras para criação de vaga
     *
     * @return Illuminate\Support\Facades\View
     */
    public function terms()
    {
        return $this->view_make('company/job/terms');
    }

    /**
     * Lista as vagas de emprego cadastrados na empresa
     *
     * @return Illuminate\Support\Facades\View
     */
    public function lists($view)
    {
        $jobs = Auth::user()->profile->jobs();
        switch ($view) {
            case '0':
                $start = Carbon::now()->startOfDay();
                $end = Carbon::now()->endOfDay();
                break;
            case '1':
                $start = Carbon::now()->subDays(1)->startOfDay();
                $end = Carbon::now()->subDays(1)->endOfDay();
                break;
            case '7':
                $start = Carbon::now()->subDays(7)->startOfDay();
                $end = Carbon::now()->endOfDay();
                break;
            case '30':
                $start = Carbon::now()->subDays(30)->startOfDay();
                $end = Carbon::now()->endOfDay();
                break;
        }

        if ($view != '-') {
            $jobs
                ->whereBetween('created_at', array($start, $end));
        }

        $this->set_context(array(
            'jobs' => $jobs->orderBy('position', 'asc')->get(),
            'reproved' => $jobs->where('status', '=', Job::REPROVED)->count(),
            'view' => $view,
            'job_types' => JobType::all()
        ));

        return $this->view_make('company/job/list');
    }

    /**
     * Visualiza a vaga na listagem de trabalhos do empregador
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function view($id)
    {
        $this->set_context(array(
            'job' => Job::find($id)
        ));

        return $this->view_make('company/job/view');
    }

    /**
     * Exibe uma lista com os candidatos de todas as vagas da empresa
     *
     * @return \Illuminate\View\View
     */
    public function candidacies()
    {
        $candidacies = Auth::user()->profile->candidates()->whereNull('archived_at')->orderBy('created_at', 'asc');

        $this->set_json_context(array(
            'urls' => array(
                'remove' => URL::action('CompanyJobController@job_candidate_archive')
            )
        ));

        $this->set_context(array(
            'candidacies' => $candidacies->paginate(20)
        ));

        return $this->view_make('company/candidacies');
    }

    /**
     * Exibe a lista de candidatos da vaga
     *
     * @param string $job
     * @return \Illuminate\View\View
     */
    public function job_candidacies($job)
    {
        $job = Job::where('id', intval(explode('-', $job)[0]))->firstOrFail();
        $candidacies = JobCandidate::where('job_id', $job->id)
            ->whereNull('archived_at')
            ->orderBy('created_at', 'asc')
            ->paginate(20);

        $this->set_json_context(array(
            'urls' => array(
                'remove' => URL::action('CompanyJobController@job_candidate_archive')
            )
        ));

        $this->set_context(array(
            'job' => $job,
            'candidacies' => $candidacies
        ));

        return $this->view_make('company/job/candidacies');
    }

    /**
     * Arquiva um candidato
     *
     * @param string $id
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function job_candidate_archive($id)
    {
        $myJobs = array_map(function ($job) {
            return intval($job['id']);
        }, Auth::user()->profile->jobs()->get()->toArray());

        $candidato = JobCandidate::where('id', $id)
            ->whereIn('job_id', $myJobs);

        $candidato->update(array(
            'archived_at' => Carbon::now()
        ));

        return Redirect::back();
    }

}
