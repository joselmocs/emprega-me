<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" xmlns:fb="http://ogp.me/ns/fb#">
<head>
    <meta charset="utf-8" />
    <meta content="IE=EDGE" http-equiv="X-UA-Compatible" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('site-title')</title>
    <meta name="description" content="{{ Config::get('domain.site.description') }}">
    <meta name="keywords" content="{{ Config::get('domain.site.keywords') }}">

    <meta property='og:title' content='@yield("site-title")' />
@foreach(Config::get('domain.facebook.image') as $image)
    <meta property='og:image' content='{{ $image }}' />
@endforeach
    <meta property='og:type' content='{{ Config::get("domain.facebook.type") }}' />
    <meta property='og:site_name' content='{{ Config::get("domain.facebook.name") }}' />

    <link rel="stylesheet" href="/styles/screen.css" type="text/css" />
    <link rel="stylesheet" href="/domains/{{ Config::get('domain.folder') }}/styles/screen_colors.css" type="text/css" />
    <link rel="stylesheet" href="/packages/fancybox/source/jquery.fancybox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/packages/fancybox/source/helpers/jquery.fancybox-buttons.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/packages/fancybox/source/helpers/jquery.fancybox-thumbs.css" type="text/css" media="screen" />
    @yield('styles')
</head>

<body>
<div id="fb-root"></div>
<div id="container"><!--container-->

<div id="header"><!--header-->

<div id="header_1"><!--header_1-->
	<div id="header_1_icons">
	<a href="/"><img src="/images/icone_home.png" title="Página inicial"/></a>
	<a href="javascript: void(0);" name="bookmarks"><img src="/images/icone_favorito.png" title="Adicionar aos favoritos"/></a>
	<a href="/m"><img src="/images/icone_movel.png" title="Versão para celular"/></a>
	<a href="{{ Config::get('domain.facebook.url') }}" target="_blank"><img src="/images/icone_facebook.png" title="Página no Facebook"/></a>
	<a href="paginadofacebook"><img src="/images/icone_twitter.png" title="Siga no Twitter"/></a>
	<a href="{{ URL::to('feed') }}" target="_blank"><img src="/images/icone_rss.png" title="Feeds RSS"/></a>
	</div>

	<div id="header_1_menu">
@foreach(Link::where('position', '=', 1)->where('active', '=', true)->orderBY('order', 'asc')->get() as $link)
<li><a href="{{ $link->url }}" title="{{ $link->title }}" @if ($link->newpage) target="_blank" @endif>{{ $link->name }}</a></li>
@endforeach
	</div>
</div><!--header_1-->



<div id="header_2"><!--header_2-->
	<div id="header_2_logotipo">
	<a href="/" title="Ir para a página inicial"><img src="/domains/{{ Config::get('domain.folder') }}/images/logotipo.png"/></a>
	</div>

	<div id="header_2_buscar_vagas-canditatos">
	<table width="0%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td>
@if (Auth::check() && Auth::user()->is('company'))
<form method="get" action="{{ URL::action('CurriculumController@search', array('-', '-')) }}">
<input type="text" name="term" placeholder="Procure candidatos" value="{{ $context->term }}" maxlength="50"><input style="display:none;" type="submit" value="" title="Buscar" />
</form>
@else
<form method="get" action="{{ URL::action('JobController@search') }}">
<input type="text" name="term" placeholder="Procure sua vaga aqui" value="{{ $context->term }}" maxlength="50"><input style="display:none;" type="submit" value="" title="Buscar" />
</form>
@endif
	</td>
	</tr>
	<tr>
	<td height="20" align="right" valign="top">
	@if (Auth::check() && Auth::user()->is('company'))
	<i><b>Exemplos: </b>Cargo, titulo da vaga...</i>
	@else
	<i><b>Exemplos: </b>Auxiliar administrativo, vendedor, advogado, motorista...</i>
	@endif
	</td>
	</tr>
	</table>
	</div>

	<div id="header_2_cadastrar_vaga"><!--header_2_cadastrar_vaga-->
	@if (Auth::check())
	@if (Auth::user()->is('company'))
	<a href="{{ URL::action('CompanyJobController@register') }}" title="Cadastrar vaga">CADASTRAR VAGA</a>
	@else
	<a href="{{ URL::action('JobController@index', array('-', '-', '-')) }}" title="ver vagas">VER VAGAS</a>
	@endif
	@else
	<a href="{{ URL::action('UserController@login') }}?company" title="Cadastrar vaga">CADASTRAR VAGA</a>
	@endif
	</div><!--header_2_cadastrar_vaga-->
</div><!--header_2-->



<div id="header_3"><!--header_3-->

<div id="header_3_todas_categorias"><!--header_3_todas_categorias-->
<a href="" title="Todas as categorias" class="class_categoria">Todas as categorias</a>

<div id="todas_categorias_menu"><!--todas_categorias_menu-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="175" align="center" valign="top" style="border-right: 1px solid #EDEDED;"><!--coluna_1-->

<div id="todas_categorias_menu_data"><!--todas_categorias_menu_data-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="center" style="color: #999999; font-size: 18px;">{{ strtoupper($context->today->format('l')) }}</td>
</tr>
<tr>
<td align="center" class="todas_categorias_menu_dia">{{ $context->today->day }}</td>
</tr>
<tr>
<td align="center" style="color: #999999; font-size: 18px;">{{ strtoupper($context->today->format('F')) }}</td>
</tr>
</table>
</div><!--todas_categorias_menu_data-->

<div id="todas_categorias_menu_set_up"><!--todas_categorias_menu_set_up-->
<img src="/images/set_up_todas_categorias_menu.png"/>
</div><!--todas_categorias_menu_set_up-->

</td><!--coluna_1-->

<td align="left" valign="top"><!--coluna_2-->

<div id="todas_categorias_menu_lista"><!--todas_categorias_menu_2-->

<div id="todas_categorias_menu_col_1" class="todas_categorias_menu_estilo"><!--todas_categorias_menu_col_1-->
<ul>
@foreach(Category::where('active', true)->orderBy('name')->take(6)->get() as $category)
<li><a href="{{ URL::action('CategoryController@view', array($category->url, '-', '-', '-')) }}" title="{{ $category->name }}">{{ Str::limit($category->name, 24, '...') }} <small>({{ $category->jobs->count() }})</small></a></li>
@endforeach
</ul>
</div><!--todas_categorias_menu_col_1-->

<div id="todas_categorias_menu_col_2" class="todas_categorias_menu_estilo"><!--todas_categorias_menu_col_2-->
@foreach(Category::where('active', true)->orderBy('name')->skip(6)->take(6)->get() as $category)
<li><a href="{{ URL::action('CategoryController@view', array($category->url, '-', '-', '-')) }}" title="{{ $category->name }}">{{ Str::limit($category->name, 24, '...') }} <small>({{ $category->jobs->count() }})</small></a></li>
@endforeach
</div><!--todas_categorias_menu_col_2-->

<div id="todas_categorias_menu_col_3" class="todas_categorias_menu_estilo"><!--todas_categorias_menu_col_3-->
@foreach(Category::where('active', true)->orderBy('name')->skip(12)->take(6)->get() as $category)
<li><a href="{{ URL::action('CategoryController@view', array($category->url, '-', '-', '-')) }}" title="{{ $category->name }}">{{ Str::limit($category->name, 24, '...') }} <small>({{ $category->jobs->count() }})</small></a></li>
@endforeach
</div><!--todas_categorias_menu_col_3-->

<div id="todas_categorias_menu_col_4" class="todas_categorias_menu_estilo"><!--todas_categorias_menu_col_4-->
@foreach(Category::where('active', true)->orderBy('name')->skip(18)->take(6)->get() as $category)
<li><a href="{{ URL::action('CategoryController@view', array($category->url, '-', '-', '-')) }}" title="{{ $category->name }}">{{ Str::limit($category->name, 24, '...') }} <small>({{ $category->jobs->count() }})</small></a></li>
@endforeach
</div><!--todas_categorias_menu_col_4-->

</div><!--todas_categorias_menu_2-->

</td><!--coluna_2-->
</tr>
</table>
</div><!--todas_categorias_menu-->

</div><!--header_3_todas_categorias-->



	<div id="header_3_categorias">
	@foreach(CategoryHighlighted::getActive() as $category)
	<li><a href="{{ URL::action('CategoryController@view', array($category->url, '-', '-', '-')) }}" title="{{ $category->name }}">{{ $category->category->splited() }}</a></li>
	@endforeach
	</div>
</div><!--header_3-->



<div id="header_4"><!--header_4-->
	<div id="header_4_navegacao"><!--header_navegacao-->
	<li><a href="/" title="Página inicial">Página inicial</a></li>
	@yield('navigation')
	</div><!--header_navegacao-->

	@if (Auth::check() && Auth::user()->can('backend'))
	<div class="header_4_menu"><!--header_4_menu_candidato-->
	<li><a href="{{ URL::action('BackendController@index') }}" title="Acessar backend" target="_blank">Backend</a></li>
	<li><a href="{{ URL::action('UserController@logout') }}" title="Sair">Sair</a></li>
	</div><!--header_4_menu_candidato-->
	@endif

	@if (Auth::check() && Auth::user()->is(array('user')))
	<div class="header_4_menu"><!--header_4_menu_candidato-->
	<li><a class="fancybox fancybox.iframe" title="Meu currículo" href="{{ URL::action('CurriculumController@view') }}" data-fancybox-height="800" data-fancybox-width="850">Meu currículo</a></li>
	<li><a href="{{ URL::action('CurriculumController@edit') }}" title="Editar Currículo">Editar currículo</a></li>
	<li><a href="{{ URL::action('CandidateController@candidacies', array('-')) }}" title="Candidaturas ({{ Auth::user()->profile->candidacies->count() }})">Canditaturas</a></li>
	<li><a href="{{ URL::action('UserController@logout') }}" title="Sair">Sair</a></li>
	</div><!--header_4_menu_candidato-->
	@endif

	@if (Auth::check() && Auth::user()->is('company'))
	<div class="header_4_menu"><!--header_4_menu_candidato-->
	<li><a href="{{ URL::action('CompanyJobController@candidacies') }}" title="Candidatos ({{ Auth::user()->profile->candidates()->whereNull('viewed_at')->count() }})">Candidatos</a></li>
	<li><a href="{{ URL::action('CompanyJobController@lists', array('-')) }}" title="Vagas cadastradas ({{ Auth::user()->profile->jobs()->where('status', '=', Job::APPROVED)->count() }})">Vagas cadastradas</a></li>
	<li><a href="#" title="Curículos arquivados">Curículos arquivados</a></li>
	<li><a href="{{ URL::action('UserController@logout') }}" title="Sair">Sair</a></li>
	</div><!--header_4_menu_candidato-->
	@endif

	<div id="header_4_ola"><!--header_4_ola_off-->
	Olá @if (Auth::check())
	@if (Auth::user()->is(array('user')))
	<a class="fancybox fancybox.iframe" title="Meu currículo" href="{{ URL::action('CurriculumController@view') }}" data-fancybox-width="800" data-fancybox-height="600">
	<b>{{ explode(" ", Auth::user()->profile->fullname)[0] }}</b>
	</a>@else
	<b>{{ explode(" ", Auth::user()->profile->fullname)[0] }}</b>@endif, {{ $context->present }}.
	@else <b>Visitante</b>, <a href="{{ URL::action('UserController@login') }}" title="Entrar">clique aqui</a> para se identificar @endif
	</div><!--header_4_ola_off-->
</div><!--header_4-->

</div><!--header-->

@yield('content')

<div id="footer_body"><!--footer_body-->

<div id="footer"><!--footer-->

<div id="footer_1"><!--footer_1-->
	<div id="footer_1_menu">
	@foreach(Link::where('position', '=', 2)->where('active', '=', true)->orderBY('order', 'asc')->get() as $link)
	<li><a href="{{ $link->url }}" title="{{ $link->title }}" @if ($link->newpage) target="_blank" @endif>{{ $link->name }}</a></li>
	@endforeach
	</div>

	<div id="footer_1_icons">
	<a href="/"><img src="/images/icone_home.png" title="Página inicial"/></a>
	<a href="javascript: void(0);" name="bookmarks"><img src="/images/icone_favorito.png" title="Adicionar aos favoritos"/></a>
	<a href="/m"><img src="/images/icone_movel.png" title="Versão para celular"/></a>
	<a href="{{ Config::get('domain.facebook.url') }}" target="_blank"><img src="/images/icone_facebook.png" title="Página no Facebook"/></a>
	<a href="paginadofacebook"><img src="/images/icone_twitter.png" title="Siga no Twitter"/></a>
	<a href="{{ URL::to('feed') }}" target="_blank"><img src="/images/icone_rss.png" title="Feeds RSS"/></a>
	</div>
</div><!--footer_1-->



<div id="footer_2"><!--footer_2-->
	<div id="footer_2_locais_texto">
	Vagas de emprego em todo Brasil:
	</div>

	<div id="footer_2_locais_colunas">

	<div id="footer_2_locais_coluna_1" class="footer_2_colunas_estilo">
	<ul>
	@foreach(LocationEstate::where('active', true)->orderBy('name')->take(7)->get() as $estate)
	<li>
	<a href="@if(!Config::get('domain.main'))http://www.emprega-me.com.br@endif/?estate={{ $estate->id }}" title="Ver vagas de {{ $estate->name }}">{{ $estate->name }}/{{ $estate->short }}</a>
	</li>
	@endforeach
	</ul>
	</div>

	<div id="footer_2_locais_coluna_2" class="footer_2_colunas_estilo">
	<ul>
	@foreach(LocationEstate::where('active', true)->orderBy('name')->skip(7)->take(7)->get() as $estate)
	<li>
	<a href="@if(!Config::get('domain.main'))http://www.emprega-me.com.br@endif/?estate={{ $estate->id }}" title="Ver vagas de {{ $estate->name }}">{{ $estate->name }}/{{ $estate->short }}</a>
	</li>
	@endforeach
	</ul>
	</div>

	<div id="footer_2_locais_coluna_3" class="footer_2_colunas_estilo">
	<ul>
	@foreach(LocationEstate::where('active', true)->orderBy('name')->skip(14)->take(7)->get() as $estate)
	<li>
	<a href="@if(!Config::get('domain.main'))http://www.emprega-me.com.br@endif/?estate={{ $estate->id }}" title="Ver vagas de {{ $estate->name }}">{{ $estate->name }}/{{ $estate->short }}</a>
	</li>
	@endforeach
	</ul>
	</div>

	<div id="footer_2_locais_coluna_4" class="footer_2_colunas_estilo">
	<ul>
	@foreach(LocationEstate::where('active', true)->orderBy('name')->skip(21)->take(7)->get() as $estate)
	<li>
	<a href="@if(!Config::get('domain.main'))http://www.emprega-me.com.br@endif/?estate={{ $estate->id }}" title="Ver vagas de {{ $estate->name }}">{{ $estate->name }}/{{ $estate->short }}</a>
	</li>
	@endforeach
	</ul>
	</div>

	</div>
</div><!--footer_2-->


<div id="footer_3"><!--footer_3-->
	<div id="footer_3_copyright"><!--footer_3_copyright-->
	© Copyright 2012-{{ Carbon::now()->year }} | <a href="http://www.emprega-me.com.br" title="Emprega-me.com.br">Emprega-me.com.br</a> - Todos os direitos reservados
	</div><!--footer_3_copyright-->
</div><!--footer_3-->

</div><!--footer-->

</div><!--footer_body-->

<script type="text/javascript" src="/scripts/libs/jquery.js"></script>
<!--fancyBox-->
<script type="text/javascript" src="/packages/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="/packages/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/packages/fancybox/source/helpers/jquery.fancybox-buttons.js"></script>
<script type="text/javascript" src="/packages/fancybox/source/helpers/jquery.fancybox-media.js"></script>
<script type="text/javascript" src="/packages/fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>
<!--fancyBox-->

@yield('scripts')

<script data-main="/scripts/bootstrap" src="/scripts/libs/require-2.1.8.js" type="text/javascript"></script>
<script type="text/javascript">
var addthis_config = {"data_track_addressbar": true};
@if (isset($json))
var _context = {{ $json }};
@else
var _context = [];
@endif
$(document).ready(function() {
requirejs(["apps/main"]);
@yield('requirejs')
});
</script>

</body>
</html>
