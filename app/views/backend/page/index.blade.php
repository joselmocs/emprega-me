@extends("backend/template")

@section('requirejs')
requirejs(["apps/backend/pages"]);
@endsection

@section("content")
<div class="page_template"><!--page_template-->

<div class="botao"><!--botao-->
@if (Auth::user()->can('manage_pages'))
<li><a href="{{ URL::action('BackendPageController@register') }}" title="Titulo">Adicionar nova página</a></li>
@endif
</div><!--botao-->

<div class="titulo_grande" style="text-transform: uppercase;"><!--titulo_grande-->
<img src="/images/backend/icone_paginas.jpg"/> PÁGINAS
</div><!--titulo_medio-->

@if (!$context->pages->count())
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a>Nenhuma página cadastrada.</a>
</span>
</div>
@else
@foreach($context->pages as $i => $page)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a href="{{ URL::action('PageController@view', array($page->url)) }}" title="{{ $page->title }}" target="_blank">{{ $page->title }}</a>
</span>

@if (Auth::user()->can('manage_pages'))
<div class="row_menu">
<a href="{{ URL::action('BackendPageController@register', array($page->id)) }}"><img src="/images/backend/row_menu_editar.png" title="Editar"/></a>
<a href="javascript:void(0);" name="remover" data-name="{{ $page->title }}" data-id="{{ $page->id }}"><img src="/images/backend/row_menu_remover.png" title="Remover"/></a>
</div>
@endif
</div><!--row/row-alt-->
@endforeach
@endif

</div><!--page_template-->

</div><!--container-->
@endsection