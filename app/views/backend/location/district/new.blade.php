@extends('backend/template')

@section('content')
<div id="localizacoes_bairros_adicionar-editar"><!--localizacoes_bairros_adicionar-editar-->

    <div class="titulo_pequeno"><!--titulo_medio-->
        Adicionar bairro:
    </div><!--titulo_medio-->

    <form action="{{ URL::action('BackendLocationController@district_save') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="city_id" value="{{ $context->city->id }}" />
        <input type="hidden" name="letter" value="{{ $context->letter }}" />
        <div style="margin: 0 0 10px 0" class="moldura_cinza formulario"><!--div margin-->
            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                <tr>
                    <td align="left" valign="bottom">Nome:</td>
                </tr>
                <tr>
                    <td align="left">
                        <input type="text" style="width: 362px;" name="name" />
                    </td>
                </tr>
            </table>
        </div><!--div margin-->

        <div class="moldura_cinza formulario"><!--div margin-->
            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                <tr>
                    <td align="left">
                        <input type="submit" value="Adicionar"/>
                    </td>
                </tr>
            </table>
        </div><!--div margin-->
    </form>

</div><!--localizacoes_bairros_adicionar-editar-->
@endsection