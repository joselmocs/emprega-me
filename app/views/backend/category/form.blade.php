@extends("backend/template")

@section("content")
<div id="todas_categorias_adicionar-editar"><!--todas_categorias_adicionar-editar-->

    <div class="titulo_pequeno"><!--titulo_medio-->
        @if ($context->category_id) Editar @else Adicionar @endif categoria:
    </div><!--titulo_medio-->

    @if ($context->category_id)
    <form action="{{ URL::action('BackendCategoryController@register', array($context->category_id)) }}" method="post">
    @else
    <form action="{{ URL::action('BackendCategoryController@register') }}" method="post">
    @endif
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <div style="margin: 0 0 10px 0" class="moldura_cinza formulario"><!--div margin-->
            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                <tr>
                    <td align="left" valign="bottom">Nome:</td>
                </tr>
                <tr>
                    <td align="left">
                        <input type="text" style="width: 362px;" name="name" value="{{ $context->input->name }}"
                        @if ($context->field_errors && $context->field_errors->has('name')) class="error" @endif />
                        @if ($context->field_errors && $context->field_errors->has('name'))
                        <div class="error">{{ $context->field_errors->first('name') }}</div>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td height="20" align="left" valign="bottom">Titulo:</td>
                </tr>
                <tr>
                    <td align="left">
                        <input type="text" style="width: 362px;" name="title" value="{{ $context->input->title }}"
                        @if ($context->field_errors && $context->field_errors->has('title')) class="error" @endif />
                        @if ($context->field_errors && $context->field_errors->has('title'))
                        <div class="error">{{ $context->field_errors->first('title') }}</div>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td height="20" align="left" valign="bottom">Descrição:</td>
                </tr>
                <tr>
                    <td align="left">
                        <input type="text" style="width: 362px;" name="description" value="{{ $context->input->description }}"
                        @if ($context->field_errors && $context->field_errors->has('description')) class="error" @endif />
                        @if ($context->field_errors && $context->field_errors->has('description'))
                        <div class="error">{{ $context->field_errors->first('description') }}</div>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td height="20" align="left" valign="bottom">Keywords:</td>
                </tr>
                <tr>
                    <td align="left">
                        <input type="text" style="width: 250px;" name="keywords" value="{{ $context->input->keywords }}" />
                    </td>
                </tr>
                <tr>
                    <td height="20" align="left" valign="bottom">URL:</td>
                </tr>
                <tr>
                    <td align="left">
                        <input type="text" style="width: 250px;" name="url" value="{{ $context->input->url }}"
                        @if ($context->field_errors && $context->field_errors->has('url')) class="error" @endif />
                        @if ($context->field_errors && $context->field_errors->has('url'))
                        <div class="error">{{ $context->field_errors->first('url') }}</div>
                        @endif
                    </td>
                </tr>
            </table>
        </div><!--div margin-->



        <div class="moldura_cinza formulario"><!--div margin-->
            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                <tr>
                    <td align="left">
                        @if ($context->category_id)
                        <input type="submit" value="Editar" />
                        @else
                        <input type="submit" value="Adicionar" />
                        @endif
                    </td>
                </tr>
            </table>
        </div><!--div margin-->
    </form>
</div><!--todas_categorias_adicionar-editar-->

@endsection