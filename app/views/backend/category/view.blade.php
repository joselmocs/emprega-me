@extends("backend/template")

@section('requirejs')
    requirejs(["apps/backend/backend"]);
@endsection

@section("content")
<div class="page_template"><!--page_template-->

<div class="titulo_grande" style="text-transform: uppercase;"><!--titulo_grande-->
<img src="/images/backend/icone_todas_categorias.jpg" width="14" height="16" alt=""/> {{ $context->category->name }}
</div><!--titulo_grande-->

<div style="margin: 0 0 20px 0"><!--div margin-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario">
<tr>
<td width="100%">
<a href="../linkaqui"><img src="../domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="../linkaqui"><img src="../domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="../linkaqui"><img src="../domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="../linkaqui"><img src="../domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="../linkaqui"><img src="../domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="../linkaqui"><img src="../domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
</td>
<td align="right">
<select onchange="window.location=options[selectedIndex].value">
<option value="{{ URL::action('BackendCategoryController@view', array($context->category->id, 0)) }}" @if ($context->estate == 0) selected="selected" @endif>Todos os estados</option>
@foreach($context->estates as $estate)
<option value="{{ URL::action('BackendCategoryController@view', array($context->category->id, $estate->id)) }}" @if ($context->estate == $estate->id) selected="selected" @endif>{{ $estate->name }}/{{ $estate->short }}</option>
@endforeach
</select>
</td>
</tr>
</table>
</div><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Vagas cadastradas <span style="color: #999999;">({{ $context->jobs->count() }})</span>
</div><!--titulo_pequeno-->

@if (!$context->jobs->count())
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a>Nenhuma vaga encontrada.</a>
</span>
</div>
@endif

@foreach($context->jobs as $job)
<div class="row"><!--row/row-alt-->
<div id="row_1"><!--row_1-->
<span class="row-info">
<img src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $job->type->image }}" title="{{ $job->type->name }}"><a class="fancybox fancybox.iframe" title="{{ $job->position }}" href="{{ URL::action('CompanyJobController@view', array($job->id)) }}" data-fancybox-width="800" data-fancybox-height="600">{{ $job->position }}</a>
<small><span>por</span> <a href="{{ URL::action('BackendCompanyController@jobs', array($job->profile->id)) }}">{{ $job->company }}</a> <span>em</span> {{ $job->city->name }}/{{ $job->estate->short }}</small>
</span>

<span class="row_menu">
@if ($job->status == Job::IN_APPROVAL)
<img src="/images/row_menu_analise.png" title="Vaga em análise">
@endif
@if ($job->status == Job::APPROVED)
<img src="/images/row_menu_yes.png" title="Vaga aprovada">
@endif
@if ($job->status == Job::REPROVED)
<img src="/images/row_menu_erro.png" title="Vaga reprovada">
@endif
</span>
@if (Auth::user()->can('manage_jobs'))
<div class="row_menu">
@if ($job->featured)
<a href="{{ URL::action('BackendCompanyController@feature', array($job->id)) }}"><img src="/images/backend/row_menu_destaque_on.png" title="Destaque ON"></a>
@else
<a href="{{ URL::action('BackendCompanyController@feature', array($job->id)) }}"><img src="/images/backend/row_menu_destaque_off.png" title="Destaque OFF"></a>
@endif
<a href="{{ URL::action('BackendCompanyController@edit', array($job->id)) }}?next={{ URL::current() }}"><img src="/images/backend/row_menu_editar.png" title="Editar"></a>
<a href="javascript: void(0);" name="remover" data-id="{{ $job->id }}" data-name="{{ $job->position }}"><img src="/images/backend/row_menu_remover.png" title="Remover"></a>
</div>
@endif

<span class="time_post">
@if (Carbon::parse($job->created_at)->isToday())
<img src="/images/hoje.png" title="Vaga publicada hoje">
@endif
@if (Carbon::parse($job->created_at)->isYesterday())
<img src="/images/ontem.png" title="Vaga publicada ontem">
@endif
<img src="/images/clock.png" title="Data da publicação">
<small title="{{ Carbon::parse($job->created_at)->format('d \d\e F \d\e Y') }}">05/03</small>
</span>

</div><!--row_1-->

<div id="row_2"><!--row_2-->
<div class="row_2-info">
▪ Email: <span>{{ $job->email }}</span>
</div>
</div><!--row_2-->
</div>

@endforeach

</div><!--categoria-->

</div><!--container-->
@endsection
