<?php

use JCS\Auth\Models\Role;


class UserSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (array("sys_admin", "moderator", "manager") as $name) {
            $user = new User;
            $user->password = $name;
            $user->email = $name ."@gmail.com";
            $user->verified = 1;
            $user->save();

            $user->profile->fullname = strtoupper($name);
            $user->profile->save();
            $user->roles()->sync(array(Role::where('tag', '=', $name)->firstOrFail()->id));
        }
    }
}
