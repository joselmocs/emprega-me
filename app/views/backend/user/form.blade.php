@extends("backend/template")

@section("requirejs")
requirejs(["apps/backend/users"]);
@endsection

@section("content")
<div id="tipos_trabalho_adicionar-editar"><!--tipos_trabalho_adicionar-editar-->

    <div class="titulo_medio"><!--titulo_medio-->
        @if ($context->user_id) Editar @else Adicionar @endif Usuário:
    </div><!--titulo_medio-->

    @if ($context->user_id)
    <form action="{{ URL::action('BackendUserController@register', array($context->user_id)) }}" method="post">
    @else
    <form action="{{ URL::action('BackendUserController@register') }}" method="post">
    @endif

    <input type="hidden" name="_token" value="{{  csrf_token() }}" />

        <div style="margin: 0 0 25px 0;"><!--div margin-->

            <div class="titulo_pequeno"><!--titulo_medio-->
                Dados de acesso:
            </div><!--titulo_medio-->

            <table width="100%" border="0" cellspacing="2" cellpadding="0" class="formulario moldura_cinza">
                <tr>
                    <td width="160" align="right">E-mail:</td>
                    <td align="left">
                        <input type="email" style="width: 250px;" name="email" required="true" value="{{ $context->input->email }}"
                        @if ($context->field_errors && $context->field_errors->has('email')) class="error" @endif />
                        @if ($context->field_errors && $context->field_errors->has('email'))
                        <div class="error">{{ $context->field_errors->first('email') }}</div>
                        @endif
                    </td>
                </tr>
                <tr class="tr-password" @if (!$context->input->password_change) style="display:none;" @endif>
                    <td width="160" align="right">Senha:</td>
                    <td align="left">
                        <input type="password" style="width: 175px;" name="password" @if ($context->input->password_change) required="true" @endif
                        @if ($context->field_errors && $context->field_errors->has('password')) class="error" @endif />
                        @if ($context->field_errors && $context->field_errors->has('password'))
                        <div class="error">{{ $context->field_errors->first('password') }}</div>
                        @endif
                    </td>
                </tr>
                <tr class="tr-password" @if (!$context->input->password_change) style="display:none;" @endif>
                    <td width="160" align="right">Confirmar senha:</td>
                    <td align="left">
                        <input type="password" style="width: 175px;" name="password_confirm" @if ($context->input->password_change) required="true" @endif
                        @if ($context->field_errors && $context->field_errors->has('password_confirm')) class="error" @endif />
                        @if ($context->field_errors && $context->field_errors->has('password_confirm'))
                        <div class="error">{{ $context->field_errors->first('password_confirm') }}</div>
                        @endif
                    </td>
                </tr>
                @if ($context->user_id)
                <tr>
                    <td width="160" align="right">Alterar senha:</td>
                    <td align="left">
                        <table width="0%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <input type="checkbox" style="width: 175px;" name="password_change" @if ($context->input->password_change) checked="checked" @endif />
                                </td>
                                <td style="color: #999999;"><small>(Marque para alterar a senha deste usuário.)</small></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                @endif
            </table>

        </div><!--div margin-->

        <div style="margin: 0 0 25px 0"><!--div margin-->

            <div class="titulo_pequeno"><!--titulo_medio-->
                Dados pessoais:
            </div><!--titulo_medio-->

            <table width="100%" border="0" cellspacing="2" cellpadding="0" class="formulario moldura_cinza">
                <tr>
                    <td width="160" align="right">Nome completo:</td>
                    <td align="left">
                        <input type="text" style="width: 350px;" name="fullname" required="true" value="{{ $context->input->fullname }}"
                        @if ($context->field_errors && $context->field_errors->has('fullname')) class="error" @endif />
                        @if ($context->field_errors && $context->field_errors->has('fullname'))
                        <div class="error">{{ $context->field_errors->first('fullname') }}</div>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td width="160" align="right">Telefone:</td>
                    <td align="left">
                        <input type="text" style="width: 150px;" name="phone" value="{{ $context->input->phone }}"
                        @if ($context->field_errors && $context->field_errors->has('phone')) class="error" @endif />
                        @if ($context->field_errors && $context->field_errors->has('phone'))
                        <div class="error">{{ $context->field_errors->first('phone') }}</div>
                        @endif
                    </td>
                </tr>
            </table>

        </div><!--div margin-->

        <div style="margin: 0 0 25px 0"><!--div margin-->

                    <div class="titulo_pequeno"><!--titulo_medio-->
                        Permissões:
                    </div><!--titulo_medio-->

                    <table width="100%" border="0" cellspacing="2" cellpadding="0" class="formulario moldura_cinza">
                        <tr>
                            <td width="160" align="right">Grupo:</td>
                            <td align="left">
                                <select name="role">
                                    @foreach($context->roles as $role)
                                    <option value="{{ $role->id }}" @if ($role->id == $context->input->role) selected="selected" @endif>{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    </table>

                </div><!--div margin-->

        <div class="formulario moldura_cinza"><!--moldura_cinza-->
            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                <tr>
                    <td width="160"></td>
                    <td align="left">
                        @if ($context->user_id)
                        <input type="submit" value="Editar" />
                        @else
                        <input type="submit" value="Adicionar" />
                        @endif
                    </td>
                </tr>
            </table>
        </div><!--moldura_cinza-->

    </form>
</div><!--tipos_trabalho_adicionar-editar-->


@endsection