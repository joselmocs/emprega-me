@extends("backend/template")

@section('requirejs')
requirejs(["apps/backend/advertising.adsense"]);
@endsection

@section("content")

<div id="publicidade_editar_banner_adsense" class="page_template"><!--publicidade_editar_banner_adsense-->

    <div class="titulo_medio"><!--titulo_medio-->
        <img src="/images/backend/icone_publicidade.jpg"/> ADSENSE/{{ $context->adsense->name }}
    </div><!--titulo_medio-->

    <form action="{{ URL::action('BackendAdvertisingController@adsense_save', array($context->adsense->short)) }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <div style="margin: 0 0 25px 0" class="moldura_cinza formulario"><!--div margin-->
            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                <tr>
                    <td>
                        <textarea style="height: 260px; width: 1052px;" name="code_empregame" placeholder="EMPREGAME">{{ $context->adsense->code_empregame }}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <textarea style="height: 260px; width: 1052px;" name="code_bhjobs" placeholder="BHJOBS">{{ $context->adsense->code_bhjobs }}</textarea>
                    </td>
                </tr>
            </table>
        </div><!--div margin-->

        <div class="moldura_cinza formulario"><!--div margin-->
            <table width="0%" border="0" cellspacing="2" cellpadding="0">
                <tr>
                    <td align="left">
                        <input type="submit" value="Salvar"/>
                    </td>
                    <td width="5"></td>
                    <td align="left">
                        <input type="button" value="Limpar" name="limpar" />
                    </td>
                </tr>
            </table>
        </div><!--div margin-->
    </form>

</div><!--publicidade_editar_banner_adsense-->

@endsection