<?php


class Configuration extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'configuration';

}