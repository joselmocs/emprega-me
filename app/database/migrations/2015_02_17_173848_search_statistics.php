<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SearchStatistics extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_statistic', function($table)
        {
            $table->increments('id');
            $table->string('keyword', 256);
            $table->integer('estate_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('estate_id')->references('id')->on('location_estates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_statistic');
    }

}
