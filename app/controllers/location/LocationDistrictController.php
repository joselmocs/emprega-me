<?php

class LocationDistrictController extends BaseController
{
    /**
     * Carrega a lista de bairros da cidade informada
     *
     * @return Illuminate\Support\Facades\Response
     */
    public function load_all_json()
    {
        $city_id = Input::get('city_id');
        $city = LocationCity::where('id', '=', $city_id)->firstOrFail();
        $districts = $city->districts()->where('active', '=', 1)->orderBy('name')->get()->toArray();
        return Response::json(array(
            'success' => true,
            'districts' => $districts
        ));
    }
}