<?php


class UserLanguage extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_languages';

    /**
     * Constantes para grau de escolaridade
     *
     */
    const LEVEL_BASIC = 1;
    const LEVEL_INTERMEDIATE = 2;
    const LEVEL_ADVANCED = 3;
    const LEVEL_FLUENT = 4;

    public static $VERBOSE_LEVEL = array(
        self::LEVEL_BASIC => "Básico",
        self::LEVEL_INTERMEDIATE => "Intermediário",
        self::LEVEL_ADVANCED => "Avançado",
        self::LEVEL_FLUENT => "Fluente"
    );

    /**
     * Retorna o perfil do usuário desde idioma
     *
     * @return UserProfile
     */
    public function profile()
    {
        return $this->belongsTo('UserProfile', 'profile_id');
    }

    /**
     * Retorna o objeto idioma
     *
     * @return Language
     */
    public function language()
    {
        return $this->belongsTo('Language', 'language_id');
    }

}