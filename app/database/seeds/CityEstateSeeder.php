<?php


class CityEstateSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arquivo = __DIR__ ."/files/estate-cities.json";

        $f = fopen($arquivo, 'r');
        $conteudo = fread($f, filesize($arquivo));
        fclose($f);

        $decoded = json_decode($conteudo, true);

        foreach ($decoded['estados'] as $estado) {
            $this->command->info('        : '. STR::slug($estado['nome']));
            $newEstate = new LocationEstate;
            $newEstate->name = $estado['nome'];
            $newEstate->short = $estado['sigla'];
            $newEstate->slug = STR::slug($estado['nome']);
            $newEstate->save();

            foreach ($estado['cidades'] as $cidade) {
                $newCity = new LocationCity;
                $newCity->name = $cidade;
                $newCity->estate_id = $newEstate->id;
                $newCity->slug = STR::slug($cidade .' '. $estado['sigla']);
                $newCity->save();
            }
        }
    }
}