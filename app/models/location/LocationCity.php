<?php

use \Illuminate\Support\Str;


class LocationCity extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'location_cities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'name', 'slug', 'estate_id');

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::creating(function($model) {
            $slug = $model->name .' '. $model->estate->short;
            $model->slug = STR::slug($slug);
        });

        static::updating(function($model) {
            $slug = $model->name .' '. $model->estate->short;
            $model->slug = STR::slug($slug);
        });
    }

    /**
     * Retorna o estado da cidade
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estate()
    {
        return $this->belongsTo('LocationEstate', 'estate_id');
    }

    /**
     * Retorna os bairros da cidade
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function districts()
    {
        return $this->hasMany('LocationDistrict', 'city_id');
    }
}