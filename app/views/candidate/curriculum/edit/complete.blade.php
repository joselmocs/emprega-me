@extends('base/template')

@section("site-title")
Editar currículo - {{ Config::get('domain.site.title') }}
@endsection

@section('content')
<div id="cadastrar-editar_candidato_1" class="page_template"><!--cadastrar-editar_candidato_1-->

    <div class="titulo_medio"><!--titulo_medio-->
        Editar Currículo
    </div><!--titulo_medio-->

    <div class="cadastrar_sleps"><!--cadastrar_sleps-->
        <div id="cadastrar_slep_1" class="cadastrar_slep_active">
            <b>4</b> Finalização do Currículo
        </div>
    </div>

    <div id="cadastrar-editar_candidato_4_texto" style="margin: 0 0 25px 0"><!--cadastrar-editar_candidato_4_texto-->
        Parabéns <b>{{ Auth::user()->profile->fullname  }}</b>, seu currículo foi atualizado com sucesso!!!
        <br>
        Você já pode começar a se candidatatar as vagas.
    </div>

    <span style="font-size:12px" class="page_template_links">
        Para voltar a página inicial <a href="/" title="Voltar para pagina inicial.">clique aqui</a>.
    </span>

</div>
@endsection