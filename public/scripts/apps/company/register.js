/*!
 * apps.company.register.js
 * Scripts responsáveis pela página de registro de empresas.
 *
 * Joselmo Cardozo
 */

require([
    "helpers/core"
], function(core) {
    var
        initialize = function() {
            load_cities(core.context.data.estate_id, core.context.data.city_id);
            events();
        },
        events = function() {
            core.phonemask('input[name=phone]');
            $("select[name=estate_id]").change(function() {
                load_cities($(this).val(), null);
            });
        },

        // Carrega as cidades do estado selecionado
        load_cities = function(estate_id, city_id) {

            $city = $("select[name=city_id]");
            $city.hide().find('option').remove();

            if (!estate_id) {
                return false;
            }

            $city
                .append("<option value=\"\">Aguarde, carregando...</option>")
                .show();

            $.ajax({
                type: 'post',
                url: '/location/city/load_all_json',
                data: {
                    _token: $("meta[name=csrf-token]").attr('content'),
                    estate_id: estate_id
                },
                error: function() {
                    alert("Ocorreu um erro ao carregar a lista de cidades, por favor contacte um administrador.");
                },
                success: function(response) {
                    if (response.success) {
                        $($city.find('option')[0]).html("Selecione a cidade");
                        $.each(response.cities, function(i, city) {
                            var $option = $("<option></option>").attr('value', city.id).html(city.name);
                            if (city.id == city_id) {
                                $option.attr("selected", "selected");
                            }
                            $city.append($option);
                        });
                    }
                }
            });
        };

    initialize();
});
