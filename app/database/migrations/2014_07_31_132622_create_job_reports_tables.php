<?php

use Illuminate\Database\Migrations\Migration;


class CreateJobReportsTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the job_reports table
        Schema::create('job_reports', function($table)
        {
            $table->increments('id');
            $table->integer('profile_id')->unsigned()->nullable();
            $table->integer('job_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('profile_id')->references('id')->on('user_profile');
            $table->foreign('job_id')->references('id')->on('jobs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_reports');
    }

}
