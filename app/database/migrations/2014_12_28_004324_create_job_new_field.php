<?php

use Illuminate\Database\Migrations\Migration;


class CreateJobNewField extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the job_reports table
        Schema::table('jobs', function($table)
        {
            $table->boolean('viewed')->default(false);
            $table->boolean('edited')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobs', function($table)
        {
            $table->dropColumn('viewed');
            $table->dropColumn('edited');
        });
    }

}
