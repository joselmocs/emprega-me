/*!
 * apps.candidate.candidacies.js
 * Scripts responsáveis listagem de candidaturas
 *
 */

require([
    "helpers/core"
], function(core) {
    var
        initialize = function() {
            events();
        },
        events = function() {
            $("select[name=view]").change(function() {
                var path_frag = core.uri.path().split('/');
                path_frag[3] = $(this).val();
                window.location = path_frag.join('/');
            });
        };

    initialize();
});
