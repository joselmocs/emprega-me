<?php

use JCS\Auth\Models\Role;


class BackendUserController extends BaseBackendController {

    /**
     * Retorna uma lista de objetos Role ou uma lista de ids dos
     * grupos da staff do sistema.
     *
     * @param bool $onlyIds
     * @return mixed
     */
    public function getStaffGroups($onlyIds = false)
    {
        $roles = Role::whereNotIn('tag', array('user', 'company'))->get();

        if ($onlyIds) {
            return array_map(function($role) {
                return $role['id'];
            }, $roles->toArray());
        }

        return $roles;
    }

    /**
     * Retorna um objeto Role referente ao grupo Gerente
     *
     * @param bool $onlyIds
     * @return mixed
     */
    public function getManagerGroup($onlyIds = false)
    {
        $roles = Role::where('tag', 'manager')->get();

        if ($onlyIds) {
            return array_map(function($role) {
                return $role['id'];
            }, $roles->toArray());
        }

        return $roles;
    }

    /**
     * Exibe a lista de usuários do sistema
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        $users = User::select('users.id', 'users.email', 'users.disabled');

        $users->join('user_role_user', 'user_role_user.user_id', '=', 'users.id');

        if (Auth::user()->is('sys_admin')){
            $users->whereIn('user_role_user.role_id', $this->getStaffGroups(true));
        }
        else {
            $users->whereIn('user_role_user.role_id', $this->getManagerGroup(true));
        }

        $users->join('user_profile', 'user_profile.user_id', '=', 'users.id');

        $this->set_json_context(array(
            'urls' => array(
                'remove' => URL::action('BackendUserController@remove')
            )
        ));

        $this->set_context(array(
            'users' => $users->get()
        ));

        return $this->view_make('backend/user/index');
    }

    /**
     * Exibe o formulário de criação e edição de candidatos no backend
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function register($id = null)
    {
        if ($id) {
            $user = User::find($id);
            $this->set_context(array(
                'user_id' => $user->id,
                'input' => (object) array(
                    'email' => Input::get('email', $user->email),
                    'password_change' => Input::get('password_change') == "on",
                    'fullname' => Input::get('fullname', $user->profile->fullname),
                    'phone' => Input::get('phone', $user->profile->phone),
                    'role' => intval(Input::get('role', $user->roles()->firstOrFail()->id))
                )
            ));
        }
        else {
            $user = new User;
            $this->set_context(array(
                'user_id' => null,
                'input' => (object) array(
                    'email' => Input::get('email', ''),
                    'password_change' => true,
                    'fullname' => Input::get('fullname', ''),
                    'phone' => Input::get('phone', ''),
                    'role' => Input::get('role', '')
                )
            ));
        }

        if (Auth::user()->is('sys_admin')){
            $this->set_context(array(
                'roles' => $this->getStaffGroups()
            ));
        }
        else {
            $this->set_context(array(
                'roles' => $this->getManagerGroup()
            ));
        }

        if (Request::isMethod('get')) {
            return $this->view_make('backend/user/form');
        }

        $validate = array(
            "email" => ($id) ? "required|unique:users,email,". $user->id : "required|unique:users",
            "password" => "required_if:password_change,on|min:3|max:30",
            "password_confirm" => "required_if:password_change,on|same:password",
            "fullname" => "required|min:2|max:256",
            "role" => "required|integer"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('backend/user/form');
        }

        DB::transaction(function() use ($id, $user) {
            $user->email = Input::get('email');
            if (!$id || Input::get('password_change') == "on") {
                $user->password = Input::get('password');
            }
            $user->verified = 1;
            $user->save();
            $user->roles()->sync(array(Role::find(Input::get('role'))->id));

            $user->profile->fullname = Input::get('fullname', '');
            $user->profile->phone = Input::get('phone', '');
            $user->profile->save();
        });

        return Redirect::action('BackendUserController@index');
    }

    /**
     * Remove uma empresa.
     *
     * @param $id int
     * @return Redirect
     */
    public function remove($id)
    {
        if ($id) {
            User::where('id', '=', $id)->firstOrFail()->delete();
        }
        return Redirect::back();
    }

    /**
     * Ativa um usuário
     *
     * @param int $id
     * @return Illuminate\Support\Facades\View
     */
    public function active($id)
    {
        $user = User::where('id', '=', $id)->firstOrFail();
        $user->disabled = false;
        $user->save();

        return Redirect::back();
    }

    /**
     * Desativa um usuário
     *
     * @param int $id
     * @return Illuminate\Support\Facades\View
     */
    public function deactive($id)
    {
        $user = User::where('id', '=', $id)->firstOrFail();
        $user->disabled = true;
        $user->save();

        return Redirect::back();
    }
}
