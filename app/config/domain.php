<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Domain Principal
    |--------------------------------------------------------------------------
    |
    | Informa se esta configuração é ou não da propriedade principal.
    |
    */

    'main' => true,

    /*
    |--------------------------------------------------------------------------
    | Codigo do Domain
    |--------------------------------------------------------------------------
    |
    | Codigo do dominio atigo
    |
    */

    'code' => 'empregame',

    /*
    |--------------------------------------------------------------------------
    | Site
    |--------------------------------------------------------------------------
    |
    | Configurações gerais da propriedade.
    |
    */

    'site' => array(
        'name' => 'Emprega-me.com.br',
        'title' => 'Emprega-me.com.br',
        'url' => 'http://www.emprega-me.com.br',
        'description' => 'Um breve resumo da descrição do site',
        'keywords' => ''
    ),

    /*
    |--------------------------------------------------------------------------
    | Facebook
    |--------------------------------------------------------------------------
    |
    | Configurações gerais do facebook da propriedade.
    |
    */

    'facebook' => array(
        'name' => 'Emprega-me.com.br',
        'url' => 'https://www.facebook.com/Empregame.com.br',
        'type' => 'website',
        'image' => array(
            '/domains/emprega-me/images/facebook_img_1.jpg',
            '/domains/emprega-me/images/facebook_img_2.jpg'
        ),
    ),

    /*
    |--------------------------------------------------------------------------
    | Diretório do domain
    |--------------------------------------------------------------------------
    |
    | Pasta onde fica os arquivos estáticos da propriedade. O caminho completo
    | é /public/domains/<folder>/.
    |
    */

    'folder' => 'emprega-me'
);