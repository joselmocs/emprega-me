<?php


class BackendCategoryController extends BaseBackendController {

    /**
     * Exibe a lista de categorias cadastradas.
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        $this->set_json_context(array(
            'urls' => array(
                'remove' => URL::action('BackendCategoryController@remove')
            )
        ));

        $this->set_context(array(
            'categories' => Category::orderBy('name', 'asc')->get()
        ));

        return $this->view_make('backend/category/index');
    }

    /**
     * Exibe a página de criação de categorias
     *
     * @param integer $id
     * @return Illuminate\Support\Facades\View
     */
    public function register($id = null)
    {
        if ($id) {
            $category = Category::find($id);
            $this->set_context(array(
                'category_id' => $category->id,
                'input' => (object) array(
                    'name' => Input::get('name', $category->name),
                    'title' => Input::get('title', $category->title),
                    'description' => Input::get('description', $category->description),
                    'keywords' => Input::get('keywords', $category->keywords),
                    'url' => Input::get('url', $category->url)
                )
            ));
        }
        else {
            $category = new Category;
            $this->set_context(array(
                'category_id' => null,
                'input' => (object) array(
                    'name' => Input::get('name'),
                    'title' => Input::get('title'),
                    'description' => Input::get('description'),
                    'keywords' => Input::get('keywords'),
                    'url' => Input::get('url')
                )
            ));
        }

        if (Request::isMethod('get')) {
            return $this->view_make('backend/category/form');
        }

        $validate = array(
            "name" => "required",
            "title" => "required",
            "description" => "required",
            "url" => ($id) ? "required|unique:categories,url,". $category->id : "required|unique:categories"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('backend/category/form');
        }

        $category->name = Input::get('name');
        $category->title = Input::get('title');
        $category->description = Input::get('description');
        $category->keywords = Input::get('keywords');
        $category->url = Input::get('url');
        $category->save();

        return Redirect::action('BackendCategoryController@index');
    }

    /**
     * Remove uma categoria.
     *
     * @param $id int
     * @return Redirect
     */
    public function remove($id)
    {
        if ($id) {
            Category::find($id)->delete();
        }
        return Redirect::back();
    }

    /**
     * Ativa uma categoria
     *
     * @param int $id código para identificação da categoria
     * @return Illuminate\Support\Facades\View
     */
    public function active($id)
    {
        $city = Category::where('id', '=', $id)->firstOrFail();
        $city->active = true;
        $city->save();

        return Redirect::back();
    }

    /**
     * Desativa uma categoria
     *
     * @param int $id código para identificação da categoria
     * @return Illuminate\Support\Facades\View
     */
    public function deactive($id)
    {
        $city = Category::where('id', '=', $id)->firstOrFail();
        $city->active = false;
        $city->save();

        return Redirect::back();
    }

    /**
     * Exibe a lista de vagas da categoria
     *
     * @param int $id codigo da categoria
     * @param int $estate código do estado
     * @return \Illuminate\Support\Facades\View
     */
    public function view($id, $estate)
    {
        $estate = intval($estate);
        $this->set_json_context(array(
            'urls' => array(
                'remove' => URL::action('BackendCategoryController@remove')
            )
        ));

        $this->set_context(array(
            'estates' => LocationEstate::all(),
            'estate' => $estate
        ));

        $jobs = Job::where('category_id', $id)
            ->orderBy('created_at', 'desc');

        if ($estate > 0) {
            $jobs->where('estate_id', $estate);
        }

        $this->set_context(array(
            'jobs' => $jobs->get(),
            'category' => Category::where('id', $id)->firstOrFail()
        ));

        return $this->view_make('backend/category/view');
    }
}
