<?php


class CategoryHighlighted extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'category_highlighted';

    /**
     * Retorna a última posição das categorias em destaque
     *
     * @return integer
     */
    public static function getLastPosition()
    {
        $last_position = CategoryHighlighted::orderBy('order', 'desc')->limit(1)->first();

        if ($last_position) {
            return $last_position->order;
        }

        return 0;
    }

    /**
     * Retorna a categoria
     *
     * @return Category
     */
    public function category()
    {
        return $this->belongsTo('Category', 'category_id');
    }

    /**
     * Retorna as categorias destaques
     *
     * @return Category
     */
    public static function getActive()
    {
        return CategoryHighlighted::join('categories', 'category_highlighted.category_id', '=', 'categories.id')
            ->where('categories.active', '=', true)
            ->orderBy('categories.name', 'asc')
            ->get();
    }

}