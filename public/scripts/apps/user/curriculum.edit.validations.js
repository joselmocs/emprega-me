/*!
 * apps.user.curriculum.edit.validation.js
 * Scripts responsáveis pela validação dos formulários de edição do currículo.
 *
 * Joselmo Cardozo
 */

(function() {
    define([], function() {
        var
            error = {
                // Quantidade de erros geradas após validação
                count: -1,

                // Primeiro input com erro de validação
                first_element_error: null,

                // Reseta todas as informações da validação
                reset: function() {
                    check.inputs = {};
                    check.objects = {};
                    this.count = 0;
                    this.first_element_error = null;
                },

                // Remove as mensagens de erros dos inputs
                clear: function($element) {
                    $element.find('input.error, select.error').removeClass('error');
                    $element.find('div.error').remove();
                },

                // Exibe os erros em cada elemento
                show: function($element, message) {
                    this.count++;

                    if (this.first_element_error == null) {
                        this.first_element_error = $element;
                    }

                    var $span = $('<div></div>')
                        .addClass('error')
                        .html(message);
                    $element.addClass('error');
                    $element.parent().append($span);
                }
            },

            // Boolean informando se todos os campos foram validados ou não
            validated = false,

            // Faz a validação de todos campos
            validate = function() {
                this.validated = false;

                error.reset();
                check.personal();
                check.contact();
                check.formations();
                check.courses();
                check.languages();
                check.positions();
                check.experiences();
                check.notes();

                if (error.count == 0) {
                    this.validated = true;
                }
            },

            check = {
                inputs: {},
                objects: {},

                input: function($element, name, message, custom) {
                    var value;

                    try {
                        value = $element.val().trim();
                    }
                    catch(e) {
                        value = 0;
                    }

                    if ($element.attr('type') == "checkbox") {
                        value = ($element.is(':checked')) ? 1 : 0;
                    }

                    this.inputs[name] = value;
                    if (value.length || message == null || typeof(message) == "undefined") {
                        if (typeof(custom) != "undefined" && custom != null) {
                            var custom_check = custom($element);
                            if (!custom_check[0]) {
                                error.show($element, custom_check[1]);
                            }
                        }
                    }
                    else {
                        error.show($element, message);
                    }
                },

                // Checagem para os dados pessoais e de contato
                personal: function() {
                    var self = this;
                    $.each($(".personal_tables"), function(i, element) {
                        self.inputs = {};
                        var $element = $(element);
                        error.clear($element);

                        // Nome Completo
                        check.input(
                            $($element.find('input[name=fullname]')),
                            'fullname',
                            'Este campo é obrigatório.'
                        );

                        // Sexo
                        check.input(
                            $($element.find('select[name=gender]')),
                            'gender',
                            'Este campo é obrigatório.'
                        );

                        // Estado Civil
                        check.input(
                            $($element.find('select[name=marital_status]')),
                            'marital_status'
                        );

                        // Estado
                        check.input(
                            $($element.find('select[name=estate_id]')),
                            'estate_id',
                            'Este campo é obrigatório.'
                        );

                        // Cidade
                        check.input(
                            $($element.find('select[name=city_id]')),
                            'city_id',
                            'Este campo é obrigatório.'
                        );

                        // Dosponibilidade para viagens
                        check.input(
                            $($element.find('select[name=travel_availability]')),
                            'travel_availability'
                        );

                        // Disponibilidade para viver em outra cidade
                        check.input(
                            $($element.find('select[name=live_another_city_availability]')),
                            'live_another_city_availability'
                        );

                        // Necessidades especiais
                        check.input(
                            $($element.find('select[name=select_special_needs]')),
                            'select_special_needs'
                        );

                        check.input(
                            $($element.find('select[name=special_needs]')),
                            'special_needs',
                            null,
                            function($input) {
                                if ($('select[name=select_special_needs] option:selected').val() == 1) {
                                    var value = $input.find('option:selected');
                                    if (!value || value.val() == 0) {
                                        return [false, 'É necessário específicar qual a sua necessidade especial.'];
                                    }
                                }
                                return [true, null];
                            }
                        );

                        // CNH
                        var cnh = ['a', 'b', 'c', 'd', 'e'];
                        $.each(cnh, function(i, letter) {
                            var name = 'cnh_'+ letter;
                            check.input(
                                $($element.find('input[name='+ name +']')),
                                name
                            );
                        });

                        // Veículos
                        var auto = ['motorcycle', 'car', 'bus', 'truck'];
                        $.each(auto, function(i, type) {
                            var name = 'auto_'+ type;
                            check.input(
                                $($element.find('input[name='+ name +']')),
                                name
                            );
                        });

                        self.objects['personal'] = self.inputs;
                    });
                },

                // Validação para campos de Contato
                contact: function() {
                    var self = this;
                    $.each($(".contact_tables"), function(i, element) {
                        self.inputs = {};
                        var $element = $(element);
                        error.clear($element);

                        // Telefone
                        check.input(
                            $($element.find('input[name=phone]')),
                            'phone',
                            'Este campo é obrigatório.'
                        );

                        // Celular
                        check.input(
                            $($element.find('input[name=mobile]')),
                            'mobile',
                            'Este campo é obrigatório.'
                        );

                        // Facebook
                        check.input(
                            $($element.find('input[name=facebook]')),
                            'facebook'
                        );

                        // LinkedIn
                        check.input(
                            $($element.find('input[name=linkedin]')),
                            'linkedin'
                        );

                        // Google Plus
                        check.input(
                            $($element.find('input[name=gplus]')),
                            'gplus'
                        );

                        // Twitter
                        check.input(
                            $($element.find('input[name=twitter]')),
                            'twitter'
                        );

                        self.objects['contact'] = self.inputs;
                    });
                },

                // Validação para Formação acadêmica
                formations: function() {
                    var self = this;
                    self.objects['formations'] = [];
                    $.each($(".formation_tables"), function(i, element) {
                        self.inputs = {};
                        var $element = $(element);
                        error.clear($element);

                        // Formação
                        check.input(
                            $($element.find('select[name=level]')),
                            'level',
                            'Este campo é obrigatório.'
                        );

                        // Situação
                        check.input(
                            $($element.find('select[name=situation]')),
                            'situation',
                            'Este campo é obrigatório.'
                        );

                        // Instituição
                        check.input(
                            $($element.find('input[name=institution]')),
                            'institution',
                            'Este campo é obrigatório.'
                        );

                        // Área de Estudo
                        check.input(
                            $($element.find('input[name=study_area]')),
                            'study_area',
                            null,
                            function($input) {
                                var $level = $element.find('select[name=level]').val();
                                if ($level != "" && $level != "1" && $level != "2") {
                                    if (!$input.val().length) {
                                        return [false, 'Este campo é obrigatório.'];
                                    }
                                }
                                return [true, null];
                            }
                        );

                        // Especialização
                        check.input(
                            $($element.find('input[name=specialization]')),
                            'specialization'
                        );

                        // Data de Início
                        check.input(
                            $($element.find('input[name=started_at]')),
                            'started_at',
                            'Este campo é obrigatório.',
                            function($input) {
                                var date = $input.val().trim().split('/');
                                if (date[0] > 12 || date[1] < 1900 || date[1] > new Date().getFullYear()) {
                                    return [false, 'Data inválida'];
                                }
                                return [true, null];
                            }
                        );

                        // Data de Conclusão
                        check.input(
                            $($element.find('input[name=ended_at]')),
                            'ended_at',
                            'Este campo é obrigatório.',
                            function($input) {
                                var date = $input.val().trim().split('/');
                                if (date[0] > 12 || date[1] < 1900) {
                                    return [false, 'Data inválida'];
                                }
                                return [true, null];
                            }
                        );

                        self.objects['formations'].push(self.inputs);
                    });
                },

                // Validação para Cursos
                courses: function() {
                    var self = this;
                    self.objects['courses'] = [];
                    $.each($(".course_tables"), function(i, element) {
                        self.inputs = {};
                        var $element = $(element);
                        error.clear($element);

                        // Curso
                        check.input(
                            $($element.find('input[name=course]')),
                            'course',
                            'Este campo é obrigatório.'
                        );

                        // Instituição
                        check.input(
                            $($element.find('input[name=institution]')),
                            'institution',
                            'Este campo é obrigatório.'
                        );

                        // Data de Início
                        check.input(
                            $($element.find('input[name=started_at]')),
                            'started_at',
                            'Este campo é obrigatório.',
                            function($input) {
                                var date = $input.val().trim().split('/');
                                if (date[0] > 12 || date[1] < 1900 || date[1] > new Date().getFullYear()) {
                                    return [false, 'Data inválida'];
                                }
                                return [true, null];
                            }
                        );

                        // Duração do curso
                        check.input(
                            $($element.find('input[name=duration]')),
                            'duration',
                            'Este campo é obrigatório.'
                        );

                        self.objects['courses'].push(self.inputs);
                    });
                },

                // Checagem para os Idiomas
                languages: function() {
                    var self = this;
                    self.objects['languages'] = [];
                    $.each($(".language_tables"), function(i, element) {
                        var $element = $(element);
                        var language = $element.find('b[data-name=language]').attr('data-language');
                        var level = $element.find('span[data-name=level]').attr('data-level');
                        self.objects['languages'].push({'language_id': language, 'level': level});
                    });
                },

                // Checagem para Cargos pretendidos
                positions: function() {
                    var self = this;
                    self.objects['positions'] = [];
                    $.each($(".position_tables"), function(i, element) {
                        var $element = $(element);

                        var position = $element.find('b[name=desired_position]').html();
                        var salary = $element.find('span[data-name=salary]').attr('data-salary');

                        self.objects['positions'].push({'position': position, 'salary_id': salary});
                    });
                },

                // Validação para Experiências profissionais
                experiences: function() {
                    var self = this;
                    self.objects['experiences'] = [];
                    $.each($(".experience_tables"), function(i, element) {
                        self.inputs = {};
                        var $element = $(element);
                        error.clear($element);

                        // Empresa
                        check.input(
                            $($element.find('input[name=company]')),
                            'company',
                            'Este campo é obrigatório.'
                        );

                        // Cargo/Função
                        check.input(
                            $($element.find('input[name=position]')),
                            'position',
                            'Este campo é obrigatório.'
                        );

                        // Salário
                        check.input(
                            $($element.find('input[name=salary]')),
                            'salary',
                            'Este campo é obrigatório.'
                        );

                        // Data de Início
                        check.input(
                            $($element.find('input[name=started_at]')),
                            'started_at',
                            'Este campo é obrigatório.',
                            function($input) {
                                var date = $input.val().trim().split('/');
                                if (date[0] > 12 || date[1] < 1900 || date[1] > new Date().getFullYear()) {
                                    return [false, 'Data inválida'];
                                }
                                return [true, null];
                            }
                        );

                        // Data de Conclusão
                        check.input(
                            $($element.find('input[name=ended_at]')),
                            'ended_at',
                            'Este campo é obrigatório.',
                            function($input) {
                                var date = $input.val().trim().split('/');
                                if (date[0] > 12 || date[1] < 1900) {
                                    return [false, 'Data inválida'];
                                }
                                return [true, null];
                            }
                        );

                        self.objects['experiences'].push(self.inputs);
                    });
                },

                // Checa as observações do currículo
                notes: function() {
                    var self = this;
                    self.objects['notes'] = $('textarea[name=notes]').val();
                }
            };

        return {
            // Retorna um objeto javascript com o nome dos inputs e seus respectivos valores
            objects: function() {
                return check.objects;
            },
            // Retorna o primeiro input com erro na validação
            first_element_error: function() {
                return error.first_element_error;
            },
            validated: validated,
            validate: validate
        }
    });
})();
