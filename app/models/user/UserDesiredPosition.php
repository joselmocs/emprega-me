<?php


class UserDesiredPosition extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_desired_positions';

    /**
     * Retorna o perfil do usuário deste cargo pretendido
     *
     * @return UserProfile
     */
    public function profile()
    {
        return $this->belongsTo('UserProfile', 'profile_id');
    }

    /**
     * Retorna o salário pretendido deste cargo.
     *
     * @return Salary
     */
    public function salary()
    {
        return $this->belongsTo('Salary', 'salary_id');
    }

}