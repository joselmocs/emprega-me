<?php

use Illuminate\Database\Migrations\Migration;


class CreateJobTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the job table
        Schema::create('jobs', function($table)
        {
            $table->increments('id');
            $table->integer('profile_id')->unsigned()->nullable();
            $table->integer('type_id')->unsigned()->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->integer('estate_id')->unsigned()->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->integer('district_id')->unsigned()->nullable();
            $table->string('slug')->nullable();
            $table->string('position');
            $table->string('code')->nullable();
            $table->text('description')->nullable();
            $table->string('company')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('phone')->nullable();
            $table->boolean('notify')->default(true);
            $table->integer('views')->default(0);
            $table->tinyInteger('status')->default(1);
            $table->boolean('featured')->default(false);
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->foreign('profile_id')->references('id')->on('user_profile');
            $table->foreign('type_id')->references('id')->on('job_types');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('estate_id')->references('id')->on('location_estates');
            $table->foreign('city_id')->references('id')->on('location_cities');
            $table->foreign('district_id')->references('id')->on('location_districts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }

}
