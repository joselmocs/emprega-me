@extends("backend/template")

@section("requirejs")
requirejs(["apps/backend/companies"]);
@endsection

@section("content")
<div id="empresas" class="page_template"><!--empresas-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="auto 100%" valign="top"><!--coluna_1-->

<div class="botao"><!--botao-->
@if (Auth::user()->can('manage_companies'))
<li><a title="Adicionar empresa" href="{{ URL::action('BackendCompanyController@register') }}">Adicionar empresa</a></li>
@endif
</div><!--botao-->

<div class="titulo_grande" style="text-transform: uppercase;"><!--titulo_grande-->
<img src="/images/backend/icone_empresas.jpg"> EMPRESAS
</div><!--titulo_medio-->

<div class="page_count-letter"><!--page_count-letter-->
<li><a href="{{ $context->filter->url }}/-" title="Todas as empresas" @if(strtoupper($context->letter) == '-') class="active" @endif>Todas</a></li>
<li><a href="{{ $context->filter->url }}/+" title="Empresas com números" @if(strtoupper($context->letter) == ' ') class="active" @endif>#</a></li>
@foreach(range('A', 'Z') as $letter)
<li><a href="{{ $context->filter->url }}/{{ strtolower($letter) }}" title="Empresas com a letra {{ $letter }}" @if(strtoupper($context->letter) == $letter) class="active" @endif>{{ $letter }}</a></li>
@endforeach
</div><!--page_count-letter-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
@if($context->new_search)
50 últimas empresas cadastradas
@else
Exibindo {{ $context->companies->getFrom() }}
 a {{ $context->companies->getTo() }}
 de {{ $context->companies->getTotal() }} empresa(s).
@endif
</div><!--titulo_pequeno-->

@if (!$context->companies->count())
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a>Nenhuma empresa encontrada.</a>
</span>
</div>
@else
@foreach($context->companies as $i => $company)

<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png"><a href="{{ URL::action('BackendCompanyController@jobs', array($company->id)) }}" title="Ver vagas da empresa">{{ $company->profile->company }}</a>
<small><span>de </span>{{ $company->profile->city->name }}/{{ $company->profile->city->estate->short }}</small>
</span>

@if (Auth::user()->can('manage_companies'))
<div class="row_menu">
<a title="Editar empresa" href="{{ URL::action('BackendCompanyController@register', array($company->id)) }}"><img src="/images/backend/row_menu_editar.png" title="Editar"></a>
<a href="javascript:void(0)" data-id="{{ $company->id }}" data-name="{{ $company->profile->company }}" name="remover"><img src="/images/backend/row_menu_remover.png" title="Remover conta"></a>
</div>
@endif

<span class="counter">
<small><b>{{ $company->profile->jobs->count() }}</b> vagas cadastradas</small>
</span>

</div><!--row/row-alt-->
@endforeach
@endif

@if (!$context->new_search)
<div class="page_count-letter"><!--page_count-letter-->
{{ $context->companies->links() }}
</div><!--page_count-letter-->
@endif

</td><!--coluna_1-->

<td style="width: 15px;"></td>

<td width="300" valign="top"><!--coluna_2-->
<div style="margin: 0 0 20px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Estatisticas
</div><!--titulo_pequeno-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
  <td align="left">
    
  <table width="0%" border="0" cellspacing="0" cellpadding="4">
  <tr>
  <td width="100%" height="25" align="left">Total de empresas: {{ $context->total_companies }}</td>
  </tr>
  </table>
    
  </td>
</tr>
<tr>
  <td height="15"><hr width="100%" style=" background: #EEEEEE; color: #EEEEEE; height:2px;"></td>
</tr>
<tr>
  <td align="left">
    
  <table width="0%" border="0" cellspacing="0" cellpadding="4">
  <tr>
  <td height="25" align="left">Empresas hoje: 00</td>
  </tr>
  <tr>
  <td height="25" align="left">Empresas ontem: 00</td>
  </tr>
  <tr>
  <td height="25" align="left">Empresas na semana: 00</td>
  </tr>
  <tr>
    <td height="25" align="left">Empresas no mês: 00</td>
  </tr>
  </table>
    
    
  </td>
</tr>
</table>

</div><!--div margin-->



<div style="margin: 0 0 20px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Filtro de empresas
</div><!--titulo_pequeno-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario moldura_cinza">
<tr>
  <td height="15">Localização/estado:</td>
</tr>
<tr>
<td align="left">

<table width="0%" border="0" cellspacing="0" cellpadding="4">
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;"><input name="checkbox" type="checkbox" id="checkbox_mark_all" @if ($context->filter->estates == '-') checked="checked" @endif></td>
<td width="100%" height="25" align="left">Todos os estados</td>
</tr>
@foreach($context->estates as $estate)
<tr>
<td width="0%" height="25" align="center" style="padding: 2px 5px 0 0;"><input name="checkbox_estates" value="{{ strtolower($estate->short) }}" type="checkbox" @if ($context->filter->estates == '-' || strpos($context->filter->estates, strtolower($estate->short)) !== false) checked="checked" @endif></td>
<td width="100%" height="25" align="left" style="color: #888888;">{{ $estate->name }}/{{ $estate->short }}</td>
</tr>
@endforeach
</table>


</td>
</tr>
</table>

</div><!--div margin-->

</td><!--coluna_2-->
</tr>
</table>

</div><!--page_template-->

</div><!--container-->
@endsection