@extends('backend.template')

@section('requirejs')
requirejs(["apps/backend/job.edit"]);
@endsection

@section('scripts')
<script type="text/javascript" src="/packages/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        skin: 'light',
        language : "pt_BR",
        width: 745,
        height: 265,
        plugins: [
            "advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
            "save contextmenu directionality template paste textcolor"
        ],
        toolbar: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | print preview fullpage | forecolor backcolor",
        style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ]
    });
</script>
@endsection

@section('content')
<div id="cadastrar-editar_vaga_1" class="page_template"><!--cadastrar-editar_vaga_1-->
    <div class="titulo_medio"><!--titulo_medio-->
        <img src="/images/backend/icone_vaga.jpg"/>
        @if ($context->editing)
            EDITAR VAGA
        @else
            CADASTRAR NOVA VAGA
        @endif
    </div><!--titulo_medio-->

    <form action="" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody><tr>

                <td width="100%" valign="top"><!--coluna_1-->

                    <div class="titulo_pequeno"><!--titulo_pequeno-->
                        Detalhes da vaga:
                    </div><!--titulo_pequeno-->

                    <table width="100%" border="0" cellspacing="2" cellpadding="0" class="moldura_cinza formulario page_template_links">
                        <tbody><tr>
                            <td align="left">Tipo de trabalho:</td>
                        </tr>
                        <tr>
                            <td align="left">
                                @foreach($context->job_types as $type)
                                    <label><input type="radio" name="type_id" value="{{ $type->id }}" @if($type->id == $context->input->type_id) checked="checked" @endif /> <img src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $type->image }}" title="{{ $type->name }}"/></label>
                                @endforeach
                                @if ($context->field_errors && $context->field_errors->has('type_id'))
                                    <div class="error">{{ $context->field_errors->first('type_id') }}</div>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td height="22" align="left" valign="bottom">Categoria:</td>
                        </tr>
                        <tr>
                            <td align="left">
                                <select name="category_id" required @if ($context->field_errors && $context->field_errors->has('category_id')) class="error" @endif >
                                    <option value="">Selecione a categoria</option>
                                    @foreach($context->categories as $category)
                                        <option value="{{ $category->id }}" @if($category->id == $context->input->category_id) selected="selected" @endif>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                                @if ($context->field_errors && $context->field_errors->has('category_id'))
                                    <div class="error">{{ $context->field_errors->first('category_id') }}</div>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td align="left">

                                <table width="0%" border="0" cellspacing="0" cellpadding="0">
                                    <tbody>
                                    <tr>
                                        <td height="22" colspan="5" align="left" valign="bottom" style="padding: 0px 0px 2px 0px;">Localização:</td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <select name="estate_id" required @if ($context->field_errors && $context->field_errors->has('estate_id')) class="error" @endif >
                                                <option value="">Selecione o estado</option>
                                                @foreach($context->estates as $estate)
                                                    <option value="{{ $estate->id }}" @if($estate->id == $context->input->estate_id) selected="selected" @endif>{{ $estate->name }}</option>
                                                @endforeach
                                            </select>
                                            @if ($context->field_errors && $context->field_errors->has('estate_id'))
                                                <div class="error">{{ $context->field_errors->first('estate_id') }}</div>
                                            @endif
                                        </td>
                                        <td width="10"></td>
                                        <td>
                                            <select name="city_id" required @if ($context->field_errors && $context->field_errors->has('city_id')) class="error" @endif >
                                                <option value="">Selecione a cidade</option>
                                            </select>
                                            @if ($context->field_errors && $context->field_errors->has('city_id'))
                                                <div class="error">{{ $context->field_errors->first('city_id') }}</div>
                                            @endif
                                        </td>
                                        <td width="10"></td>
                                        <td>
                                            <select name="district_id">
                                                <option value="">Selecione o bairro</option>
                                            </select>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                            </td>
                        </tr>
                        <tr>
                            <td height="22" align="left" valign="bottom">Título:</td>
                        </tr>
                        <tr>
                            <td align="left">

                                <table width="0%" border="0" cellspacing="0" cellpadding="0">
                                    <tbody><tr>
                                        <td>
                                            <input type="text" style="width: 350px;" name="position" required="required" value="{{ $context->input->position }}"
                                            @if ($context->field_errors && $context->field_errors->has('position')) class="error" @endif >
                                            @if ($context->field_errors && $context->field_errors->has('position'))
                                                <div class="error">{{ $context->field_errors->first('position') }}</div>
                                            @endif
                                        </td>
                                    </tr>
                                    </tbody></table>

                            </td>
                        </tr>
                        <tr>
                            <td height="22" align="left" valign="bottom">Descrição:</td>
                        </tr>
                        <tr>
                            <td align="left">
                        <textarea name="description"
                        @if ($context->field_errors && $context->field_errors->has('city_id')) class="error" @endif >{{ $context->input->description }}</textarea>
                                @if ($context->field_errors && $context->field_errors->has('description'))
                                    <div class="error">{{ $context->field_errors->first('description') }}</div>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </td><!--coluna_1-->



                <td width="0%" valign="top"><!--coluna_2-->
                    <div id="vaga_cadastrar-editar_sidebar"><!--vaga_cadastrar-editar_sidebar-->

                        <div style="margin: 0px 0px 10px 0px"><!--div margin-->

                            <div class="titulo_pequeno"><!--titulo_pequeno-->
                                Dados do empregador/responsável:
                            </div><!--titulo_pequeno-->

                            <table width="100%" border="0" cellspacing="2" cellpadding="0" class="moldura_cinza formulario page_template_links">
                                <tbody><tr>
                                    <td align="left" valign="bottom">Empresa:</td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <input type="text" style="width: 200px;" required="required" value="{{ $context->input->company }}" name="company"
                                        @if ($context->field_errors && $context->field_errors->has('company')) class="error" @endif >
                                        @if ($context->field_errors && $context->field_errors->has('company'))
                                            <div class="error">{{ $context->field_errors->first('company') }}</div>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td height="22" align="left" valign="bottom">E-mail:</td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <input type="email" style="width: 200px;" required="required" value="{{ $context->input->email }}" name="email"
                                        @if ($context->field_errors && $context->field_errors->has('email')) class="error" @endif >
                                        @if ($context->field_errors && $context->field_errors->has('email'))
                                            <div class="error">{{ $context->field_errors->first('email') }}</div>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td height="22" align="left">Phone:</td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <input type="text" style="width: 175px;" value="{{ $context->input->phone }}" name="phone" />
                                    </td>
                                </tr>
                                <tr>
                                    <td height="22" align="left">Website:</td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <input type="text" style="width: 175px;" value="{{ $context->input->website }}" name="website" />
                                    </td>
                                </tr>
                            </tbody>
                            </table>

                        </div><!--div margin-->

                        <div class="moldura_cinza"><!--moldura_cinza-->
                            <table width="100%" border="0" cellspacing="2" cellpadding="0" class="formulario page_template_links">
                                <tbody><tr>
                                    <td>
                                        <button type="submit" value="1" name="action">Salvar</button>
                                        <button type="submit" value="2" name="action">Salvar e aprovar</button>
                                    </td>
                                </tr>
                                </tbody></table>
                        </div><!--moldura_cinza-->

                    </div><!--vaga_cadastrar-editar_sidebar-->
                </td><!--coluna_2-->

            </tr>
            </tbody>
        </table>

    </form>
</div><!--cadastrar-editar_vaga_1-->

</div><!--container-->
@endsection
