<?php

use Illuminate\Database\Migrations\Migration;


class CreateUserEducationsTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the user_educations table
        Schema::create('user_educations', function($table)
        {
            $table->increments('id');
            $table->integer('profile_id')->unsigned()->nullable();
            $table->tinyInteger('level');
            $table->tinyInteger('situation');
            $table->string('study_area');
            $table->string('specialization')->nullable();
            $table->string('institution');
            $table->timestamp('started_at')->default('0000-00-00 00:00:00');
            $table->timestamp('ended_at')->default('0000-00-00 00:00:00');
            $table->timestamps();

            $table->foreign('profile_id')->references('id')->on('user_profile');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop user_educations table
        Schema::dropIfExists('user_educations');
    }

}
