<?php


class UserCourse extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_courses';

    /**
     * Retorna o perfil do usuário deste curso
     *
     * @return UserProfile
     */
    public function profile()
    {
        return $this->belongsTo('UserProfile', 'profile_id');
    }

}