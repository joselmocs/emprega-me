<?php


class JobReport extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'job_reports';

    /**
     * Retorna o usuário
     *
     * @return UserProfile
     */
    public function profile()
    {
        return $this->belongsTo('UserProfile', 'profile_id');
    }

    /**
     * Retorna a vaga
     *
     * @return User
     */
    public function job()
    {
        return $this->belongsTo('Job', 'job_id');
    }

}