@extends("base/template")

@section("scripts")
<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5311bb1b620b58b8"></script>
@endsection

@section("site-title")
{{ $context->job->estate->short .' - '. $context->job->position .' por '. $context->job->company .' - '. Config::get('domain.site.title') }}
@endsection

@section("navigation")
<li><a href="{{ URL::action('CategoryController@index') }}" title="Lista de Categorias">Categorias</a></li>
<li><a href="{{ URL::action('CategoryController@view', array($context->job->category->url, '-', '-', '-')) }}" title="{{ $context->job->category->name }}">{{ $context->job->category->name }}</a></li>
@endsection

@section("content")
@include("advertising/header")
<div class="page_template"><!--page_template-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="auto 100%" valign="top"><!--coluna_1-->

@if ($context->job->type->slug == "vaga-pcd")
<div class="alerta_cinza">Esta é vaga para pessoas com deficiência (PCD).</div>
@endif

<div id="vaga_candidatos" title="{{ $context->job->candidacies()->count() }} canditatos"><!--vaga_candidatos-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>{{ $context->job->candidacies->count() }}</td>
</tr>
<tr>
<td><span>candidatos</span></td>
</tr>
</table>
</div><!--vaga_candidatos-->

<div id="vaga_like_button"><!--vaga_like_button-->
<div class="fb-like" data-layout="box_count" data-href="{{ URL::current() }}" data-action="like" data-show-faces="false" data-share="false"></div>
</div><!--vaga_like_button-->

<div class="titulo_grande"><!--titulo_grande-->
{{ $context->job->position }} <img style="position: relative; top: 2px;" src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $context->job->type->image }}" title="{{ $context->job->type->name }}"/>
</div><!--titulo_grande-->

<div class="page_template_links" style="font-size: 12px; margin: 0 0 20px 0;"><!--vaga_dados-->
Por <a href="vagas da empresa" title="Empresa">{{ $context->job->company }}</a> em <a href="vagas da cidade" title="nomecidade">{{ $context->job->city->name }}/{{ $context->job->estate->short }}</a>@if($context->job->district_id), bairro  <a href="{{ $context->job->district->name }}" title="Bairro">{{ $context->job->district->name }}</a>@endif.
</div><!--vaga_dados-->


<div style="margin: 0 0 20px 0;"><!--div margin-->

<div class="page_template_links" style="font-size: 12px; min-height: 100px;"><!--vaga_descricao-->
{{ nl2br($context->job->description) }}
</div><!--vaga_descricao-->

<div style="margin: 5px 0 0 0; color: #999999; font-size: 11px;"><!--aviso_responsabilidade-->
O {{ Config::get('domain.site.name') }} não participa de qualquer processo seletivo, limitando-se apenas a divulgar as oportunidades de seus anunciantes.
</div><!--aviso_responsabilidade-->

@if ($context->job->phone && $context->job->show_phone)
<div class="page_template_links" style="color: #999999; font-size: 11px;"><!--vaga_empresa_contato-->
- Se preferir, ligue: {{ $context->job->phone }} - Ao ligar, informe que viu este anúncio no {{ Config::get('domain.site.name') }}
<br />
@endif
@if ($context->job->website && $context->job->show_website)
<a href="{{ $context->job->website }}" title="Acessar o website do anunciante" target="_blank">- Acessar o website do anunciante.</a>
@endif
</div><!--vaga_empresa_contato-->


</div><!--div margin-->

<div align="right" style=" float: right; position: relative; right: -2px;"><!--vaga_rede_sociais-->
<div class="addthis_sharing_toolbox"></div>
</div><!--vaga_rede_sociais-->

<div id="vaga_enviar_curriculo"><!--vaga_enviar_curriculo_botao-->
@if ((Auth::check() && Auth::user()->is('user') && !$context->job->candidates()->where('profile_id', Auth::user()->profile->id)->count()))
<a href="{{ URL::action('JobController@join', array($context->request->job_type, $context->request->job, $context->request->city)) }}" title="Enviar currículo">Enviar currículo</a>
@endif
@if (!Auth::check())
<a href="{{ URL::action('UserController@login') }}?candidate&next=/{{ Request::path() }}" title="Enviar currículo">Enviar currículo</a>
@endif
@if (Auth::check() && Auth::user()->is('user') && $context->job->candidates()->where('profile_id', Auth::user()->profile->id)->count())
<spam>Currículo enviado</spam>
@endif
</div><!--vaga_enviar_curriculo_botao-->

<div style="margin: 0px 0 5px 0;"><!--div margin-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td></td>
<td></td>
<td>

<table width="0%" border="0" align="right" cellpadding="0" cellspacing="0">
<tr>
<td><img src="/images/icone_vaga_localizacao.jpg" width="10" height="16" alt=""/></td>
<td style="padding: 0 0 0 5px;">
<div class="titulo_pequeno" align="right"><!--titulo_pequeno-->
Localização
</div><!--titulo_pequeno-->
</td>
</tr>
</table>

</td>
</tr>
<tr>
<td width="336" align="center">{{ AdvertisingController::adsense('job_rectangle_336x280') }}</td>
<td style="width: 10px;"></td>
<td width="auto 100%" align="left" style="position: relative; top: 1px;">
<iframe width="100%" height="280" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=@if ($context->job->district) {{ $context->job->district->name }}, @endif{{ $context->job->city->name }}, +{{ $context->job->estate->short }}&amp;hnear={{ $context->job->city->name }},+{{ $context->job->estate->short }}&amp;output=embed"></iframe>
</td>
</tr>
</table>
</div><!--div margin-->

<div class="page_template_links" style="background: #F5F5F5; font-size: 12px; padding: 4px;"><!--vaga_data_estatisticas-->
Publicado em {{ Carbon::parse($context->job->created_at)->format('d \d\e F \d\e Y') }}, visualizado {{ $context->job->views }} vezes.
</div><!--vaga_data_estatisticas-->

<div class="page_template_links" align="right" style="font-size: 11px; margin: 2px 0 25px 0;"><!--vaga_data_estatisticas-->
Tem algo errado com essa vaga? <a class="fancybox fancybox.iframe" title="Fazer denúncia" href="vaga_denunciar.php" data-fancybox-height="auto" data-fancybox-width="420">Denuncie!</a>
</div><!--vaga_data_estatisticas-->



<div style="margin: 0 0 25px 0;"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Vagas recentes de <b>{{ $context->job->company }}</b>
</div><!--titulo_pequeno-->

@foreach($context->recents as $i => $job)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $job->type->image }}" title="{{ $job->type->name }}"/><a href="{{ URL::action('JobController@view', array($job->type->slug, $job->id, $job->city->slug, $job->slug)) }}" title="{{ $job->position }}">{{ $job->position }}</a>
<small><span>por </span>{{ $job->company }}<span> em </span>{{ $job->city->name }}/{{ $job->estate->short }}</small>
</span>

<span class="time_post">
@if (Carbon::parse($job->created_at)->isToday())
<img src="/images/hoje.png" title="Vaga publicada hoje"/>
@endif
@if (Carbon::parse($job->created_at)->isYesterday())
<img src="/images/ontem.png" title="Vaga publicada ontem"/>
@endif
<img src="/images/clock.png" title="Data da publicação"/>
<small title="{{ Carbon::parse($job->created_at)->format('d \d\e F \d\e Y') }}">{{ Carbon::parse($job->created_at)->format('d/m') }}</small>
</span>
</div><!--row/row-alt-->
@endforeach

</div><!--div margin-->



<div class="titulo_pequeno"><!--titulo_pequeno-->
Vagas semelhantes
</div><!--titulo_pequeno-->

<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $job->type->image }}" title="Emprego"/><a href="vaga.php" title="titulo">Auxiliar Administrativo</a>
<small><span>por </span>Empresa/pessoa<span> em </span>Belo Horizonte</small>
</span>

<span class="time_post">
<img src="/images/hoje.png" title="Vaga publicada hoje"/>
<img src="/images/ontem.png" title="Vaga publicada ontem"/>
<img src="/images/clock.png" title="Data da publicação"/>
<small title="00 de março de 2014">05/03</small>
</span>
</div><!--row/row-alt-->
</td><!--coluna_1-->

<td style="width: 15px;"></td>

<td width="300" valign="top"><!--coluna_2-->
<div style="margin: 0 0 25px 0"><!--div margin-->
{{ AdvertisingController::adsense('large_600x300') }}
</div><!--div margin-->


<div class="categorias_sidebar" style="font-size: 12px; margin: 0 0 25px 0"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Escolha a categoria
</div><!--titulo_pequeno-->

<ul>
@foreach($context->categories as $category)
<li><a href="{{ URL::action('CategoryController@view', array($category->url, '-', '-', '-')) }}">{{ Str::limit($category->name, 40, '...') }} <small>({{ $category->jobs->count() }})</small></a></li>
@endforeach
</ul>

</div><!--div margin-->


<div style="margin: 0 0 25px 0"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Mais concorridas da categoria
</div><!--titulo_pequeno-->

@if (!$context->busiest_category->count())
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a>Nenhuma vaga encontrada na categoria</a>
</span>
</div>
@else
@foreach($context->busiest_category as $i => $job)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a href="{{ URL::action('JobController@view', array($job->type->slug, $job->id, $job->city->slug, $job->slug)) }}" title="{{ $job->type->name }} - {{ $job->position }} por {{ $job->company }}">Auxiliar Administrativo</a>
</span>

<span class="counter">
<small title="{{ $job->t }} candidatos"><b>{{ $job->t }}</b></small>
</span>
</div><!--row/row-alt-->
@endforeach
@endif

</div><!--div margin-->
</td><!--coluna_2-->
</tr>
</table>
</div><!--vaga-->

</div><!--container-->
@include("advertising/facebook")
@endsection
