@extends("backend/template")

@section("content")
<div id="publicidade_editar_banner_adicionar-editar"><!--publicidade_editar_banner_adicionar-editar-->

    <div class="titulo_pequeno"><!--titulo_medio-->
        Adicionar Banner:
    </div><!--titulo_medio-->

    <form action="{{ URL::action('BackendAdvertisingController@banners_save') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" name="type" value="{{ $context->banner_type->short }}" />
        <table width="100%" border="0" cellspacing="2" cellpadding="0" class="moldura_cinza formulario">
            <tr>
                <td align="left">Nome:</td>
            </tr>
            <tr>
                <td align="left">
                    <input type="text" style="width: 362px;" name="name" required="required" />
                </td>
            </tr>
            <tr>
                <td height="5" align="left"></td>
            </tr>
            <tr>
                <td align="left">URL:</td>
            </tr>
            <tr>
                <td align="left">
                    <input type="text" style="width: 250px;" name="url" required="required" />
                </td>
            </tr>
            <tr>
                <td align="left">Imagem:</td>
            </tr>
            <tr>
                <td align="left">
                    <input type="text" style="width: 250px;" name="image" required="required" />
                </td>
            </tr>
            <tr>
                <td align="left">Limite de visualizações:</td>
            </tr>
            <tr>
                <td align="left">
                    <input type="text" style="width: 250px;" name="max_views" required="required" value="0" />
                    <small>0 = Sem limite de visualizações</small>
                </td>
            </tr>
            <tr>
                <td align="left">Ativo:</td>
            </tr>
            <tr>
                <td align="left">
                    <select name="active" required="required">
                        <option value="0">Não</option>
                        <option value="1" selected="selected">Sim</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td align="left">Domínio:</td>
            </tr>
            <tr>
                <td align="left">
                    <select name="domain">
                        <option value="">Todos</option>
                        <option value="empregame">Emprega-me</option>
                        <option value="bhjobs">BHJobs</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td>
                    <input type="submit" value="Adicionar" />
                </td>
            </tr>
        </table>
    </form>

</div><!--publicidade_editar_banner_adicionar-editar-->
@endsection
