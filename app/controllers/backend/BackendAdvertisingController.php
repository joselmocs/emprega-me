<?php

class BackendAdvertisingController extends BaseBackendController {

    /**
     * Exibe a página inicial para configurações de publicidade
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        $this->set_context(array(
            'banner_types' => AdvertisingBannerType::all(),
            'adsenses' => AdvertisingAdsense::all()
        ));

        return $this->view_make('backend/advertising/index');
    }

    /**
     * Exibe a página para visualização e edição do banner adsense
     *
     *  @param string $short código para identificação do adsense
     *  @return Illuminate\Support\Facades\View
     */
    public function adsense_view($short)
    {
        $adsense = AdvertisingAdsense::where('short', '=', $short)->firstOrFail();

        $this->set_context(array(
            'adsense' => $adsense
        ));

        return $this->view_make('backend/advertising/adsense/view');
    }

    /**
     * Salva o código do banner adsense
     *
     * @param string $short código para identificação do adsense
     * @return Illuminate\Support\Facades\View
     */
    public function adsense_save($short)
    {
        $adsense = AdvertisingAdsense::where('short', '=', $short)->firstOrFail();
        $adsense->code_empregame = Input::get('code_empregame');
        $adsense->code_bhjobs = Input::get('code_bhjobs');
        $adsense->save();

        return Redirect::action('BackendAdvertisingController@index');
    }

    /**
     * Ativa um banner adsense
     *
     * @param string $short código para identificação do adsense
     * @return Illuminate\Support\Facades\View
     */
    public function adsense_active($short)
    {
        $adsense = AdvertisingAdsense::where('short', '=', $short)->firstOrFail();
        $adsense->active = true;
        $adsense->save();

        return Redirect::action('BackendAdvertisingController@index');
    }

    /**
     * Desativa um banner adsense
     *
     * @param string $short código para identificação do adsense
     * @return Illuminate\Support\Facades\View
     */
    public function adsense_deactive($short)
    {
        $adsense = AdvertisingAdsense::where('short', '=', $short)->firstOrFail();
        $adsense->active = false;
        $adsense->save();

        return Redirect::action('BackendAdvertisingController@index');
    }

    /**
     * Exibe a página de listagem de banners
     *
     * @param string $short código para identificação do tipo do banner
     * @return Illuminate\Support\Facades\View
     */
    public function banners_list($short)
    {
        $this->set_json_context(array(
            'urls' => array(
                'remove' => URL::action('BackendAdvertisingController@banners_remove')
            )
        ));

        $type = AdvertisingBannerType::where('short', '=', $short)->firstOrFail();

        $this->set_context(array(
            'banner_type' => $type,
            'banners' => $type->banners
        ));

        return $this->view_make('backend/advertising/banner/list');
    }

    /**
     * Formulário de criação de banners
     *
     * @param string $type código para identificação do tipo do banner
     * @return Illuminate\Support\Facades\View
     */
    public function banners_new($type)
    {
        $type = AdvertisingBannerType::where('short', '=', $type)->firstOrFail();
        $this->set_context(array(
            'banner_type' => $type
        ));

        return $this->view_make('backend/advertising/banner/new');
    }

    public function banners_edit($id)
    {
        $banner = AdvertisingBanner::find($id);
        if (Request::isMethod('get')) {
            $this->set_context(array(
                'banner' => $banner
            ));
            return $this->view_make('backend/advertising/banner/edit');
        }
    }

    /**
     * Cria um banner
     *
     * @return Illuminate\Support\Facades\View
     */
    public function banners_save()
    {
        $banner_type = AdvertisingBannerType::where('short', '=', Input::get('type'))
            ->firstOrFail();

        if (Input::get('id')) {
            $banner = AdvertisingBanner::find(Input::get('id'));
        }
        else {
            $banner = new AdvertisingBanner;
        }

        $banner->type_id = $banner_type->id;
        $banner->name = Input::get('name');
        $banner->url = Input::get('url');
        $banner->image = Input::get('image');
        $banner->max_views = Input::get('max_views', 0);
        $banner->active = Input::get('active') == "1";
        $banner->domain = trim(Input::get('domain'));
        $banner->save();

        return Redirect::action('BackendAdvertisingController@banners_list', array($banner_type->short));
    }

    /**
     * Remove um banner.
     *
     * @param $id int
     * @return Redirect
     */
    public function banners_remove($id)
    {
        if ($id) {
            AdvertisingBanner::find($id)->delete();
        }
        return Redirect::back();
    }

    /**
     * Ativa um banner
     *
     * @param string $id
     * @return Illuminate\Support\Facades\View
     */
    public function banners_active($id)
    {
        $banner = AdvertisingBanner::where('id', '=', $id)->firstOrFail();
        $banner->active = true;
        $banner->save();

        return Redirect::back();
    }

    /**
     * Desativa um banner
     *
     * @param string $id
     * @return Illuminate\Support\Facades\View
     */
    public function banners_deactive($id)
    {
        $banner = AdvertisingBanner::where('id', '=', $id)->firstOrFail();
        $banner->active = false;
        $banner->save();

        return Redirect::back();
    }
}
