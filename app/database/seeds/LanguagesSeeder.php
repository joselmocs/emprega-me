<?php


class LanguagesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idiomas = array(
            "Alemão",
            "Árabe",
            "Chinês",
            "Coreano",
            "Espanhol",
            "Francês",
            "Grego",
            "Hebraico",
            "Inglês",
            "Irlandês",
            "Italiano",
            "Japonês",
            "Romeno",
            "Russo",
            "Sueco"
        );

        foreach ($idiomas as $idioma) {
            $newLan = new Language;
            $newLan->language = $idioma;
            $newLan->save();
        }
    }
}