@extends("base/template")

@section("site-title")
Lista de candidatos - {{ Config::get('domain.site.title') }}
@endsection

@section("navigation")
<li><a href="{{ URL::action('CompanyJobController@lists', array('-')) }}" title="Minhas Vagas">Minhas Vagas</a></li>
@endsection

@section('requirejs')
requirejs(["apps/company/candidacy"]);
@endsection

@section("content")
@include("advertising/header")

<div class="page_template"><!--page_template-->

<div class="titulo_medio"><!--titulo_medio-->
Candidatos <span style="color: #999999;">({{ Auth::user()->profile->candidates()->whereNull('viewed_at')->count() }})</span>
</div><!--titulo_medio-->

<div style="margin: 0 0 20px 0"><!--div margin-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario">
<tr>
<td width="100%">
<a href="linkaqui"><img src="domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="linkaqui"><img src="domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="linkaqui"><img src="domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="linkaqui"><img src="domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="linkaqui"><img src="domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
<a href="linkaqui"><img src="domains/Emprega-me/images/tipos_trabalho/emprego_emprega-me.png" title="Emprego"/></a>
</td>
<td align="right">
<select>
<option value="Todos os generos">Todos os generos</option>
<option value="Masculino">Masculino</option>
</select>
</td>
<td align="right" style="padding: 0 0 0 5px;">
<select>
<option value="Todos as cidades">Todos as cidades</option>
<option value="Belo Horizonte">Belo Horizonte</option>
</select>
</td>
<td align="right" style="padding: 0 0 0 5px;">
<select>
<option value="Todos os estado">Todos os estado</option>
<option value="Minas Gerais/MG">Minas Gerais/MG</option>
</select>
</td>
</tr>
</table>
</div><!--div margin-->


@if (!$context->candidacies->count())
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a>Nenhum candidato encontrado.</a>
</span>
</div>
@else

<div class="titulo_pequeno"><!--titulo_pequeno-->
Exibindo {{ $context->candidacies->getFrom() }}
a {{ $context->candidacies->getTo() }}
de {{ $context->candidacies->getTotal() }} candidato(s).
</div><!--titulo_pequeno-->

@foreach($context->candidacies as $i => $candidacy)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $candidacy->job->type->image }}" title="{{ $candidacy->job->type->name }}"/>
<a class="fancybox fancybox.iframe" title="Currículo {{ $candidacy->profile->fullname }}" href="{{ URL::action('CurriculumController@view', array($candidacy->profile->user->id)) }}" data-fancybox-width="800" data-fancybox-height="850">{{ $candidacy->profile->fullname }}</a>
<small><span>de </span>{{ $candidacy->profile->city->name }}/{{ $candidacy->profile->city->estate->short }}</small>
</span>

<span class="row_menu">
<a href="javascript: void(0);" name="remover" data-name="{{ $candidacy->profile->fullname }}" data-id="{{ $candidacy->id }}"><img src="/images/row_menu_remover.png" title="Remover candidatura"></a>
</span>

<span class="row_menu">
<a class="fancybox fancybox.iframe" title="{{ $candidacy->job->id }} - {{ $candidacy->job->position }}" href="{{ URL::action('CompanyJobController@view', array($candidacy->job->id)) }}" data-fancybox-width="800" data-fancybox-height="600">
<img src="/images/row_menu_ver_vaga.png" title="Ver vaga"/>
</a>
</span>

<span class="time_post">
@if (Carbon::parse($candidacy->created_at)->isToday())
<img src="/images/hoje.png" title="Se candidatou hoje">
@endif
@if (Carbon::parse($candidacy->created_at)->isYesterday())
<img src="/images/ontem.png" title="Se candidatou ontem">
@endif
<img src="/images/clock.png" title="Data da candidatura">
<small title="{{ Carbon::parse($candidacy->created_at)->format('d \d\e F \d\e Y') }}">{{ Carbon::parse($candidacy->created_at)->format('d/m') }}</small>
</span>

</div><!--row/row-alt-->
@endforeach
@endif

<div class="page_count-letter"><!--page_count-letter-->
{{ $context->candidacies->links() }}
</div><!--page_count-letter-->

</div><!--candidatos-->

</div><!--container-->
@include("advertising/footer")
@endsection
