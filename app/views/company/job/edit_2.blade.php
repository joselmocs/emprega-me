@extends('base/template')

@section("site-title")
Anuncie um novo emprego - {{ Config::get('domain.site.title') }}
@endsection

@section('content')
<div class="page_template"><!--page_template-->

<form action="" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}" />
<input type="hidden" name="type_id" value="{{ $context->input->type_id }}" />
<input type="hidden" name="category_id" value="{{ $context->input->category_id }}" />
<input type="hidden" name="position" value="{{ $context->input->position }}" />
<input type="hidden" name="estate_id" value="{{ $context->input->estate_id }}" />
<input type="hidden" name="description" value="{{ $context->input->description }}" />
<input type="hidden" name="company" value="{{ $context->input->company }}" />
<input type="hidden" name="email" value="{{ $context->input->email }}" />
<input type="hidden" name="show_phone" value="{{ $context->input->show_phone }}" />
<input type="hidden" name="show_website" value="{{ $context->input->show_website }}" />
<input type="hidden" name="website" value="{{ $context->input->website }}" />
<input type="hidden" name="phone" value="{{ $context->input->phone }}" />
<input type="hidden" name="city_id" value="{{ $context->city->id }}" />
<input type="hidden" name="district_id" value="@if ($context->district){{ $context->district->id }}@endif" />

<div class="titulo_medio"><!--titulo_medio-->
Verificar vaga
</div><!--titulo_medio-->


<div class="titulo_grande"><!--titulo_grande-->
{{ $context->input->position }} <img style="position: relative; top: 2px;" src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $context->job_type->image }}" title="{{ $context->job_type->name }}"/>
</div><!--titulo_grande-->

<div class="page_template_links" style="font-size: 12px; margin: 0 0 20px 0;"><!--vaga_dados-->
Por <a title="{{ $context->input->company }}">{{ $context->input->company }}</a> em <a title="{{ $context->city->name }}/{{ $context->city->estate->short }}">{{ $context->city->name }}/{{ $context->city->estate->short }}</a>@if($context->district), bairro <a title="{{ $context->district->name }}">{{ $context->district->name }}</a>@endif.
</div><!--vaga_dados-->

<div style="margin: 0 0 20px 0;"><!--div margin-->

<div class="page_template_links" style="font-size: 12px; min-height: 100px;"><!--vaga_descricao-->
{{ nl2br($context->input->description) }}
</div><!--vaga_descricao-->

<div style="margin: 5px 0 0 0; color: #999999; font-size: 11px;"><!--aviso_responsabilidade-->
O {{ Config::get('domain.site.name') }} não participa de qualquer processo seletivo, limitando-se apenas a divulgar as oportunidades de seus anunciantes.
</div><!--aviso_responsabilidade-->

@if ($context->input->phone && $context->input->show_phone)
<div class="page_template_links" style="color: #999999; font-size: 11px;"><!--vaga_empresa_contato-->
- Se preferir, ligue: {{ $context->input->phone }} - Ao ligar, informe que viu este anúncio no {{ Config::get('domain.site.name') }}
<br />
@endif
@if ($context->input->website && $context->input->show_website)
<a href="{{ $context->input->website }}" title="Acessar o website do anunciante" target="_blank">- Acessar o website do anunciante.</a>
@endif
</div><!--vaga_empresa_contato-->

</div><!--div margin-->

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="moldura_cinza formulario">
<tr>
<td align="left">
<button type="submit" name="submit" value="save">Publicar vaga</button>
</td>
</tr>
</table>

<br/ >
<span style="font-size: 12px;" class="page_template_links">
Quer editar seu anúncio? <button type="submit" name="submit" value="edit">Clique aqui</button> para voltar.
<br/ >
Se mudou de ideia, <a href="" title="Cancelar publicação.">clique aqui</a> para cancelar a publicação deste anuncio.
</span>

</div><!--page_template-->
</form>
</div><!--container-->
@endsection
