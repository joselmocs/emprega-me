<?php


class BackendLocationController extends BaseBackendController {

    /**
     * Exibe a lista de estados do brasil.
     *
     * @return Illuminate\Support\Facades\View
     */
    public function estates()
    {
        $this->set_context(array(
            'estates' => LocationEstate::all()
        ));

        return $this->view_make('backend/location/estates');
    }

    /**
     * Ativa um estado
     *
     * @param string $short código para identificação do estado
     * @return Illuminate\Support\Facades\View
     */
    public function estate_active($short)
    {
        $adsense = LocationEstate::where('short', '=', $short)->firstOrFail();
        $adsense->active = true;
        $adsense->save();

        return Redirect::action('BackendLocationController@estates');
    }

    /**
     * Desativa um estado
     *
     * @param string $short código para identificação do estado
     * @return Illuminate\Support\Facades\View
     */
    public function estate_deactive($short)
    {
        $adsense = LocationEstate::where('short', '=', $short)->firstOrFail();
        $adsense->active = false;
        $adsense->save();

        return Redirect::action('BackendLocationController@estates');
    }

    /**
     * Exibe a lista de cidades do estado selecionado.
     *
     * @param string $estate abreviação de 2 caracteres do estado
     * @param string $letter letra inicial do nome da cidade para paginação
     * @return Illuminate\Support\Facades\View
     */
    public function cities($estate, $letter)
    {
        $estate = LocationEstate::where('short', '=', strtoupper($estate))->firstOrFail();
        $cities = $estate->cities()
            ->where('name', 'like', strtoupper($letter) .'%')
            ->orderBy('active', 'desc')
            ->orderBy('name', 'asc')
            ->get();

        $this->set_context(array(
            'estate' => $estate,
            'cities' => $cities,
            'letter' => $letter,
            'url_to_filter' => URL::action('BackendLocationController@cities', array(strtolower($estate->short), ''))
        ));

        return $this->view_make('backend/location/cities');
    }

    /**
     * Ativa uma cidade
     *
     * @param int $id código para identificação da cidade
     * @return Illuminate\Support\Facades\View
     */
    public function city_active($id)
    {
        $city = LocationCity::where('id', '=', $id)->firstOrFail();
        $city->active = true;
        $city->save();

        return Redirect::back();
    }

    /**
     * Desativa uma cidade
     *
     * @param int $id código para identificação da cidade
     * @return Illuminate\Support\Facades\View
     */
    public function city_deactive($id)
    {
        $city = LocationCity::where('id', '=', $id)->firstOrFail();
        $city->active = false;
        $city->save();

        return Redirect::back();
    }

    /**
     * Exibe a lista de bairros da cidade selecionada.
     *
     * @param string $city abreviação de 2 caracteres do estado
     * @param string $letter letra inicial do nome do bairro para paginação
     * @return Illuminate\Support\Facades\View
     */
    public function districts($city, $letter)
    {
        $city = LocationCity::where('slug', '=', $city)->firstOrFail();
        $districts = $city->districts()
            ->where('name', 'like', strtoupper($letter) .'%')
            ->orderBy('name', 'asc')
            ->get();

        $this->set_json_context(array(
            'urls' => array(
                'remove' => URL::action('BackendLocationController@district_remove')
            )
        ));

        $this->set_context(array(
            'city' => $city,
            'districts' => $districts,
            'letter' => $letter,
            'url_to_filter' => URL::action('BackendLocationController@districts', array(strtolower($city->slug), ''))
        ));

        return $this->view_make('backend/location/districts');
    }

    /**
     * Ativa um bairro
     *
     * @param int $id código para identificação do bairro
     * @return Illuminate\Support\Facades\View
     */
    public function district_active($id)
    {
        $city = LocationDistrict::where('id', '=', $id)->firstOrFail();
        $city->active = true;
        $city->save();

        return Redirect::back();
    }

    /**
     * Desativa um bairro
     *
     * @param int $id código para identificação do bairro
     * @return Illuminate\Support\Facades\View
     */
    public function district_deactive($id)
    {
        $city = LocationDistrict::where('id', '=', $id)->firstOrFail();
        $city->active = false;
        $city->save();

        return Redirect::back();
    }

    /**
     * Pagina de criacao de novo bairro
     *
     * @param int $city
     * @param string $letter
     * @return Illuminate\Support\Facades\View
     */
    public function district_new($city, $letter)
    {
        $city = LocationCity::find($city);
        $this->set_context(array(
            'city' => $city,
            'letter' => $letter
        ));

        return $this->view_make('backend/location/district/new');
    }

    /**
     * Salva o bairro
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function district_save()
    {
        if (Input::get('id')) {
            $district = LocationDistrict::find(Input::get('id'));
            $city = $district->city;
        }
        else {
            $district = new LocationDistrict;
            $city = LocationCity::find(Input::get('city_id'));
            $district->city_id = $city->id;
        }

        if (!trim(Input::get('name'))) {
            return Redirect::back();
        }

        $district->name = trim(Input::get('name'));
        $district->save();

        return Redirect::action('BackendLocationController@districts', array(strtolower($city->slug), Input::get('letter')));
    }

    /**
     * Edita um bairro
     *
     * @param int $id
     * @param string $letter
     * @return \Illuminate\Http\RedirectResponse
     */
    public function district_edit($id, $letter)
    {
        $district = LocationDistrict::find($id);
        $this->set_context(array(
            'city' => $district->city,
            'district' => $district,
            'letter' => $letter
        ));

        return $this->view_make('backend/location/district/edit');
    }

    /**
     * Remove um bairro.
     *
     * @param $id int
     * @return Redirect
     */
    public function district_remove($id)
    {
        if ($id) {
            LocationDistrict::find($id)->delete();
        }
        return Redirect::back();
    }
}
