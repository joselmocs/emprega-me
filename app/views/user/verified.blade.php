@extends("base/template")

@section("site-title")
Registro de novo candidato - {{ Config::get('domain.site.title') }}
@endsection

@section("content")
@include("advertising/header")
<div class="page_template page_template_links"><!--page_template-->

<div class="titulo_grande"><!--titulo_grande-->
Cadastro ativado
</div><!--titulo_grande-->

<div style="font-size: 12px; margin: 0 0 25px 0;"><!--div-->
Obrigado <b>{{ $context->fullname }}</b>, seu cadastro foi ativado!
</div><!--div-->

<div style="font-size: 12px;"><!--div-->
Para voltar a página inicial <a href="/" title="Voltar para pagina inicial.">clique aqui</a>.
</div><!--div-->

</div><!--page_template-->

</div><!--container-->
@endsection
