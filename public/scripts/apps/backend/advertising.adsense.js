/*!
 * apps.backend.advertising.adsense.js
 * Scripts responsáveis pela edição do código adsense.
 *
 * Joselmo Cardozo
 */

require([
    "helpers/core"
], function(core) {
    var
        initialize = function() {
            events();
        },
        events = function() {
            $('input[name=limpar]').unbind('click').click(function() {
                $("textarea[name=code]").html('').focus();
            });
        };

    initialize();
});