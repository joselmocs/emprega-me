<?php


class Category extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'name', 'title', 'url', 'description', 'keywords', 'active');

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::deleting(function($model) {
            CategoryHighlighted::where('category_id', '=', $model->id)->delete();
        });
    }

    /**
     * Retorna as vagas da categoria
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobs()
    {
        return $this->hasMany('Job', 'category_id');
    }

    /**
     * Retorna o primeiro nome da categoria
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function splited()
    {
        return explode(' ', explode(',', $this->title)[0])[0];
    }
}
