<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ $context->title }}</title>
    <link href="/styles/backend/screen.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/packages/fancybox/source/jquery.fancybox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/packages/fancybox/source/helpers/jquery.fancybox-buttons.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/packages/fancybox/source/helpers/jquery.fancybox-thumbs.css" type="text/css" media="screen" />
</head>

<body>
<div id="container"><!--container-->

<div id="header"><!--header-->

<div id="header_1"><!--header_1-->
	<div id="header_1_sites">
	<li><a href="http://www.emprega-me.com.br/" title="Emprega-me.com.br" target="_blank">Emprega-me</a></li>
	<li><a href="http://www.bhjobs.com.br/" title="BHjobs.com.br" target="_blank">BHjobs</a></li>
	</div>

	<div id="header_1_menu">
	<li><a href="{{ URL::action('UserController@logout') }}" title="Sair">Sair</a></li>
	<li><a href="editar_dados.php" title="Titulo aqui">Editar dados</a></li>
	</div>
</div><!--header_1-->

<div id="header_2"><!--header_2-->

	<div id="header_2_logotipo"><!--header_2_logotipo-->
	<a href="/" title="Ir para a página inicial"><img src="/images/backend/logotipo.png"/></a>
	</div><!--header_2_logotipo-->

	<div id="header_2_buscar_vagas-canditatos">
	<table width="0%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td>
	<form method="POST" action="--WEBBOT-SELF--">
	<input type="text" placeholder="Faça buscas aqui."><input style="display:none;" type="submit" value="" title="Buscar"></input>

<select>
<option selected="selected">Vagas</option>
<option>Canditatos</option>
<option>Empresas</option>
</select>
</form>
	</td>
	</tr>
	<tr>
	<td height="20" align="right" valign="top">
	<i><b>Dica:</b> Escolha por vagas, candidatos e empresas...</i>
	</td>
	</tr>
	</table>
	</div>

	<div id="header_2_cadastrar_vaga"><!--header_2_cadastrar_vaga-->
	<a href="cadastrar_vaga.php" title="Cadastrar vaga">CADASTRAR VAGA</a>
	</div><!--header_2_cadastrar_vaga-->

</div><!--header_2-->


<div id="header_3"><!--header_3-->

<div id="header_3_todas_categorias"><!--header_3_todas_categorias-->
<a href="{{ URL::action('BackendCategoryController@index') }}" title="Todas as categorias">Todas as categorias</a>
</div><!--header_3_todas_categorias-->

<div id="header_3_menus">
<li><a href="/backend" title="Painel incial (+999)">Painel incial (+999)</a></li>
@if (Auth::user()->can('view_candidates'))
<li><a href="{{ URL::action('BackendCandidateController@index', array('-', '-')) }}" title="Candidatos">Candidatos</a></li>
@endif
@if (Auth::user()->can('view_companies'))
<li><a href="{{ URL::action('BackendCompanyController@index', array('-', '-')) }}" title="Empresas">Empresas</a></li>
@endif
@if (Auth::user()->can('view_stats'))
<li><a href="{{ URL::action('BackendStatisticsController@index', array(0)) }}" title="Estatísticas">Estatísticas</a></li>
@endif
@if (Auth::user()->can('view_locations'))
<li><a href="{{ URL::action('BackendLocationController@estates') }}" title="Localizações">Localizações</a></li>
@endif
@if (Auth::user()->can('manage_advertising'))
<li><a href="{{ URL::action('BackendAdvertisingController@index') }}" title="Publicidade">Publicidade</a></li>
@endif
@if (Auth::user()->can('view_pages'))
<li><a href="{{ URL::action('BackendPageController@index') }}" title="Páginas">Paginas</a></li>
@endif
@if (Auth::user()->can('view_links'))
<li><a href="{{ URL::action('BackendLinkController@index') }}" title="Links">Links</a></li>
@endif
@if (Auth::user()->can('manage_users'))
<li><a href="{{ URL::action('BackendUserController@index') }}" title="Usuários">Usuários</a></li>
@endif
@if (Auth::user()->can('manage_config'))
<li><a href="{{ URL::action('BackendConfigController@index') }}" title="Configurações">Configurações</a></li>
@endif
</div>

</div><!--header_3-->

</div><!--header-->

@yield("content")

<div id="footer_body"><!--footer_body-->

<div id="footer"><!--footer-->

<div id="footer_1"><!--footer_1-->
	<div id="footer_1_menu">
	@if (Auth::user()->can('view_log'))
	<li><a href="error_logs.php" title="titulo">Error logs</a></li>
	@endif
	<li><a href="denuncias.php" title="Titulo aqui">Denúncias (+999)</a></li>
	</div>
</div><!--footer_1-->


<div id="footer_3"><!--footer_3-->
	<div id="footer_3_copyright">
	© Copyright 2012-2014 | <a href="http://www.emprega-me.com.br" title="Emprega-me.com.br">Emprega-me.com.br</a> - Todos os direitos reservados
	</div>
</div><!--footer_3-->

</div><!--footer-->

</div><!--footer_body-->

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<!--fancyBox-->
<script type="text/javascript" src="/packages/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="/packages/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/packages/fancybox/source/helpers/jquery.fancybox-buttons.js"></script>
<script type="text/javascript" src="/packages/fancybox/source/helpers/jquery.fancybox-media.js"></script>
<script type="text/javascript" src="/packages/fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>
<!--fancyBox-->

@yield('scripts')

<script data-main="/scripts/bootstrap" src="/scripts/libs/require-2.1.8.js" type="text/javascript"></script>
<script type="text/javascript">
var _title = "{{ $context->title }}";
var _notviewed = {{ $context->notviewed }};
@if (isset($json))
var _context = {{ $json }};
@else
var _context = [];
@endif
$(document).ready(function() {
requirejs(["apps/main"]);
requirejs(["apps/backend/loadjobs"]);
@yield('requirejs')
});
</script>

<audio id="notification_ring">
    <source src="/sounds/notify.ogg" type="audio/ogg">
    <source src="/sounds/notify.mp3" type="audio/mpeg">
    <source src="/sounds/notify.wav" type="audio/wav">
</audio>

</body>
</html>
