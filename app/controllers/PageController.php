<?php

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class PageController extends BaseController {

    /**
     * Exibe uma página estática
     *
     * @param string $page Slug da página requisitada
     * @throws Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return Illuminate\Support\Facades\View
     */
    public function view($page)
    {
        $page = Page::where('url', '=', $page)->first();
        if (!$page) {
            throw new NotFoundHttpException(null, null, 404);
        }

        $this->set_context(array(
            'page' => $page
        ));

        return $this->view_make('base/page');
    }
}