<?php

use Illuminate\Database\Migrations\Migration;


class CreateUserDesidedPositionsTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the salary table
        Schema::create('salary', function($table)
        {
            $table->increments('id');
            $table->string('description');
            $table->timestamps();
        });

        // Create the user_desired_positions table
        Schema::create('user_desired_positions', function($table)
        {
            $table->increments('id');
            $table->integer('profile_id')->unsigned()->nullable();
            $table->integer('salary_id')->unsigned()->nullable();
            $table->string('position');
            $table->timestamps();

            $table->foreign('salary_id')->references('id')->on('salary');
            $table->foreign('profile_id')->references('id')->on('user_profile');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_desired_positions');
        Schema::dropIfExists('salary');
    }

}
