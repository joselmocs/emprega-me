/*!
 * apps.job.register.js
 * Scripts responsáveis pela criação e edição de vagas de trabalho.
 *
 * Joselmo Cardozo
 */

require([
    "helpers/core"
], function(core) {
    var
        initialize = function() {
            load_cities(core.context.input.estate_id, core.context.input.city_id, false);
            load_districts(core.context.input.city_id, core.context.input.district_id, false);

            events();
        },
        events = function() {
            core.phonemask('input[name=phone]');

            $("select[name=estate_id]").unbind('change').change(function() {
                load_cities($(this).val(), null, true);
            });

            $("select[name=city_id]").unbind('change').change(function() {
                load_districts($(this).val(), null, true);
            });

            $("select[name=profile_name]").unbind('change').change(function () {
                $('input[name=company]').val($(this).val());
            });
        },

        // Carrega as cidades do estado selecionado
        load_cities = function(estate_id, city_id, load) {
            $("select[name=district_id]").hide().find('option').remove();

            $city = $("select[name=city_id]");
            $city.hide().find('option').remove();

            if (!estate_id) {
                return false;
            }

            $city
                .append("<option value=\"\">Aguarde, carregando...</option>")
                .show();

            $.ajax({
                type: 'post',
                url: '/location/city/load_all_json',
                data: {
                    _token: $("meta[name=csrf-token]").attr('content'),
                    estate_id: estate_id
                },
                error: function() {
                    alert("Ocorreu um erro ao carregar a lista de cidades, por favor contacte um administrador.");
                },
                success: function(response) {
                    if (response.success) {
                        $($city.find('option')[0]).html("Selecione a cidade");
                        $.each(response.cities, function(i, city) {
                            var $option = $("<option></option>")
                                .attr('value', city.id)
                                .html(city.name);
                            if (city.id == city_id) {
                                $option.attr("selected", "selected");
                            }
                            $city.append($option);
                        });
                    }
                }
            });
        },

        // Carrega os bairros da cidade selecionada
        load_districts = function(city_id, district_id, load) {

            $district = $("select[name=district_id]");
            $district.hide().find('option').remove();

            if (!city_id) {
                return false;
            }

            $district
                .append("<option value=\"\">Aguarde, carregando...</option>")
                .show();

            $.ajax({
                type: 'post',
                url: '/location/district/load_all_json',
                data: {
                    _token: $("meta[name=csrf-token]").attr('content'),
                    city_id: city_id
                },
                error: function() {
                    alert("Ocorreu um erro ao carregar a lista de bairros, por favor contacte um administrador.");
                },
                success: function(response) {
                    if (response.success) {
                        if (response.districts.length) {
                            $($district.find('option')[0]).html("Selecione o bairro");

                            $.each(response.districts, function(i, district) {
                                var $option = $("<option></option>")
                                    .attr('value', district.id)
                                    .html(district.name);
                                if (district.id == district_id) {
                                    $option.attr("selected", "selected");
                                }
                                $district.append($option);
                            });
                        }
                        else {
                            $district.hide();
                        }
                    }
                }
            });
        };

    initialize();
});
