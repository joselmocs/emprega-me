<?php


class BaseBackendController extends BaseController {

    /**
     * Sobreescreve o método que constroi o template
     *
     * @param string $template
     * @return \Illuminate\View\View
     */
    public function view_make($template)
    {
        $title = Config::get('domain.site.title');
        $this->context['template']['title'] = $title;
        $this->context['json']['title'] = $title;
        $this->context['template']['notviewed'] = Job::where('viewed', false)->count();

        return parent::view_make($template);
    }
}
