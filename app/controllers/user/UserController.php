<?php

use JCS\Auth\Models\Role;


class UserController extends BaseController {

    /**
     * Página e controle de autenticação do usuário
     *
     * @return Illuminate\Support\Facades\View
     */
    public function login()
    {
        $this->set_context(array(
            'input' => (object) array(
                'email' => Input::get('email', ''),
                'nemail' => Input::get('nemail', ''),
                'type' => Input::get('remember_me', '1'),
                'remember_me' => (bool) Input::get('remember_me', false),
                'company' => array_key_exists("company", Input::all()),
                'candidate' => array_key_exists("candidate", Input::all()),
                'next' => Input::get('next', '')
            )
        ));

        if (!Input::get('submit')) {
            return $this->view_make('user/login');
        }

        $validate = array(
            "email" => "required|email",
            "password" => "required"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('user/login');
        }

        $errors = array();
        $login_failed_message = "Desculpe, o email e senha informados estão incorretos.";
        try {
            Auth::attempt(array(
                'identifier' => Input::get('email'),
                'password' => Input::get('password')
            ), (bool) Input::get('remember_me', false));
        }
        catch (JCS\Auth\UserPasswordIncorrectException $e) {
            $user = User::where('email', '=', Input::get('email'))->first();
            if ($user) {
                // Atualizamos as informações de falha na antenticação
                $user->profile->increment('count_login_failed');
                $user->profile->last_login_failed = date("Y-m-d H:i:s");
                $user->profile->save();
            }
            array_push($errors, $login_failed_message);
        }
        catch (JCS\Auth\UserUnverifiedException $e) {
            array_push($errors, "Por favor, verifique seu email para ativação da sua conta.");
        }
        catch (Exception $e) {
            array_push($errors, $login_failed_message);
        }

        if (Auth::check() && Auth::user()->is(array('sys_admin', 'manager', 'moderator'))) {
            array_push($errors, "Acesso não permitido.");
            Auth::logout();
        }

        if (count($errors) > 0) {
            $this->set_context(array(
                'auth_errors' => $errors
            ));
            return $this->view_make('user/login');
        }

        // Atualizamos as informações de último login do usuário
        $user = Auth::user();
        $user->profile->increment('count_login');
        $user->profile->previous_login = $user->profile->last_login;
        $user->profile->last_login = date("Y-m-d H:i:s");
        $user->profile->save();

        if (Input::get('next')) {
            return Redirect::away(Input::get('next'));;
        }
        return Redirect::action('IndexController@index');
    }

    /**
     * Faz logout do usuário
     *
     * @return Illuminate\Support\Facades\Redirect
     */
    public function logout()
    {
        Auth::logout();
        return Redirect::action('IndexController@index');
    }

    /**
     * Verifica o email do visitante para criação de um novo cadastro
     *
     * @return Illuminate\Support\Facades\View
     */
    public function check_new_user()
    {
        $this->set_context(array(
            'input' => (object) array(
                'email' => Input::get('email', ''),
                'nemail' => Input::get('nemail', ''),
                'type' => Input::get('type'),
                'remember_me' => (bool) Input::get('remember_me', false),
                'company' => Input::get('type') == "2",
                'candidate' => Input::get('type') == "1",
                'next' => Input::get('next', '')
            )
        ));

        $validate = array(
            "nemail" => "required|email",
            "type" => "required"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('user/login');
        }

        try {
            User::where('email', '=', Input::get('nemail'))->firstOrFail();
        }
        catch (Exception $e) {
            Session::set('new_user_email', Input::get('nemail', ''));

            if (Input::get('type') == '2') {
                return Redirect::action('CompanyController@register');
            }
            else {
                return Redirect::action('CandidateController@register');
            }
        }

        $this->set_context(array(
            'form_errors' => array("Já existe um cadastro com o email informado. Tente recuperar seu acesso.")
        ));

        return $this->view_make('user/login');
    }

    /**
     * Carrega a lista de cidades do estado informado
     *
     * @return Illuminate\Support\Facades\Response
     */
    public function load_cities()
    {
        $estate_id = Input::get('estate_id');
        $estate = LocationEstate::where('id', '=', $estate_id)->firstOrFail();
        $cities = $estate->cities()->where('active', '=', 1)->get()->toArray();
        return Response::json(array(
            'success' => true,
            'cities' => $cities
        ));
    }

    /**
     * Ativa o cadastro do usuário
     *
     * @param str $user
     * @param str $salt
     * @return \Illuminate\View\View
     */
    public function verify($user, $salt)
    {
        $user = User::where('username', $user)
            ->where('salt', $salt);
        try {
            $user = $user->firstOrFail();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return Redirect::action('UserController@login');
        }

        if ($user->verified) {
            return Redirect::action('UserController@login');
        }

        $user->verified = true;
        $user->save();

        $this->set_context(array(
            'fullname' => $user->profile->fullname
        ));

        return $this->view_make('user/verified');
    }

    /**
     * Gera uma nova senha para o usuario
     * @return string
     */
    public function generateNewPassword()
    {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 6);
    }

    /**
     * Exibe a página de recuperação de senhas
     *
     * @return \Illuminate\View\View
     */
    public function lostPassword()
    {
        $response = array(
            'error' => null,
            'success' => null,
            'input' => (object) array(
                'email' => trim(Input::get('email'), '')
            )
        );

        if (Request::isMethod('post')) {
            $user = User::where('email', trim(Input::get('email')));

            try {
                $user = $user->firstOrFail();
            }
            catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                $response['error'] = "E-mail não cadastrado no sistema.";
            }

            if (!$response['error']) {
                $new_password = $this->generateNewPassword();

                $user->setPasswordAttribute($new_password);
                $user->save();

                $message = View::make('user/email/lost_password')
                    ->with('site_name', Config::get('domain.site.name'))
                    ->with('fullname', $user->profile->fullname)
                    ->with('username', $user->username)
                    ->with('password', $new_password)
                    ->with('salt', $user->remember_token)
                    ->render();

                $campaign = new \JCS\Mail\Campaign();
                $campaign->addAddress(new \JCS\Mail\ArraySource(array($user->email)));

                $campaign
                    ->setMessage("Recuperação de senha", $message, "html")
                    ->setMode(\JCS\Mail\Models\Queue::MODE_SINGLE)
                    ->save();

                $campaign->execute();

                $response['success'] = "Instruções para a recuperação da sua senha foram enviadas para seu email.";
            }
        }

        $this->set_context($response);

        return $this->view_make('user/lost_password');
    }
}
