<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUserProfileFields extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the job_reports table
        Schema::table('user_profile', function($table)
        {
            $table->boolean('cnh_a')->default(false);
            $table->boolean('cnh_b')->default(false);
            $table->boolean('cnh_c')->default(false);
            $table->boolean('cnh_d')->default(false);
            $table->boolean('cnh_e')->default(false);
            $table->boolean('auto_motorcycle')->default(false);
            $table->boolean('auto_car')->default(false);
            $table->boolean('auto_bus')->default(false);
            $table->boolean('auto_truck')->default(false);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_profile', function($table)
        {
            $table->dropColumn('cnh_a');
            $table->dropColumn('cnh_b');
            $table->dropColumn('cnh_c');
            $table->dropColumn('cnh_d');
            $table->dropColumn('cnh_e');
            $table->dropColumn('auto_motorcycle');
            $table->dropColumn('auto_car');
            $table->dropColumn('auto_bus');
            $table->dropColumn('auto_truck');
        });
    }

}
