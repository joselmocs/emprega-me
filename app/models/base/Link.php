<?php


class Link extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'links';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'name', 'title', 'url', 'position', 'newpage', 'active');

    /**
     * Retorna a última posição da ordem dos links
     *
     * @param int $position Posição dos links (header 1 ou footer 2)
     * @return integer
     */
    public static function getLastPosition($position)
    {
        $last_position = Link::where('position', $position)->orderBy('order', 'desc')->limit(1)->first();

        if ($last_position) {
            return $last_position->order;
        }

        return 0;
    }

}