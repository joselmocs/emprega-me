<!doctype html>
<html>
<head>

    <meta charset="utf-8">
    <title>{{ $context->title }}</title>
    <link href="/styles/backend/screen.css" rel="stylesheet" type="text/css"/>
</head>

<body>

    <div id="login"><!--login-->

        <div style="margin: 0px 0px 10px 0px"><!--div margin-->
            <img src="/images/backend/header_login.png"/>
        </div><!--div margin-->

        <form action="{{ URL::action('BackendController@login') }}" method="post">
        <input type="hidden" name="_token" value="{{  csrf_token() }}" />

        <table width="100%" border="0" cellspacing="2" cellpadding="0" class="moldura_cinza formulario">
            <tr>
                <td align="left">Digite seu e-mail:</td>
            </tr>
            <tr>
                <td align="left">
                    <input type="email" name="email" value="{{ $context->input->email }}" required="true" style="width: 350px;"
                    @if ($context->field_errors && $context->field_errors->has('email')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('email'))
                    <div class="error">{{ $context->field_errors->first('email') }}</div>
                    @endif
                </td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td align="left">Digite sua senha:</td>
            </tr>
            <tr>
                <td align="left">
                    <input type="password" name="password" required="true" style="width: 350px;"
                    @if ($context->field_errors && $context->field_errors->has('password')) class="error" @endif />
                    @if ($context->field_errors && $context->field_errors->has('password'))
                    <div class="error">{{ $context->field_errors->first('password') }}</div>
                    @endif
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table width="0%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="0%" align="center" style="padding: 2px 4px 0px 0px">
                                <input type="checkbox" name="remember_me" checked>
                            </td>
                            <td align="left" style="font-size: 11px;">Mantenha-me conectado</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="10" align="left"></td>
            </tr>
            <tr>
                <td align="left">
                    <input type="submit" name="submit" value="Entrar"/>
                </td>
            </tr>
        </table>
        </form>

    </div><!--login-->

</body>
</html>
