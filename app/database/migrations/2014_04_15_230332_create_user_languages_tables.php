<?php

use Illuminate\Database\Migrations\Migration;


class CreateUserLanguagesTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the user_languages table
        Schema::create('user_languages', function($table)
        {
            $table->increments('id');
            $table->integer('profile_id')->unsigned()->nullable();
            $table->integer('language_id')->unsigned()->nullable();
            $table->tinyInteger('level')->default(1);
            $table->timestamps();

            $table->foreign('profile_id')->references('id')->on('user_profile');
            $table->foreign('language_id')->references('id')->on('languages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop user_languages table
        Schema::dropIfExists('user_languages');
    }

}
