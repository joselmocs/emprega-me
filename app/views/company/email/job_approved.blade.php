<p>Olá {{ $fullname }},</p>

<p>Sua vaga <b>{{ $job->position }}</b> foi aprovada e já está sendo exibida em nosso site.</p>

<p>Para ver sua vaga clique no link abaixo ou copie e cole no seu navegador:<br />
{{ URL::action('JobController@short', array($job->id)) }}</p>

<p>Cordialmente,<br />
Equipe {{ $site_name }}</p>
