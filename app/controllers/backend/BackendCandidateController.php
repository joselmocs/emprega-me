<?php

use JCS\Auth\Models\Role;


class BackendCandidateController extends BaseBackendController {

    /**
     * Exibe a lista de candidatos no backend
     *
     * @param string $estates
     * @param string $letter
     * @return Illuminate\Support\Facades\View
     */
    public function index($estates = '-', $letter = '-')
    {
        $candidates = User::select('users.id', 'users.email');
        $group = Role::where('tag', '=', 'user')->firstOrFail();

        $candidates->join('user_role_user', 'user_role_user.user_id', '=', 'users.id')
            ->where('user_role_user.role_id', '=', $group->id);

        $this->set_context(array(
            'total_candidates' => $candidates->count()
        ));

        $candidates->join('user_profile', 'user_profile.user_id', '=', 'users.id');

        if ($letter == ' ') {
            $candidates->whereRaw("user_profile.fullname REGEXP '^[0-9]'");
        }
        elseif ($letter != '-') {
            $candidates->where('user_profile.fullname', 'like', $letter .'%');
        }

        $new_search = false;
        if ($estates == '-') {
            if ($letter == '-') {
                $new_search = true;
            }
        }
        else {
            $explode_estates = explode(',', $estates);
            $list_estates = array();

            foreach ($explode_estates as $short) {
                $estate = LocationEstate::where('short', '=', strtoupper($short))->firstOrFail();
                array_push($list_estates, $estate->id);
            }

            $candidates->join('location_cities', 'location_cities.id', '=', 'user_profile.city_id')
                ->whereIn('location_cities.estate_id', $list_estates);
        }

        $this->set_json_context(array(
            'urls' => array(
                'remove' => URL::action('BackendCandidateController@remove')
            )
        ));

        $this->set_context(array(
            'filter' => (object) array(
                'url' => URL::action('BackendCandidateController@index', array($estates, '')),
                'estates' => $estates
            ),
            'new_search' => $new_search,
            'estates' => LocationEstate::all(),
            'candidates' => ($new_search) ? $candidates->limit(50)->get() : $candidates->paginate(50),
            'letter' => $letter
        ));

        return $this->view_make('backend/candidate/index');
    }

    /**
     * Exibe o formulário de criação e edição de candidatos no backend
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function register($id = null)
    {
        if ($id) {
            $candidate = User::find($id);
            $this->set_context(array(
                'candidate_id' => $candidate->id,
                'input' => (object) array(
                    'email' => Input::get('email', $candidate->email),
                    'password' => '',
                    'password_confirm' => '',
                    'fullname' => Input::get('fullname', $candidate->profile->fullname),
                    'birthday' => Input::get('birthday', Carbon::parse($candidate->profile->birthday)->format('d/m/Y')),
                    'city_id' => Input::get('estate_id', $candidate->profile->city->id),
                    'estate_id' => Input::get('estate_id', $candidate->profile->city->estate_id),
                    'password_change' => Input::get('password_change') == "on"
                )
            ));
            $this->set_json_context(array(
                'data' => (object) array(
                    'estate_id' => Input::get('estate_id', $candidate->profile->city->estate->id),
                    'city_id' => Input::get('city_id', $candidate->profile->city_id),
                )
            ));
        }
        else {
            $candidate = new User;
            $this->set_context(array(
                'candidate_id' => null,
                'input' => (object) array(
                    'email' => Input::get('email', ''),
                    'password' => '',
                    'password_confirm' => '',
                    'fullname' => Input::get('fullname', ''),
                    'birthday' => Input::get('birthday'),
                    'estate_id' => Input::get('estate_id'),
                    'city_id' => Input::get('city_id'),
                    'password_change' => true
                )
            ));
            $this->set_json_context(array(
                'data' => (object) array(
                    'estate_id' => Input::get('estate_id', Input::get('estate_id')),
                    'city_id' => Input::get('city_id', Input::get('city_id')),
                ),
                'input' => (object) array(
                    'city_id' => Input::get('city_id', ''),
                )
            ));
        }

        if (Request::isMethod('get')) {
            return $this->view_make('backend/candidate/form');
        }

        $validate = array(
            "email" => ($id) ? "required|unique:users,email,". $candidate->id : "required|unique:users",
            "password" => "required_if:password_change,on|min:3|max:30",
            "password_confirm" => "required_if:password_change,on|same:password",
            "fullname" => "required|min:2|max:256",
            "birthday" => "required",
            "estate_id" => "required|numeric",
            "city_id" => "required|numeric"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('backend/candidate/form');
        }

        DB::transaction(function() use ($id, $candidate) {
            $candidate->email = Input::get('email');
            if (!$id || Input::get('password_change') == "on") {
                $candidate->password = Input::get('password');
            }
            $candidate->verified = 1;
            $candidate->save();
            $candidate->roles()->sync(array(Role::where('tag', '=', 'user')->firstOrFail()->id));

            $candidate->profile->city_id = Input::get('city_id');
            $candidate->profile->fullname = Input::get('fullname', '');
            $candidate->profile->birthday = Carbon::createFromFormat('d/m/Y', Input::get('birthday'))->startOfDay();
            $candidate->profile->save();
        });

        return Redirect::action('BackendCandidateController@index', array('-', '-'));
    }

    /**
     * Remove uma empresa.
     *
     * @param $id int
     * @return Redirect
     */
    public function remove($id)
    {
        if ($id) {
            User::find($id)->delete();
        }
        return Redirect::back();
    }
}
