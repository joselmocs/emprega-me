<?php

use Illuminate\Database\Migrations\Migration;


class CreateLanguagesTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the languages table
        Schema::create('languages', function($table)
        {
            $table->increments('id');
            $table->string('language')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop languages table
        Schema::dropIfExists('languages');
    }

}
