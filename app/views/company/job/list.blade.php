@extends("base/template")

@section("site-title")
Vagas cadastradas - {{ Config::get('domain.site.title') }}
@endsection

@section("navigation")
<li><a href="{{ URL::action('CompanyJobController@lists', array('-')) }}" title="Lista de Categorias">Minhas Vagas</a></li>
@endsection

@section('requirejs')
requirejs(["apps/company/jobs"]);
@endsection

@section('content')
@include("advertising/header")
<div class="page_template"><!--page_template-->

<div class="titulo_medio"><!--titulo_medio-->
Vagas cadastradas <span style="color: #999999;">({{ $context->jobs->count() }})</span>
</div><!--titulo_medio-->

<div style="margin: 0 0 5px 0"><!--div margin-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="formulario">
<tr>
<td width="100%">
<a href="linkaqui"><img src="../../../../domains/emprega-me/images/tipos_trabalho/emprego.png" title="Emprego"/></a>
<a href="linkaqui"><img src="../../../../domains/emprega-me/images/tipos_trabalho/emprego.png" title="Emprego"/></a>
<a href="linkaqui"><img src="../../../../domains/emprega-me/images/tipos_trabalho/emprego.png" title="Emprego"/></a>
<a href="linkaqui"><img src="../../../../domains/emprega-me/images/tipos_trabalho/emprego.png" title="Emprego"/></a>
<a href="linkaqui"><img src="../../../../domains/emprega-me/images/tipos_trabalho/emprego.png" title="Emprego"/></a>
<a href="linkaqui"><img src="../../../../domains/emprega-me/images/tipos_trabalho/emprego.png" title="Emprego"/></a>
</td>
<td align="right">
<select name="view">
<option value="-" @if ($context->view == "-") selected="selected" @endif>Todo o período</option>
<option value="0" @if ($context->view == "0") selected="selected" @endif>Hoje</option>
<option value="1" @if ($context->view == "1") selected="selected" @endif>Ontem</option>
<option value="7" @if ($context->view == "7") selected="selected" @endif>Últimos 7 dias</option>
<option value="30" @if ($context->view == "30") selected="selected" @endif>Últimos 30 dias</option>
</select>
</td>
</tr>
</table>
</div><!--div margin-->

@if (!$context->jobs->count())
<span style="color: #666666; font-size: 12px;">Nenhuma vaga cadastrada.</span>
@else

@if ($context->reproved)
<div class="alerta_vermelho"><!--alerta_cinza-->
Você tem <b>{{ $reproved }}</b> vaga(s) reprovada(s), verifique essas vagas para que elas não sejam removidas.
</div><!--alerta_cinza-->
@endif

@foreach($context->jobs as $i => $job)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $job->type->image }}" title="{{ $job->type->name }}">
<a class="fancybox fancybox.iframe" title="{{ $job->position }}" href="{{ URL::action('CompanyJobController@view', array($job->id)) }}" data-fancybox-width="800" data-fancybox-height="600">{{ $job->position }}</a>
<small><span>por</span> {{ $job->company }} <span>em</span> {{ $job->city->name }}/{{ $job->estate->short }}</small>
</span>

<span class="row_menu">
@if ($job->status == Job::IN_APPROVAL)
<img src="/images/row_menu_analise.png" title="Vaga em análise">
@endif
@if ($job->status == Job::APPROVED)
<img src="/images/row_menu_yes.png" title="Vaga aprovada">
@endif
@if ($job->status == Job::REPROVED)
<img src="/images/row_menu_erro.png" title="Vaga reprovada">
@endif
</span>

<span class="row_menu">
<a href="{{ URL::action('CompanyJobController@job_candidacies', array($job->id .'-'. Illuminate\Support\Str::slug($job->position))) }}"><img src="/images/row_menu_candidatos.png" title="Ver candidatos ({{ $job->candidates()->whereNull('archived_at')->count() }})"></a>
<a href="{{ URL::action('CompanyJobController@edit', array($job->id)) }}"><img src="/images/row_menu_editar.png" title="Editar vaga"></a>
<a href=""><img src="/images/row_menu_remover.png" title="Remover vaga"></a>
</span>

<span class="time_post">
@if (Carbon::parse($job->created_at)->isToday())
<img src="/images/hoje.png" title="Vaga publicada hoje">
@endif
@if (Carbon::parse($job->created_at)->isYesterday())
<img src="/images/ontem.png" title="Vaga publicada ontem">
@endif
<img src="/images/clock.png" title="Data da publicação">
<small title="{{ Carbon::parse($job->created_at)->format('d \d\e F \d\e Y') }}">{{ Carbon::parse($job->created_at)->format('d/m') }}</small>
</span>
</div><!--row/row-alt-->
@endforeach

@endif
</div>

</div><!--container-->
@endsection
