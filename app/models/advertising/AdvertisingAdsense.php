<?php


class AdvertisingAdsense extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'advertising_adsense';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'name', 'short', 'code', 'active');
}