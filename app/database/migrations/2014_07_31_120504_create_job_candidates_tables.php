<?php

use Illuminate\Database\Migrations\Migration;


class CreateJobCandidatesTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the job_candidates table
        Schema::create('job_candidates', function($table)
        {
            $table->increments('id');
            $table->integer('job_id')->unsigned()->nullable();
            $table->integer('profile_id')->unsigned()->nullable();
            $table->timestamp('viewed_at')->nullable();
            $table->timestamp('archived_at')->nullable();
            $table->timestamps();

            $table->foreign('profile_id')->references('id')->on('user_profile');
            $table->foreign('job_id')->references('id')->on('jobs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_candidates');
    }

}
