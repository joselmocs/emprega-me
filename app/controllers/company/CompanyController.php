<?php

use JCS\Auth\Models\Role;


class CompanyController extends BaseController {

    /**
     * Página de registro de nova empresa
     *
     * @return Illuminate\Support\Facades\View
     */
    public function register()
    {
        $this->set_json_context(array(
            'data' => (object) array(
                'city_id' => Input::get('city_id'),
                'estate_id' => Input::get('estate_id', Session::get('estate'))
            )
        ));

        $this->set_context(array(
            'input' => (object) array(
                'email' => Input::get('email', Session::get('new_user_email')),
                'password' => Input::get('password'),
                'password_confirm' => Input::get('password'),
                'fullname' => Input::get('fullname'),
                'company' => Input::get('company'),
                'estate_id' => Input::get('estate_id', Session::get('estate')),
                'phone' => Input::get('phone'),
                'website' => Input::get('website')
            )
        ));

        Session::remove('new_user_email');

        if (!Input::get('submit')) {
            return $this->view_make('company/register');
        }

        $validate = array(
            "email" => "required|email|unique:users",
            "password" => "required|min:3|max:30",
            "password_confirm" => "required|same:password",
            "fullname" => "required|min:2|max:256",
            "company" => "required|min:2|max:256",
            "phone" => "required",
            "estate_id" => "required|numeric",
            "city_id" => "required|numeric"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('company/register');
        }

        $user = new User();
        DB::transaction(function() use ($user) {
            $user->email = Input::get('email');
            $user->password = Input::get('password');
            $user->verified = 1;
            $user->save();
            $user->roles()->sync(array(Role::where('tag', '=', 'company')->firstOrFail()->id));

            $user->profile->city_id = Input::get('city_id');
            $user->profile->fullname = Input::get('fullname');
            $user->profile->company = Input::get('company');
            $user->profile->website = Input::get('website');
            $user->profile->phone = Input::get('phone');
            $user->profile->save();
        });

        $message = View::make('user/email/new_account')
            ->with('site_name', Config::get('domain.site.name'))
            ->with('fullname', Input::get('fullname'))
            ->with('username', $user->username)
            ->with('salt', $user->salt)
            ->render();

        $campaign = new \JCS\Mail\Campaign();
        $campaign->addAddress(new \JCS\Mail\ArraySource(array($user->email)));

        $campaign
            ->setMessage("Ativação de Conta", $message, "html")
            ->setMode(\JCS\Mail\Models\Queue::MODE_SINGLE)
            ->save();
        $campaign->execute();

        return Redirect::action('CompanyController@register_complete');
    }

    /**
     * Exibe a página de conclusão de cadastro de nova empresa
     *
     * @return \Illuminate\View\View
     */
    public function register_complete()
    {
        return $this->view_make('company/register_complete');
    }

    /**
     * Ativa o cadastro do usuário
     *
     * @param str $user
     * @param str $salt
     * @return \Illuminate\View\View
     */
    public function verify($user, $salt)
    {
        $user = User::where('username', $user)
            ->where('salt', $salt);
        try {
            $user = $user->firstOrFail();
        }
        catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return Redirect::action('UserController@login');
        }

        if ($user->verified) {
            return Redirect::action('UserController@login');
        }

        $user->verified = true;
        $user->save();

        $this->set_context(array(
            'fullname' => $user->profile->fullname
        ));

        return $this->view_make('company/verified');
    }

    /**
     * Página de edição de empresa
     *
     * @return Illuminate\Support\Facades\View
     */
    public function edit()
    {
        $profile = Auth::user()->profile;

        $this->set_context(array(
            'input' => (object) array(
                'fullname' => Input::get('fullname', $profile->fullname),
                'company' => Input::get('company', $profile->company),
                'estate_id' => Input::get('estate_id', $profile->city->estate->id),
                'phone' => Input::get('phone', $profile->phone),
                'website' => Input::get('website', $profile->website)
            )
        ));

        if (Request::isMethod('get')) {
            $this->set_json_context(array(
                'data' => (object) array(
                    'estate_id' => Input::get('estate_id', $profile->city->estate->id),
                    'city_id' => Input::get('city_id', $profile->city_id),
                )
            ));

            return $this->view_make('company/edit/template');
        }

        $validate = array(
            "fullname" => "required|min:2|max:256",
            "company" => "required|min:2|max:256",
            "phone" => "required",
            "estate_id" => "required|numeric",
            "city_id" => "required|numeric"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            $this->set_context(array(
                'field_errors' => $validated->messages()
            ));
            return $this->view_make('company/edit/template');
        }

        $profile->city_id = Input::get('city_id');
        $profile->fullname = Input::get('fullname', '');
        $profile->company = Input::get('company', '');
        $profile->website = Input::get('website');
        $profile->phone = Input::get('phone');
        $profile->save();

        return Redirect::action('CompanyController@complete');
    }

    /**
     * Página de confirmação de alterações da empresa
     *
     * @return Illuminate\Support\Facades\View
     */
    public function complete() {
        return $this->view_make('company/edit/complete');
    }
}
