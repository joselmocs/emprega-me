/*!
 * apps.backend.companies.js
 * Scripts responsáveis pela criação e edição de empresas.
 *
 * Joselmo Cardozo
 */

require([
    "helpers/core"
], function(core) {
    var
        initialize = function() {
            events();
        },
        estates = {
            all_checked: function() {
                var all_checked = true;
                $.each($("[name=checkbox_estates]"), function(i, estate) {
                    if (!$(estate).is(':checked')) {
                        all_checked = false;
                    }
                });

                return all_checked;
            }
        },
        events = function() {

            $('a[name=remover]').unbind('click').click(function() {
                if (confirm('Deseja remover a empresa *'+ $(this).attr('data-name') +'*?')) {
                    window.location = core.context.urls.remove +'/'+ $(this).attr('data-id');
                }
            });

            $("input[name=password_change]").change(function() {
                if ($(this).is(':checked')) {
                    $(".tr-password").show();
                    $(".tr-password").find('input').attr('required', true);
                }
                else {
                    $(".tr-password").hide();
                    $(".tr-password").find('input').removeAttr('required');
                }
            });

            $("#checkbox_mark_all").change(function() {
                var $check_estates = $("[name=checkbox_estates]");
                if ($(this).is(':checked')) {
                    $check_estates.prop('checked', true);
                }
                else {
                    $check_estates.prop('checked', false);
                }
            });

            $("input[name=checkbox_estates]").change(function() {
                if (estates.all_checked()) {
                    $("#checkbox_mark_all").prop('checked', true);
                }
                else {
                    $("#checkbox_mark_all").prop('checked', false);
                }
            });

            $("input[name=filter]").unbind('click').click(function() {
                var list_estates = [];
                var url_estate;
                $.each($("[name=checkbox_estates]"), function(i, estate) {
                    if ($(estate).is(':checked')) {
                        list_estates.push(estate.value);
                    }
                });

                if (!list_estates.length || estates.all_checked()) {
                    url_estate = '-';
                }
                else {
                    url_estate = list_estates.join(',');
                }

                var path = core.uri.path().split('/');
                path[4] = url_estate;

                window.location = path.join('/');
            });
        };

    initialize();
});