<?php


class BackendStatisticsController extends BaseBackendController {

    /**
     * Exibe a página inicial de estatísticas
     *
     * @param int $estate id do estado
     * @return \Illuminate\Support\Facades\View
     */
    public function index($estate)
    {
        $estate = intval($estate);
        $candidates = JobCandidate::select('job_candidates.*')
            ->orderBy('created_at', 'desc');

        if ($estate > 0) {
            $candidates
                ->join('jobs', 'jobs.id', '=', 'job_candidates.job_id')
                ->where('jobs.estate_id', '=', $estate);
        }

        $this->set_context(array(
            'candidates' => $candidates->get(),
            'estates' => LocationEstate::all(),
            'estate' => $estate
        ));

        return $this->view_make('backend/statistics/index');
    }
}
