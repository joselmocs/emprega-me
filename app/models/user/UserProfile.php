<?php


class UserProfile extends Eloquent {

    /**
     * Constantes para genero de sexo
     *
     */
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    public static $VERBOSE_GENDER = array(
        self::GENDER_MALE => "Masculino",
        self::GENDER_FEMALE => "Feminino"
    );

    /**
     * Constantes para estado civil
     *
     */
    const MARITAL_SINGLE = 1;
    const MARITAL_MARRIED = 2;
    const MARITAL_DIVORCED = 3;
    const MARITAL_WIDOWER = 4;

    public static $VERBOSE_MARITAL = array(
        self::MARITAL_SINGLE => "Solteiro(a)",
        self::MARITAL_MARRIED => "Casado(a)",
        self::MARITAL_DIVORCED => "Divorciado(a)",
        self::MARITAL_WIDOWER => "Viúvo(a)"
    );

    /**
     * Constantes para tipos de deficiencia
     */
    const SPECIAL_NEED_NONE = 0;
    const SPECIAL_NEED_AUDIO = 1;
    const SPECIAL_NEED_PHISIC = 2;
    const SPECIAL_NEED_MENTAL = 3;
    const SPECIAL_NEED_MULTIPLE = 4;
    const SPECIAL_NEED_VISUAL = 5;

    public static $VERBOSE_SPECIAL_NEEDS = array(
        self::SPECIAL_NEED_NONE => "Selecione",
        self::SPECIAL_NEED_AUDIO => "Deficiência auditiva",
        self::SPECIAL_NEED_PHISIC => "Deficiência física",
        self::SPECIAL_NEED_MENTAL => "Deficiência mental",
        self::SPECIAL_NEED_MULTIPLE => "Deficiência múltipla",
        self::SPECIAL_NEED_VISUAL => "Deficiência visual"
    );

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_profile';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::deleting(function ($model) {
            $model->educations()->delete();
            $model->courses()->delete();
            $model->languages()->delete();
            $model->experiences()->delete();
            $model->desiredpositions()->delete();
            $model->jobs()->delete();
            $model->names()->delete();
        });

        static::saving(function ($model) {
            $search = array($model->fullname);
            foreach($model->educations()->get() as $education) {
                array_push($search, $education->study_area);
                array_push($search, $education->specialization);
            }
            foreach($model->experiences()->get() as $experience) {
                array_push($search, $experience->position);
            }
            foreach($model->desiredpositions()->get() as $position) {
                array_push($search, $position->position);
            }
            foreach($model->courses()->get() as $course) {
                array_push($search, $course->course);
            }
            $model->search_string = join(' ', $search);
        });
    }

    /**
     * Retorna o Usuario pertencente a este Profile
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * Retorna a cidade do usuário
     *
     * @return LocationCity
     */
    public function city()
    {
        return $this->belongsTo('LocationCity', 'city_id');
    }

    /**
     * Retorna a formação acadêmica do usuário
     *
     * @return UserEducation
     */
    public function educations()
    {
        return $this->hasMany('UserEducation', 'profile_id');
    }

    /**
     * Retorna os cursos do usuário
     *
     * @return UserCourse
     */
    public function courses()
    {
        return $this->hasMany('UserCourse', 'profile_id');
    }

    /**
     * Retorna os idiomas do usuário
     *
     * @return UserLanguage
     */
    public function languages()
    {
        return $this->hasMany('UserLanguage', 'profile_id');
    }

    /**
     * Retorna as experiências profissionais do usuário
     *
     * @return UserExperience
     */
    public function experiences()
    {
        return $this->hasMany('UserExperience', 'profile_id');
    }

    /**
     * Retorna os cargos pretendidos do usuário
     *
     * @return UserDesiredPosition
     */
    public function desiredpositions()
    {
        return $this->hasMany('UserDesiredPosition', 'profile_id');
    }

    /**
     * Retorna as vagas de emprego do usuário. Uso estrito para empresas.
     *
     * @return Job
     */
    public function jobs()
    {
        return $this->hasMany('Job', 'profile_id');
    }

    /**
     * Retorna as candidaturas de todos as vagas cadastradas. Uso estrito para empresas.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function candidates()
    {
        return JobCandidate::select('job_candidates.*')
            ->join('jobs', 'jobs.id', '=', 'job_id')
            ->where('jobs.profile_id', $this->id);
    }

    /**
     * Retorna as candidaturas do usuario. Uso estrito para candidatos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function candidacies()
    {
        return $this->hasMany('JobCandidate', 'profile_id', 'id');
    }

    /**
     * Retorna os nomes usados na criação da vaga pela companhia
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function names()
    {
        return $this->hasMany('UserProfileName', 'profile_id', 'id');
    }

    public function cnh()
    {
        $cnh = array('a', 'b', 'c', 'd', 'e');

        $has_cnh = false;
        $return = "Categoria ";
        foreach($cnh as $letter) {
            if ($this->{'cnh_'.$letter}) {
                $has_cnh = true;
                $return .= strtoupper($letter) .", ";
            }
        }

        return ($has_cnh) ? rtrim($return, ", ") : "Não";
    }

    public function auto()
    {
        $autos = array(
            'motorcycle' => 'moto',
            'car' => 'carro',
            'bus' => 'ônibus',
            'truck' => 'caminhão'
        );

        $has_auto = false;
        $return = "";
        foreach($autos as $k => $auto) {
            if ($this->{'auto_'.$k}) {
                $has_auto = true;
                $return .= $auto .", ";
            }
        }

        return ($has_auto) ? rtrim($return, ", ") : "Não";
    }

    public function ultima_candidatura() {
        $ultima = null;
        $jobs = JobCandidate::where('profile_id', $this->id)
            ->orderBy('created_at', 'desc')
            ->take(1)
            ->get();

        foreach($jobs as $job) {
            $ultima = $job->created_at;
        }

        return $ultima;
    }

    public function get_avatar() {
        if (!$this->photo) {
            $gender = ($this->gender == 1) ? "m" : "f";
            return "/images/photos/foto_padrao_". $gender .".jpg";
        }
        return $this->photo;
    }
}
