<?php

use \Illuminate\Support\Str;


class LocationEstate extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'location_estates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'name', 'slug', 'short');

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::creating(function($model) {
            $model->slug = STR::slug($model->name);
        });

        static::updating(function($model) {
            $model->slug = STR::slug($model->name);
        });
    }

    /**
     * Retorna as cidades do estado
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cities()
    {
        return $this->hasMany('LocationCity', 'estate_id');
    }

    public function jobs()
    {
        return $this->hasMany('Job', 'estate_id');
    }
}
