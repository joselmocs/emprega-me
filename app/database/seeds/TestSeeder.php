<?php

use \Illuminate\Support\Str;


class TestSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        |--------------------------------------------------------------------------
        | Categorias
        |--------------------------------------------------------------------------
        */
        $categorias = array("Administrativo", "Comércio", "Informática", "Saúde");
        foreach ($categorias as $categoria) {
            $newCat = new Category;
            $newCat->name = $categoria;
            $newCat->title = $categoria;
            $newCat->url = STR::slug($categoria);
            $newCat->description = $categoria;
            $newCat->active = true;
            $newCat->save();
        }

        $config = new Configuration;
        $config->save();
    }
}