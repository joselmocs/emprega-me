@extends('backend/template')

@section('content')
<div id="estatisticas" class="page_template"><!--estatisticas-->

    <div class="filtro"><!--filtro-->

        <div class="filtro_select formulario"><!--filtro_localizacao-->
            <select onchange="window.location=options[selectedIndex].value">
                <option value="{{ URL::action('BackendStatisticsController@index', array(0)) }}" @if ($context->estate == 0) selected="selected" @endif>Todos os estados</option>
                @foreach($context->estates as $estate)
                    <option value="{{ URL::action('BackendStatisticsController@index', array($estate->id)) }}" @if ($context->estate == $estate->id) selected="selected" @endif>{{ $estate->name }}/{{ $estate->short }}</option>
                @endforeach
            </select>
        </div><!--filtro_localizacao-->

    </div><!--filtro-->

    <div class="titulo_medio"><!--titulo_medio-->
        <img src="/images/backend/icone_estatisticas.jpg"/> ESTATÍSTICAS
    </div><!--titulo_medio-->

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>

            <td width="100%" valign="top"><!--coluna_1-->

                <div class="titulo_pequeno"><!--titulo_pequeno-->
                    250 últimas candidaturas:
                </div><!--titulo_pequeno-->

                @if (!$context->candidates->count())
                <span style="color: #666666; font-size: 12px;">Nenhuma candidatura encontrada.</span>
                @endif

                @foreach($context->candidates as $i => $candidate)
                <div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
                    <div id="row_1"><!--row_1-->
                        <span class="row-info">
                            <img src="/domains/{{ Config::get('domain.folder') }}/images/tipos_trabalho/{{ $candidate->job->type->image }}" title="{{ $candidate->job->type->name }}">
                            <a class="fancybox fancybox.iframe" title="{{ $candidate->job->position }}" href="{{ URL::action('CompanyJobController@view', array($candidate->job->id)) }}" data-fancybox-width="800" data-fancybox-height="600">
                                {{ $candidate->job->position }}
                            </a>
                            <small><span>por</span> {{ $candidate->job->company }} <span>em</span> {{ $candidate->job->city->name }}/{{ $candidate->job->estate->short }}</small>
                        </span>

                        <div class="row_menu">
                            <a class="fancybox fancybox.iframe" data-fancybox-width="800" data-fancybox-height="600" title="Currículo {{ $candidate->profile->fullname }}" href="{{ URL::action('CurriculumController@view', array($candidate->profile->user->id)) }}"><img src="/images/backend/row_menu_ver_curriculo.png" title="Ver currículo"></a>
                        </div>

                        <span class="time_post">
                        @if (Carbon::parse($candidate->job->created_at)->isToday())
                            <img src="/images/hoje.png" title="Vaga publicada hoje">
                        @endif
                        @if (Carbon::parse($candidate->job->created_at)->isYesterday())
                            <img src="/images/ontem.png" title="Vaga publicada ontem">
                        @endif
                        <img src="/images/clock.png" title="Data da publicação">
                            <small title="{{ Carbon::parse($candidate->job->created_at)->format('d \d\e F \d\e Y') }}">05/03</small>
                        </span>
                    </div><!--row_1-->

                    <div id="row_2"><!--row_2-->
                        <div class="row_2-info">
                            ▪ {{ $candidate->profile->fullname }} ▪ E-mail: <span>{{ $candidate->profile->user->email }}</span>
                        </div>
                    </div><!--row_2-->
                </div><!--row/row-alt-->
                @endforeach

            </td><!--coluna_1-->

            </td><!--coluna_1-->



            <td width="0%" valign="top"><!--coluna_2-->
                <div id="estatisticas_sidebar"><!--estatisticas_sidebar-->

                    <div style="margin: 0px 0px 25px 0px"><!--div margin-->

                        <div class="titulo_pequeno"><!--titulo_pequeno-->
                            Estatisticas de candidaturas:
                        </div><!--titulo_pequeno-->

                        <table width="100%" border="0" cellspacing="2" cellpadding="0" class="moldura_cinza page_template_links">
                            <tr>
                                <td align="center" style="padding: 4px 0px 0px 0px;"><img src="/images/set_right.png"/></td>
                                <td align="left">Candidaturas hoje: {{ JobCandidateController::today($context->estate) }}</td>
                            </tr>
                            <tr>
                                <td align="center" style="padding: 4px 0px 0px 0px;"><img src="/images/set_right.png"/></td>
                                <td align="left">Candidaturas ontem: {{ JobCandidateController::yesterday($context->estate) }}</td>
                            </tr>
                            <tr>
                                <td align="center" style="padding: 4px 0px 0px 0px;"><img src="/images/set_right.png"/></td>
                                <td width="100%" align="left">Candidaturas na semana: {{ JobCandidateController::week($context->estate) }}</td>
                            </tr>
                            <tr>
                                <td align="center" style="padding: 4px 0px 0px 0px;"><img src="/images/set_right.png"/></td>
                                <td width="100%" align="left">Candidaturas no mês: {{ JobCandidateController::month($context->estate) }}</td>
                            </tr>
                        </table>

                    </div><!--div margin-->

                    <div style="margin: 0px 0px 25px 0px"><!--div margin-->

                        <div class="titulo_pequeno"><!--titulo_pequeno-->
                            Estatisticas de pesquisas:
                        </div><!--titulo_pequeno-->

                        <table width="100%" border="0" cellspacing="2" cellpadding="0" class="moldura_cinza page_template_links">
                            <tr>
                                <td align="center" style="padding: 4px 0px 0px 0px;"><img src="/images/set_right.png"/></td>
                                <td align="left">Pesquisas hoje: {{ SearchStatistic::today($context->estate) }}</td>
                            </tr>
                            <tr>
                                <td align="center" style="padding: 4px 0px 0px 0px;"><img src="/images/set_right.png"/></td>
                                <td align="left">Pesquisas ontem: {{ SearchStatistic::yesterday($context->estate) }}</td>
                            </tr>
                            <tr>
                                <td align="center" style="padding: 4px 0px 0px 0px;"><img src="/images/set_right.png"/></td>
                                <td width="100%" align="left">Pesquisas na semana: {{ SearchStatistic::week($context->estate) }}</td>
                            </tr>
                            <tr>
                                <td align="center" style="padding: 4px 0px 0px 0px;"><img src="/images/set_right.png"/></td>
                                <td width="100%" align="left">Pesquisas no mês: {{ SearchStatistic::month($context->estate) }}</td>
                            </tr>
                        </table>

                    </div><!--div margin-->



                    <div class="titulo_pequeno"><!--titulo_pequeno-->
                        100 últimas palavras pesquisas:
                    </div><!--titulo_pequeno-->

                    @foreach (SearchStatistic::latest_100($context->estate)->get() as $i => $stat)
                    <div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
                        <span class="row-info" style="text-transform: lowercase">
                        <img src="/images/set_right.png" />
                            <a href="{{ URL::action('JobController@search') }}?term={{ $stat->keyword }}">{{ $stat->keyword }}</a>
                        </span>

                        <span class="time_post">
                            <img src="/images/clock.png" title="Pesquisada em {{ Carbon::parse($stat->created_at)->format('d \d\e F \d\e Y') }}"/>
                        </span>
                    </div><!--row/row-alt-->
                    @endforeach

                </div><!--estatisticas_sidebar-->
            </td><!--coluna_2-->

        </tr>
    </table>

</div><!--estatisticas-->
@endsection
