<?php

use Illuminate\Support\Str;


class Job extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'jobs';

    /**
     * Constantes para status da vaga
     *
     */
    const IN_APPROVAL = 1;
    const APPROVED = 2;
    const REPROVED = 3;

    public static $VERBOSE_STATUS = array(
        self::IN_APPROVAL => "Em Aprovação",
        self::APPROVED => "Aprovada",
        self::REPROVED => "Reprovada"
    );

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::created(function($model) {
            $model->type->increment('count');
        });

        static::saving(function($model) {
            $model->slug = Str::slug($model->position);
            $model->estate()->associate($model->city->estate);
        });

        static::deleting(function($model) {
            JobCandidate::where('job_id', $model->id)->delete();
            JobReport::where('job_id', $model->id)->delete();
            $model->type->decrement('count');
        });
    }

    /**
     * Retorna o tipo da vaga
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('JobType', 'type_id');
    }

    /**
     * Retorna a cidade da vaga
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('LocationCity', 'city_id');
    }

    /**
     * Retorna o estado da vaga
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estate()
    {
        return $this->belongsTo('LocationEstate', 'estate_id');
    }

    /**
     * Retorna o bairro da vaga
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function district()
    {
        return $this->belongsTo('LocationDistrict', 'district_id');
    }

    /**
     * Retorna a categoria da vaga
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('Category', 'category_id');
    }

    /**
     * Retorna o perfil da empresa que criou a vaga.
     *
     * @return UserProfile
     */
    public function profile()
    {
        return $this->belongsTo('UserProfile', 'profile_id');
    }

    /**
     * Retorna as candidaturas desta vaga
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function candidacies()
    {
        return $this->hasMany('JobCandidate', 'job_id', 'id');
    }

    /**
     * Retorna os usuários que se candidataram à esta vaga
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function candidates()
    {
        return $this->belongsToMany('UserProfile', 'job_candidates', 'job_id', 'profile_id');
    }

    /**
     * Retorna os reportes que esta vaga recebeu
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function reports()
    {
        return $this->hasMany('JobReport', 'job_id', 'id');
    }
}
