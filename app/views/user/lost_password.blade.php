<!doctype html>
<html>
<head>

<meta charset="utf-8">
<link href="/styles/screen.css" rel="stylesheet" type="text/css"/>
<link href="/domains/{{ Config::get('domain.folder') }}/styles/screen_colors.css" rel="stylesheet" type="text/css"/>

<style type="text/css">
body {
    overflow-x: hidden;
    overflow-y: hidden;
}
</style>

</head>

<body>

<div id="login_recuperar_senha"><!--login_recuperar_senha-->

    <div class="titulo_pequeno"><!--titulo_medio-->
        Recuperar senha:
    </div><!--titulo_medio-->

    <div class="alerta_verde" @if (!$context->success) style="display:none;" @endif>{{ $context->success }}</div>
    <div class="alerta_vermelho" @if (!$context->error) style="display:none;" @endif>{{ $context->error }}</div>

    <form action="{{ URL::action('UserController@lostPassword') }}" method="post">
    <table width="100%" border="0" cellspacing="2" cellpadding="0" class="formulario moldura_cinza">
        <tr>
            <td align="left">Digite abaixo seu e-mail e receba uma nova senha em sua caixa de entrada.</td>
        </tr>
        <tr>
            <td align="left">
                <input type="text" name="email" style="width: 350px;" value="{{ $context->input->email }}" />
            </td>
        </tr>
        <tr>
            <td height="10" align="left"></td>
        </tr>
        <tr>
            <td align="left" style="font-size: 11px;">
                Se tiver problemas para recuperar sua senha,  entre em contato.
            </td>
        </tr>
        <tr>
            <td height="10" align="left"></td>
        </tr>
        <tr @if ($context->success) style="display:none;" @endif>
            <td align="left">
                <input type="submit" value="Continuar"/>
            </td>
        </tr>
    </table>
</div><!--login_recuperar_senha-->

</body>
</html>
