<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Domain Principal
    |--------------------------------------------------------------------------
    |
    | Informa se esta configuração é ou não da propriedade principal.
    |
    */

    'main' => false,

    /*
    |--------------------------------------------------------------------------
    | Codigo do Domain
    |--------------------------------------------------------------------------
    |
    | Codigo do dominio atigo
    |
    */

    'code' => 'bhjobs',

    /*
    |--------------------------------------------------------------------------
    | Site
    |--------------------------------------------------------------------------
    |
    | Configurações gerais da propriedade.
    |
    */

    'site' => array(
        'name' => 'BHJobs.com.br',
        'title' => 'BHJobs.com.br',
        'url' => 'http://www.bhjobs.com.br',
        'description' => 'Um breve resumo da descrição do site',
        'keywords' => ''
    ),

    /*
    |--------------------------------------------------------------------------
    | Facebook
    |--------------------------------------------------------------------------
    |
    | Configurações gerais do facebook da propriedade.
    |
    */

    'facebook' => array(
        'name' => 'Emprega-me.com.br',
        'url' => 'https://www.facebook.com/Empregame.com.br',
        'type' => 'website',
        'image' => array(
            '/domains/emprega-me/images/facebook_img_1.jpg',
            '/domains/emprega-me/images/facebook_img_2.jpg'
        ),
    ),

    /*
    |--------------------------------------------------------------------------
    | Diretório do domain
    |--------------------------------------------------------------------------
    |
    | Pasta onde fica os arquivos estáticos da propriedade. O caminho completo
    | é /public/domains/<folder>/.
    |
    */

    'folder' => 'bhjobs',

    /*
    |--------------------------------------------------------------------------
    | Estado
    |--------------------------------------------------------------------------
    |
    | Configuração principal da propriedade. Esta opção serve como filtro para
    | exibir somente as vagas do estado selecionado.
    |
    | Lista de estados com sua respectiva identificação:
    |
    | 1: Acre
    | 2: Alagoas
    | 3: Amazonas
    | 4: Amapá
    | 5: Bahia
    | 6: Ceará
    | 7: Distrito Federal
    | 8: Espírito Santo
    | 9: Goiás
    | 10: Maranhão
    | 11: Minas Gerais
    | 12: Mato Grosso do Sul
    | 13: Mato Grosso
    | 14: Pará
    | 15: Paraíba
    | 16: Pernambuco
    | 17: Piauí
    | 18: Paraná
    | 19: Rio de Janeiro
    | 20: Rio Grande do Norte
    | 21: Rondônia: rondonia
    | 22: Roraima: roraima
    | 23: Rio Grande do Sul
    | 24: Santa Catarina
    | 25: Sergipe
    | 26: São Paulo
    | 27: Tocantins
    |
    */

    'estate' => 11

);
