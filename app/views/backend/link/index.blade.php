@extends("backend/template")

@section('requirejs')
requirejs(["apps/backend/links"]);
@endsection

@section("content")

<div class="page_template"><!--page_template-->

<div class="botao"><!--botao-->
@if (Auth::user()->can('manage_links'))
<li><a title="Adicionar link" href="{{ URL::action('BackendLinkController@register') }}">Adicionar link</a></li>
@endif
</div><!--botao-->

<div class="titulo_grande" style="text-transform: uppercase;"><!--titulo_grande-->
<img src="/images/backend/icone_links.jpg"/> LINKS
</div><!--titulo_grande-->

<div style="margin: 0 0 25px 0"><!--div margin-->

<div class="titulo_pequeno"><!--titulo_pequeno-->
Links do cabeçalho:
</div><!--titulo_pequeno-->

@if (!$context->links_header->count())
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><u>Nenhum link cadastrado.</u>
</span>
</div>
@else
<ol style="list-style-type: none;" class="highlighted">
@foreach($context->links_header as $i => $link)
<li class="row" data-id="{{ $link->id }}" data-position="1"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" title="{{ $link->title }}"/><a href="{{ $link->url }}" title="{{ $link->title }}" target="_blank">{{ $link->name }}</a>
</span>

@if (Auth::user()->can('manage_links'))
<div class="row_menu">
@if ($link->active)
<a href="{{ URL::action('BackendLinkController@deactive', array($link->id)) }}"><img src="/images/backend/row_menu_power_on.png" title="Ligado"/></a>
@else
<a href="{{ URL::action('BackendLinkController@active', array($link->id)) }}"><img src="/images/backend/row_menu_power_off.png" title="Desligado"/></a>
@endif
</div>

<div class="row_menu">
<a title="Editar" href="{{ URL::action('BackendLinkController@edit', array($link->id)) }}"><img src="/images/backend/row_menu_editar.png" title="Editar"/></a>
<a href="javascript:void(0)" name="remover" data-name="{{ $link->name }}" data-id="{{ $link->id }}"><img src="/images/backend/row_menu_remover.png" title="Remover"/></a>
</div>
@endif
</li><!--row/row-alt-->
@endforeach
</ol>
@endif

</div><!--div margin-->


<div class="titulo_pequeno"><!--titulo_pequeno-->
Links do rodapé:
</div><!--titulo_pequeno-->

@if (!$context->links_footer->count())
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><u>Nenhum link cadastrado.</u>
</span>
</div>
@else
<ol style="list-style-type: none;" class="highlighted">
@foreach($context->links_footer as $i => $link)
<li class="row" data-id="{{ $link->id }}" data-position="2"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" title="{{ $link->name }}"/><a href="{{ $link->url }}" title="{{ $link->title }}" target="_blank">{{ $link->name }}</a>
</span>

@if (Auth::user()->can('manage_links'))
<div class="row_menu">
@if ($link->active)
<img src="/images/backend/row_menu_power_on.png" title="Ligado"/>
@else
<img src="/images/backend/row_menu_power_off.png" title="Desligado"/>
@endif
</div>

<div class="row_menu">
<a title="Editar link" href="{{ URL::action('BackendLinkController@edit', array($link->id)) }}"><img src="/images/backend/row_menu_editar.png" title="Editar"/></a>
<a href="javascript:void(0)" name="remover" data-name="{{ $link->name }}" data-id="{{ $link->id }}"><img src="/images/backend/row_menu_remover.png" title="Remover"/></a>
</div>
@endif
</li><!--row/row-alt-->
@endforeach
</ol>
@if ($context->links_footer->count())
<div class="moldura_cinza formulario"><!--div margin-->
<table width="100%" border="0" cellspacing="2" cellpadding="0">
<tr>
<td align="left">
<input style="font-size: 11px;" type="button" value="Salvar ordenação" name="save_order" data-position="2" />
</td>
</tr>
</table>
</div><!--div margin-->
@endif
@endif

</div><!--links-->

</div><!--container-->
@endsection