<?php


class UserEducation extends Eloquent {

    /**
     * Constantes para grau de escolaridade
     *
     */
    const LEVEL_PRIMARY = 1;
    const LEVEL_HIGH = 2;
    const LEVEL_TECHNICAL = 3;
    const LEVEL_HIGHER = 4;
    const LEVEL_POSTGRADUATE = 5;
    const LEVEL_MASTER = 6;
    const LEVEL_DOCTORATE = 7;

    public static $VERBOSE_LEVEL = array(
        self::LEVEL_PRIMARY => "Ensino Fundamental",
        self::LEVEL_HIGH => "Ensino Médio",
        self::LEVEL_TECHNICAL => "Ensino Técnico",
        self::LEVEL_HIGHER => "Ensino Superior",
        self::LEVEL_POSTGRADUATE => "Pós-graduação",
        self::LEVEL_MASTER => "Mestrado",
        self::LEVEL_DOCTORATE => "Doutorado"
    );

    /**
     * Constantes para situação do estudo
     *
     */
    const SITUATION_INCOMPLETE = 1;
    const SITUATION_STUDYNG = 2;
    const SITUATION_COMPLETE = 3;

    public static $VERBOSE_SITUATION = array(
        self::SITUATION_INCOMPLETE => "Incompleto",
        self::SITUATION_STUDYNG => "Cursando",
        self::SITUATION_COMPLETE => "Completo"
    );

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_educations';

    /**
     * Retorna o perfil do usuário desta formação
     *
     * @return UserProfile
     */
    public function profile()
    {
        return $this->belongsTo('UserProfile', 'profile_id');
    }

}