<p>Olá {{ $fullname }},</p>

<p>Um usuário acabou de se candidatar em uma vaga de emprego cadastrada por você.<br /></p>

<p>Vaga: {{ $job->position }}<br />Candidato: {{ $candidate->fullname }}<br />

<p>Para acessar o curriculum deste e de outros candidatos clique no link abaixo ou copie e cole no seu navegador:<br />
{{ URL::action('CompanyJobController@candidacies') }}</p>

<p>Cordialmente,<br />
Equipe {{ $site_name }}</p>
