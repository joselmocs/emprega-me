@extends("backend/template")

@section('requirejs')
requirejs(["apps/backend/categories"]);
@endsection

@section("content")
<div class="page_template"><!--page_template-->

<div class="botao"><!--botao-->
@if (Auth::user()->can('manage_category'))
<li><a title="Adicionar categoria" href="{{ URL::action('BackendCategoryController@register') }}">Adicionar categoria</a></li>
@endif
</div><!--botao-->

<div class="titulo_grande" style="text-transform: uppercase;"><!--titulo_grande-->
<img src="/images/backend/icone_todas_categorias.jpg"/> TODAS AS CATEGORIAS
</div><!--titulo_medio-->

@if (!$context->categories->count())
<div class="row"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a>Nenhuma categoria cadastrada.</a>
</span>
</div>
@else
@foreach($context->categories as $i => $category)
<div class="@if ($i % 2) row-alt @else row @endif"><!--row/row-alt-->
<span class="row-info">
<img src="/images/set_right.png" /><a href="{{ URL::action('BackendCategoryController@view', array($category->id, 0)) }}" title="{{ $category->title }}">{{ $category->name }}</a>
</span>

@if (Auth::user()->can('manage_category'))
<div class="row_menu">
@if ($category->active)
<a href="{{ URL::action('BackendCategoryController@deactive', array($category->id)) }}"><img src="/images/backend/row_menu_power_on.png" title="Ligado"/></a>
@else
<a href="{{ URL::action('BackendCategoryController@active', array($category->id)) }}"><img src="/images/backend/row_menu_power_off.png" title="Desligado"/></a>
@endif
</div>

<div class="row_menu">
<a title="Editar categoria" href="{{ URL::action('BackendCategoryController@register', array($category->id)) }}"><img src="/images/backend/row_menu_editar.png" title="Editar"/></a>
<a href="javascript:void(0);" data-id="{{ $category->id }}" data-name="{{ $category->name }}" name="remover"><img src="/images/backend/row_menu_remover.png" title="Remover"/></a>
</div>
@endif

<span class="counter">
<small><b>{{ $category->jobs->count() }}</b> vagas cadastradas</small>
</span>
</div><!--row/row-alt-->
@endforeach
@endif

</div><!--page_template-->

</div><!--container-->
@endsection