@extends("base/template")

@section("site-title")
Registro de nova empresa - {{ Config::get('domain.site.title') }}
@endsection

@section("content")
@include("advertising/header")
<div class="page_template page_template_links"><!--page_template-->

<div class="titulo_grande"><!--titulo_grande-->
Candidato cadastrado
</div><!--titulo_grande-->

<div style="font-size: 12px; margin: 0 0 25px 0;"><!--div-->
Parabéns <b>Fulano</b>, seu cadastro foi realizado com sucesso!
<br />
Você já pode começar a divulgar suas vagas e receber currículos, <a href="#" title="Cadastrar nova vaga.">clique aqui</a>.
<br /><br />
Enviamos um e-mail para <b>email@email.com.br</b>, verifique e ative seu cadastro em até 30 dias.
</div><!--div-->

<div style="font-size: 12px;"><!--div-->
Para voltar a página inicial <a href="/" title="Voltar para pagina inicial.">clique aqui</a>.
</div><!--div-->

</div><!--page_template-->

</div><!--container-->
@endsection